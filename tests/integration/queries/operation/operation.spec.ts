/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prisma } from '../../../../src/modules/queries-graphql/database/prisma';
import { createTestGraphQLClientUtils } from '../utils/test-client';

const testUtils = createTestGraphQLClientUtils();

describe('Operation Resolver', () => {
    afterAll(async () => {
        await prisma.$disconnect();
    });

    describe('Endorsement operation', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Endorsement {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        id
                        delegate
                        slots
                    }
                }
            }
        `;
        const hash = 'ongmCE4AABhEn1nv931KwpeWDHaJ5PATLKYk8UuiR77MrkfKBpy';

        it('should return endorsement operation data', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data.operations).toEqual([
                {
                    __typename: 'Endorsement',
                    batch_position: 0,
                    block: 'BL9AZ9nc46vQnykoVujrMRgDgbU34JMsn5Xf7amC6dRHowoNTHK',
                    delegate: 'tz1aRoaRhSpRYvFdyvgWLL6TGyRoGF51wDjM',
                    hash: 'ongmCE4AABhEn1nv931KwpeWDHaJ5PATLKYk8UuiR77MrkfKBpy',
                    id: '13019820000012',
                    kind: 'endorsement',
                    level: 1301982,
                    slots: [15, 12, 7],
                    source: {
                        address: 'tz1aRoaRhSpRYvFdyvgWLL6TGyRoGF51wDjM',
                    },
                    timestamp: '2021-01-14T21:31:18.000Z',
                },
            ]);
        });
    });

    describe('Reveal operation', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Reveal {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        fee
                        counter
                        gas_limit
                        public_key
                        id
                        consumed_milligas
                    }
                }
            }
        `;
        const hash = 'ooomsUiGgn4Tn33cBnS47cHTNYP28xic6zv8gCrSxjQduxpQyPg';

        it('should return reveal operation data', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data.operations).toEqual([
                {
                    __typename: 'Reveal',
                    batch_position: 0,
                    block: 'BM1m5FojCN6RGSVPXprCGmjgTuQ1bWEDgbbwL6DDsVjkqLxpXgQ',
                    consumed_milligas: '1000000',
                    counter: '9634734',
                    fee: '2120',
                    gas_limit: '10100',
                    hash: 'ooomsUiGgn4Tn33cBnS47cHTNYP28xic6zv8gCrSxjQduxpQyPg',
                    id: '13019750000024',
                    kind: 'reveal',
                    level: 1301975,
                    public_key: 'edpku4dqwQ7RwzifYhAngykCSgtTvddyxEh8BMey3kGYNxtNfetGdJ ',
                    source: {
                        address: 'tz1eps3jMsKj86q78F9conHjSGCTJZrSh5Mz',
                    },
                    timestamp: '2021-01-14T21:24:18.000Z',
                },
            ]);
        });
    });

    describe('Origination operation', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Origination {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        fee
                        counter
                        gas_limit
                        id
                        contract_address
                        consumed_milligas
                    }
                }
            }
        `;
        const hash = 'oowHaMcwCGFjswygaB1fNiJFPLrF79EySKVQys9oDNYtc6AzSMc';

        it('should return origination operation data', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data.operations).toEqual([
                {
                    __typename: 'Origination',
                    batch_position: 0,
                    block: 'BM6X2d3aSmCxDosHJZaEas8CKnCVGAVbeQUHowM7rBNeTnEppuX',
                    consumed_milligas: '5091365',
                    contract_address: 'KT1HedYVpGi5N2NVTXhRY5zw6PRjf8NFy5te',
                    counter: '9538052',
                    fee: '47640',
                    gas_limit: '100000',
                    hash: 'oowHaMcwCGFjswygaB1fNiJFPLrF79EySKVQys9oDNYtc6AzSMc',
                    id: '12969770000028',
                    kind: 'transaction',
                    level: 1296977,
                    source: {
                        address: 'tz2AtvFj92rvLU3HsjpVTvTNeRoXfiMYu9h7',
                    },
                    timestamp: '2021-01-11T06:14:13.000Z',
                },
            ]);
        });
    });

    describe('Transaction operation', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Transaction {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        counter
                        fee
                        id
                        counter
                        gas_limit
                        storage_limit
                        amount
                        parameters
                        entrypoint
                        destination
                        consumed_milligas
                    }
                }
            }
        `;
        const hash = 'opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo';

        it('should return transaction operation data', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data.operations).toEqual([
                {
                    __typename: 'Transaction',
                    amount: '9090000000',
                    batch_position: 0,
                    block: 'BLUtkgXLTg9ZvUo6FX4s99kBSkXTdte8VR1ZPQSx4tYZMFnj7Yf',
                    consumed_milligas: '1427000',
                    counter: '138259',
                    destination: 'tz1geePonXEknLZdwSso9vWAPPJGm4UVmiBF',
                    entrypoint: 'default',
                    fee: '17920',
                    gas_limit: '153850',
                    hash: 'opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo',
                    id: '13019810000023',
                    kind: 'transaction',
                    level: 1301981,
                    parameters: '{ "prim": "Unit" }',
                    source: {
                        address: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                    },
                    storage_limit: '300',
                    timestamp: '2021-01-14T21:30:18.000Z',
                },
            ]);
        });
    });

    describe('Delegation operation', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Delegation {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        counter
                        fee
                        id
                        counter
                        gas_limit
                        delegate
                        consumed_milligas
                    }
                }
            }
        `;
        const hash = 'oo6NZT12G8yNk6hRjusR3Qr7hhssW5x6Gh266tLgmwhMkG6FXLQ';

        it('should return delegation operation data', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data.operations).toEqual([
                {
                    __typename: 'Delegation',
                    batch_position: 0,
                    block: 'BLqxQvjp3vkRh427a2LWjKcKtkoh2oQvY7PEjY4jfdih2v76iRb',
                    consumed_milligas: '1000000',
                    counter: '6804322',
                    delegate: null,
                    fee: '30000',
                    gas_limit: '18136',
                    hash: 'oo6NZT12G8yNk6hRjusR3Qr7hhssW5x6Gh266tLgmwhMkG6FXLQ',
                    id: '13019790000028',
                    kind: 'delegation',
                    level: 1301979,
                    source: {
                        address: 'tz1gASbftBY3JZZvJ4F8XGCLt8QjPEDa41Cv',
                    },
                    timestamp: '2021-01-14T21:28:18.000Z',
                },
            ]);
        });
    });

    describe('Multiple operations', () => {
        const testQuery = gql`
            query OperationsQuery($hash: OperationHash!) {
                operations(hash: $hash) {
                    __typename
                    hash
                    source {
                        address
                    }
                    ... on Transaction {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        counter
                        fee
                        id
                        counter
                        gas_limit
                        storage_limit
                        amount
                        parameters
                        entrypoint
                        destination
                        transaction_consumed_milligas: consumed_milligas
                    }
                    ... on Reveal {
                        batch_position
                        kind
                        timestamp
                        level
                        block
                        fee
                        counter
                        gas_limit
                        public_key
                        id
                        reveal_consumed_milligas: consumed_milligas
                    }
                }
            }
        `;
        const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';

        it('should return a list of operations data for single hash', async () => {
            const res = await testUtils.testClient.query({
                query: testQuery,
                variables: { hash },
            });

            expect(res.errors).toBeUndefined();
            // eslint-disable-next-line no-warning-comments
            // FIXME: change to 4 and add missing transaction data assertion in array
            expect(res.data.operations).toHaveLength(3);
            expect(res.data.operations).toEqual(
                expect.arrayContaining([
                    {
                        __typename: 'Transaction',
                        amount: '0',
                        batch_position: 1,
                        block: 'BMU5LYNswNSKymTFXkxroXXZk7p4EvsVPG2jUJTdyyw8dZEzhLw',
                        counter: null,
                        destination: 'KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH',
                        entrypoint: 'mint',
                        fee: '15000',
                        gas_limit: null,
                        hash: 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ',
                        id: '13005130000036',
                        kind: 'transaction',
                        level: 1300513,
                        parameters: expect.any(String), // Multiline string hard to assert
                        source: {
                            address: 'KT1V4Vp7zhynCNuaBjWMpNU535Xm2sgqkz6M',
                        },
                        storage_limit: null,
                        timestamp: '2021-01-13T19:55:09.000Z',
                        transaction_consumed_milligas: '47850177',
                    },
                    {
                        __typename: 'Transaction',
                        amount: '0',
                        batch_position: 1,
                        block: 'BMU5LYNswNSKymTFXkxroXXZk7p4EvsVPG2jUJTdyyw8dZEzhLw',
                        counter: '9618124',
                        destination: 'KT1V4Vp7zhynCNuaBjWMpNU535Xm2sgqkz6M',
                        entrypoint: 'runEntrypointLambda',
                        fee: '15000',
                        gas_limit: '115000',
                        hash: 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ',
                        id: '13005130000034',
                        kind: 'transaction',
                        level: 1300513,
                        parameters: expect.any(String), // Multiline string hard to assert
                        source: {
                            address: 'tz1VsPcXRar3GKaAW34S5NiNHUKvEaR7Z7q9',
                        },
                        storage_limit: '1100',
                        timestamp: '2021-01-13T19:55:09.000Z',
                        transaction_consumed_milligas: '60247359',
                    },
                    {
                        __typename: 'Reveal',
                        batch_position: 0,
                        block: 'BMU5LYNswNSKymTFXkxroXXZk7p4EvsVPG2jUJTdyyw8dZEzhLw',
                        counter: '9618123',
                        fee: '0',
                        gas_limit: '10600',
                        hash: 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ',
                        id: '13005130000033',
                        kind: 'reveal',
                        level: 1300513,
                        public_key: 'edpkvWvcmkAikWmDSTgUpija5wBZphW7SUQaTWGbnLArJYdUhYWM61 ',
                        reveal_consumed_milligas: '1000000',
                        source: {
                            address: 'tz1VsPcXRar3GKaAW34S5NiNHUKvEaR7Z7q9',
                        },
                        timestamp: '2021-01-13T19:55:09.000Z',
                    },
                ]),
            );
        });
    });
});
