import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery } from './account.utils';

jest.setTimeout(30000);

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    const timestamps = [
        '2019-06-13 16:22',
        '2019-06-13 4:22PM',
        '2019-06-13 4:22 PM',
        '2019 06-13 4:22PM',
        '2019 06-13 4:22 PM',
        '2019 06/13 4:22PM',
        '2019 06/13 4:22 PM',
        '2019/06/13 16:22',
        '2019/06/13 4:22PM',
        '2019/06/13 4:22 PM',
        '2019 06 13 16:22',
        '2019 06 13 4:22PM',
        '2019 06 13 4:22 PM',
        '06 13 2019 16:22',
        '06 13 2019 4:22PM',
        '06 13 2019 4:22 PM',
        '06-13-2019 16:22',
        '06-13-2019 4:22PM',
        '06-13-2019 4:22 PM',
        '06/13/2019 16:22',
        '06/13/2019 4:22PM',
        '06/13/2019 4:22 PM',
        '2020',
        '2020-01',
        '2020-01-01',
        '2020-01-01 00:',
        '2020-01-01 00:00',
        '2020-01-01 00:00:00',
        '2020-01-01 00:00:00.000',
        '2020/01',
        '2020/01/01',
        '2020/01/01 00:',
        '2020/01/01 00:00',
        '2020/01/01 00:00:00',
        '2020/01/01 00:00:00.000',
    ];

    timestamps.forEach(timestamp => {
        it(`should not produce and error when querying for an account.transactions record with date_range->lte = '${timestamp}'`, async () => {
            const res = await executeTestQuery('transactions', {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                last: 1,
                date_range: {
                    gte: '2010-01-01 00:00',
                    lte: timestamp,
                },
            });
            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });
    });
});
