/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const operation = 'operations';

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return the first 3 account.operations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            after: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opPU1SBdqQvqsSHBkYbJ4A2yEmZaoKpTTMXgzMKSn62yPw5S2Tt:0',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.operations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            before: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            before: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2020-11-04T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opPU1SBdqQvqsSHBkYbJ4A2yEmZaoKpTTMXgzMKSn62yPw5S2Tt:0',
            date_range: {
                gte: '2019-10-22T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.operations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return date range error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-21T12:46:11Z',
            },
        });
        expect(res.errors?.[0]?.message).toBeDefined();
        expect(res.errors?.[0]?.message)
            .toContain(`UserInputError: Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.`);
    });

    it('should return a page size limit error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 201,

        });
        expect(res.errors).toBeDefined();
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an account.operations record with all of the operation fields', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
            ) {
            account(address: $address) {
                address
                activated
                operations(
                    last: $last
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            fee
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                            delegate
                            slots
                            consumed_milligas
                        }
                    }
                }
            }
        }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return an account.operations record with all of the operation fields with cursor', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
                $before: Cursor
            ) {
            account(address: $address) {
                address
                activated
                operations(
                    last: $last
                    before: $before
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            fee
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                            delegate
                            slots
                            consumed_milligas
                        }
                    }
                }
            }
        }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
            before: 'ongbfjXRCKQDesD93BrwCe8m2r9zzKXRtSoFdJEpb7K3n8VmmLs:0',
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
