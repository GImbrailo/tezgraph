/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const operation = 'endorsements';

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return the first 3 account.endorsements records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            after: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opST2sKbzb4in9YWfzSd6rmpeX3sYqKyQ25XMjoK43tTYzsV727:0',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opPCPkT3V97XHqumey6Z7nhp8eNLy46YMnELTDtm8ma3Uoip51a:0',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opMTjmmjpnX6ZRTpNBEuEXXG1wvtmpX4nRYda8GwTtR5kthTkXz:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.endorsements records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            before: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            after: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            before: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opEzXxddQYjJQRWeXqynBTA22raRESWgfKUx6LPCGp194nFW6tR:0',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },

        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.endorsements records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return date range error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-21T12:46:11Z',
            },
        });
        expect(res.errors?.[0]?.message).toBeDefined();
        expect(res.errors?.[0]?.message)
            .toContain(`UserInputError: Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.`);
    });
    it('account.endorsements query limit error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 201,
        });
        expect(res.errors).toBeDefined();
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an account.endorsements record with all of the endorsement fields', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
            ) {
                account(address: $address) {
                    address
                    activated
                    endorsements(
                        last: $last
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                kind
                                timestamp
                                level
                                block
                                id
                                source {
                                    address
                                }
                                delegate
                                slots
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return an account.endorsements record with all of the endorsement fields with cursor', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
                $before: Cursor
            ) {
                account(address: $address) {
                    address
                    activated
                    endorsements(
                        last: $last
                        before: $before
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                kind
                                timestamp
                                level
                                block
                                id
                                source {
                                    address
                                }
                                delegate
                                slots
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
            before: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
