/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const operation = 'transactions';

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return the first 3 account.transactions records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            before: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            after: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            before: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-06-13T16:25:03Z',
                lte: '2020-07-15T20:58:27Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-06-13T16:25:03Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-06-13T16:25:03Z',
                lte: '2020-07-15T20:58:27Z',
            },
            after: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-06-13T16:25:03Z',
                lte: '2020-07-15T20:58:27Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2020-03-14T03:09:17Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2020-03-14T03:09:17Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2020-03-14T03:09:17Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'oomuawT2jcbrRCmnXBCpBsT88wCXah5fihxmiQh5iKq7Kwxr4bC:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2020-03-14T03:09:17Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'oomuawT2jcbrRCmnXBCpBsT88wCXah5fihxmiQh5iKq7Kwxr4bC:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
    it('should return the first 3 account.transactions records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2020-03-14T03:09:17Z',
                gte: '2019-06-13T16:26:03Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.transactions records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                lte: '2020-03-14T03:09:17Z',
                gte: '2019-06-13T16:26:03Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            before: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            after: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2020-02-27T17:55:56Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2020-02-27T17:55:56Z',
            },
            before: 'oowsKfXqnNGCebjHJMUooAGynGrV5h5EPnY8Ck6NaNEMvdVf4bN:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2020-02-27T17:55:56Z',
            },
            after: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
            },
            before: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
            },
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
                gte: '2019-06-13T16:22:03Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
                gte: '2019-06-13T16:22:03Z',
            },
            before: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
                gte: '2019-06-13T16:22:03Z',
            },
            after: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-06-13T16:25:03Z',
                lte: '2020-07-15T20:58:27Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                gte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'oobxTtkqJcJEmR6N9begZXpgUm37DipMV3nrXHkG9eEAV69jnb8:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'oobxTtkqJcJEmR6N9begZXpgUm37DipMV3nrXHkG9eEAV69jnb8:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
                gte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            before: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.transactions records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            date_range: {
                lte: '2020-02-27T17:55:56Z',
                gte: '2019-06-13T16:22:03Z',
            },
            order_by: { field: ID, direction: ASC },
            after: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return date range error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-21T12:46:11Z',
            },
        });
        expect(res.errors?.[0]?.message).toBeDefined();
        expect(res.errors?.[0]?.message)
            .toContain(`UserInputError: Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.`);
    });

    it('should return a page size limit error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 201,
        });
        expect(res.errors).toBeDefined();
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an account.transactions record with all of the transaction fields', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
            ) {
                account(address: $address) {
                    address
                    activated
                    transactions(
                        last: $last
                    ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source {
                                address
                            }
                            kind
                            timestamp
                            level
                            block
                            fee
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                            consumed_milligas
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return an account.transactions record with all of the transaction fields while using cursor', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
                $before: Cursor
            ) {
                account(address: $address) {
                    address
                    activated
                    transactions(
                        last: $last
                        before: $before
                    ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source {
                                address
                            }
                            kind
                            timestamp
                            level
                            block
                            fee
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                            consumed_milligas
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
            before: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
