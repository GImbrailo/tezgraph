import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const testQuery = gql`
    query AccountQuery(
        $address: Address!
        $first: Int
        $last: Int
        $before: Cursor
        $after: Cursor
        $order_by: OrderBy
        $date_range: DateRange
    ) {
        account(address: $address) {
            transactions(
                first: $first
                last: $last
                before: $before
                after: $after
                order_by: $order_by
                date_range: $date_range
            ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
            }
        }
    }
`;

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return account.transactions when paginating with desc id order first', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with desc id order first and after', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            after: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with desc id order first and before', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            before: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order first', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            order_by: { field: ID, direction: ASC },
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order first and after', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            after: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            order_by: { field: ID, direction: ASC },
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order first and before', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            first: 3,
            before: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
            order_by: { field: ID, direction: ASC },
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with desc id order last', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with desc id order last and after', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            after: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with desc id order last and before', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            before: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order last', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order last and after', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return account.transactions when paginating with asc id order last and before', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 3,
            before: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
            order_by: { field: ID, direction: ASC },
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
