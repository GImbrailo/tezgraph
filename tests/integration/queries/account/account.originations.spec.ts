/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const operation = 'originations';

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return the first 3 account.originations records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
            before: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
            after: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2018-10-14T13:27:39Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-06-30T23:27:42Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
            date_range: {
                gte: '2018-06-30T23:46:42Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                gte: '2018-06-30T23:46:42Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.originations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-06-30T23:46:42Z',
            },
            after: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                lte: '2018-10-14T13:27:39Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
            date_range: {
                gte: '2018-06-30T23:46:42Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                gte: '2018-06-30T23:46:42Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.originations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
            date_range: {
                gte: '2018-08-02T11:20:38Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return date range error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 3,
            date_range: {
                gte: '2019-10-22T11:20:38Z',
                lte: '2019-10-21T12:46:11Z',
            },
        });
        expect(res.errors?.[0]?.message).toBeDefined();
        expect(res.errors?.[0]?.message)
            .toContain(`UserInputError: Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.`);
    });

    it('should return a page size limit error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            first: 201,
        });
        expect(res.errors).toBeDefined();
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an account.originations record with all of the origination fields', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
            ) {
                account(address: $address) {
                    address
                    activated
                    originations(
                        last: $last
                    ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source {
                                address
                            }
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                            consumed_milligas,
                            fee
                            }
                        }
                    }
                }
            }
        `;

        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 1,
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return an account.originations record with all of the origination fields with cursor', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
                $before: Cursor
            ) {
                account(address: $address) {
                    address
                    activated
                    originations(
                        last: $last
                        before: $before
                    ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source {
                                address
                            }
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                            consumed_milligas,
                            fee
                            }
                        }
                    }
                }
            }
        `;

        const res = await executeTestQuery(operation, {
            address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            last: 1,
            before: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
