/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery } from './account.utils';
describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return an account.reveals record with all of the reveal fields', async () => {
        const testQuery = gql`
        query AccountQuery(
            $address: Address!
            $last: Int
        ) {
            account(address: $address) {
                address
                    activated
                    public_key
                    reveals (
                        last: $last
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source {
                                    address
                                }
                                kind
                                timestamp
                                level
                                block
                                fee
                                counter
                                gas_limit
                                public_key
                                id,
                                consumed_milligas
                            }
                        }
                    }
                }
            }
        `;

        const res = await executeTestQuery('reveals', {
            address: 'tz1a3Gs9whDXUAVzKSbJ3abYaEUxhQm1wdb1',
            last: 1,
        }, testQuery);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
