/* eslint-disable max-lines */
import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery, ID, ASC } from './account.utils';

const operation = 'delegations';

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return the first 3 account.delegations records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            before: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the first 3 account.delegations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            before: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });

        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                lte: '2019-10-22T18:56:24Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            before: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id desc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date and lte date', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
            order_by: { field: ID, direction: ASC },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                gte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
            date_range: {
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date and lte date before cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            before: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return the last 3 account.delegations records in id asc order gte date and lte date after cursor', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            last: 3,
            order_by: { field: ID, direction: ASC },
            after: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-27T12:46:11Z',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return date range error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 3,
            date_range: {
                gte: '2019-10-22T18:56:24Z',
                lte: '2019-10-21T12:46:11Z',
            },
        });
        expect(res.errors?.[0]?.message).toBeDefined();
        expect(res.errors?.[0]?.message)
            .toContain(`UserInputError: Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.`);
    });

    it('should return a page size limit error', async () => {
        const res = await executeTestQuery(operation, {
            address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
            first: 201,
        });
        expect(res.errors).toBeDefined();
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an account.delegations record with all of the delegation fields', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
            ) {
                account(address: $address) {
                    address
                    activated
                    delegations(
                        last: $last
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source {
                                    address
                                }
                                kind
                                timestamp
                                level
                                block
                                fee
                                counter
                                gas_limit
                                id
                                delegate,
                                consumed_milligas
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return an account.delegations record with all of the delegation fields with cursor', async () => {
        const query = gql`
            query AccountQuery(
                $address: Address!
                $last: Int
                $before: Cursor
            ) {
                account(address: $address) {
                    address
                    activated
                    delegations(
                        last: $last
                        before: $before
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source {
                                    address
                                }
                                kind
                                timestamp
                                level
                                block
                                fee
                                counter
                                gas_limit
                                id
                                delegate,
                                consumed_milligas
                            }
                        }
                    }
                }
            }
        `;
        const res = await executeTestQuery(operation, {
            address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
            last: 1,
            before: 'opRJBeBXu9MZFqHXcYZy3oiL6ah1r9GcmrMHqWLnkY5UwsMQ1pB:1',
        }, query);
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
