import { gql } from 'apollo-server';

import { prismaRawClient } from '../../../../src/modules/queries-graphql/database/prisma';
import { executeTestQuery } from './account.utils';
const testQuery = gql`
    query AccountQuery(
        $address: Address!
    ) {
        account(address: $address) {
            address
        }
    }
`;

describe('Account Resolvers', () => {
    afterAll(async () => {
        await prismaRawClient.$disconnect();
    });

    it('should return account an error explaining that the given address string is not a valid Base58Check encoded hash', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'tz1W3VYi3gSt9Xgfw6WNV1aGZRPH21nva11d',
            last: 1,
        }, testQuery);
        expect(res.errors).toMatchSnapshot();
    });

    it('should return account an error explaining the given address string must be a valid tz1/tz2/tz3/KT1 address with a length of 36', async () => {
        const res = await executeTestQuery('transaction', {
            address: 'fooobar',
            last: 1,
        }, testQuery);
        expect(res.errors).toMatchSnapshot();
    });
});
