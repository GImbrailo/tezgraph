import { gql } from 'apollo-server';
import { DocumentNode } from 'graphql';

import { DateRange } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { createTestGraphQLClientUtils } from '../utils/test-client';

export const ID = 'ID';
export const ASC = 'ASC';

export function buildTestQuery(fieldResolver: string) {
    return gql`
        query AccountQuery(
            $address: Address!
            $first: Int
            $last: Int
            $before: Cursor
            $after: Cursor
            $order_by: OrderBy
            $date_range: DateRange
        ) {
            account(address: $address) {
                ${fieldResolver}(
                    first: $first
                    last: $last
                    before: $before
                    after: $after
                    order_by: $order_by
                    date_range: $date_range
                ) {
                    edges {
                        node {
                            hash
                            batch_position
                            timestamp
                            id
                        }
                    }
                }
            }
        }`;
}

export interface TestQueryOptionsVariables {
    address: string;
    first?: number;
    last?: number;
    before?: string;
    after?: string;
    order_by?: { field: string; direction: string };
    date_range?: DateRange;
}

export interface TestQueryOptions {
    query: DocumentNode;
    variables: TestQueryOptionsVariables;
    errorPolicy: string;
}

const testUtils = createTestGraphQLClientUtils();

export async function executeTestQuery(operation: string, variables: TestQueryOptionsVariables, customQuery?: DocumentNode) {
    return testUtils.testClient.query({
        query: customQuery ? customQuery : buildTestQuery(operation),
        variables: {
            ...variables,
        },
    });
}
