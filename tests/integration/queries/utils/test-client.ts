import { ApolloServer } from 'apollo-server-express';
import { ApolloServerTestClient, createTestClient } from 'apollo-server-testing';

import { ApolloServerFactory } from '../../../../src/bootstrap/apollo-server-factory';
import { ModuleName } from '../../../../src/modules/module';
import { Names } from '../../../../src/utils/configuration/env-config';
import { createTestDIContainer } from '../../helpers';

interface TestUtils {
    testClient: ApolloServerTestClient;
}

export function createTestGraphQLClientUtils() {
    const diContainer = createTestDIContainer({
        ...process.env,
        [Names.Modules]: ModuleName.QueriesGraphQL,
        ENABLE_DEBUG: 'true',
    });
    const apolloServerFactory = diContainer.resolve(ApolloServerFactory);
    let apolloServer: ApolloServer;
    const testUtils = {} as TestUtils;

    beforeAll(async () => {
        apolloServer = await apolloServerFactory.create();
        testUtils.testClient = createTestClient(apolloServer);
    });

    afterAll(async () => {
        await apolloServer.stop();
    });

    return testUtils;
}
