import { gql } from 'apollo-server';

export const doubleBakingEvidenceAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $delegate: NullableAddressArrayFilter
) {
    doubleBakingEvidenceAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            delegate: $delegate
        }
    ) {
        kind,
        bh1 {
            level,
            proto,
            predecessor,
            timestamp,
            validation_pass,
            operations_hash,
            fitness,
            context,
            priority,
            proof_of_work_nonce,
            seed_nonce_hash,
            signature
        },
        bh2 {
            level,
            proto,
            predecessor,
            timestamp,
            validation_pass,
            operations_hash,
            fitness,
            context,
            priority,
            proof_of_work_nonce,
            seed_nonce_hash,
            signature
        },
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            }
        }
    }
}
`;
