import { gql } from 'apollo-server';

export const originationAdded = gql`
subscription (
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $delegate: NullableAddressFilter,
    $originated_contract: NullableAddressArrayFilter,
    $status: NullableOperationResultStatusFilter
) {
    originationAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            status: $status,
            and: [
                { delegate: $delegate, },
                { originated_contract: $originated_contract, }
            ]
        }
    ) {
        kind,
        source,
        fee,
        counter,
        gas_limit,
        storage_limit,
        balance,
        delegate,
        script {
            code,
            storage
        },
        metadata {
            balance_updates {
               kind,
                delegate
            },
            internal_operation_results {
                kind,
                source,
                nonce,
                amount,
                destination
            },
            operation_result {
                status,
                consumed_gas,
                consumed_milligas,
                errors {
                    kind,
                    id
                }
            }
        }
    }
}
`;
