import { gql } from 'apollo-server';

export const ballotAdded = gql`
subscription(
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $source: AddressFilter,
    $proposal: ProtocolHashFilter,
    $ballot: BallotVoteFilter
) {
    ballotAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            source: $source,
            proposal: $proposal,
            ballot: $ballot
        }
    ) {
        kind,
        source,
        period,
        proposal,
        ballot
    }
}
`;
