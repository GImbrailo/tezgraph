import { gql } from 'apollo-server';

export const activateAccountAdded = gql`
subscription(
    # Common parameters.
    $includeMempool: Boolean,
    $replayFromBlockLevel: Int,
    $hash: NullableOperationHashFilter,
    $protocol: ProtocolHashFilter,
    $branch: BlockHashFilter,

    # Specific parameters.
    $pkh: AddressFilter
) {
    activateAccountAdded(
        # Common parameters.
        includeMempool: $includeMempool,
        replayFromBlockLevel: $replayFromBlockLevel,
        filter: {
            hash: $hash
            protocol: $protocol,
            branch: $branch,

            # Specific parameters.
            pkh: $pkh
        }
    ) {
        kind,
        pkh,
        secret,
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            }
        }
    }
}
`;
