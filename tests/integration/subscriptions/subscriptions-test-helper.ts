import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { DocumentNode } from 'graphql';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';
import { PassThrough } from 'stream';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import ws from 'ws';

import { App } from '../../../src/app';
import { tezosRpcPaths } from '../../../src/rpc/tezos-rpc-paths';
import { Names } from '../../../src/utils/configuration/env-config';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { Logger, LoggerFactory } from '../../../src/utils/logging';
import { createTestDIContainer, getPort, readFileText, waitUntil } from '../helpers';
import { rpcMocks } from './rpc-mocks';

export function createSubscriptionsTestHelper(envModules: string[]) {
    let app: App;
    let logger: Logger;
    let blockMonitorStream: PassThrough;
    let mempoolMonitorStream: PassThrough;
    let wsClient: SubscriptionClient | undefined;
    let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;
    let subscriptions: ZenObservable.Subscription[];

    beforeEach(async () => {
        const diContainer = createTestDIContainer({ [Names.Modules]: envModules.join() });
        app = diContainer.resolve(App);
        logger = diContainer.resolve(LoggerFactory).getLogger('IntegrationTests');
        subscriptions = [];

        // Setup RPC mocks.
        blockMonitorStream = new PassThrough();
        mempoolMonitorStream = new PassThrough();
        rpcMocks.setup({
            urlPath: tezosRpcPaths.blockMonitor,
            responseBody: blockMonitorStream,
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.block('BLBiBqzhMU1z5VpcTxHZc1wwwvU92LE4YMU96vTfiQSSbVHCqss'),
            responseBody: readTestDataFile('block.json'),
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.mempoolMonitor,
            responseBody: mempoolMonitorStream,
        });

        logger.logInformation('Testing with {environmentVariables}.', { environmentVariables: process.env });
        await app.start();
    });

    afterEach(async () => {
        logger.logInformation('Cleaning up after tests.');
        subscriptions.forEach(s => s.unsubscribe());
        blockMonitorStream.destroy();
        mempoolMonitorStream.destroy();
        apolloClient?.stop();
        wsClient?.unsubscribeAll();
        await app.stop();
        rpcMocks.cleanAll();
    });

    return {
        get logger() {
            return logger;
        },
        get blockMonitorStream() {
            return blockMonitorStream;
        },
        get mempoolMonitorStream() {
            return mempoolMonitorStream;
        },
        setupSubscriptionToTest(
            fileNameWithExpected: string,
            query: DocumentNode,
            filter?: Record<string, unknown>,
        ) {
            const expectedNotifications = JSON.parse(readTestDataFile(`expected/${fileNameWithExpected}-notifications.json`)) as unknown[];
            const receivedNotifications: unknown[] = [];

            if (!apolloClient) {
                wsClient = new SubscriptionClient(
                    `ws://localhost:${getPort(app)}${appUrlPaths.graphQL}`,
                    { reconnect: true },
                    ws,
                );
                apolloClient = new ApolloClient({
                    link: new WebSocketLink(wsClient),
                    cache: new InMemoryCache(),
                });
            }

            subscriptions.push(apolloClient
                .subscribe({
                    query,
                    variables: filter,
                })
                .subscribe({
                    next: d => receivedNotifications.push(d),
                }));

            return {
                async verifyReceivedNotifications() {
                    await waitUntil(() => {
                        try {
                            /* eslint-disable jest/no-standalone-expect */
                            expect(receivedNotifications).toHaveLength(expectedNotifications.length);
                            expect(receivedNotifications).toEqual(expectedNotifications);
                            /* eslint-enable jest/no-standalone-expect */
                        } catch (error: unknown) {
                            throw new Error(`Test '${test.name}' didn't receive data as expected. ${(error as Error).toString()}`);
                        }
                    });
                },
            };
        },
    };
}

export function readTestDataFile(fileName: string): string {
    return readFileText(`./tests/integration/subscriptions/test-data/${fileName}`);
}

export function updateReceivedOn(currentData: any, epxectedData: any): void {
    currentData.received_from_tezos_on = epxectedData.received_from_tezos_on;
    currentData.operations.forEach((o: any) => {
        o.info.received_from_tezos_on = epxectedData.received_from_tezos_on;
    });
}

export function getRedisPubSub() {
    const connectionStr = process.env.REDIS_CONNECTION_STRING;
    return new RedisPubSub({
        publisher: new Redis(connectionStr),
        subscriber: new Redis(connectionStr),
    });
}
