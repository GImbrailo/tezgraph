import { ModuleName } from '../../../src/modules/module';
import { runStandaloneSubscriptionsTest } from './standalone-subscriptions';

runStandaloneSubscriptionsTest('Redis PubSub', [ModuleName.SubscriptionsSubscriber, ModuleName.TezosMonitor, ModuleName.RedisPubSub]);
