import { ModuleName } from '../../../src/modules/module';
import { delay } from '../helpers';
import { blockAdded } from './gql/block-added';
import { createSubscriptionsTestHelper, getRedisPubSub, readTestDataFile } from './subscriptions-test-helper';

describe('Subscriptions from Redis PubSub', () => {
    const helper = createSubscriptionsTestHelper([ModuleName.SubscriptionsSubscriber, ModuleName.RedisPubSub]);

    it('should receive notifications from Redis PubSub and publish them to clients', async () => {
        const originationFilter = {
            delegate: { isNull: true },
            originated_contract: { isNull: false, includes: 'tz1X6MxebHJ4DLu6VAcEaCcqn6JoPg6tqrWW' },
        };

        const subscriptionTests = [helper.setupSubscriptionToTest('block', blockAdded, originationFilter)];

        // Let the app with workers start before simulating new monitor block header chunk.
        await delay(500);

        const inputNotification = JSON.parse(readTestDataFile('block-redis.json'));
        await getRedisPubSub().publish('BLOCKS', inputNotification);

        await Promise.all(subscriptionTests.map(async t => t.verifyReceivedNotifications()));
    });
});
