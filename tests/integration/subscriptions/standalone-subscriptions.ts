import { delay } from '../helpers';
import { activateAccountAdded } from './gql/activate-account-added';
import { ballotAdded } from './gql/ballot-added';
import { blockAdded } from './gql/block-added';
import { delegationAdded } from './gql/delegation-added';
import { doubleBakingEvidenceAdded } from './gql/double-baking-evidence-added';
import { doubleEndorsementEvidenceAdded } from './gql/double-endorsement-evidence-added';
import { endorsementAdded } from './gql/endorsement-added';
import { originationAdded } from './gql/origination-added';
import { proposalsAdded } from './gql/proposals-added';
import { revealAdded } from './gql/reveal-added';
import { seedNonceRevelationAdded } from './gql/seed-nonce-revelation-added';
import { transactionAdded } from './gql/transaction-added';
import { createSubscriptionsTestHelper, readTestDataFile } from './subscriptions-test-helper';

// eslint-disable-next-line jest/no-export
export function runStandaloneSubscriptionsTest(desc: string, modules: string[]): void {
    describe(`Subscriptions when running standalone app with ${desc}`, () => {
        const helper = createSubscriptionsTestHelper(modules);

        it('should receive notifications from Tezos node, process and publish them to clients', async () => {
            const endorsementFilter = {
                delegate: {
                    in: ['tz1e7uhpwmiKp8Yd2KwFwmVhoT31L478KUe3', 'tz1VxS7ff4YnZRs8b4mMP4WaMVpoQjuo1rjf'],
                    notIn: ['tz1NRTQeqcuwybgrZfJavBY3of83u8uLpFBj', 'tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3'],
                    notEqualTo: 'tz1TEZtYnuLiZLdA6c7JysAUJcHMrogu4Cpr',
                },
            };

            const originationFilter = {
                delegate: { isNull: true },
                originated_contract: { isNull: false, includes: 'tz1X6MxebHJ4DLu6VAcEaCcqn6JoPg6tqrWW' },
            };

            const transactionFilter = {
                source: {
                    equalTo: 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7',
                    notEqualTo: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                    in: ['tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7', 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'],
                },
                destination: { notIn: ['tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7'] },
                status: { notEqualTo: 'applied', notIn: ['backtracked', 'skipped'] },
                amount: { greaterThan: 49400000 },
            };

            const revealFromMempoolFilter = {
                includeMempool: true,
                status: { isNull: true }, // Filters out reveals from block.
            };

            const subscriptionTests = [
                helper.setupSubscriptionToTest('block', blockAdded, originationFilter),
                helper.setupSubscriptionToTest('activate-account', activateAccountAdded),
                helper.setupSubscriptionToTest('ballot', ballotAdded),
                helper.setupSubscriptionToTest('delegation', delegationAdded),
                helper.setupSubscriptionToTest('double-baking-evidence', doubleBakingEvidenceAdded),
                helper.setupSubscriptionToTest('double-endorsement-evidence', doubleEndorsementEvidenceAdded),
                helper.setupSubscriptionToTest('endorsement', endorsementAdded),
                helper.setupSubscriptionToTest('endorsement-filtered', endorsementAdded, endorsementFilter),
                helper.setupSubscriptionToTest('origination', originationAdded),
                helper.setupSubscriptionToTest('origination-filtered', originationAdded, originationFilter),
                helper.setupSubscriptionToTest('proposals', proposalsAdded),
                helper.setupSubscriptionToTest('reveal', revealAdded),
                helper.setupSubscriptionToTest('reveal-from-mempool', revealAdded, revealFromMempoolFilter),
                helper.setupSubscriptionToTest('seed-nonce-revelation', seedNonceRevelationAdded),
                helper.setupSubscriptionToTest('transaction', transactionAdded),
                helper.setupSubscriptionToTest('transaction-filtered', transactionAdded, transactionFilter),
            ];

            // Let the app with workers start before simulating new monitor block header chunk.
            await delay(500);

            helper.logger.logInformation('Pushing test data to Tezos monitor.');
            helper.blockMonitorStream.push(readTestDataFile('block-monitor.json'));
            helper.mempoolMonitorStream.push(readTestDataFile('mempool-monitor.json'));

            // Wait for app to receive, process and publish all notifications. Then assert.
            await Promise.all(subscriptionTests.map(async t => t.verifyReceivedNotifications()));
        });
    });
}
