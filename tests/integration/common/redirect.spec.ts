import supertest from 'supertest';

import { App } from '../../../src/app';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { startTestApp } from '../helpers';

describe('Redirects', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;

    beforeAll(async () => {
        app = await startTestApp();
        request = supertest(app.httpServer);
    });

    it(`should redirect "${appUrlPaths.root}" to "${appUrlPaths.graphQL}"`, async () => {
        await request.get(appUrlPaths.root)
            .expect(302)
            .expect('Location', appUrlPaths.graphQL);
    });

    afterAll(async () => {
        await app.stop();
    });
});
