import supertest from 'supertest';

import { App } from '../../../src/app';
import { VersionVariables } from '../../../src/utils/app-version/version-provider';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { createTestDIContainer, startTestApp } from '../helpers';

describe('Prometheus "tezgraph_build_info" metric', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;

    beforeAll(async () => {
        app = await startTestApp(createTestDIContainer({
            [VersionVariables.GitSha]: 'ee323ca7eaab5e317f54c66c4478cdbe8ffd5b73',
            [VersionVariables.GitTag]: 'v0.8.0-beta.1',
        }));
        request = supertest(app.httpServer);
    });

    afterAll(async () => {
        await app.stop();
    });

    it('should expose metric value after bootstrap', async () => {
        const res = await request.get(appUrlPaths.metrics);

        expect(res.status).toBe(200);
        expect(res.text).toInclude('tezgraph_build_info{git_sha="ee323ca7eaab5e317f54c66c4478cdbe8ffd5b73",tag="v0.8.0-beta.1"} 1');
    });
});
