import { Registry } from 'prom-client';
import supertest from 'supertest';
import { Query, Resolver } from 'type-graphql';

import { App } from '../../../src/app';
import { graphQLResolversDIToken } from '../../../src/bootstrap/graphql-resolver-type';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { createTestDIContainer } from '../helpers';

describe('Prometheus metrics endpoints', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;
    let promClientRegistry: Registry;

    beforeAll(async () => {
        @Resolver()
        class TestResolver {
            @Query()
            firstQuery(): boolean {
                return true;
            }

            @Query()
            secondQuery(): boolean {
                return true;
            }

            @Query()
            errorQuery(): boolean {
                throw new Error('Test error');
            }
        }

        const diContainer = createTestDIContainer();
        diContainer.registerInstance(graphQLResolversDIToken, TestResolver);

        app = diContainer.resolve(App);
        promClientRegistry = diContainer.resolve(Registry);
        await app.start();
        request = supertest(app.httpServer);
    });

    beforeEach(() => {
        promClientRegistry.resetMetrics();
    });

    afterAll(async () => {
        await app.stop();
    });

    describe('graphql_query_method_total', () => {
        it('should correctly return metric for single graphql call', async () => {
            await sendFirstQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 1');
            expect(text).not.toInclude('graphql_resolver_error_total{type="');
        });

        it('should correctly count subsequent graphql calls', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendFirstQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toInclude('graphql_query_method_total{method="firstQuery",service="indexer"} 2');
            expect(text).not.toInclude('graphql_resolver_error_total{type="');
        });

        it('should correctly return metric for different graphql calls', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendSecondQueryGraphQLRequest();
            await sendSecondQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toIncludeMultiple([
                'graphql_query_method_total{method="firstQuery",service="indexer"} 1',
                'graphql_query_method_total{method="secondQuery",service="indexer"} 2',
            ]);
            expect(text).not.toInclude('graphql_resolver_error_total{type="');
        });
    });

    describe('graphql_resolver_error_total', () => {
        it('should correctly return error metric value when graphql calls failed', async () => {
            await sendFirstQueryGraphQLRequest();
            await sendErrorQueryGraphQLRequest();
            await sendErrorQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toIncludeMultiple([
                'graphql_query_method_total{method="firstQuery",service="indexer"} 1',
                'graphql_query_method_total{method="errorQuery",service="indexer"} 2',
                'graphql_resolver_error_total{type="Query",field="errorQuery",service="indexer"} 2',
            ]);
        });
    });

    async function getMetrics() {
        const res = await request.get(appUrlPaths.metrics);

        expect(res.status).toBe(200);
        return res.text;
    }

    async function sendFirstQueryGraphQLRequest() {
        const res = await request.post('/graphql').send({
            query: 'query { firstQuery }',
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeUndefined();
        expect(res.body.data.firstQuery).toEqual(true);
    }

    async function sendSecondQueryGraphQLRequest() {
        const res = await request.post('/graphql').send({
            query: 'query { secondQuery }',
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeUndefined();
        expect(res.body.data.secondQuery).toEqual(true);
    }

    async function sendErrorQueryGraphQLRequest() {
        const res = await request.post('/graphql').send({
            query: 'query { errorQuery }',
        });

        expect(res.status).toEqual(200);
        expect(res.body.errors).toBeDefined();
    }
});
