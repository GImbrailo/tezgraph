import { SleepHelper } from '../../../../src/utils/time/sleep-helper';

describe(SleepHelper.name, () => {
    it('should wake after given time', async () => {
        const target = new SleepHelper();
        const startTime = Date.now();
        const millis = 500;

        await target.sleep(millis);

        const duration = Date.now() - startTime;
        expect(duration).toBeGreaterThanOrEqual(millis);
        expect(duration).toBeLessThanOrEqual(2 * millis);
    });
});
