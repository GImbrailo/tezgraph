import { EnvConfig } from '../../../../src/utils/configuration/env-config';
import { createRootLogger, offRootLogger, RootLogger } from '../../../../src/utils/logging/root-logger-factory';
import { nameof } from '../../../../src/utils/reflection';

describe(createRootLogger.name, () => {
    it('should return offRootLogger if no transports', () => {
        const envConfig = {
            enableDebug: false,
            logging: { console: null, file: null },
        } as EnvConfig;

        const logger = createRootLogger(envConfig);

        expect(logger).toBe(offRootLogger);
    });
});

describe('offRootLogger', () => {
    describe(nameof<RootLogger>('isLevelEnabled'), () => {
        it('should always return false', () => {
            expect(offRootLogger.isLevelEnabled(null!)).toBe(false);
        });
    });

    describe(nameof<RootLogger>('log'), () => {
        it('should do nothing', () => {
            expect(() => offRootLogger.log(null!)).not.toThrow();
        });
    });
});
