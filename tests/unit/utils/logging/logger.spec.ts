import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, spy, verify, when } from 'ts-mockito';

import { LogData, Logger, LogLevel, RootLogger } from '../../../../src/utils/logging';
import { LoggerLogCountLabel, MetricsContainer } from '../../../../src/utils/metrics/metrics-container';
import { KeyOfType, nameof } from '../../../../src/utils/reflection';
import { TestClock } from '../../mocks/test-clock';

describe(Logger.name, () => {
    let target: Logger;
    let rootLogger: RootLogger;
    let clock: TestClock;
    let metricsLogCounter: Counter<LoggerLogCountLabel>;

    beforeEach(() => {
        rootLogger = mock<RootLogger>();
        clock = new TestClock();
        const metrics = new MetricsContainer();
        target = new Logger('Foo', instance(rootLogger), clock, metrics);
        metricsLogCounter = spy(metrics.loggerLogCount);
    });

    describe(nameof<Logger>('isEnabled'), () => {
        testIsEnabled(t => t.isEnabled(LogLevel.Debug), LogLevel.Debug);
        testIsEnabled(t => t.isEnabled(LogLevel.Critical), LogLevel.Critical);
        testIsEnabled(t => t.isEnabled(LogLevel.Error), LogLevel.Error);
    });

    testIsEnabledProperty('isCriticalEnabled', LogLevel.Critical);
    testIsEnabledProperty('isErrorEnabled', LogLevel.Error);
    testIsEnabledProperty('isWarningEnabled', LogLevel.Warning);
    testIsEnabledProperty('isInformationEnabled', LogLevel.Information);
    testIsEnabledProperty('isDebugEnabled', LogLevel.Debug);

    function testIsEnabledProperty(property: KeyOfType<Logger, boolean>, expectedLevel: LogLevel) {
        describe(property, () => testIsEnabled(t => t[property], expectedLevel));
    }

    function testIsEnabled(act: (l: Logger) => boolean, expectedLevel: LogLevel) {
        [true, false].forEach(expectedReturn => {
            it(`should check ${expectedLevel} level and return ${expectedReturn}`, () => {
                when(rootLogger.isLevelEnabled(expectedLevel)).thenReturn(expectedReturn);

                const actual = act(target);

                expect(actual).toBe(expectedReturn);
            });
        });
    }

    describe(nameof<Logger>('log'), () => {
        testLog((t, m, d) => t.log(LogLevel.Information, m, d), LogLevel.Information);
        testLog((t, m, d) => t.log(LogLevel.Warning, m, d), LogLevel.Warning);
    });

    testLogMethod('logCritical', LogLevel.Critical);
    testLogMethod('logError', LogLevel.Error);
    testLogMethod('logWarning', LogLevel.Warning);
    testLogMethod('logInformation', LogLevel.Information);
    testLogMethod('logDebug', LogLevel.Debug);

    function testLogMethod(method: KeyOfType<Logger, (m: string) => void>, expectedLevel: LogLevel) {
        describe(method, () => testLog((t, m, d) => t[method](m, d), expectedLevel));
    }

    function testLog(act: (t: Logger, m: string, d: LogData) => void, expectedLevel: LogLevel) {
        it(`should log ${expectedLevel} level`, () => {
            const data: LogData = { name: 'Batman' };
            when(rootLogger.isLevelEnabled(expectedLevel)).thenReturn(true);

            act(target, 'test msg', data);

            verify(rootLogger.log(deepEqual({
                timestamp: clock.nowDate,
                category: 'Foo',
                level: expectedLevel,
                message: 'test msg',
                data,
            }))).once();
            verify(metricsLogCounter.inc(deepEqual({ level: expectedLevel.toString() }))).once();
        });

        it(`should not log ${expectedLevel} level if not enabled`, () => {
            const data: LogData = { name: 'Batman' };
            when(rootLogger.isLevelEnabled(expectedLevel)).thenReturn(false);

            act(target, 'test msg', data);

            verify(rootLogger.log(anything())).never();
            verify(metricsLogCounter.inc(anything())).never();
        });
    }

    describe(nameof<Logger>('close'), () => {
        it('should close root logger', () => {
            target.close();

            verify(rootLogger.close()).once();
        });
    });
});
