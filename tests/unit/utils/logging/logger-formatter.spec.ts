import { cyan, gray, green, red, yellow } from 'colors/safe';
import { Writable } from 'ts-essentials';

import { LogLevel } from '../../../../src/utils/logging';
import { LogData, LogEntry } from '../../../../src/utils/logging/log-entry';
import { formatEntry, formatMessage } from '../../../../src/utils/logging/logger-formatter';

describe('Logger formatter', () => {
    describe(formatMessage.name, () => {
        function act(message: string, data: LogData | undefined) {
            return formatMessage({ message, data } as LogEntry, d => `<d>${d}</d>`);
        }

        const noFormattingTestCases = [
            {
                desc: 'no data',
                message: 'Hello world',
            },
            {
                desc: 'empty data',
                message: 'Hello world',
                data: {},
            },
            {
                desc: 'empty message',
                message: '',
                data: { ignored: 123 },
            },
            {
                desc: 'message with no placeholders',
                message: 'Hello world',
                data: { ignored: 123 },
            },
        ];

        for (const testCase of noFormattingTestCases) {
            it(`should return same message if ${testCase.desc}`, () => {
                const formatted = act(testCase.message, testCase.data);

                expect(formatted).toBe(testCase.message);
            });
        }

        it('should format all placedeholers correctly', () => {
            const msg = 'Hello {hero} from {city}.';
            const data = {
                hero: 'Batman',
                city: 'Gotham',
                ignored: 'wtf',
            };

            const formatted = act(msg, data);

            expect(formatted).toBe('Hello hero <d>"Batman"</d> from city <d>"Gotham"</d>.');
        });

        const valueFormattingTestCases = [
            {
                desc: 'undefined',
                expected: 'null',
            },
            {
                desc: 'null',
                value: null,
                expected: 'null',
            },
            {
                desc: 'empty string',
                value: '',
                expected: '""',
            },
            {
                desc: 'string',
                value: 'omg',
                expected: '"omg"',
            },
            {
                desc: 'number',
                value: 123,
                expected: '123',
            },
            {
                desc: 'JSON',
                value: { level: 123 },
                expected: '{"level":123}',
            },
            {
                desc: 'array',
                value: [1, 2],
                expected: '[1,2]',
            },
        ];

        for (const testCase of valueFormattingTestCases) {
            it(`should format ${testCase.desc} correctly`, () => {
                const msg = 'Testing {value}.';
                const data = { value: testCase.value };

                const formatted = act(msg, data);

                expect(formatted).toBe(`Testing value <d>${testCase.expected}</d>.`);
            });
        }
    });

    describe(formatEntry.name, () => {
        let timeStr: string;
        let entry: Writable<LogEntry>;

        beforeEach(() => {
            timeStr = '2020-11-23T11:51:16.451Z';
            entry = {
                timestamp: new Date(timeStr),
                category: 'Foo',
                level: LogLevel.Information,
                message: 'Hello {name}.',
                data: { name: 'Batman' },
            };
        });

        it('should build message correctly', () => {
            const formatted = formatEntry(entry);

            expect(formatted).toBe(`${timeStr} information [Foo] Hello name "Batman".`);
        });

        it.each([
            [LogLevel.Critical, red('critical')],
            [LogLevel.Error, red('error')],
            [LogLevel.Warning, yellow('warning')],
            [LogLevel.Information, green('information')],
            [LogLevel.Debug, gray('debug')],
        ])('should correctly colorize message with level %s', (level, expectedLevel) => {
            entry.level = level;

            const formatted = formatEntry(entry, { colorize: true });

            expect(formatted).toBe(`${gray(timeStr)} ${expectedLevel} [${cyan('Foo')}] Hello name ${gray('"Batman"')}.`);
        });
    });
});
