/* eslint-disable max-len */
import OrderByUtils, { OrderBy, OrderByField, OrderByDirection } from '../../../../src/modules/queries-graphql/repositories/order-by.utils';

describe('Account OrderBy Utils', () => {
    const orderByUtils: OrderByUtils = new OrderByUtils();

    const createOrderByClauseCases: [string, number | undefined, OrderBy][] = [
        ['ORDER BY id ASC NULLS LAST, hash ASC, op_id ASC', 3, orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.DESC)],
        ['ORDER BY id ASC NULLS LAST, hash ASC, op_id ASC', undefined, orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.ASC)],
    ];
    it.each(createOrderByClauseCases)(`should return an SQL order by clause string - %s from %s and %o`, (expected, takeLimit, orderBy) => {
        expect(orderByUtils.createOrderByClause(takeLimit, orderBy)).toEqual(expected);
    });

    const createOrderByCases: [OrderBy, number | undefined, OrderBy][] = [
        [orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.ASC), 3, orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.DESC)],
        [orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.ASC), undefined, orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.ASC)],
    ];
    it.each(createOrderByCases)(`should return an OrderBy object - %o from %s and %o`, (expected, takeLimit, orderBy) => {
        expect(orderByUtils.createBeforeOrderBy(takeLimit, orderBy)).toEqual(expected);
    });

    const applyRelayOrderCases: [string, number | undefined, number | undefined, string | undefined, string | undefined, OrderBy, string][] = [
        [
            `SELECT * FROM (SELECT id FROM get_transaction('tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS') WHERE ((id <= '1427150' AND hash >= 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt' AND op_id > '1') OR (id <= '1427150' AND hash != 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt')) ORDER BY id ASC, hash ASC, op_id ASC LIMIT 3) AS result ORDER BY id DESC`,
            3,
            undefined,
            'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            undefined,
            orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.DESC),
            `SELECT id FROM get_transaction('tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS') WHERE ((id <= '1427150' AND hash >= 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt' AND op_id > '1') OR (id <= '1427150' AND hash != 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt')) ORDER BY id ASC, hash ASC, op_id ASC LIMIT 3`,
        ],
        [
            `SELECT * FROM (SELECT id FROM get_transaction('tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS') WHERE ((id <= '1427150' AND hash >= 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt' AND op_id > '1') OR (id <= '1427150' AND hash != 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt')) ORDER BY id DESC, hash ASC, op_id ASC LIMIT 3) AS result ORDER BY id ASC`,
            undefined,
            3,
            'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            undefined,
            orderByUtils.createOrderBy(OrderByField.ID, OrderByDirection.ASC),
            `SELECT id FROM get_transaction('tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS') WHERE ((id <= '1427150' AND hash >= 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt' AND op_id > '1') OR (id <= '1427150' AND hash != 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt')) ORDER BY id DESC, hash ASC, op_id ASC LIMIT 3`,
        ],
    ];
    it.each(applyRelayOrderCases)(`should return an a full SQL select statement - %s`, (expected, first, last, before, after, orderBy, query) => {
        expect(orderByUtils.applyRelayOrder(first, last, before, after, orderBy, query)).toEqual(expected);
    });
});
