/* eslint-disable max-lines */
import { Prisma, PrismaClient } from '@prisma/client';
import { mock } from 'ts-mockito';

import { PageInfo } from '../../../../src/entity/pageInfo';
import { TransactionEdge } from '../../../../src/entity/transaction';
import ConnectionUtils from '../../../../src/modules/queries-graphql/repositories/connection.utils';
import CursorUtils from '../../../../src/modules/queries-graphql/repositories/cursor.utils';
import DateRangeUtils from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import OrderByUtils, { OrderBy, OrderByField, OrderByDirection } from '../../../../src/modules/queries-graphql/repositories/order-by.utils';
import { createRelayPageData, RelayPageData } from '../../../../src/modules/queries-graphql/utils';
import { Logger } from '../../../../src/utils/logging';

describe('Account Connection Utils', () => {
    const prisma = mock<PrismaClient>();
    const logger = mock<Logger>();
    const dateRangeUtils: DateRangeUtils = new DateRangeUtils(logger);
    const cursorUtils: CursorUtils = new CursorUtils(prisma, dateRangeUtils, logger);
    const orderByUtils: OrderByUtils = new OrderByUtils();
    const connectionUtils: ConnectionUtils = new ConnectionUtils(prisma, cursorUtils, orderByUtils, logger);

    it('should return a PageInfo object with a start_cursor, end_cursor, has_next_page, and !has_previous_page', () => {
        const res = connectionUtils.pageInfoBuilder(
            'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
            'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
            true,
            false,
        );
        expect(res).toEqual({
            end_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
            has_next_page: true,
            has_previous_page: false,
            start_cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
        });
    });

    it('should return an RelayEdge array with 2 edges in the array each contain a cursor string and a node object', () => {
        const operations = [
            {
                id: BigInt(1427150),
                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                batch_position: 1,
                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 102031,
                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                counter: '19869',
                gas_limit: '0',
                storage_limit: '0',
                entrypoint: null,
                fee: 0,
                amount: 1000000,
                parameters: null,
                timestamp: '2018-09-13T16:12:50.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            },
            {
                id: BigInt(1425714),
                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                batch_position: 0,
                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 101957,
                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                counter: '59234',
                gas_limit: '200',
                storage_limit: '0',
                entrypoint: null,
                fee: 10,
                amount: 14,
                parameters: null,
                timestamp: '2018-09-13T14:51:20.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            },
        ];
        const res = connectionUtils.edgesBuilder(operations, 'transaction');
        expect(res).toMatchSnapshot();
    });

    it('should return an RelayConnection(transaction) object with edges and page_info', () => {
        const operationEdges: TransactionEdge[] = [
            {
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                node:
                {
                    id: BigInt(1427150),
                    hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                    batch_position: 1,
                    sourceAddress: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                    destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                    kind: 'transaction',
                    level: 102031,
                    block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                    counter: new Prisma.Decimal(19869),
                    gas_limit: new Prisma.Decimal(0),
                    storage_limit: new Prisma.Decimal(0),
                    entrypoint: null,
                    fee: 0 as unknown as bigint,
                    amount: 1000000 as unknown as bigint,
                    parameters: null,
                    timestamp: '2018-09-13T16:12:50.000Z' as unknown as Date,
                    consumed_milligas: new Prisma.Decimal(100000),
                },
            },
            {
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                node:
                {
                    id: BigInt(1425714),
                    hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                    batch_position: 0,
                    sourceAddress: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                    destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                    kind: 'transaction',
                    level: 101957,
                    block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                    counter: new Prisma.Decimal(59234),
                    gas_limit: new Prisma.Decimal(200),
                    storage_limit: new Prisma.Decimal(0),
                    entrypoint: null,
                    fee: 10 as unknown as bigint,
                    amount: 14 as unknown as bigint,
                    parameters: null,
                    timestamp: '2018-09-13T14:51:20.000Z' as unknown as Date,
                    consumed_milligas: new Prisma.Decimal(100000),
                },
            },
        ];
        const pageInfo: PageInfo = {
            start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
            end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            has_next_page: true,
            has_previous_page: false,
        };
        const res = connectionUtils.connectionBuilder('transaction', operationEdges, pageInfo);
        expect(res).toMatchSnapshot();
    });

    it('should return an error due to invalid cursor format in getConnection()', async () => {
        const operations = [
            {
                id: BigInt(1427150),
                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                batch_position: 1,
                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 102031,
                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                counter: '19869',
                gas_limit: '0',
                storage_limit: '0',
                entrypoint: null,
                fee: 0,
                amount: 1000000,
                parameters: null,
                timestamp: '2018-09-13T16:12:50.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:AA',
            },
            {
                id: BigInt(1425714),
                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                batch_position: 0,
                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                kind: 'transaction',
                level: 101957,
                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                counter: '59234',
                gas_limit: '200',
                storage_limit: '0',
                entrypoint: null,
                fee: 10,
                amount: 14,
                parameters: null,
                timestamp: '2018-09-13T14:51:20.000Z',
                contract_address: null,
                consumed_milligas: 100000,
                cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6j:0',
            },
        ];
        const pageInfoRelayPageData = createRelayPageData(undefined, undefined, 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENRdio34:0', undefined);
        let error;
        try {
            await connectionUtils.getConnection('transaction', operations, pageInfoRelayPageData, 'pkh', 'uuid', undefined, undefined);
        } catch (err) {
            error = err;
        }
        // eslint-disable-next-line max-len
        expect(error).toEqual(new Error('The start cursor (oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:AA) and/or end cursor(opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6j:0) retrieved from the Account Query results in getConnection() are not in a valid cursor format.'));
    });

    it('should return a SQL order by clause string with ORDER BY id DESC', () => {
        const relayPageData: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };
        const res = connectionUtils.getOrderByClause(relayPageData, orderBy);
        expect(res).toEqual('ORDER BY id DESC, hash ASC, op_id ASC');
    });

    it('should return a SQL order by clause string with ORDER BY id ASC', () => {
        const relayPageData: RelayPageData = {
            direction: 'first',
            order: 'after',
        };
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.ASC,
        };
        const res = connectionUtils.getOrderByClause(relayPageData, orderBy);
        expect(res).toEqual('ORDER BY id ASC, hash ASC, op_id ASC');
    });
});
