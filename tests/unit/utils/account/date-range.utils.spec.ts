/* eslint-disable max-lines */
import { format } from 'fecha';
import { mock } from 'ts-mockito';

import DateRangeUtils, { DateRange } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { Logger } from '../../../../src/utils/logging/logger';

describe('Account DateRange Utils', () => {
    const logger = mock<Logger>();
    const dateRangeUtils = new DateRangeUtils(logger);

    it('should return a DateRange object after processing a custom timestamp format', () => {
        const validCustomFormat: DateRange = { gte: '2020/07/17 19:46:24', lte: '2020-07-17 7:50PM' };
        const res = dateRangeUtils.dateRangeFormatter(validCustomFormat);
        const expected: DateRange = {
            gte: '2020-07-17T19:46:24',
            lte: '2020-07-17T19:50:00',
        };
        expect(res).toEqual(expected);
    });
    it('should return a isoDateTime', () => {
        const res = dateRangeUtils.dateRangeTimestampFormat('2020/07/17 19:46:24', 'isoDateTime');
        const expected = format(new Date('2020/07/17 19:46:24'), 'isoDateTime');
        expect(res).toEqual(expected);
    });

    it('should return an error due to lte < gte', () => {
        const invalidDateRange: DateRange = { lte: '2020-07-17T19:46:24', gte: '2020-07-17T19:50:24' };
        expect(
            () => dateRangeUtils.checkDateRangeValid(invalidDateRange),
        ).toThrow(
            'Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.',
        );
    });

    it('should return a SQL where clause string', () => {
        const validDateRange: DateRange = { gte: '2020-07-17T19:46:24', lte: '2020-07-17T19:50:24' };
        const res = dateRangeUtils.buildDateRangeClause(validDateRange);
        expect(res).toEqual(`WHERE (timestamp >= '2020-07-17T19:46:24' AND timestamp <= '2020-07-17T19:50:24')`);
    });

    const timestamps: [string, string][] = [
        ['2019-06-13 16:22', '2019-06-13 16:22'],
        ['2019-06-13 4:22PM', '2019-06-13 4:22 PM'],
        ['2019-06-13 4:22 PM', '2019-06-13 4:22  PM'],
        ['2019 06-13 4:22PM', '2019 06-13 4:22 PM'],
        ['2019 06-13 4:22 PM', '2019 06-13 4:22  PM'],
        ['2019 06/13 4:22PM', '2019 06/13 4:22 PM'],
        ['2019 06/13 4:22 PM', '2019 06/13 4:22  PM'],
        ['2019/06/13 16:22', '2019/06/13 16:22'],
        ['2019/06/13 4:22PM', '2019/06/13 4:22 PM'],
        ['2019/06/13 4:22 PM', '2019/06/13 4:22  PM'],
        ['2019 06 13 16:22', '2019 06 13 16:22'],
        ['2019 06 13 4:22PM', '2019 06 13 4:22 PM'],
        ['2019 06 13 4:22 PM', '2019 06 13 4:22  PM'],
        ['06 13 2019 16:22', '06 13 2019 16:22'],
        ['06 13 2019 4:22PM', '06 13 2019 4:22 PM'],
        ['06 13 2019 4:22 PM', '06 13 2019 4:22  PM'],
        ['06-13-2019 16:22', '06-13-2019 16:22'],
        ['06-13-2019 4:22PM', '06-13-2019 4:22 PM'],
        ['06-13-2019 4:22 PM', '06-13-2019 4:22  PM'],
        ['06/13/2019 16:22', '06/13/2019 16:22'],
        ['06/13/2019 4:22PM', '06/13/2019 4:22 PM'],
        ['06/13/2019 4:22 PM', '06/13/2019 4:22  PM'],
        ['2020', '1/1/20'],
        ['2020-01', '1/1/20'],
        ['2020-01-01', '1/1/20'],
        ['2020-01-01 00:', '2020-01-01 00:'],
        ['2020-01-01 00:00', '2020-01-01 00:00'],
        ['2020-01-01 00:00:00', '2020-01-01 00:00:00'],
        ['2020-01-01 00:00:00.000', '2020-01-01 00:00:00.000'],
        ['2020/01', '1/1/20'],
        ['2020/01/01', '1/1/20'],
        ['2020/01/01 00:', '2020/01/01 00:'],
        ['2020/01/01 00:00', '2020/01/01 00:00'],
        ['2020/01/01 00:00:00', '2020/01/01 00:00:00'],
        ['2020/01/01 00:00:00.000', '2020/01/01 00:00:00.000'],
    ];
    it.each(timestamps)(`should parse '%s' to '%s'`, (input, expected) => {
        expect(dateRangeUtils.dateRangeTimestampCleanUp(input)).toEqual(expected);
    });
});
