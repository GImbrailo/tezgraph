/* eslint-disable max-len */
/* eslint-disable max-lines */
import { PrismaClient } from '@prisma/client';
import { mock } from 'ts-mockito';

import CursorUtils from '../../../../src/modules/queries-graphql/repositories/cursor.utils';
import DateRangeUtils, { DateRange } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { OrderBy, OrderByField, OrderByDirection } from '../../../../src/modules/queries-graphql/repositories/order-by.utils';
import { RelayPageData } from '../../../../src/modules/queries-graphql/utils';
import { Logger } from '../../../../src/utils/logging';

describe('Account Cursor Utils', () => {
    const prisma = mock<PrismaClient>();
    const logger = mock<Logger>();
    const dateRangeUtils: DateRangeUtils = new DateRangeUtils(logger);
    const cursorUtils: CursorUtils = new CursorUtils(prisma, dateRangeUtils, logger);
    it('should return a "<"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'before',
        };

        const res = cursorUtils.getRelayDirection(orderBy, relayPageData);
        expect(res).toEqual('<');
    });

    it('should return a ">"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'first',
            order: 'before',
        };

        const res = cursorUtils.getRelayDirection(orderBy, relayPageData);
        expect(res).toEqual('>');
    });

    it('should return "DESC"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'before',
        };

        const res = cursorUtils.getOrderByDirection(orderBy, relayPageData);
        expect(res).toEqual('DESC');
    });

    it('should return "ASC"', () => {
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };

        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'after',
        };

        const res = cursorUtils.getOrderByDirection(orderBy, relayPageData);
        expect(res).toEqual('ASC');
    });

    it('should return a full SQL query for a cursor with orderBy', () => {
        const operation = 'transaction';
        const pkh = 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS';
        const selectStatement = `SELECT id`;
        const orderByClause = 'ORDER BY id DESC, hash ASC, op_id ASC';
        const dateRange = undefined;
        // eslint-disable-next-line max-len
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.DESC,
        };
        const relayPageData: RelayPageData = {
            direction: 'last',
            order: 'after',
        };
        const res = cursorUtils.getCursorQuery({
            operation,
            pkh,
            orderByClause,
            selectStatement,
            requestId: '437u35t-1d',
            dateRange,
            takeLimit: 1,
            paginationDirection: '>',
            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            cursorRecordID: '1019570000019',
            orderBy,
            relayPageData,
        });
        expect(res).toEqual(`
        SELECT * FROM 
        (
            SELECT * FROM
            (
                SELECT id FROM
                get_transaction_neighbour_greater(
                    'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS', '1019570000019', 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP', '0'
                )
                
            )
            AS results ORDER BY "id" DESC
        )
        AS results ORDER BY "id" DESC LIMIT 1`);
    });

    it('should return a full SQL query for a cursor with orderBy and dateRange', () => {
        const operation = 'transaction';
        const pkh = 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS';
        const selectStatement = `SELECT id`;
        const orderByClause = 'ORDER BY id ASC, hash ASC, op_id ASC';
        const dateRange: DateRange = { gte: '2020-07-17T19:46:24', lte: '2020-07-17T19:50:24' };
        // eslint-disable-next-line max-len
        const orderBy: OrderBy = {
            field: OrderByField.ID,
            direction: OrderByDirection.ASC,
        };
        const relayPageData: RelayPageData = {
            direction: 'first',
            order: 'before',
        };
        const res = cursorUtils.getCursorQuery({
            operation,
            pkh,
            orderByClause,
            selectStatement,
            requestId: '437u35t-1d',
            dateRange,
            takeLimit: 1,
            paginationDirection: '<',
            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
            cursorRecordID: '1019570000019',
            orderBy,
            relayPageData,
        });
        expect(res).toEqual(`
        SELECT * FROM 
        (
            SELECT * FROM
            (
                SELECT id FROM
                get_transaction_neighbour_lesser(
                    'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS', '1019570000019', 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP', '0'
                )
                WHERE (timestamp >= '2020-07-17T19:46:24' AND timestamp <= '2020-07-17T19:50:24')
            )
            AS results ORDER BY "id" ASC
        )
        AS results ORDER BY "id" ASC LIMIT 1`);
    });
});
