import { EnvConfigProvider, IntegerLimit } from '../../../../src/utils/configuration/env-config-provider';
import { Nullish } from '../../../../src/utils/reflection';
import { expectToThrow } from '../../mocks';

describe(EnvConfigProvider.name, () => {
    let target: EnvConfigProvider;
    let processEnv: Record<string, string | undefined>;

    beforeEach(() => {
        processEnv = {};
        target = new EnvConfigProvider(processEnv);
    });

    describeMethod('getString', () => {
        const act = (n: string) => target.getString(n);

        testValue({ desc: 'non-empty string', act, input: 'abc', expected: 'abc' });
        testMissingValue(act);
    });

    describeMethod('getStringArray', () => {
        const act = (n: string) => target.getStringArray(n);

        testValue({ desc: 'non-empty string', act, input: 'abc', expected: ['abc'] });
        testValue({ desc: 'string array', act, input: 'a,b,c', expected: ['a', 'b', 'c'] });
        testFailed({ desc: 'empty item', act, input: 'a,,b', expectedError: ['non-empty string'] });
        testFailed({ desc: 'white-space item', act, input: 'a,  ,b', expectedError: ['non-empty string'] });
        testMissingValue(act);
    });

    describeMethod('getInteger', () => {
        describe('no limit', () => {
            const act = (n: string) => target.getInteger(n);

            testValue({ desc: 'positive integer', act, input: '123', expected: 123 });
            testValue({ desc: 'zero', act, input: '0', expected: 0 });
            testValue({ desc: 'negative integer', act, input: '-123', expected: -123 });
        });

        describe(`limit is ${IntegerLimit.Positive}`, () => {
            const act = (n: string) => target.getInteger(n, IntegerLimit.Positive);

            testValue({ desc: 'positive integer', act, input: '123', expected: 123 });
            testFailed({ desc: 'zero', act, input: '0', expectedError: ['integer greater than zero'] });
            testFailed({ desc: 'negative integer', act, input: '-123', expectedError: ['integer greater than zero'] });
        });

        describe(`limit is ${IntegerLimit.PositiveOrZero}`, () => {
            const act = (n: string) => target.getInteger(n, IntegerLimit.PositiveOrZero);

            testValue({ desc: 'positive integer', act, input: '123', expected: 123 });
            testValue({ desc: 'zero', act, input: '0', expected: 0 });
            testFailed({ desc: 'negative integer', act, input: '-123', expectedError: ['integer greater than zero or equal to zero'] });
        });

        describe(`value isn't integer`, () => {
            const act = (n: string) => target.getInteger(n);

            testMissingValue(act);
            testFailed({ desc: 'string', act, input: 'abc', expectedError: ['integer'] });
            testFailed({ desc: 'decimal', act, input: '4.56', expectedError: ['integer'] });
        });
    });

    describeMethod('getBoolean', () => {
        const act = (n: string) => target.getBoolean(n);

        testValue({ desc: 'true', act, input: 'true', expected: true });
        testValue({ desc: 'false', act, input: 'false', expected: false });
        testMissingValue(act);
        testFailed({ desc: 'not boolean', act, input: 'abc', expectedError: ['Supported', '"true"', '"false"'] });
    });

    describeMethod('getIntegerArray', () => {
        const act = (n: string) => target.getIntegerArray(n, IntegerLimit.Positive);

        testValue({ desc: 'valid array', act, input: '1,2,3', expected: [1, 2, 3] });
        testMissingValue(act);
        testFailed({ desc: 'failed integer limit', act, input: '1,0,2', expectedError: ['integer greater than zero'] });
        testFailed({ desc: 'failed array', act, input: ',', expectedError: ['integer greater than zero'] });
    });

    describeMethod('getAbsoluteUrl', () => {
        const act = (n: string) => target.getAbsoluteUrl(n);

        testValue({ desc: 'valid URL', act, input: 'http://tezos.org/path?q=1', expected: 'http://tezos.org/path?q=1' });
        testMissingValue(act);
        testFailed({ desc: 'invalid URL', act, input: 'wtf', expectedError: ['absolute URL'] });
    });

    enum TestEnum {
        First = 'first',
        Second = 'second',
    }

    describeMethod('getEnum', () => {
        const act = (n: string) => target.getEnum(n, TestEnum);

        testValue({ desc: 'some value', act, input: 'first', expected: TestEnum.First });
        testValue({ desc: 'another value', act, input: 'second', expected: TestEnum.Second });
        testMissingValue(act);
        testFailed({ desc: 'unsupported', act, input: 'wtf', expectedError: ['Supported', '"first"', '"second"'] });
    });

    describeMethod('getOneOf', () => {
        const act = (n: string) => target.getOneOf(n, ['omg', 'lol']);

        testValue({ desc: 'some value', act, input: 'omg', expected: 'omg' });
        testValue({ desc: 'another value', act, input: 'lol', expected: 'lol' });
        testMissingValue(act);
        testFailed({ desc: 'unsupported', act, input: 'wtf', expectedError: ['Supported', '"omg"', '"lol"'] });
    });

    interface PassedTestCase<T> {
        desc: string;
        input: string;
        expected: T;
        act(n: string): T;
    }

    function testValue<T>(testCase: PassedTestCase<T>) {
        it(`should get value correctly if ${testCase.desc}`, () => {
            processEnv.Foo = testCase.input;

            const result = testCase.act('Foo');

            expect(result).toEqual(testCase.expected);
        });
    }

    function describeMethod(property: keyof EnvConfigProvider, tests: () => void): void {
        describe(property, tests); // eslint-disable-line jest/valid-describe
    }

    function testMissingValue(act: (n: string) => any): void {
        for (const input of [null, undefined, '', '  ']) {
            testFailed({ desc: JSON.stringify(input), act, input, expectedError: ['missing'] });
        }
    }

    interface FailedTestCase {
        desc: string;
        input: Nullish<string>;
        expectedError: string[];
        act(n: string): any;
    }

    function testFailed(testCase: FailedTestCase) {
        it(`should fail if ${testCase.desc}`, () => {
            processEnv.Foo = testCase.input!;

            const error = expectToThrow(() => testCase.act('Foo'));

            expect(error.message).toIncludeMultiple([
                ...testCase.expectedError,
                'Failed parsing',
                '"Foo"',
                `${JSON.stringify(testCase.input)}`,
            ]);
        });
    }
});
