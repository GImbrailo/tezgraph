import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { EnvConfig, Names, offLogLevel } from '../../../../src/utils/configuration/env-config';
import { EnvConfigProvider, IntegerLimit } from '../../../../src/utils/configuration/env-config-provider';
import { LogLevel } from '../../../../src/utils/logging';
import { LogFormat } from '../../../../src/utils/logging/log-config';
import { nameof } from '../../../../src/utils/reflection';

describe(EnvConfig.name, () => {
    let env: EnvConfigProvider;
    const act = () => new EnvConfig(instance(env));

    beforeEach(() => {
        env = mock(EnvConfigProvider);

        const expectedLogLevels = ['off', 'critical', 'error', 'warning', 'information', 'debug'];
        when(env.getString(Names.Host)).thenReturn('1.2.3.4');
        when(env.getInteger(Names.Port, IntegerLimit.PositiveOrZero)).thenReturn(3200);
        when(env.getAbsoluteUrl(Names.TezosNode)).thenReturn('https://api.tezos.org.ua/');
        when(env.getBoolean(Names.EnableDebug)).thenReturn(true);
        when(env.getInteger(Names.ReplayFromBlockLevelMaxFromHead, IntegerLimit.Positive)).thenReturn(66);
        when(env.getIntegerArray(Names.RpcRetryDelaysMillis, IntegerLimit.PositiveOrZero)).thenReturn([1, 2, 3]);
        when(env.getInteger(Names.SubscriptionKeepAliveMillis, IntegerLimit.PositiveOrZero)).thenReturn(4);
        when(env.getInteger(Names.HealthStatusCacheTtlSeconds, IntegerLimit.Positive)).thenReturn(60);
        when(env.getInteger(Names.GraphQLDepthLimit, IntegerLimit.PositiveOrZero)).thenReturn(7);
        when(env.getInteger(Names.GraphQLComplexityLimit, IntegerLimit.PositiveOrZero)).thenReturn(8);
        when(env.getStringArray(Names.Modules)).thenReturn(['foo', 'bar']);
        when(env.getBoolean(Names.EnableApiKey)).thenReturn(true);
        when(env.getString(Names.ApiKey)).thenReturn('api-kkk');
        when(env.getInteger(Names.TezosProcessingMaxAgeSecondsDegraded, IntegerLimit.Positive)).thenReturn(71);
        when(env.getInteger(Names.TezosProcessingMaxAgeSecondsUnhealthy, IntegerLimit.Positive)).thenReturn(72);

        when(env.getOneOf(Names.LogConsoleMinLevel, deepEqual(expectedLogLevels))).thenReturn('consoleMinLogLevel' as LogLevel);
        when(env.getEnum(Names.LogConsoleFormat, LogFormat)).thenReturn('consoleLogFormat' as LogFormat);
        when(env.getOneOf(Names.LogFileMinLevel, deepEqual(expectedLogLevels))).thenReturn('fileMinLogLevel' as LogLevel);
        when(env.getEnum(Names.LogFileFormat, LogFormat)).thenReturn('fileLogFormat' as LogFormat);
        when(env.getString(Names.LogFilePath)).thenReturn('logs/file.json');
    });

    it(`should be created correctly`, () => {
        const config = act();

        expect(config).toEqual<EnvConfig>({
            host: '1.2.3.4',
            port: 3200,
            tezosNodeUrl: 'https://api.tezos.org.ua/',
            enableDebug: true,
            replayFromBlockLevelMaxFromHead: 66,
            rpcRetryDelaysMillis: [1, 2, 3],
            subscriptionKeepAliveMillis: 4,
            healthStatusCacheTtlSeconds: 60,
            graphQLDepthLimit: 7,
            graphQLComplexityLimit: 8,
            apiKey: 'api-kkk',
            modules: ['foo', 'bar'],
            tezosProcessingMaxAgeMillis: {
                degraded: 71_000,
                unhealthy: 72_000,
            },
            logging: {
                console: {
                    format: 'consoleLogFormat' as LogFormat,
                    minLevel: 'consoleMinLogLevel' as LogLevel,
                },
                file: {
                    format: 'fileLogFormat' as LogFormat,
                    minLevel: 'fileMinLogLevel' as LogLevel,
                    path: 'logs/file.json',
                },
            },
        });
    });

    describe(nameof<EnvConfig>('enableDebug'), () => {
        it.each([
            [true],
            [false],
        ])('should return %s', value => {
            when(env.getBoolean(Names.EnableDebug)).thenReturn(value);

            const config = act();

            expect(config.enableDebug).toBe(value);
        });
    });

    describe(nameof<EnvConfig>('apiKey'), () => {
        it('should be null if disabled', () => {
            when(env.getBoolean(Names.EnableApiKey)).thenReturn(false);

            const config = act();

            expect(config.apiKey).toBeNull();
            verify(env.getString(Names.ApiKey)).never();
        });
    });

    describe(nameof<EnvConfig>('logging'), () => {
        it('should support disabled console', () => {
            when(env.getOneOf(Names.LogConsoleMinLevel, anything())).thenReturn(offLogLevel);

            const config = act();

            expect(config.logging.console).toBeNull();
            verify(env.getEnum(Names.LogConsoleFormat, LogFormat)).never();
        });

        it('should support disabled file', () => {
            when(env.getOneOf(Names.LogFileMinLevel, anything())).thenReturn(offLogLevel);

            const config = act();

            expect(config.logging.file).toBeNull();
            verify(env.getEnum(Names.LogFileFormat, LogFormat)).never();
            verify(env.getString(Names.LogFilePath)).never();
        });
    });
});
