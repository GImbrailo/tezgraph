import { isNotNullish, isNullish, nameof } from '../../../src/utils/reflection';

class TestData {
    value!: string;
}

describe('Reflection utils', () => {
    it(`${nameof.name}() should return member name`, () => {
        const name = nameof<TestData>('value');

        expect(name).toBe('value');
    });

    const nullishTestCases = [
        { desc: 'undefined', value: undefined, expected: true },
        { desc: 'null', value: null, expected: true },
        { desc: 'empty string', value: '', expected: false },
        { desc: 'zero', value: 0, expected: false },
        { desc: 'string', value: 'abc', expected: false },
        { desc: 'number', value: 123, expected: false },
    ];

    testNullish(isNullish, expected => expected);
    testNullish(isNotNullish, expected => !expected);

    function testNullish(act: (v: any) => boolean, transformExpected: (v: boolean) => boolean) {
        describe(`${act.name}()`, () => {
            nullishTestCases.forEach(({ desc, value, expected }) => {
                it(`should return ${transformExpected(expected)} if value is ${desc}`, () => {
                    const actual = act(value);

                    expect(actual).toBe(transformExpected(expected));
                });
            });
        });
    }
});
