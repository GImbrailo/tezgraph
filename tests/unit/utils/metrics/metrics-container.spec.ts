import { MetricsContainer } from '../../../../src/utils/metrics/metrics-container';

describe(MetricsContainer.name, () => {
    it('should have metrics created correctly', () => {
        expect(() => new MetricsContainer()).not.toThrow();
    });
});
