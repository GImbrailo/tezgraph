import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, verify } from 'ts-mockito';
import { NextFn, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../../../src/bootstrap/resolver-context';
import { OperationArgs } from '../../../../src/entity/subscriptions/args/operation-args';
import { SubscriptionArgs } from '../../../../src/entity/subscriptions/args/subscription-args';
import { OperationNotification } from '../../../../src/entity/subscriptions/operation-notification';
import {
    GraphQLErrorCounterLabel,
    GraphQLQueryMethodLabel,
    GraphQLSubscriptionMethodLabel,
    MetricsContainer,
} from '../../../../src/utils/metrics/metrics-container';
import { MetricsMiddleware, ParentType, SERVICE_NAME } from '../../../../src/utils/metrics/metrics-middleware';
import { nameof } from '../../../../src/utils/reflection';
import { expectToThrowAsync, TestLogger } from '../../mocks';
import { TestClock } from '../../mocks/test-clock';

describe(MetricsMiddleware.name, () => {
    let target: MetricsMiddleware;
    let logger: TestLogger;
    let graphQLQueryMethodCounter: Counter<GraphQLQueryMethodLabel>;
    let graphQLSubscriptionMethodCounter: Counter<GraphQLSubscriptionMethodLabel>;
    let graphQLErrorCounter: Counter<GraphQLErrorCounterLabel>;
    let metrics: MetricsContainer;

    let resolverData: ResolverData<ResolverContext>;
    let next: NextFn;

    const act = async () => target.use(resolverData, next);

    beforeEach(() => {
        logger = new TestLogger();
        graphQLQueryMethodCounter = mock(Counter);
        graphQLSubscriptionMethodCounter = mock(Counter);
        graphQLErrorCounter = mock(Counter);
        metrics = {
            graphQLQueryMethodCounter: instance(graphQLQueryMethodCounter),
            graphQLSubscriptionMethodCounter: instance(graphQLSubscriptionMethodCounter),
            graphQLErrorCounter: instance(graphQLErrorCounter),
        } as MetricsContainer;
        target = new MetricsMiddleware(metrics, new TestClock(), logger);

        resolverData = {
            info: {
                fieldName: 'transactions',
                parentType: { name: 'Explosion' },
            },
            args: {},
        } as ResolverData<ResolverContext>;
        next = async () => Promise.resolve('resultData');
    });

    it('should not increment query counter if not query', async () => {
        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLQueryMethodCounter.inc(anything())).never();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should not increment subscription counter if not subscription', async () => {
        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLSubscriptionMethodCounter.inc(anything())).never();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it.each([ParentType.Query, ParentType.Mutation])('should increment query counter if %s', async (type) => {
        resolverData.info.parentType.name = type;

        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLQueryMethodCounter.inc(anything())).once();
        verify(graphQLQueryMethodCounter.inc(deepEqual({ method: 'transactions', service: SERVICE_NAME }))).once();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should increment subscription counter if subscription', async () => {
        resolverData.info.parentType.name = ParentType.Subscription;

        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLSubscriptionMethodCounter.inc(anything())).once();
        verify(graphQLSubscriptionMethodCounter.inc(deepEqual({ method: 'transactions', mempool: 'false', replay: 'false' }))).once();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should increment subscription counter with arguments', async () => {
        resolverData.info.parentType.name = ParentType.Subscription;
        resolverData.args[nameof<OperationArgs<OperationNotification>>('includeMempool')] = true;
        resolverData.args[nameof<SubscriptionArgs>('replayFromBlockLevel')] = 123;

        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLSubscriptionMethodCounter.inc(anything())).once();
        verify(graphQLSubscriptionMethodCounter.inc(deepEqual({ method: 'transactions', mempool: 'true', replay: 'true' }))).once();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should increment error counter if next throws', async () => {
        const nextError = new Error('oups');
        next = async () => Promise.reject(nextError);

        const error = await expectToThrowAsync(act);

        expect(error).toBe(nextError);
        verify(graphQLErrorCounter.inc(anything())).once();
        verify(graphQLErrorCounter.inc(deepEqual({ type: 'Explosion', field: 'transactions', service: SERVICE_NAME }))).once();
    });
});
