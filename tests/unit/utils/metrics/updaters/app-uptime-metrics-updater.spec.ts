import { Gauge } from 'prom-client';
import { instance, mock, spy, verify, when } from 'ts-mockito';

import { MetricsContainer } from '../../../../../src/utils/metrics/metrics-container';
import { AppUptimeMetricsUpdater } from '../../../../../src/utils/metrics/updaters/app-uptime-metrics-updater';

describe(AppUptimeMetricsUpdater.name, () => {
    let target: AppUptimeMetricsUpdater;
    let nodeProcess: NodeJS.Process;
    let uptimeGauge: Gauge<string>;

    beforeEach(() => {
        const metrics = new MetricsContainer();
        nodeProcess = mock<NodeJS.Process>();
        target = new AppUptimeMetricsUpdater(metrics, instance(nodeProcess));

        uptimeGauge = spy(metrics.uptime);
    });

    it('should report process uptime', () => {
        when(nodeProcess.uptime()).thenReturn(123.987);

        target.update();

        verify(uptimeGauge.set(123));
    });
});
