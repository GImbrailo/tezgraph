import { Gauge } from 'prom-client';
import { deepEqual, instance, mock, spy, verify, when } from 'ts-mockito';

import { HealthCheckResult, HealthStatus } from '../../../../../src/utils/health/health-check';
import { HealthProvider, HealthReport } from '../../../../../src/utils/health/health-provider';
import { MetricsUpdater } from '../../../../../src/utils/metrics/metrics-collector';
import {
    HealthyMetricName,
    MetricsContainer,
    UnhealthyComponentLabel,
} from '../../../../../src/utils/metrics/metrics-container';
import { HealthMetricsUpdater } from '../../../../../src/utils/metrics/updaters/health-metrics-updater';

describe(HealthMetricsUpdater.name, () => {
    let target: MetricsUpdater;
    let healthProvider: HealthProvider;
    let unhealthyGauge: Gauge<UnhealthyComponentLabel>;

    beforeEach(() => {
        const metrics = new MetricsContainer();
        healthProvider = mock(HealthProvider);
        target = new HealthMetricsUpdater(metrics, instance(healthProvider));

        unhealthyGauge = spy(metrics.unhealthyCount);
    });

    it.each([
        [true, 0],
        [false, 1],
    ])('should update health gauge accordingly if aggregated isHealthy = %s', async (aggregatedIsHealthy, expectedAggregatedValue) => {
        when(healthProvider.generateHealthReport()).thenResolve({
            isHealthy: aggregatedIsHealthy,
            results: {
                good: { status: HealthStatus.Healthy } as HealthCheckResult,
                bad: { status: HealthStatus.Unhealthy } as HealthCheckResult,
                ugly: { status: HealthStatus.Degraded } as HealthCheckResult,
            } as Record<string, HealthCheckResult>,
        } as HealthReport);

        await target.update();

        verify(unhealthyGauge.set(deepEqual({ name: HealthyMetricName.Aggregated }), expectedAggregatedValue)).once();
        verify(unhealthyGauge.set(deepEqual({ name: 'good' }), 0)).once();
        verify(unhealthyGauge.set(deepEqual({ name: 'bad' }), 1)).once();
        verify(unhealthyGauge.set(deepEqual({ name: 'ugly' }), 0)).once();
    });
});
