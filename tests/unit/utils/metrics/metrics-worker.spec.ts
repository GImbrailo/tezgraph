import { Counter, Gauge, Registry } from 'prom-client';
import { instance, mock, verify } from 'ts-mockito';

import { MetricsRecord } from '../../../../src/utils/metrics/metrics-container';
import { MetricsWorker } from '../../../../src/utils/metrics/metrics-worker';
import { nameof } from '../../../../src/utils/reflection';

interface TestMetrics extends MetricsRecord {
    foo: Counter<string>;
    bar: Gauge<string>;
}

describe(MetricsWorker.name, () => {
    let target: MetricsWorker;
    let metrics: TestMetrics;
    let prometheusRegistry: Registry;

    beforeEach(() => {
        metrics = {
            foo: mock(Counter),
            bar: mock(Gauge),
        };
        prometheusRegistry = mock(Registry);
        target = new MetricsWorker(metrics, instance(prometheusRegistry));
    });

    describe(nameof<MetricsWorker>('start'), () => {
        it('should register all metrics', () => {
            target.start();

            verify(prometheusRegistry.registerMetric(metrics.foo)).once();
            verify(prometheusRegistry.registerMetric(metrics.bar)).once();
        });
    });

    describe(nameof<MetricsWorker>('stop'), () => {
        it('should clear metrics', () => {
            target.stop();

            verify(prometheusRegistry.clear()).once();
        });
    });
});
