import { PubSubEngine } from 'graphql-subscriptions';
import { deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { toAsyncIterable } from '../../../../src/utils/collections/async-iterable-utils';
import { LogLevel } from '../../../../src/utils/logging';
import { PubSubTrigger, TypedPubSub } from '../../../../src/utils/pubsub/typed-pub-sub';
import { nameof } from '../../../../src/utils/reflection';
import { TestLogger } from '../../mocks';

class TargetPubSub extends TypedPubSub {}
interface Foo { value: string }

describe(TypedPubSub.name, () => {
    let target: TypedPubSub;
    let pubSub: PubSubEngine;
    let logger: TestLogger;
    let trigger: PubSubTrigger<Foo>;

    beforeEach(() => {
        pubSub = mock<PubSubEngine>();
        logger = new TestLogger();
        target = new TargetPubSub(instance(pubSub), logger);

        trigger = new PubSubTrigger('FOO');
    });

    describe(nameof<TypedPubSub>('publish'), () => {
        it(`should publish given payload for given trigger`, async () => {
            const payload: Foo = { value: 'hhh' };

            await target.publish(trigger, payload);

            verify(pubSub.publish('FOO', payload)).once();
            logger.loggedSingle().verify(LogLevel.Debug, { payload, trigger: 'FOO' });
        });
    });

    describe(nameof<TypedPubSub>('subscribe'), () => {
        it(`should subscribe to given trigger`, () => {
            const handler = () => { /* Nothing */ };

            target.subscribe(trigger, handler);

            verify(pubSub.subscribe('FOO', handler, deepEqual({}))).once();
        });
    });

    describe(nameof<TypedPubSub>('iterate'), () => {
        it(`should iterate payloads for given trigger`, () => {
            const testIterable = toAsyncIterable<Foo>([]);
            when(pubSub.asyncIterator(deepEqual(['FOO']))).thenReturn(testIterable);

            const iterable = target.iterate([trigger]);

            expect(iterable).toBe(testIterable);
        });
    });
});
