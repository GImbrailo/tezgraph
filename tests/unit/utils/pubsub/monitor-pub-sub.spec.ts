import { externalTriggers } from '../../../../src/utils/pubsub/external-pub-sub';

describe('monitorTriggers', () => {
    it('should have correct names', () => {
        expect(externalTriggers.blocks.name).toBe('BLOCKS');
        expect(externalTriggers.mempoolOperationGroups.name).toBe('MEMPOOL_OPERATION_GROUPS');
    });
});
