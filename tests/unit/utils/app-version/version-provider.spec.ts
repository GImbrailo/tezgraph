import { Version } from '../../../../src/utils/app-version/version';
import { VersionProvider, VersionVariables } from '../../../../src/utils/app-version/version-provider';

describe(VersionProvider.name, () => {
    it(`should determine version correctly`, () => {
        const processEnv: NodeJS.ProcessEnv = {
            [VersionVariables.AppName]: 'tezgraph',
            [VersionVariables.GitSha]: 'hahaha',
            [VersionVariables.GitTag]: 'ttt',
            [VersionVariables.AppUrl]: 'https://gitlab/tezgraph',
        };

        const target = new VersionProvider(processEnv);

        expect(target.version).toEqual<Version>({
            name: 'tezgraph',
            git_sha: 'hahaha',
            tag: 'ttt',
            release_notes: 'https://gitlab/tezgraph/-/releases/ttt',
        });
    });

    it(`should handle release notes url without git tag`, () => {
        const processEnv: NodeJS.ProcessEnv = {
            [VersionVariables.AppUrl]: 'https://gitlab/tezgraph',
        };

        const target = new VersionProvider(processEnv);

        expect(target.version.release_notes).toBe('https://gitlab/tezgraph/-/releases/');
    });

    it(`should return empty version`, () => {
        const target = new VersionProvider({});

        expect(target.version).toEqual<Version>({
            name: null,
            git_sha: null,
            tag: null,
            release_notes: null,
        });
    });
});
