import { instance, mock, when } from 'ts-mockito';

import {
    asIterableIterator,
    filter,
    take,
    toArray,
    toAsyncIterable,
} from '../../../../src/utils/collections/async-iterable-utils';

describe('AsyncIterable utils', () => {
    describe(asIterableIterator.name, () => {
        it('should return same object if already iterable', () => {
            const inputIterable = mockIterable([1, 2, 3]);

            const iterable = asIterableIterator(inputIterable);

            expect(iterable).toBe(inputIterable);
        });

        it('should wrap iterator if not already iterable', () => {
            const inputIterator = mock<AsyncIterator<number>>();
            const nextResult = {} as Promise<IteratorResult<number>>;
            const returnResult = {} as Promise<IteratorResult<number>>;
            const throwResult = {} as Promise<IteratorResult<number>>;
            when(inputIterator.next()).thenReturn(nextResult);
            when(inputIterator.return!()).thenReturn(returnResult);
            when(inputIterator.throw!()).thenReturn(throwResult);

            const iterable = asIterableIterator(instance(inputIterator));

            expect(iterable).not.toBe(inputIterator);
            expect(iterable[Symbol.asyncIterator]()).toBe(iterable);
            expect(iterable.next()).toBe(nextResult);
            expect(iterable.return!()).toBe(returnResult);
            expect(iterable.throw!()).toBe(throwResult);
        });

        /* eslint-disable @typescript-eslint/unbound-method */
        it('should wrap iterator if not already iterable without return() and throw()', () => {
            const inputIterator = mock<AsyncIterator<number>>();
            when(inputIterator.return).thenReturn(undefined);
            when(inputIterator.throw).thenReturn(undefined);

            const iterable = asIterableIterator(instance(inputIterator));

            expect(iterable.return).toBeUndefined();
            expect(iterable.throw).toBeUndefined();
        });
        /* eslint-enable @typescript-eslint/unbound-method */
    });

    describe(toArray.name, () => {
        it('should iterate all items to array', async () => {
            const inputIterable = mockIterable([1, 2, 3]);

            const items = await toArray(inputIterable);

            expect(items).toEqual([1, 2, 3]);
        });
    });

    describe(toAsyncIterable.name, () => {
        it('should convert array to iterable', async () => {
            const iterable = toAsyncIterable([1, 2, 3]);

            expect(await iterate(iterable)).toEqual([1, 2, 3]);
        });
    });

    describe(filter.name, () => {
        it('should filter items according to predicate', async () => {
            const inputIterable = mockIterable([1, 2, 3, 4]);

            const iterable = filter(inputIterable, x => (x % 2) === 0);

            expect(await iterate(iterable)).toEqual([2, 4]);
        });
    });

    describe(take.name, () => {
        it.each([
            [0, []],
            [2, [1, 2]],
        ])('should return only %s first items', async (count, expectedItems) => {
            let iteratedCount = 0;
            const inputIterable = (async function *() {
                iteratedCount++;
                await Promise.resolve();
                yield 1;
                iteratedCount++;
                yield 2;
                iteratedCount++;
                yield 3;
            }());

            const iterable = take(inputIterable, count);

            expect(await iterate(iterable)).toEqual(expectedItems);
            expect(iteratedCount).toBe(count);
        });
    });
});

async function *mockIterable(items: number[]) {
    await Promise.resolve();
    yield* items;
}

async function iterate(iterable: AsyncIterableIterator<number>) {
    const result = [];
    for await (const item of iterable) {
        result.push(item);
    }
    return result;
}
