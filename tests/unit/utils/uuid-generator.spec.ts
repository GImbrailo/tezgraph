import { range, uniq } from 'lodash';

import { UuidGenerator } from '../../../src/utils/uuid-generator';

describe(UuidGenerator.name, () => {
    it('should generate UUID', () => {
        const target = new UuidGenerator();

        const uuid = target.generate();

        expect(uuid).toMatch(/\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/u);
    });

    it('generated UUIDs should be random', () => {
        const target = new UuidGenerator();

        const uuids = range(10).map(() => target.generate());

        expect(uniq(uuids)).toHaveLength(uuids.length);
    });
});
