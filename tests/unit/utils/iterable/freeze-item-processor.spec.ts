import { instance, mock, verify } from 'ts-mockito';

import { FreezeItemProcessor } from '../../../../src/utils/iterable/freeze-item-processor';
import { ItemProcessor } from '../../../../src/utils/iterable/interfaces';
import { expectToThrow } from '../../mocks';

interface Foo {
    value: string;
}

describe(FreezeItemProcessor.name, () => {
    let target: ItemProcessor<Foo>;
    let nextProcessor: ItemProcessor<Foo>;

    beforeEach(() => {
        nextProcessor = mock<ItemProcessor<Foo>>();
        target = new FreezeItemProcessor(instance(nextProcessor));
    });

    it('should freeze iterated items', async () => {
        const item: Foo = { value: 'aa' };

        await target.processItem(item);

        verify(nextProcessor.processItem(item)).once();
        expectToThrow(() => {
            item.value = 'bb';
        });
    });
});
