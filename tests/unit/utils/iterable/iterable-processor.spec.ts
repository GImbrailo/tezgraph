import { AbortSignal } from 'abort-controller';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { toAsyncIterable } from '../../../../src/utils/collections/async-iterable-utils';
import { ItemProcessor, IterableProvider } from '../../../../src/utils/iterable/interfaces';
import { IterableProcessor } from '../../../../src/utils/iterable/iterable-processor';
import { LogLevel } from '../../../../src/utils/logging';
import { TestAbortError, TestLogger } from '../../mocks';
import { TestClock } from '../../mocks/test-clock';

describe(IterableProcessor.name, () => {
    let target: IterableProcessor<number>;
    let iterableProvider: IterableProvider<number>;
    let itemProcessor: ItemProcessor<number>;
    let abortSignal: AbortSignal;
    let clock: TestClock;
    let logger: TestLogger;

    beforeEach(() => {
        iterableProvider = mock<IterableProvider<number>>();
        itemProcessor = mock<ItemProcessor<number>>();
        const itemInfoProvider = { getInfo: (x: number) => `info-${x}` };
        abortSignal = mock(AbortSignal);
        clock = new TestClock();
        logger = new TestLogger();
        target = new IterableProcessor(instance(iterableProvider), instance(itemProcessor), itemInfoProvider, instance(abortSignal), clock, logger);

        when(iterableProvider.iterate())
            .thenReturn(toAsyncIterable([1, 2]))
            .thenReturn(mockAbortedIterable([3]));
        when(abortSignal.aborted)
            .thenReturn(false)
            .thenReturn(false)
            .thenReturn(true);
    });

    it('should expose empty defaults', () => {
        expect(target.currentIterable).toBeUndefined();
        expect(target.lastItemInfo).toBeUndefined();
        expect(target.lastError).toBeUndefined();
        expect(target.lastSuccessTime).toEqual(new Date(0));
    });

    it('should continuously process items', async () => {
        const promise = target.processItems();
        expect(target.currentIterable).toBeDefined();
        await promise;

        verify(itemProcessor.processItem(anything())).times(3);
        verify(itemProcessor.processItem(1)).calledBefore(itemProcessor.processItem(2));
        verify(itemProcessor.processItem(2)).calledBefore(itemProcessor.processItem(3));

        expect(target.lastItemInfo).toBe('info-3');
        expect(target.lastSuccessTime).toBe(clock.nowDate);
        expect(target.lastError).toBeUndefined();
        expect(target.currentIterable).toBeUndefined();

        logger.verifyLoggedCount(3);
        logger.logged(0).verify(LogLevel.Information).verifyMessageIncludesAll('Start');
        logger.logged(1).verify(LogLevel.Error);
        logger.logged(2).verify(LogLevel.Information).verifyMessageIncludesAll('aborted');
    });

    it('should keep processing items if some item processing failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(1)).thenThrow(error);

        await target.processItems();

        verify(itemProcessor.processItem(2)).once();
        expect(target.lastError).toBeUndefined();

        logger.verifyLoggedCount(4);
        logger.logged(1).verify(LogLevel.Error, { item: 'info-1', error });
    });

    it('should expose error if last item failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(3)).thenThrow(error);

        await target.processItems();

        expect(target.lastError).toBe(error);
    });
});

async function *mockAbortedIterable<T>(array: readonly T[]): AsyncIterableIterator<T> {
    await Promise.resolve();
    yield* array;
    throw new TestAbortError();
}
