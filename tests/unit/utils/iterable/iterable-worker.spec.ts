import { instance, mock, spy, verify, when } from 'ts-mockito';

import { IterableProcessor } from '../../../../src/utils/iterable/iterable-processor';
import { IterableWorker } from '../../../../src/utils/iterable/iterable-worker';
import { nameof } from '../../../../src/utils/reflection';
import { expectToThrow } from '../../mocks';

describe(IterableWorker.name, () => {
    let target: IterableWorker;
    let processor: IterableProcessor;

    beforeEach(() => {
        processor = {} as IterableProcessor;
        target = new IterableWorker('Foo', processor);
    });

    describe(nameof<IterableWorker>('name'), () => {
        it('should be correct', () => expect(target.name).toBe('Foo'));
    });

    describe(nameof<IterableWorker>('start'), () => {
        it('should start processing items', () => {
            processor.processItems = async (): Promise<void> => Promise.resolve();
            const processorSpy = spy(processor);

            target.start();

            verify(processorSpy.processItems()).once();
        });

        it('should throw if processing iterable already exists', () => {
            processor.currentIterable = {} as AsyncIterableIterator<unknown>;

            expect(() => target.start()).toThrow();
        });
    });

    describe(nameof<IterableWorker>('stop'), () => {
        it('should call iterable.return() on processor', () => {
            const iterable = mock<AsyncIterableIterator<unknown>>();
            when(iterable.return!()).thenResolve();
            processor.currentIterable = instance(iterable);

            target.stop();

            verify(iterable.return!()).once();
            expect(processor.currentIterable).toBeUndefined();
        });

        it('should throw if no iterable.return on processor', () => {
            processor.currentIterable = {} as AsyncIterableIterator<unknown>;

            expectToThrow(() => target.stop());
        });

        it('should throw if no iterable on processor', () => {
            expectToThrow(() => target.stop());
        });
    });
});
