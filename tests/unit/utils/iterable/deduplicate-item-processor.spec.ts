import { anything, instance, mock, verify } from 'ts-mockito';

import { DeduplicateItemProcessor } from '../../../../src/utils/iterable/deduplicate-item-processor';
import { ItemProcessor } from '../../../../src/utils/iterable/interfaces';

interface Foo {
    id: string;
    value: string;
}

describe(DeduplicateItemProcessor.name, () => {
    let target: ItemProcessor<Foo>;
    let nextProcessor: ItemProcessor<Foo>;

    beforeEach(() => {
        nextProcessor = mock<ItemProcessor<Foo>>();
        target = new DeduplicateItemProcessor(100, f => f.id, instance(nextProcessor));
    });

    it('should deduplicate items', async () => {
        const items: [Foo, Foo, Foo, Foo, Foo, Foo] = [
            { id: '1', value: 'val.1.1' },
            { id: '2', value: 'val.2.1' },
            { id: '1', value: 'val.1.2' },
            { id: '2', value: 'val.2.2' },
            { id: '3', value: 'val.3' },
            { id: '2', value: 'val.2.3' },
        ];

        await Promise.all(items.map(async i => target.processItem(i)));

        verify(nextProcessor.processItem(anything())).times(3);
        verify(nextProcessor.processItem(items[0])).once();
        verify(nextProcessor.processItem(items[1])).once();
        verify(nextProcessor.processItem(items[4])).once();
    });
});
