import { EnvConfig } from '../../../../src/utils/configuration/env-config';
import { HealthStatus } from '../../../../src/utils/health/health-check';
import { IterableProcessor } from '../../../../src/utils/iterable/iterable-processor';
import { IterableProcessorHealthCheck, messages } from '../../../../src/utils/iterable/iterable-processor-health-check';
import { addMillis } from '../../../../src/utils/time/date-utils';
import { TestClock } from '../../mocks/test-clock';

describe(IterableProcessorHealthCheck.name, () => {
    let target: IterableProcessorHealthCheck;
    let processor: IterableProcessor;
    let clock: TestClock;

    beforeEach(() => {
        processor = {} as IterableProcessor;
        const envConfig = { tezosProcessingMaxAgeMillis: { degraded: 100, unhealthy: 200 } } as EnvConfig;
        clock = new TestClock();
        target = new IterableProcessorHealthCheck('Foo', processor, envConfig, clock);

        processor.currentIterable = {} as AsyncIterableIterator<unknown>;
        processor.lastItemInfo = 'itm';
        processor.lastSuccessTime = addMillis(clock.nowDate, -50);
    });

    it('should have correct name', () => {
        expect(target.name).toBe('Foo');
    });

    it('should be healthy if everything fine', () => {
        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.everythingWorks,
            lastItem: 'itm',
            lastItemTime: processor.lastSuccessTime,
        });
        expect(health.status).toBe(HealthStatus.Healthy);
    });

    it('should report unhealthy if not running', () => {
        processor.currentIterable = undefined;

        const health = target.checkHealth();

        expect(health.data).toBe(messages.notRunning);
        expect(health.status).toBe(HealthStatus.Unhealthy);
    });

    it.each([
        [HealthStatus.Degraded, 150],
        [HealthStatus.Unhealthy, 250],
    ])('should report %s if no success for long time', (expectedStatus, ageMillis) => {
        processor.lastSuccessTime = addMillis(clock.nowDate, -1 * ageMillis);
        processor.lastError = 'oups';

        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.noSuccessForLongTime,
            lastItem: 'itm',
            lastError: 'oups',
            lastSuccessTime: processor.lastSuccessTime,
            successAgeMillis: ageMillis,
            maxAgeMillisConfig: {
                degraded: 100,
                unhealthy: 200,
            },
        });
        expect(health.status).toBe(expectedStatus);
    });

    it('should report degraded if error occurred recently', () => {
        processor.lastError = 'oups';

        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.errorRecently,
            failedItem: 'itm',
            error: 'oups',
            lastSuccessTime: processor.lastSuccessTime,
        });
        expect(health.status).toBe(HealthStatus.Degraded);
    });
});
