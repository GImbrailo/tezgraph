import { anything, instance, mock, verify, when } from 'ts-mockito';

import { ConvertItemProcessor, ItemConverter } from '../../../../src/utils/iterable/convert-item-processor';
import { ItemProcessor } from '../../../../src/utils/iterable/interfaces';

describe(ConvertItemProcessor.name, () => {
    let target: ItemProcessor<number>;
    let converter: ItemConverter<number, string>;
    let nextProcessor: ItemProcessor<string>;

    beforeEach(() => {
        converter = mock<ItemConverter<number, string>>();
        nextProcessor = mock<ItemProcessor<string>>();
        target = new ConvertItemProcessor(instance(converter), instance(nextProcessor));
    });

    it.each([
        ['sync value', 'abc'],
        ['async value', Promise.resolve('abc')],
    ])('should convert item to %s', async (_desc, convertedResult) => {
        when(converter.convert(123)).thenReturn(convertedResult);

        await target.processItem(123);

        verify(nextProcessor.processItem(anything())).once();
        verify(nextProcessor.processItem('abc')).once();
    });
});
