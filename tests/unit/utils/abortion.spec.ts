import { instance, mock, verify } from 'ts-mockito';

import { addAbortListener, isAbortError } from '../../../src/utils/abortion';
import { TestAbortError } from '../mocks';

describe('Abortion utils', () => {
    describe(isAbortError.name, () => {
        it.each([
            [true, 'AbortError', new TestAbortError()],
            [false, 'other Error', new Error()],
            [false, 'other type', 'foo'],
        ])('should return %s if %s', (expected: boolean, _desc: string, error: unknown) => {
            expect(isAbortError(error)).toBe(expected);
        });
    });

    describe(addAbortListener.name, () => {
        it('should register listener', () => {
            const signal = mock<AbortSignal>();
            const listener = () => { /* Nothing. */ };

            addAbortListener(instance(signal), listener);

            verify(signal.addEventListener('abort', listener)).once();
        });
    });
});
