import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { VersionProvider } from '../../../../src/utils/app-version/version-provider';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../../../../src/utils/health/health-check';
import { HealthProvider, HealthReport } from '../../../../src/utils/health/health-provider';
import { TestClock } from '../../mocks/test-clock';

describe(HealthProvider.name, () => {
    let target: HealthProvider;
    let check1: HealthCheck;
    let check2: HealthCheck;
    let versionProvider: VersionProvider;
    let clock: TestClock;

    let checkResult1: Writable<HealthCheckResult>;
    let checkResult2: Writable<HealthCheckResult>;

    beforeEach(() => {
        check1 = mock<HealthCheck>();
        check2 = mock<HealthCheck>();
        versionProvider = { version: { name: 'v1.2' } } as VersionProvider;
        clock = new TestClock();
        target = new HealthProvider([instance(check1), instance(check2)], versionProvider, clock);

        when(check1.name).thenReturn('b_first');
        when(check2.name).thenReturn('a_second');

        checkResult1 = { status: HealthStatus.Healthy, data: 'firstResult' } as HealthCheckResult;
        checkResult2 = { status: HealthStatus.Healthy, data: 'secondResult' } as HealthCheckResult;

        when(check1.checkHealth()).thenReturn(checkResult1);
        when(check2.checkHealth()).thenReturn(Promise.resolve(checkResult2));
    });

    it('should generate report correctly', async () => {
        const report = await target.generateHealthReport();

        expect(report).toEqual<HealthReport>({
            isHealthy: true,
            version: versionProvider.version,
            evaluatedOn: clock.nowDate,
            results: {
                b_first: checkResult1,
                a_second: checkResult2,
            },
        });
        expect(Object.keys(report.results)).toEqual(['a_second', 'b_first']); // Should be sorted.
    });

    it.each([
        [false, HealthStatus.Unhealthy, HealthStatus.Unhealthy],
        [false, HealthStatus.Unhealthy, HealthStatus.Healthy],
        [true, HealthStatus.Healthy, HealthStatus.Healthy],
        [true, HealthStatus.Healthy, HealthStatus.Degraded],
        [true, HealthStatus.Degraded, HealthStatus.Degraded],
    ])('should indicate unhealthy if at least one check is unhealthy e.g. %s and %s', async (
        expectedIsHealthy,
        status1,
        status2,
    ) => {
        checkResult1.status = status1;
        checkResult2.status = status2;

        const report = await target.generateHealthReport();

        expect(report.isHealthy).toBe(expectedIsHealthy);
    });

    it('should report failure if check throws', async () => {
        const error = new Error('Oups');
        when(check1.checkHealth()).thenThrow(error);

        const report = await target.generateHealthReport();

        expect(report.isHealthy).toBe(false);
        expect(report.results.b_first).toEqual<HealthCheckResult>({
            status: HealthStatus.Unhealthy,
            data: { error },
            evaluatedOn: clock.nowDate,
        });
    });
});
