import { instance, mock, when } from 'ts-mockito';

import { BallotNotification } from '../../../src/entity/subscriptions/ballot-notification';
import { BlockNotification } from '../../../src/entity/subscriptions/block-notification';
import { Filter } from '../../../src/entity/subscriptions/filters/filter';
import { OperationNotification } from '../../../src/entity/subscriptions/operation-notification';
import { ProposalsNotification } from '../../../src/entity/subscriptions/proposals-notification';
import { RevealNotification } from '../../../src/entity/subscriptions/reveal-notification';
import {
    createBlockOperationsResolverClass,
} from '../../../src/modules/subscriptions-subscriber/block-subscription/block-operations-resolver-factory';
import { asReadonly } from '../../../src/utils/conversion';

const TargetResolver = createBlockOperationsResolverClass(BallotNotification, null!);

describe(createBlockOperationsResolverClass.name, () => {
    let target: any;

    let ballot1: BallotNotification;
    let ballot2: BallotNotification;
    let block: BlockNotification;

    beforeEach(() => {
        target = new TargetResolver();

        ballot1 = new BallotNotification();
        ballot2 = new BallotNotification();
        block = {
            operations: asReadonly<OperationNotification>([
                ballot1,
                new RevealNotification(),
                ballot2,
                new ProposalsNotification(),
            ]),
        } as BlockNotification;
    });

    it('should expose operations of particular type', () => {
        const result = target.ballots(block);

        expect(result).toEqual([ballot1, ballot2]);
    });

    it('should apply given filter', () => {
        const filter = mock<Filter<BallotNotification>>();
        when(filter.passes(ballot1)).thenReturn(false);
        when(filter.passes(ballot2)).thenReturn(true);

        const result = target.ballots(block, instance(filter));

        expect(result).toEqual([ballot2]);
    });
});
