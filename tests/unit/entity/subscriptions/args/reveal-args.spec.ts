import { RevealSpecificFilter } from '../../../../../src/entity/subscriptions/args/reveal-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import {
    NullableOperationResultStatusFilter,
} from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { RevealNotification } from '../../../../../src/entity/subscriptions/reveal-notification';
import { create } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from './mocks';

describe(`${RevealSpecificFilter.name}.${nameof<RevealSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, statusResult] of getFilterTestCases(2)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, status=${statusResult}`, () => {
            const operation = {
                source: 'sss',
                metadata: { operation_result: { status: OperationResultStatus.backtracked } },
            } as RevealNotification;
            const target = create(RevealSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                status: mockFilter(NullableOperationResultStatusFilter, statusResult, operation.metadata!.operation_result.status),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
