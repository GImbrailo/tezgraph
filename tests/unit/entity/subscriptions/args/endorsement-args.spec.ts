import { EndorsementSpecificFilter } from '../../../../../src/entity/subscriptions/args/endorsement-args';
import { EndorsementNotification } from '../../../../../src/entity/subscriptions/endorsement-notification';
import { NullableAddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { create } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from './mocks';

describe(`${EndorsementSpecificFilter.name}.${nameof<EndorsementSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: { delegate: 'ddd' },
            } as EndorsementNotification;
            const target = create(EndorsementSpecificFilter, {
                delegate: mockFilter(NullableAddressFilter, delegateResult, operation.metadata!.delegate),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
