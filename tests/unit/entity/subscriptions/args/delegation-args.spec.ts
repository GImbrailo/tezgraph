import { DelegationSpecificFilter } from '../../../../../src/entity/subscriptions/args/delegation-args';
import { DelegationNotification } from '../../../../../src/entity/subscriptions/delegation-notification';
import { AddressFilter, NullableAddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import {
    NullableOperationResultStatusFilter,
} from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { create } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from './mocks';

describe(`${DelegationSpecificFilter.name}.${nameof<DelegationSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, delegateResult, statusResult] of getFilterTestCases(3)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, delegate=${delegateResult}, status=${statusResult}`, () => {
            const operation = {
                source: 'sss',
                delegate: 'ddd',
                metadata: { operation_result: { status: OperationResultStatus.backtracked } },
            } as DelegationNotification;
            const target = create(DelegationSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                delegate: mockFilter(NullableAddressFilter, delegateResult, operation.delegate),
                status: mockFilter(NullableOperationResultStatusFilter, statusResult, operation.metadata!.operation_result.status),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
