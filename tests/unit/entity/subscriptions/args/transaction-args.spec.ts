import { TransactionSpecificFilter } from '../../../../../src/entity/subscriptions/args/transaction-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { MutezFilter } from '../../../../../src/entity/subscriptions/filters/numeric-filters';
import {
    NullableOperationResultStatusFilter,
} from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { TransactionNotification } from '../../../../../src/entity/subscriptions/transaction-notification';
import { create } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from './mocks';

describe(`${TransactionSpecificFilter.name}.${nameof<TransactionSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, destinationResult, statusResult, amountResult] of getFilterTestCases(4)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult},`
            + ` destination=${destinationResult}, status=${statusResult}, amount=${amountResult}`, () => {
            const operation = {
                source: 'sss',
                destination: 'ddd',
                amount: BigInt(123),
                metadata: { operation_result: { status: OperationResultStatus.backtracked } },
            } as TransactionNotification;
            const target = create(TransactionSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                destination: mockFilter(AddressFilter, destinationResult, operation.destination),
                status: mockFilter(NullableOperationResultStatusFilter, statusResult, operation.metadata!.operation_result.status),
                amount: mockFilter(MutezFilter, amountResult, operation.amount),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
