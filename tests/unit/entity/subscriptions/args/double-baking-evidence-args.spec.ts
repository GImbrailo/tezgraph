import {
    DoubleBakingEvidenceSpecificFilter,
} from '../../../../../src/entity/subscriptions/args/double-baking-evidence-args';
import {
    DoubleBakingEvidenceNotification,
} from '../../../../../src/entity/subscriptions/double-baking-evidence-notification';
import { NullableAddressArrayFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { asReadonly, create } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from './mocks';

describe(`${DoubleBakingEvidenceSpecificFilter.name}.${nameof<DoubleBakingEvidenceSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: {
                    balance_updates: asReadonly([
                        { delegate: 'd1' },
                        { delegate: null },
                        {},
                        { delegate: 'd2' },
                    ]),
                },
            } as DoubleBakingEvidenceNotification;
            const target = create(DoubleBakingEvidenceSpecificFilter, {
                delegate: mockFilter(NullableAddressArrayFilter, delegateResult, ['d1', 'd2']),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
