import { createArrayFilterClass } from '../../../../../src/entity/subscriptions/filters/array-filter';
import { nameof } from '../../../../../src/utils/reflection';
import { getRunTestFunc } from './filter-test-helper';

class TargetFilter extends createArrayFilterClass<string>(String) {}

const runTest = getRunTestFunc<string[], TargetFilter>(TargetFilter);

describe(createArrayFilterClass.name, () => {
    runTest('filter is empty', {
        value: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3'],
        filter: {},
        expectedPassed: true,
    });

    describe(nameof<TargetFilter>('includes'), () => {
        runTest('matches', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { includes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd' },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expectedPassed: false,
        });
    });

    describe(nameof<TargetFilter>('notIncludes'), () => {
        runTest('matches', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { notIncludes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd' },
            expectedPassed: false,
        });
    });

    describe('combined multiple filters', () => {
        runTest('match', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: {
                includes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd',
                notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
            },
            expectedPassed: true,
        });
        runTest('do not match', {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: {
                includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
                notIncludes: 'tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6',
            },
            expectedPassed: false,
        });
    });
});
