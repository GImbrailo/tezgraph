import { createComparableFilterClass } from '../../../../../src/entity/subscriptions/filters/comparable-filter';
import { nameof } from '../../../../../src/utils/reflection';
import { getRunTestFunc } from './filter-test-helper';

class TargetFilter extends createComparableFilterClass<string>(String) {}

const runTest = getRunTestFunc<string, TargetFilter>(TargetFilter);

describe(createComparableFilterClass.name, () => {
    runTest('filter is empty', {
        value: 'aa',
        expectedPassed: true,
    });

    runComparisonTests('lessThan', {
        passesIfLess: true,
        passesIfEqual: false,
        passesIfGreater: false,
    });
    runComparisonTests('lessThanOrEqualTo', {
        passesIfLess: true,
        passesIfEqual: true,
        passesIfGreater: false,
    });
    runComparisonTests('greaterThanOrEqualTo', {
        passesIfLess: false,
        passesIfEqual: true,
        passesIfGreater: true,
    });
    runComparisonTests('greaterThan', {
        passesIfLess: false,
        passesIfEqual: false,
        passesIfGreater: true,
    });

    describe('combined with base filters', () => {
        runTest('match', {
            value: 'xx',
            filter: {
                in: ['xx', 'yy'],
                greaterThan: 'ff',
            },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: 'xx',
            filter: {
                in: ['aa', 'bb'],
                greaterThan: 'ff',
            },
            expectedPassed: false,
        });
    });

    function runComparisonTests(
        filterKey: keyof TargetFilter,
        expected: { passesIfLess: boolean; passesIfEqual: boolean; passesIfGreater: boolean },
    ) {
        describe(nameof<TargetFilter>('greaterThan'), () => {
            runTest('value less than filter', {
                value: 'aa',
                filter: { [filterKey]: 'ff' },
                expectedPassed: expected.passesIfLess,
            });
            runTest('value equal to filter', {
                value: 'ff',
                filter: { [filterKey]: 'ff' },
                expectedPassed: expected.passesIfEqual,
            });
            runTest('value greater than filter', {
                value: 'xx',
                filter: { [filterKey]: 'ff' },
                expectedPassed: expected.passesIfGreater,
            });
        });
    }
});
