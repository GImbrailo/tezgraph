import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { Filter } from '../../../../../src/entity/subscriptions/filters/filter';
import { FilterOptimizer } from '../../../../../src/entity/subscriptions/filters/filter-optimizer';
import { create } from '../../../../../src/utils/conversion';
import { Nullish } from '../../../../../src/utils/reflection';

class TargetFilter implements Filter<unknown> {
    source: Nullish<AddressFilter>;
    destination: Nullish<AddressFilter>;

    passes(_value: unknown): boolean {
        throw new Error('Method not implemented.');
    }
}

describe(FilterOptimizer.name, () => {
    const target = new FilterOptimizer();

    it('should recreate correctly', () => {
        const filter = create(TargetFilter, {
            source: create(AddressFilter, {
                equalTo: 'ss',
            }),
        });

        const optimized = target.optimize(filter);

        expect(optimized).toBeInstanceOf(TargetFilter);
        expect(optimized!.source).toBeInstanceOf(AddressFilter);
        expect(optimized!.source!.equalTo).toBe('ss');
        expect(optimized!.destination).toBeUndefined();
    });

    it('it should return undefined if empty', () => {
        const filter = new TargetFilter();
        filter.source = new AddressFilter();

        const optimized = target.optimize(filter);

        expect(optimized).toBeNull();
    });

    it('should tree-shake empty nested filters', () => {
        const filter = create(TargetFilter, {
            source: create(AddressFilter, {
                equalTo: 'ss',
            }),
            destination: new AddressFilter(),
        });

        const optimized = target.optimize(filter);

        expect(optimized).toBeInstanceOf(TargetFilter);
        expect(optimized!.source).toBeInstanceOf(AddressFilter);
        expect(optimized!.source!.equalTo).toBe('ss');
        expect(optimized!.destination).toBeNull();
    });
});
