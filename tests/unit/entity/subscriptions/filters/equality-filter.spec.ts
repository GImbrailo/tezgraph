import { createEqualityFilterClass } from '../../../../../src/entity/subscriptions/filters/equality-filter';
import { nameof } from '../../../../../src/utils/reflection';
import { getRunTestFunc } from './filter-test-helper';

class TargetFilter extends createEqualityFilterClass<string>(String) {}

const runTest = getRunTestFunc<string, TargetFilter>(TargetFilter);

describe(createEqualityFilterClass.name, () => {
    runTest('filter is empty', {
        value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
        expectedPassed: true,
    });

    describe(nameof<TargetFilter>('equalTo'), () => {
        runTest('matches', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { equalTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3' },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { equalTo: 'tz1K3fu4GXBXp9A8fchu1px3zzMDKtagDBh8' },
            expectedPassed: false,
        });
    });

    describe(nameof<TargetFilter>('notEqualTo'), () => {
        runTest('matches', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notEqualTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3' },
            expectedPassed: false,
        });
        runTest('does not match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notEqualTo: 'tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8' },
            expectedPassed: true,
        });
    });

    describe(nameof<TargetFilter>('in'), () => {
        runTest('matches', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { in: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { in: ['tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expectedPassed: false,
        });
    });

    describe(nameof<TargetFilter>('notIn'), () => {
        runTest('matches', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notIn: ['tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expectedPassed: true,
        });
        runTest('does not match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notIn: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expectedPassed: false,
        });
    });

    describe('combined filters', () => {
        runTest('match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: {
                notEqualTo: 'tz1F8fu4GXBXp9A8fchu1px3zzMDKtagDBh8',
                in: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
                notIn: ['tz1R8fu4GXBXp9A8fchu1px3zzMDKtagDBh1', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
            },
            expectedPassed: true,
        });
        runTest('do not match', {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: {
                equalTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
                notEqualTo: 'tz1F8fu4GXBXp9A8fchu1px3zzMDKtagDBh8',
                in: ['tz1R8fu4GXBXp9A8fchu1px3zzMDKtagDBh1', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
                notIn: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz2Y7fu4GXBXp9A8fchu1px3zzMDKtagDBg4'],
            },
            expectedPassed: false,
        });
    });
});
