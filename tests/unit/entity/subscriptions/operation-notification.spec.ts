import {
    getSignature,
    MempoolOperationGroup,
    OperationNotification,
} from '../../../../src/entity/subscriptions/operation-notification';

describe(getSignature.name, () => {
    it('should get info about mempool operation group', () => {
        const group: MempoolOperationGroup = [
            { info: { signature: 's1' } } as OperationNotification,
            { info: { signature: 's2' } } as OperationNotification,
        ];

        const signature = getSignature(group);

        expect(signature).toBe('s1');
    });

    it.each([
        ['no operation', []],
        ['no signature', [{} as OperationNotification]],
        ['white-space signature', [{ info: { signature: '  ' } } as OperationNotification]],
    ])('should throw if %s in the group', (_desc, group) => {
        expect(() => getSignature(group)).toThrow();
    });
});
