import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, spy, verify } from 'ts-mockito';

import { MempoolOperationGroup, OperationKind, OperationNotification } from '../../../../../src/entity/subscriptions/operation-notification';
import { MempoolExternalPubSubPublisher } from '../../../../../src/modules/tezos-monitor/mempool/mempool-external-pubsub-publisher';
import { ItemProcessor } from '../../../../../src/utils/iterable/interfaces';
import { LogLevel } from '../../../../../src/utils/logging';
import { MetricsContainer, OperationCountLabel, OperationSource } from '../../../../../src/utils/metrics/metrics-container';
import { ExternalPubSub, externalTriggers } from '../../../../../src/utils/pubsub/external-pub-sub';
import { expectToThrowAsync, TestLogger } from '../../../mocks';

describe(MempoolExternalPubSubPublisher.name, () => {
    let target: ItemProcessor<MempoolOperationGroup>;
    let externalPubSub: ExternalPubSub;
    let logger: TestLogger;
    let metrics: MetricsContainer;
    let operationsCountSpy: Counter<OperationCountLabel>;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        logger = new TestLogger();
        metrics = new MetricsContainer();
        target = new MempoolExternalPubSubPublisher(instance(externalPubSub), logger, metrics);
        operationsCountSpy = spy(metrics.tezosMonitorOperationCount);
    });

    it('should publish mempool operation group', async () => {
        const group: MempoolOperationGroup = [
            { kind: OperationKind.ballot, info: { signature: 's1' } } as OperationNotification,
            { kind: OperationKind.reveal, info: { signature: 's2' } } as OperationNotification,
        ];

        await target.processItem(group);

        verify(externalPubSub.publish(anything(), anything())).once();
        verify(externalPubSub.publish(externalTriggers.mempoolOperationGroups, group)).once();

        logger.loggedSingle().verify(LogLevel.Information, {
            signature: 's1',
            operationCount: 2,
        });

        verify(operationsCountSpy.inc(deepEqual({ source: OperationSource.Mempool }), 2)).once();
    });

    it('should throw if empty group', async () => {
        const group: MempoolOperationGroup = [];

        await expectToThrowAsync(async () => target.processItem(group));
    });
});
