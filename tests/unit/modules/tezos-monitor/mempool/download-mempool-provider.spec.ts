import { DownloadMempoolProvider } from '../../../../../src/modules/tezos-monitor/mempool/download-mempool-provider';
import { RpcMempoolOperationGroup } from '../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group';
import { EnvConfig } from '../../../../../src/utils/configuration/env-config';

describe(DownloadMempoolProvider.name, () => {
    let target: DownloadMempoolProvider;

    beforeEach(() => {
        const envConfig = { tezosNodeUrl: 'http://tezos:66/' } as EnvConfig;
        target = new DownloadMempoolProvider(envConfig, null!, null!);
    });

    it('should use correct url', () => {
        expect(target.url).toBe('http://tezos:66/chains/main/mempool/monitor_operations');
    });

    it('should cast downloaded data as array of mempool operation groups', () => {
        const testGroup1 = { signature: 's1' } as RpcMempoolOperationGroup;
        const testGroup2 = { signature: 's2' } as RpcMempoolOperationGroup;

        const groups = target.tryCastData([testGroup1, testGroup2]);

        expect(groups).toEqual([testGroup1, testGroup2]);
    });

    it.each([
        ['undefined', undefined],
        ['null', null],
        ['not array', 'omg'],
        ['empty array', []],
        ['item with missing signature', [{ signature: 's1' }, {}]],
        ['item with mismatched signature', [{ signature: 's1' }, { signature: 123 }]],
        ['item with white-space signature', [{ signature: 's1' }, { signature: '  ' }]],
    ])('should skip data if %s', (_desc, data) => {
        const groups = target.tryCastData(data);

        expect(groups).toBeUndefined();
    });
});
