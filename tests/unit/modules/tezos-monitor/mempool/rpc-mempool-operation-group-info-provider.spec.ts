import * as rpc from '@taquito/rpc';

import { RpcMempoolOperationGroup } from '../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group';
import {
    RpcMempoolOperationGroupInfoProvider,
} from '../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group-info-provider';

describe(RpcMempoolOperationGroupInfoProvider.name, () => {
    const target = new RpcMempoolOperationGroupInfoProvider();

    it('should get info about mempool operation group', () => {
        const group = {
            signature: 's1',
            contents: [{} as rpc.OperationContentsEndorsement, {} as rpc.OperationContentsRevelation],
        } as RpcMempoolOperationGroup;

        const info = target.getInfo(group);

        expect(info).toEqual({
            signature: 's1',
            operationCount: 2,
        });
    });
});
