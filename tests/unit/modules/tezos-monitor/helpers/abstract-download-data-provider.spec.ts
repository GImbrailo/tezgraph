import { instance, mock, spy, when } from 'ts-mockito';

import {
    AbstractDownloadDataProvider,
} from '../../../../../src/modules/tezos-monitor/helpers/abstract-download-data-provider';
import { TezosMonitorClient } from '../../../../../src/modules/tezos-monitor/helpers/tezos-monitor-client';
import { toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { LogLevel } from '../../../../../src/utils/logging';
import { TestLogger } from '../../../mocks';

class TargetProvider extends AbstractDownloadDataProvider<string> {
    tryCastData(_data: unknown): string[] | undefined {
        throw Error('Should be mocked.');
    }
}

describe(AbstractDownloadDataProvider.name, () => {
    let target: TargetProvider;
    let targetSpy: TargetProvider;
    let monitorClient: TezosMonitorClient;
    let logger: TestLogger;
    const url = 'http://tezos:66/test';

    beforeEach(() => {
        monitorClient = mock(TezosMonitorClient);
        logger = new TestLogger();
        target = new TargetProvider(instance(monitorClient), logger, url);
        targetSpy = spy(target);
    });

    it('should cast downloaded data correctly', async () => {
        when(monitorClient.iterateMonitor(url)).thenReturn(toAsyncIterable(['data1', 'data2']));
        when(targetSpy.tryCastData('data1')).thenReturn(['a', 'b']);
        when(targetSpy.tryCastData('data2')).thenReturn(['c']);

        const groups = await toArray(target.iterate());

        expect(groups).toEqual(['a', 'b', 'c']);
        logger.verifyNothingLogged();
    });

    it('should skip invalid data and log it', async () => {
        when(monitorClient.iterateMonitor(url)).thenReturn(toAsyncIterable(['data1', 'data2']));
        when(targetSpy.tryCastData('data1')).thenReturn(undefined);
        when(targetSpy.tryCastData('data2')).thenReturn(['a', 'b']);

        const groups = await toArray(target.iterate());

        expect(groups).toEqual(['a', 'b']);
        logger.loggedSingle().verify(LogLevel.Error, { data: 'data1', url });
    });
});
