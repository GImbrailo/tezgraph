import { AbortSignal } from 'abort-controller';
import { range } from 'lodash';
import { Response } from 'node-fetch';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import { TezosMonitorClient } from '../../../../../src/modules/tezos-monitor/helpers/tezos-monitor-client';
import { TezosMonitorEnvConfig } from '../../../../../src/modules/tezos-monitor/tezos-monitor-env-config';
import { take, toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { HttpClient } from '../../../../../src/utils/http-client';
import { LogLevel } from '../../../../../src/utils/logging';
import { expectToThrowAsync, TestLogger } from '../../../mocks';

describe(TezosMonitorClient.name, () => {
    let target: TezosMonitorClient;
    let httpClient: HttpClient;
    let abortSignal: AbortSignal;
    let logger: TestLogger;
    const url = 'http://tezos/monitor';

    const act = async (count: number) => toArray(take(target.iterateMonitor(url), count));

    beforeEach(() => {
        const envConfig = {
            tezosMonitorWarnChunkCount: 5,
            tezosMonitorFailChunkCount: 7,
        } as TezosMonitorEnvConfig;
        httpClient = mock(HttpClient);
        abortSignal = {} as AbortSignal;
        logger = new TestLogger();
        target = new TezosMonitorClient(envConfig, instance(httpClient), abortSignal, logger);
    });

    it('should continuously download data', async () => {
        setupFetch([
            '{ "id": "007", "name": "James Bond" }',
            { toString: () => '{ "id": "001", "name": "Johnny English" }' }, // Imitates Buffer.
        ]);

        const [foo1, foo2] = await act(2);

        expect(foo1).toEqual({ id: '007', name: 'James Bond' });
        expect(foo2).toEqual({ id: '001', name: 'Johnny English' });

        logger.verifyLoggedCount(2);
        logger.logged(0).verify(LogLevel.Information, { url }).verifyMessageIncludesAll('Connecting');
        logger.logged(1).verify(LogLevel.Information, { url }).verifyMessageIncludesAll('Connected');
    });

    it('should concat JSON split to multiple chunks', async () => {
        setupFetch(['{ "id":', ' "007", "na', 'me": "James Bond" }']);

        const [foo] = await act(1);

        expect(foo).toEqual({ id: '007', name: 'James Bond' });
        logger.verifyNotLogged(LogLevel.Error, LogLevel.Warning);
    });

    it('should warn if JSON split to more than configured number of chunks ', async () => {
        setupFetch(['{ ', '"id":', ' "007", "na', 'me":', ' "James ', 'Bond" }']);

        const [foo] = await act(1);

        expect(foo).toEqual({ id: '007', name: 'James Bond' });
        logger.loggedSingle(LogLevel.Warning).verifyData({ url, chunkCount: 6 });
        logger.verifyNotLogged(LogLevel.Error);
    });

    it('should fail if JSON split to more than configured number of chunks ', async () => {
        setupFetch(range(7).map(() => '{'));

        await runThrowTest({ expectedError: ['SyntaxError', '7 chunks', url] });
    });

    it('should fail if not OK response', async () => {
        whenFetch().thenResolve({ status: 512, statusText: 'WTF' } as Response);

        await runThrowTest({ expectedError: ['connect', '512', 'WTF', url] });
    });

    function whenFetch() {
        return when(httpClient.fetch(url, deepEqual({ signal: abortSignal })));
    }

    function setupFetch(jsons: (string | { toString(): string })[]) {
        whenFetch().thenResolve({
            status: 200,
            body: { [Symbol.asyncIterator]: () => toAsyncIterable(jsons) } as NodeJS.ReadableStream,
        } as Response);
    }

    async function runThrowTest(options: { expectedError: string[] }) {
        const error = await expectToThrowAsync(async () => act(1));

        expect(error.message).toIncludeMultiple(options.expectedError);
    }
});
