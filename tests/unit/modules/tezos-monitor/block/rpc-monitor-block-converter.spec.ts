import { BlockResponse } from '@taquito/rpc';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { RpcMonitorBlockConverter } from '../../../../../src/modules/tezos-monitor/block/rpc-monitor-block-converter';
import { RpcMonitorBlockHeader } from '../../../../../src/modules/tezos-monitor/block/rpc-monitor-block-header';
import { BlockConverter } from '../../../../../src/rpc/converters/block-converter';
import { TezosRpcClient } from '../../../../../src/rpc/tezos-rpc-client';
import { ItemConverter } from '../../../../../src/utils/iterable/convert-item-processor';
import { Clock } from '../../../../../src/utils/time/clock';
import { getRandomDate } from '../../../mocks/test-clock';

describe(RpcMonitorBlockConverter.name, () => {
    let target: ItemConverter<RpcMonitorBlockHeader, BlockNotification>;
    let rpcClient: TezosRpcClient;
    let blockConverter: BlockConverter;
    let clock: Clock;

    beforeEach(() => {
        rpcClient = mock<TezosRpcClient>();
        blockConverter = mock<BlockConverter>();
        clock = mock(Clock);
        target = new RpcMonitorBlockConverter(instance(rpcClient), instance(blockConverter), instance(clock));
    });

    it('should get block from RPC and convert it', async () => {
        const monitorBlock = { hash: 'haha' } as RpcMonitorBlockHeader;
        const rpcBlock = {} as BlockResponse;
        const finalBlock = {} as BlockNotification;
        const nowDate = getRandomDate();
        when(clock.getNowDate()).thenReturn(nowDate);
        when(rpcClient.getBlock('haha')).thenResolve(rpcBlock);
        when(blockConverter.convert(rpcBlock, nowDate)).thenReturn(finalBlock);

        const block = await target.convert(monitorBlock);

        expect(block).toBe(finalBlock);
        verify(clock.getNowDate()).calledBefore(rpcClient.getBlock(anything()));
    });
});
