import { Counter, Gauge } from 'prom-client';
import { anything, deepEqual, instance, mock, spy, verify } from 'ts-mockito';

import { BlockHeader, BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../../src/entity/subscriptions/operation-notification';
import { BlockExternalPubSubPublisher } from '../../../../../src/modules/tezos-monitor/block/block-external-pubsub-publisher';
import { asReadonly } from '../../../../../src/utils/conversion';
import { ItemProcessor } from '../../../../../src/utils/iterable/interfaces';
import { LogLevel } from '../../../../../src/utils/logging';
import { MetricsContainer, OperationCountLabel, OperationSource } from '../../../../../src/utils/metrics/metrics-container';
import { ExternalPubSub, externalTriggers } from '../../../../../src/utils/pubsub/external-pub-sub';
import { TestLogger } from '../../../mocks';

describe(BlockExternalPubSubPublisher.name, () => {
    let target: ItemProcessor<BlockNotification>;
    let externalPubSub: ExternalPubSub;
    let logger: TestLogger;
    let metrics: MetricsContainer;
    let blockLevelSpy: Gauge<string>;
    let operationsCountSpy: Counter<OperationCountLabel>;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        logger = new TestLogger();
        metrics = new MetricsContainer();
        target = new BlockExternalPubSubPublisher(instance(externalPubSub), logger, metrics);
        operationsCountSpy = spy(metrics.tezosMonitorOperationCount);
        blockLevelSpy = spy(metrics.tezosMonitorBlockLevel);
    });

    it('should publish block', async () => {
        const block = {
            hash: 'haha',
            operations: asReadonly([{} as OperationNotification, {} as OperationNotification]),
            header: { level: 123 } as BlockHeader,
        } as BlockNotification;

        await target.processItem(block);

        verify(externalPubSub.publish(anything(), anything())).once();
        verify(externalPubSub.publish(externalTriggers.blocks, block)).once();

        logger.loggedSingle().verify(LogLevel.Information, {
            hash: 'haha',
            operationCount: 2,
        });

        verify(operationsCountSpy.inc(deepEqual({ source: OperationSource.Block }), 2)).once();
        verify(blockLevelSpy.set(123)).once();
    });
});
