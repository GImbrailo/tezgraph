import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../../../src/entity/subscriptions/operation-notification';
import {
    UnassignBlockOnOperationsConverter,
} from '../../../../../src/modules/tezos-monitor/block/unassign-block-on-operations-converter';
import { asReadonly, deepFreeze } from '../../../../../src/utils/conversion';

describe(UnassignBlockOnOperationsConverter.name, () => {
    const target = new UnassignBlockOnOperationsConverter();

    it('should assign block to operations of cloned block', () => {
        const inputBlock = deepFreeze({
            hash: 'haha',
            operations: asReadonly([
                { kind: OperationKind.ballot, block: {} } as OperationNotification,
                { kind: OperationKind.reveal, block: {} } as OperationNotification,
            ]),
        }) as BlockNotification;

        const block = target.convert(inputBlock);

        expect(block.hash).toBe('haha');
        expect(block.operations[0]!.kind).toBe(OperationKind.ballot);
        expect(block.operations[0]!.block).toBeUndefined();
        expect(block.operations[1]!.kind).toBe(OperationKind.reveal);
        expect(block.operations[1]!.block).toBeUndefined();
    });
});
