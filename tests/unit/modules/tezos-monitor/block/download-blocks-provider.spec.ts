import { DownloadBlocksProvider } from '../../../../../src/modules/tezos-monitor/block/download-blocks-provider';
import { RpcMonitorBlockHeader } from '../../../../../src/modules/tezos-monitor/block/rpc-monitor-block-header';
import { EnvConfig } from '../../../../../src/utils/configuration/env-config';

describe(DownloadBlocksProvider.name, () => {
    let target: DownloadBlocksProvider;

    beforeEach(() => {
        const envConfig = { tezosNodeUrl: 'http://tezos:66/' } as EnvConfig;
        target = new DownloadBlocksProvider(envConfig, null!, null!);
    });

    it('should use correct url', () => {
        expect(target.url).toBe('http://tezos:66/monitor/heads/main');
    });

    it('should cast downloaded data as a block header', () => {
        const testHeader = { hash: 'haha' } as RpcMonitorBlockHeader;

        const headers = target.tryCastData(testHeader);

        expect(headers).toEqual([testHeader]);
    });

    it.each([
        ['undefined', undefined],
        ['null', null],
        ['not object', 'omg'],
        ['no hash', {}],
        ['mismatched hash', { hash: 123 }],
        ['white-space hash', { hash: '  ' }],
    ])('should skip data if %s', (_desc, data) => {
        const headers = target.tryCastData(data);

        expect(headers).toBeUndefined();
    });
});
