import { instance, mock, when } from 'ts-mockito';

import { Names, TezosMonitorEnvConfig } from '../../../../src/modules/tezos-monitor/tezos-monitor-env-config';
import { EnvConfigProvider, IntegerLimit } from '../../../../src/utils/configuration/env-config-provider';

describe(TezosMonitorEnvConfig.name, () => {
    let env: EnvConfigProvider;
    const act = () => new TezosMonitorEnvConfig(instance(env));

    beforeEach(() => {
        env = mock(EnvConfigProvider);

        when(env.getIntegerArray(Names.RpcMonitorReconnectDelaysMillis, IntegerLimit.PositiveOrZero)).thenReturn([4, 5, 6]);
        when(env.getInteger(Names.TezosMonitorWarnChunkCount, IntegerLimit.PositiveOrZero)).thenReturn(9);
        when(env.getInteger(Names.TezosMonitorFailChunkCount, IntegerLimit.PositiveOrZero)).thenReturn(10);
    });

    it(`should be created correctly`, () => {
        const config = act();

        expect(config).toEqual<TezosMonitorEnvConfig>({
            rpcMonitorReconnectDelaysMillis: [4, 5, 6],
            tezosMonitorWarnChunkCount: 9,
            tezosMonitorFailChunkCount: 10,
        });
    });
});
