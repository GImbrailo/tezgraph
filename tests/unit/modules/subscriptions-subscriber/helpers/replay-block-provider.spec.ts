import { BlockResponse } from '@taquito/rpc';
import { UserInputError } from 'apollo-server';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { HeadBlockProvider } from '../../../../../src/modules/subscriptions-subscriber/helpers/head-block-provider';
import { ReplayBlockProvider } from '../../../../../src/modules/subscriptions-subscriber/helpers/replay-block-provider';
import { RpcMonitorBlockHeader } from '../../../../../src/modules/tezos-monitor/block/rpc-monitor-block-header';
import { BlockConverter } from '../../../../../src/rpc/converters/block-converter';
import { TezosRpcClient } from '../../../../../src/rpc/tezos-rpc-client';
import { toArray } from '../../../../../src/utils/collections/async-iterable-utils';
import { EnvConfig } from '../../../../../src/utils/configuration/env-config';
import { LogLevel } from '../../../../../src/utils/logging';
import { expectToThrowAsync, TestLogger } from '../../../mocks';
import { TestClock } from '../../../mocks/test-clock';

describe(ReplayBlockProvider.name, () => {
    let target: ReplayBlockProvider;
    let rpcClient: TezosRpcClient;
    let blockConverter: BlockConverter;
    let headBlockProvider: HeadBlockProvider;
    let config: EnvConfig;
    let clock: TestClock;
    let logger: TestLogger;

    const act = async (fromLevel: number) => toArray(target.iterateFrom(fromLevel, 'req'));

    beforeEach(() => {
        rpcClient = mock(TezosRpcClient);
        blockConverter = mock(BlockConverter);
        config = { replayFromBlockLevelMaxFromHead: 7 } as EnvConfig;
        headBlockProvider = mock(HeadBlockProvider);
        clock = new TestClock();
        logger = new TestLogger();
        target = new ReplayBlockProvider(instance(rpcClient), instance(blockConverter), instance(headBlockProvider), config, clock, logger);

        when(headBlockProvider.getHeader()) // Head changed in the meantime
            .thenResolve({ level: 600 } as RpcMonitorBlockHeader)
            .thenResolve({ level: 601 } as RpcMonitorBlockHeader);
    });

    it(`should replay from specified block`, async () => {
        const expectedBlocks = [mockBlock(599), mockBlock(600), mockBlock(601)];

        const blocks = await act(599);

        expect(blocks).toEqual(expectedBlocks);
        logger.verifyNothingLogged();
    });

    it(`should proceed to next if a block processing failed`, async () => {
        const expectedBlocks = [mockBlock(599), mockBlock(601)];
        const error = new Error('Oups');
        when(rpcClient.getBlock(600)).thenReject(error);

        const blocks = await act(599);

        expect(blocks).toEqual(expectedBlocks);
        logger.loggedSingle(LogLevel.Error).verifyData({
            blockLevel: 600,
            error,
            requestId: 'req',
        });
    });

    it(`should fail if specified block level is in far past`, async () => {
        const error = await expectToThrowAsync(async () => act(550));

        expect(error).toBeInstanceOf(UserInputError);
        expect(error.message).toIncludeMultiple(['replayFromBlockLevel', '7 levels', '#550', '#600']);
        verify(headBlockProvider.getHeader()).once();
        verify(rpcClient.getBlock(anything())).never();
        verify(blockConverter.convert(anything(), clock.nowDate)).never();
    });

    function mockBlock(level: number) {
        const rpcBlock = { hash: `rpc-${level}` } as BlockResponse;
        const block = { hash: `final-${level}` } as BlockNotification;
        when(rpcClient.getBlock(level)).thenResolve(rpcBlock);
        when(blockConverter.convert(rpcBlock, clock.nowDate)).thenReturn(block);
        return block;
    }
});
