import { anything, instance, mock, verify } from 'ts-mockito';

import {
    MempoolOperationGroup,
    OperationKind,
    OperationNotification,
} from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    MempoolPubSubPublisher,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-pubsub-pubsliher';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { ItemProcessor } from '../../../../../../src/utils/iterable/interfaces';

describe(MempoolPubSubPublisher.name, () => {
    let target: ItemProcessor<MempoolOperationGroup>;
    let subscriberPubSub: SubscriberPubSub;

    beforeEach(() => {
        subscriberPubSub = mock(SubscriberPubSub);
        target = new MempoolPubSubPublisher(instance(subscriberPubSub));
    });

    it('should publish all operations in the group', async () => {
        const operation1 = { kind: OperationKind.ballot, info: { signature: 's1' } } as OperationNotification;
        const operation2 = { kind: OperationKind.reveal, info: { signature: 's2' } } as OperationNotification;

        await target.processItem([operation1, operation2]);

        verify(subscriberPubSub.publish(anything(), anything())).times(2);
        verify(subscriberPubSub.publish(subscriberTriggers.mempoolOperations[OperationKind.ballot], operation1)).once();
        verify(subscriberPubSub.publish(subscriberTriggers.mempoolOperations[OperationKind.reveal], operation2)).once();
    });
});
