import { deepEqual, instance, mock, when } from 'ts-mockito';

import { MempoolOperationGroup } from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    MempoolPubSubProvider,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-pubsub-provider';
import { IterableProvider } from '../../../../../../src/utils/iterable/interfaces';
import { ExternalPubSub, externalTriggers } from '../../../../../../src/utils/pubsub/external-pub-sub';

describe(MempoolPubSubProvider.name, () => {
    let target: IterableProvider<MempoolOperationGroup>;
    let externalPubSub: ExternalPubSub;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        target = new MempoolPubSubProvider(instance(externalPubSub));
    });

    it('should iterate mempool operation groups', () => {
        const pubSubIterable = {} as AsyncIterableIterator<MempoolOperationGroup>;
        when(externalPubSub.iterate(deepEqual([externalTriggers.mempoolOperationGroups]))).thenReturn(pubSubIterable);

        const iterable = target.iterate();

        expect(iterable).toBe(pubSubIterable);
    });
});
