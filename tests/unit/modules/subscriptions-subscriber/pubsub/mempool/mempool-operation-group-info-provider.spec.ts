import {
    MempoolOperationGroup,
    OperationNotification,
} from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    MempoolOperationGroupInfoProvider,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-operation-group-info-provider';

describe(MempoolOperationGroupInfoProvider.name, () => {
    const target = new MempoolOperationGroupInfoProvider();

    it('should get info about mempool operation group', () => {
        const group: MempoolOperationGroup = [
            { info: { signature: 's1' } } as OperationNotification,
            { info: { signature: 's2' } } as OperationNotification,
        ];

        const info = target.getInfo(group);

        expect(info).toEqual({
            signature: 's1',
            operationCount: 2,
        });
    });
});
