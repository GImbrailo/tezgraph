import { anything, instance, mock, verify } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../../../../src/entity/subscriptions/operation-notification';
import {
    BlockPubSubPublisher,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-publisher';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { asReadonly } from '../../../../../../src/utils/conversion';
import { ItemProcessor } from '../../../../../../src/utils/iterable/interfaces';

describe(BlockPubSubPublisher.name, () => {
    let target: ItemProcessor<BlockNotification>;
    let subscriberPubSub: SubscriberPubSub;

    beforeEach(() => {
        subscriberPubSub = mock(SubscriberPubSub);
        target = new BlockPubSubPublisher(instance(subscriberPubSub));
    });

    it('should publish block and all its operations', async () => {
        const operation1 = { kind: OperationKind.ballot, info: { signature: 's1' } } as OperationNotification;
        const operation2 = { kind: OperationKind.reveal, info: { signature: 's2' } } as OperationNotification;
        const block = { hash: 'hh', operations: asReadonly([operation1, operation2]) } as BlockNotification;

        await target.processItem(block);

        verify(subscriberPubSub.publish(anything(), anything())).times(3);
        verify(subscriberPubSub.publish(subscriberTriggers.blocks, block)).once();
        verify(subscriberPubSub.publish(subscriberTriggers.blockOperations[OperationKind.ballot], operation1)).once();
        verify(subscriberPubSub.publish(subscriberTriggers.blockOperations[OperationKind.reveal], operation2)).once();
    });
});
