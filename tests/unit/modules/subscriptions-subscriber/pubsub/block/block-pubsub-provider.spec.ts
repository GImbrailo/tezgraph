import { deepEqual, instance, mock, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions/block-notification';
import {
    BlockPubSubProvider,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-provider';
import { IterableProvider } from '../../../../../../src/utils/iterable/interfaces';
import { ExternalPubSub, externalTriggers } from '../../../../../../src/utils/pubsub/external-pub-sub';

describe(BlockPubSubProvider.name, () => {
    let target: IterableProvider<BlockNotification>;
    let externalPubSub: ExternalPubSub;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        target = new BlockPubSubProvider(instance(externalPubSub));
    });

    it('should iterate blocks', () => {
        const pubSubIterable = {} as AsyncIterableIterator<BlockNotification>;
        when(externalPubSub.iterate(deepEqual([externalTriggers.blocks]))).thenReturn(pubSubIterable);

        const iterable = target.iterate();

        expect(iterable).toBe(pubSubIterable);
    });
});
