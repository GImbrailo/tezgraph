import { range } from 'lodash';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { SubscriptionArgs } from '../../../../../src/entity/subscriptions/args/subscription-args';
import { BallotNotification } from '../../../../../src/entity/subscriptions/ballot-notification';
import { BlockHeader, BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import {
    BlockSubscription,
    BlockSubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/block-subscription/block-subscription';
import {
    ReplayBlockSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/block-subscription/replay-block-subscription';
import { ReplayBlockProvider } from '../../../../../src/modules/subscriptions-subscriber/helpers/replay-block-provider';
import {
    SubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/operation-subscription';
import { toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { create } from '../../../../../src/utils/conversion';
import { LogLevel } from '../../../../../src/utils/logging';
import { TestLogger } from '../../../mocks';

describe(ReplayBlockSubscription.name, () => {
    let target: BlockSubscription;
    let innerProvider: BlockSubscription;
    let replayBlockProvider: ReplayBlockProvider;
    let logger: TestLogger;

    let options: Writable<BlockSubscriptionOptions>;
    let testBlocks: BlockNotification[];

    const act = async () => toArray(target.subscribe(options));

    beforeEach(() => {
        innerProvider = mock<BlockSubscription>();
        replayBlockProvider = mock(ReplayBlockProvider);
        logger = new TestLogger();
        target = new ReplayBlockSubscription(
            instance(innerProvider),
            instance(replayBlockProvider),
            logger,
        );

        options = {
            args: {},
            operationType: BallotNotification,
            context: { requestId: 'req' },
        } as SubscriptionOptions<BallotNotification>;
        testBlocks = range(2).map(i => create(BlockNotification, { hash: `hh-${i}`, header: { level: 100 + i } as BlockHeader }));
    });

    it(`should iterate fresh blocks if no replay block specified`, async () => {
        when(innerProvider.subscribe(options)).thenReturn(toAsyncIterable(testBlocks));

        const blocks = await act();

        expect(blocks).toEqual(testBlocks);
        verify(replayBlockProvider.iterateFrom(anything(), anything())).never();
        logger.verifyNothingLogged();
    });

    it(`should replay from specified block`, async () => {
        options.args = { replayFromBlockLevel: 66 } as SubscriptionArgs;
        when(replayBlockProvider.iterateFrom(66, 'req')).thenReturn(toAsyncIterable(testBlocks.slice(0, 2)));
        when(innerProvider.subscribe(options)).thenReturn(toAsyncIterable(testBlocks.slice(2)));

        const blocks = await act();

        expect(blocks).toEqual(testBlocks);

        const requestId = options.context.requestId;
        logger.verifyLoggedCount(4);
        logger.logged(0).verify(LogLevel.Information, { blockLevel: 66, requestId });
        logger.logged(1).verify(LogLevel.Debug, { block: testBlocks[0], blockLevel: 100, requestId });
        logger.logged(2).verify(LogLevel.Debug, { block: testBlocks[1], blockLevel: 101, requestId });
        logger.logged(3).verify(LogLevel.Debug, { requestId });
    });
});
