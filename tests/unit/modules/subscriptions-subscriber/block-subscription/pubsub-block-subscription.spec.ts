import { deepEqual, instance, mock, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import {
    BlockSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/block-subscription/block-subscription';
import {
    PubSubBlockSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/block-subscription/pubsub-block-subscription';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';

describe(PubSubBlockSubscription.name, () => {
    let target: BlockSubscription;
    let pubSub: SubscriberPubSub;

    beforeEach(() => {
        pubSub = mock(SubscriberPubSub);
        target = new PubSubBlockSubscription(instance(pubSub));
    });

    it('should iterate blocks', () => {
        const pubSubIterable = {} as AsyncIterableIterator<BlockNotification>;
        when(pubSub.iterate(deepEqual([subscriberTriggers.blocks]))).thenReturn(pubSubIterable);

        const iterable = target.subscribe(null!);

        expect(iterable).toBe(pubSubIterable);
    });
});
