import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { OperationArgs } from '../../../../../src/entity/subscriptions/args/operation-args';
import { EndorsementNotification } from '../../../../../src/entity/subscriptions/endorsement-notification';
import { OperationKind, OperationNotification } from '../../../../../src/entity/subscriptions/operation-notification';
import {
    OperationSubscription,
    SubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/operation-subscription';
import {
    PubSubOperationSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/pubsub-operation-subscription';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { PubSubTrigger } from '../../../../../src/utils/pubsub/typed-pub-sub';

describe(PubSubOperationSubscription.name, () => {
    let target: OperationSubscription;
    let pubSub: SubscriberPubSub;

    let options: Writable<SubscriptionOptions<EndorsementNotification>>;
    let testIterable: AsyncIterableIterator<EndorsementNotification>;

    beforeEach(() => {
        pubSub = mock(SubscriberPubSub);
        target = new PubSubOperationSubscription(instance(pubSub));

        options = {
            operationType: EndorsementNotification,
            args: { includeMempool: false },
        } as SubscriptionOptions<EndorsementNotification>;
        testIterable = {} as AsyncIterableIterator<EndorsementNotification>;
        when(pubSub.iterate(anything())).thenReturn(testIterable);
    });

    it('should iterate block operations of specified kind', () => {
        runTest([subscriberTriggers.blockOperations[OperationKind.endorsement]]);
    });

    it('should include mempool if requested', () => {
        options.args = { includeMempool: true } as OperationArgs<EndorsementNotification>;

        runTest([
            subscriberTriggers.blockOperations[OperationKind.endorsement],
            subscriberTriggers.mempoolOperations[OperationKind.endorsement],
        ]);
    });

    function runTest(expectedTriggers: PubSubTrigger<OperationNotification>[]) {
        const iterable = target.subscribe(options);

        expect(iterable).toBe(testIterable);
        verify(pubSub.iterate(deepEqual(expectedTriggers))).once();
    }
});
