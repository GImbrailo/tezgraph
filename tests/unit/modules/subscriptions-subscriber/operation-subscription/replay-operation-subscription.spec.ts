import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { OperationArgs } from '../../../../../src/entity/subscriptions/args/operation-args';
import { BallotNotification } from '../../../../../src/entity/subscriptions/ballot-notification';
import { BlockNotification } from '../../../../../src/entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../../src/entity/subscriptions/operation-notification';
import { RevealNotification } from '../../../../../src/entity/subscriptions/reveal-notification';
import { TransactionNotification } from '../../../../../src/entity/subscriptions/transaction-notification';
import { ReplayBlockProvider } from '../../../../../src/modules/subscriptions-subscriber/helpers/replay-block-provider';
import {
    OperationSubscription,
    SubscriptionOptions,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/operation-subscription';
import {
    ReplayOperationSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/operation-subscription/replay-operation-subscription';
import { toArray, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { asReadonly, create } from '../../../../../src/utils/conversion';
import { LogLevel } from '../../../../../src/utils/logging';
import { generateRange, TestLogger } from '../../../mocks';

describe(ReplayOperationSubscription.name, () => {
    let target: OperationSubscription;
    let innerProvider: OperationSubscription;
    let replayBlockProvider: ReplayBlockProvider;
    let logger: TestLogger;

    let options: Writable<SubscriptionOptions<BallotNotification>>;
    let testBallots: [BallotNotification, BallotNotification, BallotNotification, BallotNotification, BallotNotification];

    const act = async () => toArray(target.subscribe(options));

    beforeEach(() => {
        innerProvider = mock<OperationSubscription>();
        replayBlockProvider = mock(ReplayBlockProvider);
        logger = new TestLogger();
        target = new ReplayOperationSubscription(
            instance(innerProvider),
            instance(replayBlockProvider),
            logger,
        );

        options = {
            args: {},
            operationType: BallotNotification,
            context: { requestId: 'req' },
        } as SubscriptionOptions<BallotNotification>;
        testBallots = generateRange(5, i => create(BallotNotification, { source: `add-${i}` }));
    });

    it(`should iterate fresh operations if no replay block specified`, async () => {
        when(innerProvider.subscribe(options)).thenReturn(toAsyncIterable(testBallots));

        const operations = await act();

        expect(operations).toEqual(testBallots);
        verify(replayBlockProvider.iterateFrom(anything(), anything())).never();
        logger.verifyNothingLogged();
    });

    it(`should replay from specified block`, async () => {
        options.args = { replayFromBlockLevel: 66 } as OperationArgs<BallotNotification>;
        when(replayBlockProvider.iterateFrom(66, 'req')).thenReturn(toAsyncIterable([
            {
                header: { level: 77 },
                operations: asReadonly<OperationNotification>([testBallots[0], testBallots[1], new RevealNotification()]),
            } as BlockNotification,
            {
                header: { level: 78 },
                operations: asReadonly<OperationNotification>([new TransactionNotification(), testBallots[2]]),
            } as BlockNotification,
        ]));
        when(innerProvider.subscribe(options)).thenReturn(toAsyncIterable([testBallots[3], testBallots[4]]));

        const operations = await act();

        expect(operations).toEqual(testBallots);

        const operationKind = 'ballot';
        const requestId = options.context.requestId;
        logger.verifyLoggedCount(4);
        logger.logged(0).verify(LogLevel.Information, { blockLevel: 66, operationKind, requestId });
        logger.logged(1).verify(LogLevel.Debug, { operations: [testBallots[0], testBallots[1]], blockLevel: 77, operationKind, requestId });
        logger.logged(2).verify(LogLevel.Debug, { operations: [testBallots[2]], blockLevel: 78, operationKind, requestId });
        logger.logged(3).verify(LogLevel.Debug, { operationKind, requestId });
    });
});
