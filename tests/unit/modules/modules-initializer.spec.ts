import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { DependencyContainer } from 'tsyringe';

import { CompositeModule, CompositeModuleName, Module, ModuleDependency, ModuleName } from '../../../src/modules/module';
import { ModulesInitializer } from '../../../src/modules/modules-initializer';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { asReadonly } from '../../../src/utils/conversion';
import { expectToThrow } from '../mocks';

describe(ModulesInitializer.name, () => {
    let envConfig: Writable<EnvConfig>;
    let modules: Module[];
    let compositeModules: CompositeModule[];
    let container: DependencyContainer;

    const getTarget = () => new ModulesInitializer(envConfig, modules, compositeModules, container);

    let fooModule: Module;
    let omgModule: Module;
    let lolModule: Module;
    let disabledModule: Module;

    let fooDependency1: ModuleDependency;
    let fooDependency2: ModuleDependency;
    let lolDependency: ModuleDependency;
    let disabledDependency: ModuleDependency;

    beforeEach(() => {
        envConfig = { modules: asReadonly(['Foo', 'Memes']) } as EnvConfig;
        container = {} as DependencyContainer;

        fooDependency1 = mock<ModuleDependency>();
        fooDependency2 = mock<ModuleDependency>();
        lolDependency = mock<ModuleDependency>();
        disabledDependency = mock<ModuleDependency>();

        fooModule = mockModule('Foo', [fooDependency1, fooDependency2]);
        omgModule = mockModule('Omg');
        lolModule = mockModule('Lol', [lolDependency]);
        disabledModule = mockModule('Unexpected', [disabledDependency]);

        modules = [instance(fooModule), instance(omgModule), instance(lolModule), instance(disabledModule)];
        compositeModules = [
            new CompositeModule('Viruses' as CompositeModuleName, asModuleNames(['Ebola', 'Covid'])),
            new CompositeModule('Memes' as CompositeModuleName, asModuleNames(['Omg', 'Lol'])),
        ];
    });

    it('should initialize modules according to config', () => {
        getTarget().initializeConfiguredModules();

        const enabledModules = deepEqual(asModuleNames(['Foo', 'Omg', 'Lol']));
        verify(fooDependency1.validate('Foo' as ModuleName, enabledModules));
        verify(fooDependency1.validate('Foo' as ModuleName, enabledModules));
        verify(lolDependency.validate('Lol' as ModuleName, enabledModules));

        verify(fooModule.initialize(container));
        verify(omgModule.initialize(container));
        verify(lolModule.initialize(container));

        verify(disabledModule.initialize(anything())).never();
        verify(disabledDependency.validate(anything(), anything())).never();
    });

    it('should throw if unknown module', () => {
        envConfig.modules = ['Foo', 'Wtf'];
        const target = getTarget();

        const error = expectToThrow(() => target.initializeConfiguredModules());

        expect(error.message).toIncludeMultiple([`'MODULES'`, `'Wtf'`, 'Foo', 'Omg', 'Lol', 'Memes', 'Viruses']);
    });

    it('should throw if multiple modules with the same name', () => {
        compositeModules.push(new CompositeModule('Foo' as CompositeModuleName, asModuleNames(['Bar'])));

        expectToThrow(getTarget);
    });

    function mockModule(name: string, dependencyMocks?: ModuleDependency[]) {
        const module = mock<Module>();
        when(module.name).thenReturn(name as ModuleName);
        when(module.dependencies).thenReturn((dependencyMocks ?? []).map(instance));
        return module;
    }

    function asModuleNames(names: string[]) {
        return names.map(n => n as ModuleName);
    }
});
