import { BlockHeaderResponse, BlockResponse, RpcClient } from '@taquito/rpc';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { TezosRpcClient } from '../../../src/rpc/tezos-rpc-client';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { asReadonly } from '../../../src/utils/conversion';
import { LogLevel } from '../../../src/utils/logging/log-entry';
import { MetricsContainer } from '../../../src/utils/metrics/metrics-container';
import { nameof } from '../../../src/utils/reflection';
import { RetryHelper } from '../../../src/utils/retry-helper';
import { TestLogger } from '../mocks';
import { TestClock } from '../mocks/test-clock';

describe(TezosRpcClient.name, () => {
    let target: TezosRpcClient;
    let rpcClient: RpcClient;
    let retryHelper: RetryHelper;
    let envConfig: EnvConfig;
    let logger: TestLogger;
    let metrics: MetricsContainer;
    let clock: TestClock;

    beforeEach(() => {
        rpcClient = mock(RpcClient);
        retryHelper = mock(RetryHelper);
        envConfig = { rpcRetryDelaysMillis: asReadonly([100, 200]) } as EnvConfig;
        logger = new TestLogger();
        metrics = new MetricsContainer();
        clock = new TestClock();
        target = new TezosRpcClient(instance(rpcClient), instance(retryHelper), envConfig, logger, metrics, clock);

        when(retryHelper.execute(anything(), envConfig.rpcRetryDelaysMillis, anything())).thenCall(async (func: () => Promise<any>) => func());
    });

    it(`${nameof<TezosRpcClient>('getBlock')}() should call RpcClient with retry`, async () => {
        const testBlock = { hash: 'response-hh' } as BlockResponse;
        when(rpcClient.getBlock(deepEqual({ block: 'input-hh' }))).thenResolve(testBlock);

        const block = await target.getBlock('input-hh');

        expect(block).toBe(testBlock);
        verify(retryHelper.execute(anything(), envConfig.rpcRetryDelaysMillis, anything())).once();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify(LogLevel.Debug, { hashOrLevel: 'input-hh' }).verifyMessageIncludesAll(target.getBlock.name);
        logger.logged(1).verify(LogLevel.Debug, { block, hash: 'response-hh' });
    });

    it(`${nameof<TezosRpcClient>('getHeadBlockHeader')}() should call RpcClient with retry`, async () => {
        const testHeader = { hash: 'hh' } as BlockHeaderResponse;
        when(rpcClient.getBlockHeader()).thenResolve(testHeader);

        const blockHeader = await target.getHeadBlockHeader();

        expect(blockHeader).toBe(testHeader);
        verify(retryHelper.execute(anything(), envConfig.rpcRetryDelaysMillis, anything())).once();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify(LogLevel.Debug).verifyMessageIncludesAll(target.getHeadBlockHeader.name);
        logger.logged(1).verify(LogLevel.Debug, { blockHeader, hash: 'hh' });
    });
});
