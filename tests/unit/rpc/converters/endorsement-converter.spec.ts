import * as rpc from '@taquito/rpc';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { EndorsementNotification } from '../../../../src/entity/subscriptions/endorsement-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { BalanceUpdateConverter } from '../../../../src/rpc/converters/common/balance-update-converter';
import { EndorsementConverter, RpcEndorsement } from '../../../../src/rpc/converters/endorsement-converter';
import { mockBalanceUpdate, mockBaseProperties } from './mocks';

describe(EndorsementConverter.name, () => {
    let target: EndorsementConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    const act = (e: RpcEndorsement) => target.convert(e, mockBaseProperties());

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new EndorsementConverter(instance(balanceUpdateConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: rpc.OperationContentsAndResultEndorsement = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
            metadata: {
                balance_updates: [{ contract: 'balance-input' } as rpc.OperationMetadataBalanceUpdates],
                delegate: 'ddd',
                slots: [222],
            },
        };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates)).thenReturn(convertedBalanceUpdates);

        const operation = act(rpcOperation);

        expect(operation).toEqual<EndorsementNotification>({
            kind: OperationKind.endorsement,
            level: 111,
            metadata: {
                balance_updates: convertedBalanceUpdates,
                delegate: 'ddd',
                slots: [222],
            },
            ...mockBaseProperties(),
        });
    });

    it('should handle no metadata correctly', () => {
        const rpcOperation: rpc.OperationContentsEndorsement = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
        };

        const operation = act(rpcOperation);

        expect(operation.metadata).toBeUndefined();
        verify(balanceUpdateConverter.convert(anything())).never();
    });
});
