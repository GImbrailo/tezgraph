import * as rpc from '@taquito/rpc';

import { BigMapDiff } from '../../../../../src/entity/subscriptions/big-map-diff';
import { BigMapDiffConverter } from '../../../../../src/rpc/converters/common/big-map-diff-converter';

describe(BigMapDiffConverter.name, () => {
    let rpcDiffs: rpc.ContractBigMapDiffItem[];

    const act = () => new BigMapDiffConverter().convertNullish(rpcDiffs);

    it(`should convert values correctly`, () => {
        rpcDiffs = [
            {
                key_hash: 'hhh',
                key: { prim: 'michelson key' },
                value: { prim: 'michelson value' },
            },
        ];

        const diffs = act();

        const expected: BigMapDiff = {
            key_hash: 'hhh',
            key: { prim: 'michelson key' },
            value: { prim: 'michelson value' },
        };
        expect(diffs).toEqual([expected]);
    });

    [undefined, null].forEach(input => {
        it(`should return undefined if ${input} input`, () => {
            rpcDiffs = input!;

            const diffs = act();

            expect(diffs).toBeUndefined();
        });
    });
});
