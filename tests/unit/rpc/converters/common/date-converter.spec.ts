import { convertDate } from '../../../../../src/rpc/converters/common/date-converter';

describe(convertDate.name, () => {
    it(`should return same value if Date`, () => {
        const date = new Date();

        const result = convertDate(date);

        expect(result).toBe(date);
    });

    it(`should parse if string`, () => {
        const result = convertDate('2020-09-27T23:57:46.123Z');

        expect(result).toEqual(new Date('2020-09-27T23:57:46.123Z'));
    });
});
