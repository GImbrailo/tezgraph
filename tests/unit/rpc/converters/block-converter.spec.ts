import * as rpc from '@taquito/rpc';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../../src/entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../src/entity/subscriptions/operation-notification';
import { BlockConverter } from '../../../../src/rpc/converters/block-converter';
import { OperationGroupConverter } from '../../../../src/rpc/converters/operation-group-converter';
import { LogLevel } from '../../../../src/utils/logging';
import { TestLogger } from '../../mocks';
import { getRandomDate } from '../../mocks/test-clock';

describe(BlockConverter.name, () => {
    let target: BlockConverter;
    let operationGroupConverter: OperationGroupConverter;
    let logger: TestLogger;

    beforeEach(() => {
        operationGroupConverter = mock(OperationGroupConverter);
        logger = new TestLogger();
        target = new BlockConverter(instance(operationGroupConverter), logger);
    });

    it(`should convert values correctly`, () => {
        const header = { level: 11, proto: 22, predecessor: 'rr', timestamp: new Date('2020-09-24T21:54:33.000Z'), signature: 'ss' };
        const blockProps = { hash: 'hh', protocol: 'pp', chain_id: 'cc', header };
        const receivedFromTezosOn = getRandomDate();

        const [rpcOpGroup1, op1, op2] = setupGroup('s1');
        const [rpcOpGroup2, op3, op4] = setupGroup('s1');
        const [rpcOpGroup3, op5, op6] = setupGroup('s1');
        const rpcBlock = {
            ...blockProps,
            operations: [
                [rpcOpGroup1, rpcOpGroup2],
                [rpcOpGroup3],
            ],
        } as rpc.BlockResponse;

        const block = target.convert(rpcBlock, receivedFromTezosOn);

        expect(block).toEqual<BlockNotification>({
            ...blockProps,
            operations: [op1, op2, op3, op4, op5, op6],
            received_from_tezos_on: receivedFromTezosOn,
        });
        verifyConversion(rpcOpGroup1, block);
        verifyConversion(rpcOpGroup2, block);
        verifyConversion(rpcOpGroup3, block);
        logger.verifyLoggedCount(2);
        logger.logged(0).verify(LogLevel.Debug, { hash: 'hh' }).verifyMessageIncludesAll('Converting');
        logger.logged(1).verify(LogLevel.Debug, { hash: 'hh' }).verifyMessageIncludesAll('Converted');
    });

    function setupGroup(signature: string): [rpc.OperationEntry, OperationNotification, OperationNotification] {
        const rpcOpGroup = { signature } as rpc.OperationEntry;
        const op1 = {} as OperationNotification;
        const op2 = {} as OperationNotification;

        when(operationGroupConverter.convert(rpcOpGroup, anything())).thenReturn([op1, op2]);
        return [rpcOpGroup, op1, op2];
    }

    function verifyConversion(rpcGroup: rpc.OperationEntry, block: BlockNotification) {
        verify(operationGroupConverter.convert(rpcGroup, block)).once();
    }
});
