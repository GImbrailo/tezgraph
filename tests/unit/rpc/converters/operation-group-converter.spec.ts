import * as rpc from '@taquito/rpc';
import { anything, capture, instance, mock, when } from 'ts-mockito';

import { BlockNotification } from '../../../../src/entity/subscriptions/block-notification';
import {
    OperationKind,
    OperationNotification,
    OperationNotificationInfo,
    OperationOrigin,
} from '../../../../src/entity/subscriptions/operation-notification';
import { BaseOperationProperties, OperationConverter } from '../../../../src/rpc/converters/operation-converter';
import { OperationGroupConverter } from '../../../../src/rpc/converters/operation-group-converter';
import { LogLevel } from '../../../../src/utils/logging';
import { TestLogger } from '../../mocks';
import { getRandomDate, TestClock } from '../../mocks/test-clock';

describe(OperationGroupConverter.name, () => {
    let target: OperationGroupConverter;
    let operationConverter: OperationConverter;
    let logger: TestLogger;
    let clock: TestClock;

    beforeEach(() => {
        operationConverter = mock(OperationConverter);
        logger = new TestLogger();
        clock = new TestClock();
        target = new OperationGroupConverter(instance(operationConverter), logger, clock);
    });

    const testCases = [
        {
            blockDesc: 'valid block',
            block: { hash: 'hh', received_from_tezos_on: getRandomDate() } as BlockNotification,
            expectedOrigin: OperationOrigin.BLOCK,
        },
        {
            condition: 'NO block',
            expectedOrigin: OperationOrigin.MEMPOOL,
        },
    ];

    testCases.forEach(({ blockDesc, block, expectedOrigin }) => {
        it(`should convert values correctly if there is ${blockDesc}`, () => {
            const rpcInfo = { protocol: 'pp', chain_id: 'cc', hash: 'hh', branch: 'bb', signature: 'ss' };
            const [rpcOp1, op1] = setupOperation(rpc.OpKind.BALLOT);
            const [rpcOp2, op2] = setupOperation(rpc.OpKind.REVEAL);
            const rpcOpGroup: rpc.OperationEntry = {
                ...rpcInfo,
                contents: [rpcOp1, rpcOp2],
            };

            const operations = target.convert(rpcOpGroup, block!);

            expect(operations).toEqual([op1, op2]);
            const expectedRpcInfo = { ...rpcInfo, received_from_tezos_on: block?.received_from_tezos_on ?? clock.nowDate };
            verifyConversion(0, expectedRpcInfo, rpcOp1);
            verifyConversion(1, expectedRpcInfo, rpcOp2);
            logger.verifyLoggedCount(2);
            logger.logged(0).verify(LogLevel.Debug, { kind: OperationKind.ballot, signature: 'ss' });
            logger.logged(1).verify(LogLevel.Debug, { kind: OperationKind.reveal, signature: 'ss' });
        });

        function setupOperation(kind: rpc.OpKind): [rpc.OperationContentsAndResult, OperationNotification] {
            const rpcOp = { kind } as rpc.OperationContentsAndResult;
            const op = {} as OperationNotification;

            when(operationConverter.convert(rpcOp, anything())).thenReturn(op);
            return [rpcOp, op];
        }

        function verifyConversion(index: number, expectedRpcInfo: OperationNotificationInfo, expectedRpcOp: rpc.OperationContentsAndResult) {
            // eslint-disable-next-line @typescript-eslint/unbound-method
            const [rpcOp, baseProps] = capture(operationConverter.convert).byCallIndex(index);

            expect(rpcOp).toBe(expectedRpcOp);
            expect(baseProps).toEqual<BaseOperationProperties>({
                info: expectedRpcInfo,
                block,
                origin: expectedOrigin,
            });
        }
    });
});
