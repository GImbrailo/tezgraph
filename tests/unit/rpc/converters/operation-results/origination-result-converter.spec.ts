import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OriginationResult } from '../../../../../src/entity/subscriptions/origination-notification';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common/balance-update-converter';
import {
    OriginationResultConverter,
} from '../../../../../src/rpc/converters/operation-results/origination-result-converter';
import { mockBalanceUpdate } from '../mocks';
import { mockedBaseOpResultProps } from './base-operation-result.mock';

describe(OriginationResultConverter.name, () => {
    let target: OriginationResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    const act = (r: rpc.OperationResultOrigination) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new OriginationResultConverter(instance(balanceUpdateConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultOrigination = {
            ...mockedBaseOpResultProps.getRpc(),
            balance_updates: [{ contract: 'balance-input' } as rpc.OperationBalanceUpdatesItem],
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
        };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convertNullish(rpcResult.balance_updates)).thenReturn(convertedBalanceUpdates);

        const result = act(rpcResult);

        expect(result).toEqual<OriginationResult>({
            ...mockedBaseOpResultProps.getExpected(),
            typeName: 'OriginationResult',
            balance_updates: convertedBalanceUpdates,
            originated_contracts: ['c1', 'c2'],
            storage_size: BigInt(222),
            paid_storage_size_diff: BigInt(333),
        });
    });
});
