import * as rpc from '@taquito/rpc';

import { RevealResult } from '../../../../../src/entity/subscriptions/reveal-notification';
import { RevealResultConverter } from '../../../../../src/rpc/converters/operation-results/reveal-result-converter';
import { mockedBaseOpResultProps } from './base-operation-result.mock';

describe(RevealResultConverter.name, () => {
    const target = new RevealResultConverter();

    const act = (r: rpc.OperationResultReveal) => target.convert(r);

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultReveal = mockedBaseOpResultProps.getRpc();

        const result = act(rpcResult);

        expect(result).toEqual<RevealResult>({
            ...mockedBaseOpResultProps.getExpected(),
            typeName: 'RevealResult',
        });
    });
});
