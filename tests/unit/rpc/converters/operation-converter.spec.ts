import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { ActivateAccountConverter } from '../../../../src/rpc/converters/activate-account-converter';
import { BallotConverter } from '../../../../src/rpc/converters/ballot-converter';
import { DelegationConverter } from '../../../../src/rpc/converters/delegation-converter';
import { DoubleBakingEvidenceConverter } from '../../../../src/rpc/converters/double-baking-evidence-converter';
import {
    DoubleEndorsementEvidenceConverter,
} from '../../../../src/rpc/converters/double-endorsement-evidence-converter';
import { EndorsementConverter } from '../../../../src/rpc/converters/endorsement-converter';
import {
    BaseOperationProperties,
    OperationConverter,
    RpcOperation,
} from '../../../../src/rpc/converters/operation-converter';
import { OriginationConverter } from '../../../../src/rpc/converters/origination-converter';
import { ProposalsConverter } from '../../../../src/rpc/converters/proposals-converter';
import { RevealConverter } from '../../../../src/rpc/converters/reveal-converter';
import { SeedNonceRevelationConverter } from '../../../../src/rpc/converters/seed-nonce-revelation-converter';
import { TransactionConverter } from '../../../../src/rpc/converters/transaction-converter';
import { mockBaseProperties } from './mocks';

describe(OperationConverter.name, () => {
    let target: OperationConverter;
    let activateAccountConverter: ActivateAccountConverter;
    let ballotConverter: BallotConverter;
    let delegationConverter: DelegationConverter;
    let doubleBakingEvidenceConverter: DoubleBakingEvidenceConverter;
    let doubleEndorsementEvidenceConverter: DoubleEndorsementEvidenceConverter;
    let endorsementConverter: EndorsementConverter;
    let originationConverter: OriginationConverter;
    let proposalsConverter: ProposalsConverter;
    let revealConverter: RevealConverter;
    let seedNonceRevelationConverter: SeedNonceRevelationConverter;
    let transactionConverter: TransactionConverter;

    beforeEach(() => {
        activateAccountConverter = mock(ActivateAccountConverter);
        ballotConverter = mock(BallotConverter);
        delegationConverter = mock(DelegationConverter);
        doubleBakingEvidenceConverter = mock(DoubleBakingEvidenceConverter);
        doubleEndorsementEvidenceConverter = mock(DoubleEndorsementEvidenceConverter);
        endorsementConverter = mock(EndorsementConverter);
        originationConverter = mock(OriginationConverter);
        proposalsConverter = mock(ProposalsConverter);
        revealConverter = mock(RevealConverter);
        seedNonceRevelationConverter = mock(SeedNonceRevelationConverter);
        transactionConverter = mock(TransactionConverter);
        target = new OperationConverter(
            instance(activateAccountConverter),
            instance(ballotConverter),
            instance(delegationConverter),
            instance(doubleBakingEvidenceConverter),
            instance(doubleEndorsementEvidenceConverter),
            instance(endorsementConverter),
            instance(originationConverter),
            instance(proposalsConverter),
            instance(revealConverter),
            instance(seedNonceRevelationConverter),
            instance(transactionConverter),
        );
    });

    executeTest(rpc.OpKind.ACTIVATION, () => activateAccountConverter);
    executeTest(rpc.OpKind.BALLOT, () => ballotConverter);
    executeTest(rpc.OpKind.DELEGATION, () => delegationConverter);
    executeTest(rpc.OpKind.DOUBLE_BAKING_EVIDENCE, () => doubleBakingEvidenceConverter);
    executeTest(rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE, () => doubleEndorsementEvidenceConverter);
    executeTest(rpc.OpKind.ENDORSEMENT, () => endorsementConverter);
    executeTest(rpc.OpKind.ORIGINATION, () => originationConverter);
    executeTest(rpc.OpKind.PROPOSALS, () => proposalsConverter);
    executeTest(rpc.OpKind.REVEAL, () => revealConverter);
    executeTest(rpc.OpKind.SEED_NONCE_REVELATION, () => seedNonceRevelationConverter);
    executeTest(rpc.OpKind.TRANSACTION, () => transactionConverter);

    function executeTest<TRpc extends RpcOperation, TOperation>(
        kind: rpc.OpKind,
        getConverter: () => { convert(r: TRpc, b: BaseOperationProperties): TOperation },
    ) {
        it(`should delegate to correct converter if ${kind} operation`, () => {
            const rpcOperation = { kind } as TRpc;
            const convertedResult = {} as TOperation;
            const baseProps = mockBaseProperties();
            when(getConverter().convert(rpcOperation, baseProps)).thenReturn(convertedResult);

            const result = target.convert(rpcOperation, baseProps);

            expect(result).toBe(convertedResult);
        });
    }
});
