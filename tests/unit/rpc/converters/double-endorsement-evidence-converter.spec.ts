import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    DoubleEndorsementEvidenceNotification,
    InlinedEndorsementKind,
} from '../../../../src/entity/subscriptions/double-endorsement-evidence-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    DoubleEndorsementEvidenceConverter,
} from '../../../../src/rpc/converters/double-endorsement-evidence-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(DoubleEndorsementEvidenceConverter.name, () => {
    let target: DoubleEndorsementEvidenceConverter;
    let rpcOperation: rpc.OperationContentsAndResultDoubleEndorsement;
    let metadataConverter: OperationMetadataConverter;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new DoubleEndorsementEvidenceConverter(instance(metadataConverter));
    });

    it(`should convert values correctly`, () => {
        rpcOperation = {
            kind: rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE,
            op1: {
                branch: 'br1',
                operations: {
                    kind: rpc.OpKind.ENDORSEMENT,
                    level: 1111,
                },
                signature: 'sig1',
            },
            op2: {
                branch: 'br2',
                operations: {
                    kind: rpc.OpKind.ENDORSEMENT,
                    level: 1112,
                },
                signature: 'sig2',
            },
            metadata: null!,
        };
        const metadata = mockOperationMetadata();
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act();

        expect(operation).toEqual<DoubleEndorsementEvidenceNotification>({
            kind: OperationKind.double_endorsement_evidence,
            op1: {
                branch: 'br1',
                operations: {
                    kind: InlinedEndorsementKind.endorsement,
                    level: 1111,
                },
                signature: 'sig1',
            },
            op2: {
                branch: 'br2',
                operations: {
                    kind: InlinedEndorsementKind.endorsement,
                    level: 1112,
                },
                signature: 'sig2',
            },
            ...mockBaseProperties(),
            metadata,
        });
    });
});
