import { BlockHeaderResponse, RpcClient } from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { TezosRpcHealthWatcher } from '../../../src/rpc/tezos-rpc-health-watcher';
import { HealthCheckResult, HealthStatus } from '../../../src/utils/health/health-check';
import { nameof } from '../../../src/utils/reflection';
import { TestLogger } from '../mocks';
import { TestClock } from '../mocks/test-clock';

const clock = new TestClock();

describe(TezosRpcHealthWatcher.name, () => {
    let target: TezosRpcHealthWatcher;
    let rpcClient: RpcClient;
    let logger: TestLogger;

    beforeEach(() => {
        rpcClient = mock(RpcClient);
        logger = new TestLogger();
        target = new TezosRpcHealthWatcher(instance(rpcClient), clock, logger);
    });

    describe(nameof<TezosRpcHealthWatcher>('name'), () => {
        it('should be correct', () => {
            expect(target.name).toBe('TezosRpcWatcher');
        });
    });

    describe(nameof<TezosRpcHealthWatcher>('checkHealth'), () => {
        it(`should return healthy if block header passed`, async () => {
            const blockHeader = {
                hash: 'abc',
                level: 123,
                chain_id: 'ch',
                timestamp: 'ttt',
            } as BlockHeaderResponse;
            when(rpcClient.getBlockHeader()).thenResolve(blockHeader);

            const result = await target.checkHealth();

            expect(result).toEqual<HealthCheckResult>({
                status: HealthStatus.Healthy,
                data: {
                    headBlock: {
                        hash: 'abc',
                        level: 123,
                        chainId: 'ch',
                        timestamp: 'ttt',
                        receivedOn: clock.nowDate,
                    },
                },
            });
        });

        it(`should return unhealthy with error if block header failed`, async () => {
            const blockHeader = {
                hash: 'abc',
                level: 123,
                chain_id: 'ch',
                timestamp: 'ttt',
            } as BlockHeaderResponse;
            const error = new Error('OMG');
            when(rpcClient.getBlockHeader())
                .thenResolve(blockHeader)
                .thenThrow(error);
            await target.checkHealth(); // Fill last head.

            const result = await target.checkHealth();

            expect(result).toEqual<HealthCheckResult>({
                status: HealthStatus.Unhealthy,
                data: {
                    error,
                    lastHealthyHeadBlock: {
                        hash: 'abc',
                        level: 123,
                        chainId: 'ch',
                        timestamp: 'ttt',
                        receivedOn: clock.nowDate,
                    },
                },
            });
        });
    });
});
