import { instance, mock, when } from 'ts-mockito';
import { DependencyContainer } from 'tsyringe';
import { ContainerType } from 'type-graphql';

import { GraphQLDependencyContainer } from '../../../src/bootstrap/graphql-dependency-container';

class Foo {}

describe(GraphQLDependencyContainer.name, () => {
    let target: ContainerType;
    let tsyringeContainer: DependencyContainer;

    beforeEach(() => {
        tsyringeContainer = mock<DependencyContainer>();
        target = new GraphQLDependencyContainer(instance(tsyringeContainer));
    });

    it('should pass resolution to Tsyringe container', () => {
        const foo = new Foo();
        when(tsyringeContainer.resolve(Foo)).thenReturn(foo);

        const result = target.get(Foo, null!);

        expect(result).toBe(foo);
    });
});
