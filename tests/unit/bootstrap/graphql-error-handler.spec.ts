import { ApolloError, UserInputError, ValidationError } from 'apollo-server';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { Writable } from 'ts-essentials';
import { instance, mock, verify, when } from 'ts-mockito';
import { ArgumentValidationError, ForbiddenError, UnauthorizedError } from 'type-graphql';

import { GraphQLErrorHandler } from '../../../src/bootstrap/graphql-error-handler';
import { ResolverContext } from '../../../src/bootstrap/resolver-context';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { LogLevel } from '../../../src/utils/logging';
import { UuidGenerator } from '../../../src/utils/uuid-generator';
import { TestLogger } from '../mocks';

describe(GraphQLErrorHandler.name, () => {
    describe.each([
        'req',
        undefined,
    ])('with requestId = %s', (requestId) => {
        let target: GraphQLErrorHandler;
        let uuidGenerator: UuidGenerator;
        let envConfig: Writable<EnvConfig>;
        let logger: TestLogger;

        const act = (error: GraphQLError) => target.handle(error, { requestId } as ResolverContext);

        beforeEach(() => {
            uuidGenerator = mock(UuidGenerator);
            envConfig = { enableDebug: false } as EnvConfig;
            logger = new TestLogger();
            target = new GraphQLErrorHandler(instance(uuidGenerator), envConfig, logger);

            when(uuidGenerator.generate()).thenReturn('uuu');
        });

        it('should log error with generated ID', () => {
            const inputError = getGraphQLError(new Error('wtf'));

            const resultError = act(inputError);

            expect(resultError.message).toContain('uuu');
            expect(resultError.message).not.toContain('omg');
            expect(resultError.message).not.toContain('wtf');
            expect(resultError).toContainAllKeys(['message']);
            logger.loggedSingle().verify(LogLevel.Error, { error: inputError, errorId: 'uuu', requestId });
        });

        it('should return original error if debug', () => {
            envConfig.enableDebug = true;
            const inputError = getGraphQLError(new Error('wtf'));

            const resultError = act(inputError);

            expect(resultError.message).toIncludeMultiple(['uuu', 'omg']);
            verifyCopiedProperties(resultError, inputError);
            logger.loggedSingle().verify(LogLevel.Error, { error: inputError, errorId: 'uuu', requestId });
        });

        it.each([
            ['ApolloError', new ApolloError('wtf')],
            ['inherited from ApolloError', new UserInputError('wtf')],
            ['TypeGraphQL ArgumentValidationError', new ArgumentValidationError([])],
            ['TypeGraphQL ForbiddenError', new ForbiddenError()],
            ['TypeGraphQL UnauthorizedError', new UnauthorizedError()],
        ])('should not change error if %s', (_desc, originalError) => {
            runNoChangeTest(getGraphQLError(originalError));
        });

        it.each([
            ['ApolloError', new ApolloError('wtf')],
            ['inherited from ApolloError', new ValidationError('wtf')],
        ])('should not change error if %s directly', (_desc, inputError) => {
            runNoChangeTest(inputError);
        });

        function runNoChangeTest(inputError: GraphQLError) {
            const resultError = act(inputError);

            expect(resultError.message).toBe(inputError.message);
            verifyCopiedProperties(resultError, inputError);
            verify(uuidGenerator.generate()).never();
            logger.loggedSingle().verify(LogLevel.Warning, { error: inputError, requestId });
        }

        function verifyCopiedProperties(resultError: GraphQLFormattedError, inputError: GraphQLError) {
            expect(Object.keys(resultError)).toEqual(['message', 'locations', 'path', 'extensions']);
            expect(resultError.locations).toEqual(inputError.locations);
            expect(resultError.path).toEqual(inputError.path);
            expect(resultError.extensions).toEqual(inputError.extensions);
        }

        function getGraphQLError(original: Error) {
            return new GraphQLError('omg', undefined, undefined, [1, 2], ['root', 'path'], original, { a: 1 });
        }
    });
});
