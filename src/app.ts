import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import { inject, injectAll, singleton } from 'tsyringe';

import { ApolloServerFactory } from './bootstrap/apollo-server-factory';
import { BackgroundWorker, backgroundWorkersDIToken } from './bootstrap/background-worker';
import { expressAppDIToken } from './bootstrap/express-app-factory';
import { EnvConfig } from './utils/configuration/env-config';
import { ApolloHealthCheck } from './utils/health/apollo-health-check';
import { injectLogger, Logger } from './utils/logging';

@singleton()
export class App {
    apolloServer: ApolloServer | undefined;

    constructor(
        private readonly envConfig: EnvConfig,
        readonly httpServer: http.Server,
        @inject(expressAppDIToken) private readonly expressApp: express.Express,
        private readonly apolloServerFactory: ApolloServerFactory,
        @injectAll(backgroundWorkersDIToken) private readonly backgroundWorkers: readonly BackgroundWorker[],
        private readonly apolloHealthCheck: ApolloHealthCheck,
        @injectLogger(App) private readonly logger: Logger,
    ) {}

    async start(): Promise<void> {
        if (!this.apolloServer) {
            await this.initialize();
        }

        await Promise.all(this.backgroundWorkers.map(async worker => {
            this.logWorker('Starting', worker);
            await worker.start();
            this.logWorker('Started', worker);
        }));

        this.logger.logInformation('Server starting at {url}.', { url: `http://${this.envConfig.host}:${this.envConfig.port}` });
        await new Promise<void>(resolve => {
            this.httpServer.listen(this.envConfig.port, this.envConfig.host, resolve);
        });
        this.logger.logInformation('Server started.');
    }

    async stop(): Promise<void> {
        if (!this.apolloServer) {
            throw new Error('App is not initialized.');
        }

        await Promise.all(this.backgroundWorkers.map(async worker => {
            if (worker.stop) {
                this.logWorker('Stopping', worker);
                await worker.stop();
                this.logWorker('Stopped', worker);
            }
        }));

        this.logger.logInformation('Server stopping.');
        await this.apolloServer.stop();
        await new Promise(resolve => {
            this.httpServer.close(resolve);
        });

        this.logger.logInformation('Server stopped.');
        this.logger.close();
    }

    async initialize(): Promise<void> {
        this.apolloServer = await this.apolloServerFactory.create();

        this.apolloServer.applyMiddleware({
            app: this.expressApp,
            path: '/graphql',
            onHealthCheck: async () => this.apolloHealthCheck.checkIsHealthy(),
        });
        this.apolloServer.installSubscriptionHandlers(this.httpServer);
    }

    private logWorker(verb: string, worker: BackgroundWorker): void {
        this.logger.logInformation(`${verb} {worker}.`, { worker: worker.name });
    }
}
