import { PubSub } from 'graphql-subscriptions';
import { DependencyContainer } from 'tsyringe';

import { externalPubSubDIToken } from '../../utils/pubsub/external-pub-sub';
import { Module, ModuleName } from '../module';
import { onlyOnePubSubModuleEnabledDependency, RequireOneOfModulesDependency } from '../module-helpers';

export const memoryPubSubModule: Module = {
    name: ModuleName.MemoryPubSub,

    dependencies: [
        new RequireOneOfModulesDependency(
            [ModuleName.TezosMonitor],
            { reason: 'some notifications are produced within this app' },
        ),
        new RequireOneOfModulesDependency(
            [ModuleName.SubscriptionsSubscriber],
            { reason: 'there is a way to subscribe to notifications produced by this app' },
        ),
        onlyOnePubSubModuleEnabledDependency,
    ],

    initialize(diContainer: DependencyContainer): void {
        diContainer.registerInstance(externalPubSubDIToken, new PubSub());
    },
};
