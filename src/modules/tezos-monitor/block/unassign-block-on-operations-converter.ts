import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { ItemConverter } from '../../../utils/iterable/convert-item-processor';

/** Unassigns block on operations because it's a circular reference and can't be JSON-stringified. */
@singleton()
export class UnassignBlockOnOperationsConverter implements ItemConverter<BlockNotification, BlockNotification> {
    convert(block: BlockNotification): BlockNotification {
        return {
            ...block,
            operations: block.operations.map(operation => ({
                ...operation,
                block: undefined,
            })),
        };
    }
}
