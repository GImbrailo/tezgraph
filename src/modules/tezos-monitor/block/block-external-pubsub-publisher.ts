import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { ItemProcessor } from '../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer, OperationSource } from '../../../utils/metrics/metrics-container';
import { ExternalPubSub, externalTriggers } from '../../../utils/pubsub/external-pub-sub';

/** Publishes GraphQL blocks to external PubSub. */
@singleton()
export class BlockExternalPubSubPublisher implements ItemProcessor<BlockNotification> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(BlockExternalPubSubPublisher) private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
    ) {}

    async processItem(block: BlockNotification): Promise<void> {
        this.logger.logInformation('Publishing block {hash} with {operationCount}.', {
            hash: block.hash,
            operationCount: block.operations.length,
        });
        this.metrics.tezosMonitorBlockLevel.set(block.header.level);
        this.metrics.tezosMonitorOperationCount.inc({ source: OperationSource.Block }, block.operations.length);
        await this.externalPubSub.publish(externalTriggers.blocks, block);
    }
}
