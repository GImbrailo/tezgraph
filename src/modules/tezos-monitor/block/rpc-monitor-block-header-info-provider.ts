import { singleton } from 'tsyringe';

import { InfoProvider } from '../../../utils/iterable/interfaces';
import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';

/** Collects generic diagnostic info about a block. */
@singleton()
export class RpcMonitorBlockHeaderInfoProvider implements InfoProvider<RpcMonitorBlockHeader> {
    getInfo(block: RpcMonitorBlockHeader): unknown {
        return {
            hash: block.hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }
}
