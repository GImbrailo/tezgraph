import { singleton } from 'tsyringe';

import { tezosRpcPaths } from '../../../rpc/tezos-rpc-paths';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { ComponentHealthState } from '../../../utils/health/component-health-state';
import { injectLogger, Logger } from '../../../utils/logging';
import { isNullish } from '../../../utils/reflection';
import { isWhiteSpace } from '../../../utils/string-manipulation';
import { AbstractDownloadDataProvider } from '../helpers/abstract-download-data-provider';
import { TezosMonitorClient } from '../helpers/tezos-monitor-client';
import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';

/** Health state of block monitor. */
@singleton()
export class BlockMonitorHealth extends ComponentHealthState {
    readonly name = 'BlockTezosMonitor';
}

/** Downloads blocks and casts them strictly. */
@singleton()
export class DownloadBlocksProvider extends AbstractDownloadDataProvider<RpcMonitorBlockHeader> {
    constructor(
        envConfig: EnvConfig,
        monitorClient: TezosMonitorClient,
        @injectLogger(DownloadBlocksProvider) logger: Logger,
    ) {
        super(monitorClient, logger, new URL(tezosRpcPaths.blockMonitor, envConfig.tezosNodeUrl).toString());
    }

    tryCastData(data: unknown): RpcMonitorBlockHeader[] | undefined {
        const rpcBlock = data as RpcMonitorBlockHeader;
        return !isNullish(rpcBlock) && typeof rpcBlock.hash === 'string' && !isWhiteSpace(rpcBlock.hash)
            ? [rpcBlock]
            : undefined;
    }
}
