import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { BlockConverter } from '../../../rpc/converters/block-converter';
import { TezosRpcClient } from '../../../rpc/tezos-rpc-client';
import { ItemConverter } from '../../../utils/iterable/convert-item-processor';
import { Clock } from '../../../utils/time/clock';
import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';

/** Converts a monitor block header from RPC format to our GraphQL block. */
@singleton()
export class RpcMonitorBlockConverter implements ItemConverter<RpcMonitorBlockHeader, BlockNotification> {
    constructor(
        private readonly rpcClient: TezosRpcClient,
        private readonly blockConverter: BlockConverter,
        private readonly clock: Clock,
    ) {}

    async convert(monitorBlock: RpcMonitorBlockHeader): Promise<BlockNotification> {
        const receivedFromTezosOn = this.clock.getNowDate();
        const rpcBlock = await this.rpcClient.getBlock(monitorBlock.hash);
        return this.blockConverter.convert(rpcBlock, receivedFromTezosOn);
    }
}
