import { AbortSignal } from 'abort-controller';
import { singleton } from 'tsyringe';

import { errorToString } from '../../../utils/conversion';
import { HttpClient } from '../../../utils/http-client';
import { HttpStatusCode } from '../../../utils/http-constants';
import { injectLogger, Logger } from '../../../utils/logging';
import { TezosMonitorEnvConfig } from '../tezos-monitor-env-config';

/** Dowloands raw JSON from RPC monitor of a Tezos node.  */
@singleton()
export class TezosMonitorClient {
    constructor(
        private readonly envConfig: TezosMonitorEnvConfig,
        private readonly httpClient: HttpClient,
        private readonly abortSignal: AbortSignal,
        @injectLogger(TezosMonitorClient) private readonly logger: Logger,
    ) {}

    async *iterateMonitor(url: string): AsyncIterableIterator<unknown> {
        const responseBody = await this.connect(url);

        // Tezos node splits long JSON (esp. mempool) to smaller chunks -> invalid JSON -> concat and try to parse it.
        let incompleteChunkCount = 0;
        let dataStr = '';

        for await (const chunk of responseBody) {
            try {
                const chunkStr = typeof chunk !== 'string' ? chunk.toString() : chunk;
                dataStr = dataStr.length ? dataStr + chunkStr : chunkStr; // Avoids string copy for '' + 'abc'.

                const item: unknown = JSON.parse(dataStr);

                this.onChunksCompleted(incompleteChunkCount + 1, url);
                dataStr = '';
                incompleteChunkCount = 0;

                yield item;
            } catch (error: unknown) {
                incompleteChunkCount++;
                this.processError(error, incompleteChunkCount, url);
            }
        }
    }

    private async connect(url: string): Promise<NodeJS.ReadableStream> {
        this.logger.logInformation('Connecting to {url}.', { url });
        const response = await this.httpClient.fetch(url, { signal: this.abortSignal });

        if (response.status !== HttpStatusCode.OK) {
            throw new Error(`Failed to connect to ${url} because of ${response.status} ${response.statusText}`);
        }

        this.logger.logInformation('Connected to {url}. Monitoring it for new chunks.', { url });
        return response.body;
    }

    private onChunksCompleted(chunkCount: number, url: string): void {
        if (chunkCount >= this.envConfig.tezosMonitorWarnChunkCount) {
            this.logger.logWarning('Finally received a valid JSON from {url} within {chunkCount}.', { url, chunkCount });
        }
    }

    private processError(error: unknown, incompleteChunkCount: number, url: string): void {
        if (incompleteChunkCount >= this.envConfig.tezosMonitorFailChunkCount) {
            throw new Error(`Received too many ${incompleteChunkCount} chunks from ${url}`
                + ` which still don't produce a valid JSON. ${errorToString(error)}`);
        }
    }
}
