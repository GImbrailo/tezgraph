import { IterableProvider } from '../../../utils/iterable/interfaces';
import { Logger } from '../../../utils/logging';
import { TezosMonitorClient } from './tezos-monitor-client';

export abstract class AbstractDownloadDataProvider<TItem> implements IterableProvider<TItem> {
    constructor(
        private readonly monitorClient: TezosMonitorClient,
        private readonly logger: Logger,
        readonly url: string,
    ) {}

    async *iterate(): AsyncIterableIterator<TItem> {
        for await (const data of this.monitorClient.iterateMonitor(this.url)) {
            const items = this.tryCastData(data);

            if (items) {
                yield* items;
            } else {
                this.logger.logError(`Data from {url} is invalid (nullish, no ID etc.). Related subscriptions won't be published. {data}`, {
                    url: this.url,
                    data,
                });
            }
        }
    }

    protected abstract tryCastData(data: unknown): TItem[] | undefined;
}
