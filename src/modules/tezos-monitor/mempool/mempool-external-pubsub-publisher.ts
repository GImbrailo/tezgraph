import { singleton } from 'tsyringe';

import { getSignature, MempoolOperationGroup } from '../../../entity/subscriptions/operation-notification';
import { ItemProcessor } from '../../../utils/iterable/interfaces';
import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer, OperationSource } from '../../../utils/metrics/metrics-container';
import { ExternalPubSub, externalTriggers } from '../../../utils/pubsub/external-pub-sub';

/** Publishes GraphQL mempool operation groups to external PubSub. */
@singleton()
export class MempoolExternalPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(MempoolExternalPubSubPublisher) private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
    ) {}

    async processItem(mempoolGroup: MempoolOperationGroup): Promise<void> {
        this.logger.logInformation('Publishing mempool operation group with {signature} and {operationCount}.', {
            signature: getSignature(mempoolGroup),
            operationCount: mempoolGroup.length,
        });
        this.metrics.tezosMonitorOperationCount.inc({ source: OperationSource.Mempool }, mempoolGroup.length);
        await this.externalPubSub.publish(externalTriggers.mempoolOperationGroups, mempoolGroup);
    }
}
