import { singleton } from 'tsyringe';

import { InfoProvider } from '../../../utils/iterable/interfaces';
import { RpcMempoolOperationGroup } from './rpc-mempool-operation-group';

/** Collects generic diagnostic info about a mempool operation group. */
@singleton()
export class RpcMempoolOperationGroupInfoProvider implements InfoProvider<RpcMempoolOperationGroup> {
    getInfo(group: RpcMempoolOperationGroup): unknown {
        return {
            signature: group.signature,
            operationCount: group.contents.length,
        };
    }
}
