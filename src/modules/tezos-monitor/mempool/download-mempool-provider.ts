import { singleton } from 'tsyringe';

import { tezosRpcPaths } from '../../../rpc/tezos-rpc-paths';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { ComponentHealthState } from '../../../utils/health/component-health-state';
import { injectLogger, Logger } from '../../../utils/logging';
import { isNullish } from '../../../utils/reflection';
import { isWhiteSpace } from '../../../utils/string-manipulation';
import { AbstractDownloadDataProvider } from '../helpers/abstract-download-data-provider';
import { TezosMonitorClient } from '../helpers/tezos-monitor-client';
import { RpcMempoolOperationGroup } from './rpc-mempool-operation-group';

/** Health state of mempool monitor. */
@singleton()
export class MempoolMonitorHealth extends ComponentHealthState {
    readonly name = 'MempoolTezosMonitor';
}

/** Downloads mempool operation groups and casts them strictly. */
@singleton()
export class DownloadMempoolProvider extends AbstractDownloadDataProvider<RpcMempoolOperationGroup> {
    constructor(
        envConfig: EnvConfig,
        monitorClient: TezosMonitorClient,
        @injectLogger(DownloadMempoolProvider) logger: Logger,
    ) {
        super(monitorClient, logger, new URL(tezosRpcPaths.mempoolMonitor, envConfig.tezosNodeUrl).toString());
    }

    tryCastData(data: unknown): RpcMempoolOperationGroup[] | undefined {
        const rpcGroups = data as RpcMempoolOperationGroup[];
        const isValid = Array.isArray(rpcGroups) && rpcGroups.length > 0
            && rpcGroups.every(g => !isNullish(g) && typeof g.signature === 'string' && !isWhiteSpace(g.signature));

        return isValid ? rpcGroups : undefined;
    }
}
