import { DependencyContainer } from 'tsyringe';

import { numberOfLastBlocksToDeduplicate } from '../../entity/subscriptions/block-notification';
import { numberOfLastMempoolGroupsToDeduplicate } from '../../entity/subscriptions/operation-notification';
import { OperationGroupConverter } from '../../rpc/converters/operation-group-converter';
import { healthChecksDIToken } from '../../utils/health/health-check';
import { ConvertItemProcessor } from '../../utils/iterable/convert-item-processor';
import { DeduplicateItemProcessor } from '../../utils/iterable/deduplicate-item-processor';
import { FreezeItemProcessor } from '../../utils/iterable/freeze-item-processor';
import { registerIterableWorker } from '../../utils/iterable/iterable-worker-dependency-injection';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { BlockExternalPubSubPublisher } from './block/block-external-pubsub-publisher';
import { BlockMonitorHealth, DownloadBlocksProvider } from './block/download-blocks-provider';
import { RpcMonitorBlockConverter } from './block/rpc-monitor-block-converter';
import { RpcMonitorBlockHeader } from './block/rpc-monitor-block-header';
import { RpcMonitorBlockHeaderInfoProvider } from './block/rpc-monitor-block-header-info-provider';
import { UnassignBlockOnOperationsConverter } from './block/unassign-block-on-operations-converter';
import { createRetryMonitorProvider } from './helpers/retry-monitor-provider';
import { DownloadMempoolProvider, MempoolMonitorHealth } from './mempool/download-mempool-provider';
import { MempoolExternalPubSubPublisher } from './mempool/mempool-external-pubsub-publisher';
import { RpcMempoolOperationGroupInfoProvider } from './mempool/rpc-mempool-operation-group-info-provider';

export const tezosMonitorModule: Module = {
    name: ModuleName.TezosMonitor,

    dependencies: [new RequirePubSubDependency({ reason: 'notifications can be published to a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        registerBlockPipeline(diContainer);
        registerMempoolPipeline(diContainer);
        registerTezosRpcHealthWatcher(diContainer);
    },
};

function registerBlockPipeline(diContainer: DependencyContainer): void {
    registerIterableWorker<RpcMonitorBlockHeader>(
        diContainer,
        'BlockPublisher',
        c => createRetryMonitorProvider(c, DownloadBlocksProvider, BlockMonitorHealth),
        c => new DeduplicateItemProcessor(
            numberOfLastBlocksToDeduplicate,
            b => b.hash,
            new ConvertItemProcessor(
                c.resolve(RpcMonitorBlockConverter),
                new ConvertItemProcessor(
                    c.resolve(UnassignBlockOnOperationsConverter),
                    new FreezeItemProcessor(
                        c.resolve(BlockExternalPubSubPublisher),
                    ),
                ),
            ),
        ),
        RpcMonitorBlockHeaderInfoProvider,
    );
    diContainer.register(healthChecksDIToken, { useToken: BlockMonitorHealth });
}

function registerMempoolPipeline(diContainer: DependencyContainer): void {
    registerIterableWorker(
        diContainer,
        'MempoolPublisher',
        c => createRetryMonitorProvider(c, DownloadMempoolProvider, MempoolMonitorHealth),
        c => new DeduplicateItemProcessor(
            numberOfLastMempoolGroupsToDeduplicate,
            g => g.signature,
            new ConvertItemProcessor(
                c.resolve(OperationGroupConverter),
                c.resolve(MempoolExternalPubSubPublisher),
            ),
        ),
        RpcMempoolOperationGroupInfoProvider,
    );
    diContainer.register(healthChecksDIToken, { useToken: MempoolMonitorHealth });
}
