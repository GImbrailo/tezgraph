import { ResolverContext } from '../../../bootstrap/resolver-context';
import { OperationArgs } from '../../../entity/subscriptions/args/operation-args';
import { OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { Logger } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';
import { isNullish } from '../../../utils/reflection';

export interface AbstractSubscriptionOptions {
    readonly args: unknown;
    readonly context: ResolverContext;
    readonly subscriptionName: string;
}

export interface AbstractSubscription {
    subscribe(options: AbstractSubscriptionOptions): AsyncIterator<unknown>;
}

export const loggerCategory = 'LoggingSubscription';

export abstract class AbstractLoggingSubscription {
    constructor(
        private readonly logger: Logger,
        private readonly innerSubscription: AbstractSubscription,
        private readonly metrics: MetricsContainer,
    ) {}

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    subscribe(options: AbstractSubscriptionOptions): AsyncIterableIterator<any> {
        this.logger.logInformation('Opening {subscription} for {requestId} with {args}.', {
            subscription: options.subscriptionName,
            requestId: options.context.requestId,
            args: options.args,
            rawQuery: options.context.connection?.query,
            rawQueryVariables: options.context.connection?.variables,
        });

        this.reportMetricsIncrements(options.subscriptionName, options.args);

        const innerIterable = this.innerSubscription.subscribe(options);

        const logSubscriptionClosing = (): void => {
            this.reportMetricsDecrements(options.subscriptionName, options.args);
            this.logger.logInformation('Closing {subscription} for {requestId}.', {
                subscription: options.subscriptionName,
                requestId: options.context.requestId,
            });
        };
        return {
            next: innerIterable.next.bind(innerIterable),

            async return(value: unknown): Promise<IteratorResult<unknown, unknown>> {
                logSubscriptionClosing();
                return await innerIterable.return?.(value) ?? { done: true, value };
            },
            async throw(error: unknown): Promise<IteratorResult<unknown, unknown>> {
                logSubscriptionClosing();
                return await innerIterable.throw?.(error) ?? { done: true, value: error };
            },
            [Symbol.asyncIterator](): AsyncIterableIterator<unknown> {
                return this;
            },
        };
    }

    private reportMetricsIncrements(subscriptionName: string, args: unknown): void {
        const metricsData = this.getMetricsData(args);
        this.metrics.subscriptionsCurrent.inc({
            name: subscriptionName,
            mempool: metricsData.mempool,
            replay: metricsData.replay,
        });

        this.metrics.subscriptionsTotal.inc({
            name: subscriptionName,
            mempool: metricsData.mempool,
            replay: metricsData.replay,
        });
    }

    private reportMetricsDecrements(subscriptionName: string, args: unknown): void {
        const tupple = this.getMetricsData(args);
        this.metrics.subscriptionsCurrent.dec({
            name: subscriptionName,
            mempool: tupple.mempool,
            replay: tupple.replay,
        });
    }

    private getMetricsData(args: unknown): { mempool: string; replay: string } {
        const operationArgs = args as OperationArgs<OperationNotification>;
        const mempool = operationArgs.includeMempool ? 'true' : 'false';
        const replay = (!isNullish(operationArgs.replayFromBlockLevel)).toString();
        return { mempool, replay };
    }
}
