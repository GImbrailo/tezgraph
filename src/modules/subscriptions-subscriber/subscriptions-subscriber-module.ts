import { DependencyContainer } from 'tsyringe';

import { graphQLResolversDIToken } from '../../bootstrap/graphql-resolver-type';
import { numberOfLastBlocksToDeduplicate } from '../../entity/subscriptions/block-notification';
import { getSignature, numberOfLastMempoolGroupsToDeduplicate } from '../../entity/subscriptions/operation-notification';
import { ConvertItemProcessor } from '../../utils/iterable/convert-item-processor';
import { DeduplicateItemProcessor } from '../../utils/iterable/deduplicate-item-processor';
import { FreezeItemProcessor } from '../../utils/iterable/freeze-item-processor';
import { registerIterableWorker } from '../../utils/iterable/iterable-worker-dependency-injection';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { blockOperationsResolvers } from './block-subscription/block-operations-resolver';
import { BlockSubscriptionResolver } from './block-subscription/block-subscription-resolver';
import { operationSubscriptionResolvers } from './operation-subscription/operation-subscription-resolvers';
import { AssignBlockToOperationsConverter } from './pubsub/block/assign-block-to-operations-converter';
import { BlockInfoProvider } from './pubsub/block/block-info-provider';
import { BlockPubSubProvider } from './pubsub/block/block-pubsub-provider';
import { BlockPubSubPublisher } from './pubsub/block/block-pubsub-publisher';
import { MempoolOperationGroupInfoProvider } from './pubsub/mempool/mempool-operation-group-info-provider';
import { MempoolPubSubProvider } from './pubsub/mempool/mempool-pubsub-provider';
import { MempoolPubSubPublisher } from './pubsub/mempool/mempool-pubsub-pubsliher';

export const subscriptionsSubscriberModule: Module = {
    name: ModuleName.SubscriptionsSubscriber,

    dependencies: [new RequirePubSubDependency({ reason: 'subscriptions can be created from a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        for (const type of [BlockSubscriptionResolver, ...operationSubscriptionResolvers, ...blockOperationsResolvers]) {
            diContainer.registerInstance(graphQLResolversDIToken, type);
        }

        registerBlockPipeline(diContainer);
        registerMempoolPipeline(diContainer);
        registerTezosRpcHealthWatcher(diContainer);
    },
};

function registerBlockPipeline(diContainer: DependencyContainer): void {
    registerIterableWorker(
        diContainer,
        'BlockSubscriber',
        c => c.resolve(BlockPubSubProvider),
        c => new DeduplicateItemProcessor(
            numberOfLastBlocksToDeduplicate,
            b => b.hash,
            new ConvertItemProcessor(
                c.resolve(AssignBlockToOperationsConverter),
                new FreezeItemProcessor(
                    c.resolve(BlockPubSubPublisher),
                ),
            ),
        ),
        BlockInfoProvider,
    );
}

function registerMempoolPipeline(diContainer: DependencyContainer): void {
    registerIterableWorker(
        diContainer,
        'MempoolSubscriber',
        c => c.resolve(MempoolPubSubProvider),
        c => new DeduplicateItemProcessor(
            numberOfLastMempoolGroupsToDeduplicate,
            getSignature,
            new FreezeItemProcessor(
                c.resolve(MempoolPubSubPublisher),
            ),
        ),
        MempoolOperationGroupInfoProvider,
    );
}
