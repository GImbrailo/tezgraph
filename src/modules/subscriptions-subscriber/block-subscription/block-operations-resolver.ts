/* istanbul ignore file */
import { ActivateAccountNotification } from '../../../entity/subscriptions/activate-account-notification';
import { ActivateAccountFilter } from '../../../entity/subscriptions/args/activate-account-args';
import { BallotFilter } from '../../../entity/subscriptions/args/ballot-args';
import { DelegationFilter } from '../../../entity/subscriptions/args/delegation-args';
import { DoubleBakingEvidenceFilter } from '../../../entity/subscriptions/args/double-baking-evidence-args';
import { DoubleEndorsementEvidenceFilter } from '../../../entity/subscriptions/args/double-endorsement-evidence-args';
import { EndorsementFilter } from '../../../entity/subscriptions/args/endorsement-args';
import { OriginationFilter } from '../../../entity/subscriptions/args/origination-args';
import { ProposalsFilter } from '../../../entity/subscriptions/args/proposals-args';
import { RevealFilter } from '../../../entity/subscriptions/args/reveal-args';
import { SeedNonceRevelationFilter } from '../../../entity/subscriptions/args/seed-nonce-revelation-args';
import { TransactionFilter } from '../../../entity/subscriptions/args/transaction-args';
import { BallotNotification } from '../../../entity/subscriptions/ballot-notification';
import { DelegationNotification } from '../../../entity/subscriptions/delegation-notification';
import { DoubleBakingEvidenceNotification } from '../../../entity/subscriptions/double-baking-evidence-notification';
import { DoubleEndorsementEvidenceNotification } from '../../../entity/subscriptions/double-endorsement-evidence-notification';
import { EndorsementNotification } from '../../../entity/subscriptions/endorsement-notification';
import { OriginationNotification } from '../../../entity/subscriptions/origination-notification';
import { ProposalsNotification } from '../../../entity/subscriptions/proposals-notification';
import { RevealNotification } from '../../../entity/subscriptions/reveal-notification';
import { SeedNonceRevelationNotification } from '../../../entity/subscriptions/seed-nonce-revelation-notification';
import { TransactionNotification } from '../../../entity/subscriptions/transaction-notification';
import { createBlockOperationsResolverClass } from './block-operations-resolver-factory';

const create = createBlockOperationsResolverClass;

/** GraphQL resolvers for all types of operations on a block GraphQL object. */
export const blockOperationsResolvers = [
    create(ActivateAccountNotification, ActivateAccountFilter),
    create(BallotNotification, BallotFilter),
    create(DelegationNotification, DelegationFilter),
    create(DoubleBakingEvidenceNotification, DoubleBakingEvidenceFilter),
    create(DoubleEndorsementEvidenceNotification, DoubleEndorsementEvidenceFilter),
    create(EndorsementNotification, EndorsementFilter),
    create(OriginationNotification, OriginationFilter),
    create(ProposalsNotification, ProposalsFilter),
    create(RevealNotification, RevealFilter),
    create(SeedNonceRevelationNotification, SeedNonceRevelationFilter),
    create(TransactionNotification, TransactionFilter),
];
