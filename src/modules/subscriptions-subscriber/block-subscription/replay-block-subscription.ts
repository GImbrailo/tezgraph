import { inject, singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { isNotNullish } from '../../../utils/reflection';
import { ReplayBlockProvider } from '../helpers/replay-block-provider';
import { BlockSubscription, BlockSubscriptionOptions } from './block-subscription';
import { PubSubBlockSubscription } from './pubsub-block-subscription';

@singleton()
export class ReplayBlockSubscription implements BlockSubscription {
    constructor(
        @inject(PubSubBlockSubscription) private readonly innerSubscription: BlockSubscription,
        private readonly replayBlockProvider: ReplayBlockProvider,
        @injectLogger(ReplayBlockSubscription) private readonly logger: Logger,
    ) {}

    async *subscribe(options: BlockSubscriptionOptions): AsyncIterableIterator<BlockNotification> {
        const fromBlockLevel = options.args.replayFromBlockLevel;
        if (isNotNullish(fromBlockLevel)) {
            const requestId = options.context.requestId;
            this.logger.logInformation('Serving historical blocks from {blockLevel} for {requestId}.', {
                blockLevel: fromBlockLevel,
                requestId,
            });

            for await (const block of this.replayBlockProvider.iterateFrom(fromBlockLevel, requestId)) {
                this.logger.logDebug('Serving historical {block} of {blockLevel} for {requestId}.', {
                    block,
                    blockLevel: block.header.level,
                    requestId,
                });
                yield block;
            }

            this.logger.logDebug('Switching to fresh blocks for {requestId}.', { requestId });
        }
        yield* this.innerSubscription.subscribe(options);
    }
}
