import { InjectionToken } from 'tsyringe';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { SubscriptionArgs } from '../../../entity/subscriptions/args/subscription-args';
import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { LoggingBlockSubscription } from './logging-block-subscription';

export interface BlockSubscriptionOptions {
    readonly args: SubscriptionArgs;
    readonly context: ResolverContext;
    readonly subscriptionName: string;
}

export interface BlockSubscription {
    subscribe(options: BlockSubscriptionOptions): AsyncIterableIterator<BlockNotification>;
}

export const blockSubscriptionDIToken: InjectionToken<BlockSubscription> = LoggingBlockSubscription;
