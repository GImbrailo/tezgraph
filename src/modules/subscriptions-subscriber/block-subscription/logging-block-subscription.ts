import { singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';
import { AbstractLoggingSubscription, loggerCategory } from '../helpers/abstract-logging-subscription';
import { BlockSubscription } from './block-subscription';
import { ReplayBlockSubscription } from './replay-block-subscription';

@singleton()
export class LoggingBlockSubscription extends AbstractLoggingSubscription implements BlockSubscription {
    constructor(
        innerSubscription: ReplayBlockSubscription,
        @injectLogger(loggerCategory) logger: Logger,
        metrics: MetricsContainer,
    ) {
        super(logger, innerSubscription, metrics);
    }
}
