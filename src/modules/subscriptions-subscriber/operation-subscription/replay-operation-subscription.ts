import { inject, singleton } from 'tsyringe';

import { getOperationKind, OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { isNotNullish } from '../../../utils/reflection';
import { ReplayBlockProvider } from '../helpers/replay-block-provider';
import { OperationSubscription, SubscriptionOptions } from './operation-subscription';
import { PubSubOperationSubscription } from './pubsub-operation-subscription';

@singleton()
export class ReplayOperationSubscription implements OperationSubscription {
    constructor(
        @inject(PubSubOperationSubscription) private readonly innerSubscription: OperationSubscription,
        private readonly replayBlockProvider: ReplayBlockProvider,
        @injectLogger(ReplayOperationSubscription) private readonly logger: Logger,
    ) {}

    async *subscribe<TOperation extends OperationNotification>(options: SubscriptionOptions<TOperation>): AsyncIterableIterator<TOperation> {
        const fromBlockLevel = options.args.replayFromBlockLevel;
        if (isNotNullish(fromBlockLevel)) {
            const requestId = options.context.requestId;
            const operationKind = getOperationKind(options.operationType);
            this.logger.logInformation('Serving historical operations of {operationKind} from {blockLevel} for {requestId}.', {
                operationKind,
                blockLevel: fromBlockLevel,
                requestId,
            });

            for await (const block of this.replayBlockProvider.iterateFrom(fromBlockLevel, requestId)) {
                const operations = block.operations.filter(o => o.kind === operationKind) as TOperation[];
                this.logger.logDebug('Serving {operations} of {operationKind} from historical {blockLevel} for {requestId}.', {
                    operations,
                    operationKind,
                    blockLevel: block.header.level,
                    requestId,
                });
                yield* operations;
            }

            this.logger.logDebug('Switching to fresh operations of {operationKind} from new blocks for {requestId}.', { operationKind, requestId });
        }
        yield* this.innerSubscription.subscribe(options);
    }
}
