/* istanbul ignore file */
import { camelCase } from 'lodash';
import { Args, Authorized, Resolver, Root, Subscription } from 'type-graphql';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { OperationArgs } from '../../../entity/subscriptions/args/operation-args';
import { OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { DefaultConstructor } from '../../../utils/reflection';
import { removeRequiredSuffix } from '../../../utils/string-manipulation';
import { createArgsInstance } from '../helpers/args-instance-factory';
import { operationSubscriptionDIToken } from './operation-subscription';

/** Creates a GraphQL resolver for the particular operation subscription. */
export function createOperationSubscriptionResolverClass<
    TOperation extends OperationNotification,
    TArgs extends OperationArgs<TOperation>,
>(
    operationType: DefaultConstructor<TOperation>,
    argsType: DefaultConstructor<TArgs>,
): DefaultConstructor {
    const operationName = removeRequiredSuffix(operationType.name, 'Notification');
    const subscriptionName = `${camelCase(operationName)}Added`;

    @Resolver()
    class OperationSubscriptionResolverClass {
        @Authorized()
        @Subscription(() => operationType, {
            description: `Subscribes to *${operationName} Operation*-s from newly baked *Block*-s on Tezos network.`,
            subscribe: (_root, argsDictionary, context: ResolverContext) => {
                const args = createArgsInstance(argsType, argsDictionary);
                const subscription = context.container.resolve(operationSubscriptionDIToken);
                return subscription.subscribe({ operationType, args, context, subscriptionName });
            },
        })
        [subscriptionName](
            @Root() operation: TOperation,
            @Args(() => argsType) _args: TArgs, // eslint-disable-line @typescript-eslint/indent
        ): TOperation {
            return operation;
        }
    }
    return OperationSubscriptionResolverClass;
}
