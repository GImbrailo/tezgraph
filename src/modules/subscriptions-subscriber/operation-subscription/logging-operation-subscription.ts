import { singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';
import { AbstractLoggingSubscription, loggerCategory } from '../helpers/abstract-logging-subscription';
import { FilteringOperationSubscription } from './filtering-operation-subscription';
import { OperationSubscription } from './operation-subscription';

@singleton()
export class LoggingOperationSubscription extends AbstractLoggingSubscription implements OperationSubscription {
    constructor(
        innerSubscription: FilteringOperationSubscription,
        @injectLogger(loggerCategory) logger: Logger,
        metrics: MetricsContainer,
    ) {
        super(logger, innerSubscription, metrics);
    }
}
