import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { OperationNotification } from '../../../../entity/subscriptions/operation-notification';
import { ItemConverter } from '../../../../utils/iterable/convert-item-processor';

@singleton()
export class AssignBlockToOperationsConverter implements ItemConverter<BlockNotification, BlockNotification> {
    convert(block: BlockNotification): BlockNotification {
        const alignedOperations = Array<OperationNotification>();
        const alignedBlock: BlockNotification = {
            ...block,
            operations: alignedOperations,
        };

        for (const operation of block.operations) {
            alignedOperations.push({
                ...operation,
                block: alignedBlock,
            });
        }
        return alignedBlock;
    }
}
