import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';

/** Publishes GraphQL blocks and its operations to local subsriber PubSub. */
@singleton()
export class BlockPubSubPublisher implements ItemProcessor<BlockNotification> {
    constructor(private readonly subscriberPubSub: SubscriberPubSub) {}

    async processItem(block: BlockNotification): Promise<void> {
        await Promise.all([
            this.subscriberPubSub.publish(subscriberTriggers.blocks, block),
            this.publishOperations(block),
        ]);
    }

    private async publishOperations(block: BlockNotification): Promise<void> {
        // The order of operations within a block is important e.g. a transaction cannot precede a reveal operation to an empty address.
        for (const operation of block.operations) {
            const trigger = subscriberTriggers.blockOperations[operation.kind];
            await this.subscriberPubSub.publish(trigger, operation);
        }
    }
}
