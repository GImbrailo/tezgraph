import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions/block-notification';
import { IterableProvider } from '../../../../utils/iterable/interfaces';
import { ExternalPubSub, externalTriggers } from '../../../../utils/pubsub/external-pub-sub';

@singleton()
export class BlockPubSubProvider implements IterableProvider<BlockNotification> {
    constructor(private readonly externalPubSub: ExternalPubSub) {}

    iterate(): AsyncIterableIterator<BlockNotification> {
        return this.externalPubSub.iterate([externalTriggers.blocks]);
    }
}
