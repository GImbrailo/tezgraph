import { singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { ItemProcessor } from '../../../../utils/iterable/interfaces';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';

/** Publishes GraphQL mempool operations groups to local subsriber PubSub. */
@singleton()
export class MempoolPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(private readonly subscriberPubSub: SubscriberPubSub) {}

    async processItem(operationGroup: MempoolOperationGroup): Promise<void> {
        // The order of operations within a block is important e.g. a transaction cannot precede a reveal operation to an empty address.
        for (const operation of operationGroup) {
            const trigger = subscriberTriggers.mempoolOperations[operation.kind];
            await this.subscriberPubSub.publish(trigger, operation);
        }
    }
}
