import { singleton } from 'tsyringe';

import { getSignature, MempoolOperationGroup } from '../../../../entity/subscriptions/operation-notification';
import { InfoProvider } from '../../../../utils/iterable/interfaces';

/** Collects generic diagnostic info about a mempool operation group. */
@singleton()
export class MempoolOperationGroupInfoProvider implements InfoProvider<MempoolOperationGroup> {
    getInfo(group: MempoolOperationGroup): unknown {
        return {
            signature: getSignature(group),
            operationCount: group.length,
        };
    }
}
