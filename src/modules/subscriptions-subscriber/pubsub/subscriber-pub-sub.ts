import { PubSub } from 'graphql-subscriptions';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../entity/subscriptions/operation-notification';
import { asReadonly } from '../../../utils/conversion';
import { injectLogger, Logger } from '../../../utils/logging';
import { PubSubTrigger, TypedPubSub } from '../../../utils/pubsub/typed-pub-sub';

/** Strongly-typed triggers for particular payloads. */
export const subscriberTriggers = {
    blocks: new PubSubTrigger<BlockNotification>('BLOCKS'),

    // Separate mempool b/c it's not included by default -> don't evaluate all consumers -> performance.
    blockOperations: getTriggerForEachKind('BLOCK_OPERATIONS'),
    mempoolOperations: getTriggerForEachKind('MEMPOOL_OPERATIONS'),
} as const;

function getTriggerForEachKind(namePrefix: string): Readonly<Record<OperationKind, PubSubTrigger<OperationNotification>>> {
    const result: Record<string, PubSubTrigger<OperationNotification>> = {};
    for (const kind of Object.keys(OperationKind)) {
        result[kind] = new PubSubTrigger<OperationNotification>(`${namePrefix}:${kind}`);
    }
    return asReadonly(result);
}

/** Local memory PubSub used only by this module in order to multiply operations per kind. */
@singleton()
export class SubscriberPubSub extends TypedPubSub {
    constructor(@injectLogger(SubscriberPubSub) logger: Logger) {
        super(new PubSub(), logger);
    }
}
