import { DependencyContainer, InjectionToken } from 'tsyringe';

/** Names declared here to avoid circular references. */
export enum ModuleName {
    QueriesGraphQL = 'QueriesGraphQL',
    SubscriptionsSubscriber = 'SubscriptionsSubscriber',
    TezosMonitor = 'TezosMonitor',
    MemoryPubSub = 'MemoryPubSub',
    RedisPubSub = 'RedisPubSub',
}

export enum CompositeModuleName {
    SubscriptionsGraphQL = 'SubscriptionsGraphQL',
}

/** Base class to express some dependency between modules. */
export interface ModuleDependency {
    validate(thisModuleName: ModuleName, enabledModuleNames: readonly ModuleName[]): void;
}

/** Application module with isolated logic which can be turned on/off. */
export interface Module {
    readonly name: ModuleName;
    readonly dependencies: readonly ModuleDependency[];

    initialize(diContainer: DependencyContainer): void;
}

/** Makes configuration easier for common admins so that they don not need to know all modules and dependencies between them. */
export class CompositeModule {
    constructor(
        readonly name: CompositeModuleName,
        readonly mappedModuleNames: readonly ModuleName[],
    ) {}

    static create(name: CompositeModuleName, modules: readonly Module[]): CompositeModule {
        return new CompositeModule(name, modules.map(m => m.name));
    }
}

export const modulesDIToken: InjectionToken<Module> = 'Modules';
export const compositeModulesDIToken: InjectionToken<CompositeModule> = 'CompositeModules';
