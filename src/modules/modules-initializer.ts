import { DependencyContainer, inject, injectAll, singleton } from 'tsyringe';

import { EnvConfig } from '../utils/configuration/env-config';
import { diContainerDIToken } from '../utils/dependency-injection';
import { CompositeModule, compositeModulesDIToken, Module, modulesDIToken } from './module';
import { modulesErrorPrefix } from './module-helpers';

@singleton()
export class ModulesInitializer {
    constructor(
        private readonly envConfig: EnvConfig,
        @injectAll(modulesDIToken) private readonly modules: Module[],
        @injectAll(compositeModulesDIToken) private readonly compositeModules: CompositeModule[],
        @inject(diContainerDIToken) private readonly container: DependencyContainer,
    ) {
        const moduleNames = [...modules, ...compositeModules].map(m => m.name.toString());
        const duplicateModuleName = moduleNames.find(n1 => moduleNames.filter(n2 => n1 === n2).length > 1);
        if (duplicateModuleName) {
            throw new Error(`The app has multiple modules registered with name '${duplicateModuleName}'.`);
        }
    }

    initializeConfiguredModules(): void {
        const strNamesToEnable = this.determineModuleNamesToEnable();
        const modulesToEnable = strNamesToEnable.map(n => this.findModule(n));
        const namesToEnable = modulesToEnable.map(m => m.name);

        for (const module of modulesToEnable) {
            for (const dependency of module.dependencies) {
                dependency.validate(module.name, namesToEnable);
            }
            module.initialize(this.container);
        }
    }

    private determineModuleNamesToEnable(): string[] {
        const namesToEnable = new Set(this.envConfig.modules);

        for (const compositeModule of this.compositeModules) {
            if (namesToEnable.delete(compositeModule.name)) {
                compositeModule.mappedModuleNames.forEach(n => namesToEnable.add(n));
            }
        }
        return Array.from(namesToEnable);
    }

    private findModule(name: string): Module {
        const module = this.modules.find(m => m.name === name);
        if (!module) {
            const allModuleNames = [...this.modules, ...this.compositeModules].map(m => m.name).sort();
            throw new Error(`${modulesErrorPrefix} Module '${name}' does not exist so it cannot be enabled.`
                + ` Existing modules: ${allModuleNames.join(', ')}.`);
        }
        return module;
    }
}
