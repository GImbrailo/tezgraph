drop function address;
drop function address_id;
drop function block_hash;
drop function block_hash_id;
drop function get_endorsement;
drop function get_transaction;
drop function get_origination;
drop function get_reveal;
drop function get_delegation;
drop function get_operations;
drop function get_endorsement_neighbour_greater;
drop function get_endorsement_neighbour_lesser;
drop function get_transaction_neighbour_greater;
drop function get_transaction_neighbour_lesser;
drop function get_origination_neighbour_greater;
drop function get_origination_neighbour_lesser;
drop function get_reveal_neighbour_greater;
drop function get_reveal_neighbour_lesser;
drop function get_delegation_neighbour_greater;
drop function get_delegation_neighbour_lesser;
drop function get_operations_neighbour_greater;
drop function get_operations_neighbour_lesser;

CREATE INDEX IF NOT EXISTS endorsement_delegate_id ON C.endorsement USING btree (delegate_id); 

CREATE OR REPLACE FUNCTION address(id BIGINT)
RETURNS CHAR
AS $$
SELECT address FROM C.addresses WHERE address_id = id;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION address_id(a CHAR)
RETURNS BIGINT
AS $$
SELECT address_id FROM C.addresses WHERE address = a;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION block_hash(id INT) RETURNS CHAR AS $$ SELECT hash FROM C.block_hash WHERE hash_id = id $$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION block_hash_id(h CHAR) RETURNS INT AS $$ SELECT hash_id FROM C.block_hash WHERE hash = h $$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_endorsement (address VARCHAR)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'endorsement' AS kind, -- kind
    oa.autoid::BIGINT AS id, -- id
    b.level AS level, -- level
    b.timestamp AS timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash AS hash, -- hash
    address(e.delegate_id) AS source, -- source
    CAST(NULL AS BIGINT) AS fee, -- fee
    CAST(NULL AS NUMERIC) AS counter, -- counter
    CAST(NULL AS NUMERIC) AS gas_limit, -- gas_limit
    CAST(NULL AS NUMERIC) AS storage_limit, -- storage_limit
    oa.id::SMALLINT AS op_id, -- op_id
    oa.internal AS internal, -- internal
    CAST(NULL AS INTEGER) AS nonce, -- nonce
    CAST(NULL AS CHAR) AS public_key, -- public_key
    CAST(NULL AS BIGINT) AS amount, -- amount
    CAST(NULL AS CHAR) AS destination, -- destination
    CAST(NULL AS CHAR) AS parameters, -- parameters
    e.slots::SMALLINT[] AS slots, -- slots
    CAST(NULL AS CHAR) AS entrypoint, -- entrypoint
    CAST(NULL AS CHAR) AS contact_address, -- contract_address
    address(e.delegate_id) AS delegate, -- delegate
    CAST(NULL AS NUMERIC) AS consumed_milligas -- consumed_milligas
FROM 
    C.endorsement e,
    C.operation_alpha oa,
    C.operation op,
    C.block b
WHERE
    e.delegate_id = address_id(address)
AND 
    e.operation_id = oa.autoid
AND 
    oa.autoid = op.hash_id
AND 
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_transaction (address VARCHAR)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'transaction', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(t.source_id), -- source
    t.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    t.nonce, -- nonce
    NULL, -- public_key
    t.amount, -- amount
    address(t.destination_id), -- destination
    t."parameters", -- parameters
    CAST (NULL AS SMALLINT[]) AS slots, -- slots
    t.entrypoint, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    t.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.tx t,
    C.manager_numbers m,
    C.operation op,
    C.block b
WHERE 
    (address_id(address) = t.destination_id or address_id(address) = t.source_id)
AND 
    oa.autoid = t.operation_id
AND 
    op.hash_id = oa.hash_id
AND 
    m.operation_id = oa.autoid
AND 
    b.hash_id = op.block_hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_origination (address VARCHAR)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'origination', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash, -- hash
    address(o.source_id), -- source
    o.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    o.nonce, -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    address(o.k_id), -- contract_address
    NULL, -- delegate
    o.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.origination o,
    C.block b,
    C.operation op
WHERE 
    (address_id(address) = o.source_id or address_id(address) = o.k_id)
AND
    o.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    m.operation_id = oa.autoid
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_reveal (address VARCHAR)
RETURNS TABLE(
 kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'reveal', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address, -- source
    r.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    r.nonce, -- nonce
    r.pk, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    r.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.block b,
    C.manager_numbers m,
    C.reveal r,
    C.operation op
WHERE
    r.source_id = address_id(address)
AND
    r.operation_id = oa.autoid
AND
    op.hash_id = oa.hash_id
AND
    b.hash_id = op.block_hash_id
AND
    m.operation_id = oa.autoid
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_delegation (address VARCHAR)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'delegation', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(d.source_id), -- source
    d.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    d.nonce,  -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    address(pkh_id), -- delegate
    d.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.delegation d,
    C.block b,
    C.operation op
WHERE
    (address_id(address) = d.pkh_id or address_id(address) = d.source_id)
AND
    d.operation_id = oa.autoid
AND
    m.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_operations (address VARCHAR)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
(
    (select * from get_origination(address))
UNION
    (select * from get_reveal(address))
UNION
    (select * from get_transaction(address))
UNION
    (select * from get_delegation(address))
UNION
    (select * from get_endorsement(address))
ORDER BY id
)
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_endorsement_neighbour_greater (address VARCHAR, e_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'endorsement' AS kind, -- kind
    oa.autoid::BIGINT AS id, -- id
    b.level AS level, -- level
    b.timestamp AS timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash AS hash, -- hash
    address(e.delegate_id) AS source, -- source
    CAST(NULL AS BIGINT) AS fee, -- fee
    CAST(NULL AS NUMERIC) AS counter, -- counter
    CAST(NULL AS NUMERIC) AS gas_limit, -- gas_limit
    CAST(NULL AS NUMERIC) AS storage_limit, -- storage_limit
    oa.id::SMALLINT AS op_id, -- op_id
    oa.internal AS internal, -- internal
    CAST(NULL AS INTEGER) AS nonce, -- nonce
    CAST(NULL AS CHAR) AS public_key, -- public_key
    CAST(NULL AS BIGINT) AS amount, -- amount
    CAST(NULL AS CHAR) AS destination, -- destination
    CAST(NULL AS CHAR) AS parameters, -- parameters
    e.slots::SMALLINT[] AS slots, -- slots
    CAST(NULL AS CHAR) AS entrypoint, -- entrypoint
    CAST(NULL AS CHAR) AS contact_address, -- contract_address
    address(e.delegate_id) AS delegate, -- delegate
    CAST(NULL AS NUMERIC) AS consumed_milligas -- consumed_milligas
FROM 
    C.endorsement e,
    C.operation_alpha oa,
    C.operation op,
    C.block b
WHERE
    e.delegate_id = address_id(address)
AND 
    oa.autoid >= e_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND 
    e.operation_id = oa.autoid
AND 
    oa.autoid = op.hash_id
AND 
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_endorsement_neighbour_lesser (address VARCHAR, e_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'endorsement' AS kind, -- kind
    oa.autoid::BIGINT AS id, -- id
    b.level AS level, -- level
    b.timestamp AS timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash AS hash, -- hash
    address(e.delegate_id) AS source, -- source
    CAST(NULL AS BIGINT) AS fee, -- fee
    CAST(NULL AS NUMERIC) AS counter, -- counter
    CAST(NULL AS NUMERIC) AS gas_limit, -- gas_limit
    CAST(NULL AS NUMERIC) AS storage_limit, -- storage_limit
    oa.id::SMALLINT AS op_id, -- op_id
    oa.internal AS internal, -- internal
    CAST(NULL AS INTEGER) AS nonce, -- nonce
    CAST(NULL AS CHAR) AS public_key, -- public_key
    CAST(NULL AS BIGINT) AS amount, -- amount
    CAST(NULL AS CHAR) AS destination, -- destination
    CAST(NULL AS CHAR) AS parameters, -- parameters
    e.slots::SMALLINT[] AS slots, -- slots
    CAST(NULL AS CHAR) AS entrypoint, -- entrypoint
    CAST(NULL AS CHAR) AS contact_address, -- contract_address
    address(e.delegate_id) AS delegate, -- delegate
    CAST(NULL AS NUMERIC) AS consumed_milligas -- consumed_milligas
FROM 
    C.endorsement e,
    C.operation_alpha oa,
    C.operation op,
    C.block b
WHERE
    e.delegate_id = address_id(address)
AND 
    oa.autoid <= e_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND 
    e.operation_id = oa.autoid
AND 
    oa.autoid = op.hash_id
AND 
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_transaction_neighbour_greater (address VARCHAR, t_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'transaction', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(t.source_id), -- source
    t.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    t.nonce, -- nonce
    NULL, -- public_key
    t.amount, -- amount
    address(t.destination_id), -- destination
    t."parameters", -- parameters
    CAST (NULL AS SMALLINT[]) AS slots, -- slots
    t.entrypoint, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    t.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.tx t,
    C.manager_numbers m,
    C.operation op,
    C.block b
WHERE 
    (address_id(address) = t.destination_id or address_id(address) = t.source_id)
AND 
    oa.autoid >= t_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND 
    oa.autoid = t.operation_id
AND 
    op.hash_id = oa.hash_id
AND 
    m.operation_id = oa.autoid
AND 
    b.hash_id = op.block_hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_transaction_neighbour_lesser (address VARCHAR, t_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'transaction', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(t.source_id), -- source
    t.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    t.nonce, -- nonce
    NULL, -- public_key
    t.amount, -- amount
    address(t.destination_id), -- destination
    t."parameters", -- parameters
    CAST (NULL AS SMALLINT[]) AS slots, -- slots
    t.entrypoint, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    t.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.tx t,
    C.manager_numbers m,
    C.operation op,
    C.block b
WHERE 
    (address_id(address) = t.destination_id or address_id(address) = t.source_id)
AND 
    oa.autoid <= t_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND 
    oa.autoid = t.operation_id
AND 
    op.hash_id = oa.hash_id
AND 
    m.operation_id = oa.autoid
AND 
    b.hash_id = op.block_hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_origination_neighbour_greater (address VARCHAR, or_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'origination', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash, -- hash
    address(o.source_id), -- source
    o.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    o.nonce, -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    address(o.k_id), -- contract_address
    NULL, -- delegate
    o.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.origination o,
    C.block b,
    C.operation op
WHERE 
    (address_id(address) = o.source_id or address_id(address) = o.k_id)
AND 
    oa.autoid >= or_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    o.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    m.operation_id = oa.autoid
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION get_origination_neighbour_lesser (address VARCHAR, or_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'origination', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id) AS block, -- block
    op.hash, -- hash
    address(o.source_id), -- source
    o.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    o.nonce, -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    address(o.k_id), -- contract_address
    NULL, -- delegate
    o.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.origination o,
    C.block b,
    C.operation op
WHERE 
    (address_id(address) = o.source_id or address_id(address) = o.k_id)
AND 
    oa.autoid <= or_id  
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    o.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    m.operation_id = oa.autoid
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_reveal_neighbour_greater (address VARCHAR, r_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
 kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'reveal', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address, -- source
    r.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    r.nonce, -- nonce
    r.pk, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    r.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.block b,
    C.manager_numbers m,
    C.reveal r,
    C.operation op
WHERE
    r.source_id = address_id(address)
AND 
    oa.autoid >= r_id 
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    r.operation_id = oa.autoid
AND
    op.hash_id = oa.hash_id
AND
    b.hash_id = op.block_hash_id
AND
    m.operation_id = oa.autoid
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_reveal_neighbour_lesser (address VARCHAR, r_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
 kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'reveal', -- kind
    oa.autoid, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address, -- source
    r.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    r.nonce, -- nonce
    r.pk, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    NULL, -- delegate
    r.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.block b,
    C.manager_numbers m,
    C.reveal r,
    C.operation op
WHERE
    r.source_id = address_id(address)
AND 
    oa.autoid <= r_id 
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    r.operation_id = oa.autoid
AND
    op.hash_id = oa.hash_id
AND
    b.hash_id = op.block_hash_id
AND
    m.operation_id = oa.autoid
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_delegation_neighbour_greater (address VARCHAR, d_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'delegation', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(d.source_id), -- source
    d.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    d.nonce,  -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    address(pkh_id), -- delegate
    d.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.delegation d,
    C.block b,
    C.operation op
WHERE
    (address_id(address) = d.pkh_id or address_id(address) = d.source_id)
AND 
    oa.autoid >= d_id 
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    d.operation_id = oa.autoid
AND
    m.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_delegation_neighbour_lesser (address VARCHAR, d_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
SELECT 
    'delegation', -- kind
    oa.autoid::BIGINT, -- id
    b.level, -- level
    b.timestamp, -- timestamp
    block_hash(b.hash_id), -- block
    op.hash, -- hash
    address(d.source_id), -- source
    d.fee, -- fee
    m.counter, -- counter
    m.gas_limit, -- gas_limit
    m.storage_limit, -- storage_limit
    oa.id, -- op_id
    oa.internal, -- internal
    d.nonce,  -- nonce
    NULL, -- public_key
    CAST(NULL AS BIGINT), -- amount
    NULL, -- destination
    NULL, -- parameters
    CAST(NULL AS SMALLINT[]), -- slots
    NULL, -- entrypoint
    NULL, -- contract_address
    address(pkh_id), -- delegate
    d.consumed_milligas -- consumed_milligas
FROM 
    C.operation_alpha oa,
    C.manager_numbers m,
    C.delegation d,
    C.block b,
    C.operation op
WHERE
    (address_id(address) = d.pkh_id or address_id(address) = d.source_id)
AND 
    oa.autoid <= d_id 
AND 
    ((op.hash >= o_hash AND oa.id > o_id) 
    OR 
    (op.hash != o_hash))
AND
    d.operation_id = oa.autoid
AND
    m.operation_id = oa.autoid
AND
    oa.hash_id = op.hash_id
AND
    op.block_hash_id = b.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_operations_neighbour_greater (address VARCHAR, or_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
((
    SELECT * FROM get_operations(address)
    WHERE
        id >= or_id 
    AND 
        ((hash >= o_hash AND op_id > o_id) 
        OR 
        (hash != o_hash))))
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION get_operations_neighbour_lesser (address VARCHAR, or_id BIGINT, o_hash CHAR, o_id SMALLINT)
RETURNS TABLE(
    kind TEXT,
    id BIGINT,
    level INTEGER,
    "timestamp" TIMESTAMP,
    block CHAR,
    hash CHAR,
    source CHAR,
    fee BIGINT,
    counter NUMERIC,
    gas_limit NUMERIC,
    storage_limit NUMERIC,
    op_id SMALLINT,
    internal SMALLINT,
    nonce INTEGER,
    public_key CHAR,
    amount BIGINT,
    destination CHAR,
    "parameters" CHAR,
    "slots" SMALLINT[],
    entrypoint CHAR,
    contract_address CHAR,
    delegate CHAR,
    consumed_milligas NUMERIC
)
AS $$
((
    SELECT * FROM get_operations(address)
    WHERE
        id <= or_id 
    AND 
        ((hash >= o_hash AND op_id > o_id) 
        OR 
        (hash != o_hash))))
$$ LANGUAGE SQL STABLE;
