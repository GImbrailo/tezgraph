/* eslint-disable max-params */
/* eslint-disable no-warning-comments */
import { injectable } from 'tsyringe';
import { Arg, ClassType, FieldResolver, Query, Resolver, Root } from 'type-graphql';

import { Account } from '../../../entity/account';
import { Delegation } from '../../../entity/delegation';
import { Endorsement } from '../../../entity/endorsement';
import { Operation } from '../../../entity/operation';
import { Origination } from '../../../entity/origination';
import { Reveal } from '../../../entity/reveal';
import { scalars } from '../../../entity/scalars';
import { Transaction } from '../../../entity/transaction';
import { injectLogger, Logger } from '../../../utils/logging';
import DelegationRepository from '../repositories/delegation';
import EndorsementRepository from '../repositories/endorsement';
import OperationAlphaRepository from '../repositories/operation-alpha';
import { BaseOperationData } from '../repositories/operation.utils';
import OriginationRepository from '../repositories/origination';
import RevealRepository from '../repositories/reveal';
import TransactionRepository from '../repositories/transaction';
import { IndexerOperationKind } from './types/indexer-operation-kind';

type OperationSubclasses = Endorsement | Reveal | Delegation | Transaction | Origination;

const operationKindToEntityMap: Partial<Record<IndexerOperationKind, ClassType<OperationSubclasses>>> = {
    [IndexerOperationKind.Endorsement]: Endorsement,
    [IndexerOperationKind.Reveal]: Reveal,
    [IndexerOperationKind.Delegation]: Delegation,
    [IndexerOperationKind.Transaction]: Transaction,
    [IndexerOperationKind.Origination]: Origination,
};

@injectable()
@Resolver(() => Operation)
export class OperationResolver {
    constructor(
        private readonly endorsementRepository: EndorsementRepository,
        private readonly revealRepository: RevealRepository,
        private readonly delegationRepository: DelegationRepository,
        private readonly originationRepository: OriginationRepository,
        private readonly transactionRepository: TransactionRepository,
        private readonly operationAlphaRepository: OperationAlphaRepository,
        @injectLogger(OperationResolver) private readonly logger: Logger,
    ) {}

    @Query(() => [Operation])
    async operations(
        @Arg('hash', () => scalars.OperationHash) operationHash: string,
    ): Promise<OperationSubclasses[]> {
        this.logger.logInformation('operation query: {operationHash}', { operationHash });

        const operationAlphasData = await this.operationAlphaRepository.findManyByOperationHash(operationHash);
        if (operationAlphasData.length === 0) {
            this.logger.logInformation('No OperationAlpha rows found for {operationHash}.', { operationHash });
            return [];
        }
        if (operationAlphasData.some(it => !it.operation_sender_and_receiver)) {
            throw new Error(`Missing operation_sender_and_receiver for operationHash=${operationHash}`);
        }
        const maybeOperations = await Promise.all(
            operationAlphasData.map(
                async data => this.mapOperationAlphaToOperationData(
                    data.autoid,
                    data.operation_kind,
                    {
                        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                        senderAddress: data.operation_sender_and_receiver!.addresses_addressesTooperation_sender_and_receiver_sender_id.address,
                        operationHash,
                    },
                ),
            ),
        );
        return maybeOperations.filter(Boolean) as OperationSubclasses[];
    }

    @FieldResolver(() => Account, {
        nullable: true,
        // eslint-disable-next-line max-len
        description: 'The implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.',
    })
    source(@Root() operation: Operation): Account | undefined {
        if (!operation.sourceAddress) {
            return;
        }
        return { address: operation.sourceAddress };
    }

    private async mapOperationAlphaToOperationData(
        operationAlphaAutoId: bigint,
        operationKind: IndexerOperationKind,
        data: BaseOperationData,
    ): Promise<OperationSubclasses | undefined> {
        const operationData = await this.findSpecificOperationData(operationAlphaAutoId, operationKind, data);
        const OperationEntity = operationKindToEntityMap[operationKind];
        if (!operationData || !OperationEntity) {
            return;
        }
        return Object.assign(new OperationEntity(), operationData);
    }

    private async findSpecificOperationData(
        operationAlphaAutoId: bigint,
        operationKind: IndexerOperationKind,
        data: BaseOperationData,
    ): Promise<OperationSubclasses | undefined> {
        switch (operationKind) {
            case IndexerOperationKind.Endorsement: {
                return this.endorsementRepository.findOneByOperationAlpha(operationAlphaAutoId, data);
            }
            case IndexerOperationKind.Reveal: {
                return this.revealRepository.findOneByOperationAlpha(operationAlphaAutoId, data);
            }
            case IndexerOperationKind.Delegation: {
                return this.delegationRepository.findOneByOperationAlpha(operationAlphaAutoId, data);
            }
            case IndexerOperationKind.Transaction: {
                return this.transactionRepository.findOneByOperationAlpha(operationAlphaAutoId, data);
            }
            case IndexerOperationKind.Origination: {
                return this.originationRepository.findOneByOperationAlpha(operationAlphaAutoId, data);
            }
            default: {
                throw new Error(`Unsupported operation kind: '${operationKind}'.`);
            }
        }
    }
}
