import { registerEnumType } from 'type-graphql';

export enum IndexerOperationKind {
    Endorsement = 0,
    Seed_Nonce_Relevation = 1,
    Double_Endorsement_Evidence = 2,
    Double_Baking_Evidence = 3,
    Activate_Account = 4,
    Proposals = 5,
    Ballot = 6,
    Reveal = 7,
    Transaction = 8,
    Origination = 9,
    Delegation = 10,
}

registerEnumType(IndexerOperationKind, { name: 'IndexerOperationKind' });
