/* eslint-disable max-lines */
/* eslint-disable max-len */
/* eslint-disable max-lines-per-function */
import { UserInputError } from 'apollo-server';
import { performance } from 'perf_hooks';
import { injectable } from 'tsyringe';
import { Resolver, Query, Args, FieldResolver, Root, Ctx } from 'type-graphql';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { Account, AccountOperationsArgs, AccountArgs } from '../../../entity/account';
import { DelegationConnection } from '../../../entity/delegation';
import { EndorsementConnection } from '../../../entity/endorsement';
import { OperationsConnection } from '../../../entity/operations';
import { OriginationConnection } from '../../../entity/origination';
import { RevealConnection } from '../../../entity/reveal';
import { TransactionConnection } from '../../../entity/transaction';
import { injectLogger, Logger } from '../../../utils/logging';
import AccountRepository from '../repositories/account-repository';
import ConnectionUtils from '../repositories/connection.utils';
import OrderByUtils from '../repositories/order-by.utils';
import { createRelayPageData, queryResultsFound } from '../utils';

@injectable()
@Resolver(() => Account)
export class AccountResolver {
    constructor(
        private readonly accountRepository: AccountRepository,
        private readonly connectionUtils: ConnectionUtils,
        private readonly orderByUtils: OrderByUtils,
        @injectLogger(AccountResolver) private readonly logger: Logger,
    ) { }

    @Query(() => Account)
    account(
        @Args() { address }: AccountArgs,
    ): Account {
        const account: Account = {
            address,
        };

        return account;
    }

    @FieldResolver(() => String, { nullable: true })
    async public_key(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
    ): Promise<string | undefined> {
        return this.accountRepository.getRevealedPublicKey(address, requestId);
    }

    @FieldResolver(() => Date, { nullable: true })
    async activated(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
    ): Promise<Date | undefined> {
        return this.accountRepository.getActivated(address, requestId);
    }

    @FieldResolver(() => OriginationConnection, { nullable: true })
    async originations(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<OriginationConnection> {
        this.logger.logInformation(`Originations Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(
                    `Argument Error - before/after: Cannot use both "before" and "after" arguments.`
                    + ` Please use "before" to paginate backwards and "after" to paginate forwards.`,
                );
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);

            const relayPageData = createRelayPageData(first, last, before, after);

            const originations = await this.accountRepository.getOriginations(
                requestId,
                address,
                first,
                last,
                before,
                after,
                dateRange,
                orderByObj,
                relayPageData,
            );
            queryResultsFound(originations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Originations Query ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('origination', originations, relayPageData, address, requestId, orderBy, dateRange) as OriginationConnection;
        } catch (err: unknown) {
            this.logger.logError(`Originations Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }

    @FieldResolver(() => RevealConnection, { nullable: true })
    async reveals(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<RevealConnection> {
        this.logger.logInformation(`Reveals Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(
                    `Argument Error - before/after: Cannot use both "before" and "after" arguments.`
                    + ` Please use "before" to paginate backwards and "after" to paginate forwards.`,
                );
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);

            const relayPageData = createRelayPageData(first, last, before, after);

            const reveals = await this.accountRepository.getReveals(
                requestId,
                address,
                first,
                last,
                before,
                after,
                dateRange,
                orderByObj,
                relayPageData,
            );
            queryResultsFound(reveals);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Reveals Query  ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('reveal', reveals, relayPageData, address, requestId, orderBy, dateRange) as RevealConnection;
        } catch (err: unknown) {
            this.logger.logError(`Reveals Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }

    @FieldResolver(() => OperationsConnection, { nullable: true })
    async operations(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<OperationsConnection> {
        this.logger.logInformation(`Operations Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);
            const relayPageData = createRelayPageData(first, last, before, after);

            const operations = await this.accountRepository.getOperations(
                requestId,
                address,
                first,
                last,
                before,
                after,
                dateRange,
                orderByObj,
                relayPageData,
            );
            queryResultsFound(operations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Operations Query  ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('operations', operations, relayPageData, address, requestId, orderBy, dateRange) as OperationsConnection;
        } catch (err: unknown) {
            this.logger.logError(`Operations Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }

    @FieldResolver(() => TransactionConnection, { nullable: true })
    async transactions(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<TransactionConnection> {
        this.logger.logInformation(`Transactions Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);

            const relayPageData = createRelayPageData(first, last, before, after);

            const transactions = await this.accountRepository.getTransactions(
                requestId,
                address,
                first,
                last,
                before,
                after,
                dateRange,
                orderByObj,
                relayPageData,
            );
            queryResultsFound(transactions);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Transactions Query  ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('transaction', transactions, relayPageData, address, requestId, orderBy, dateRange) as TransactionConnection;
        } catch (err: unknown) {
            this.logger.logError(`Transactions Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }

    @FieldResolver(() => DelegationConnection, { nullable: true })
    async delegations(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<DelegationConnection> {
        this.logger.logInformation(`Delegations Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);

            const relayPageData = createRelayPageData(first, last, before, after);

            const delegations = await this.accountRepository.getDelegations(
                requestId,
                address,
                first,
                last,
                before,
                after,
                dateRange,
                orderByObj,
                relayPageData,
            );
            queryResultsFound(delegations);

            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Delegations Query  ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('delegation', delegations, relayPageData, address, requestId, orderBy, dateRange) as DelegationConnection;
        } catch (err: unknown) {
            this.logger.logError(`Delegations Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }

    @FieldResolver(() => EndorsementConnection, { nullable: true })
    async endorsements(
        @Root() { address }: Account,
            @Ctx() { requestId }: ResolverContext,
            @Args() { first, last, before, after, date_range: dateRange, order_by: orderBy }: AccountOperationsArgs,
    ): Promise<EndorsementConnection> {
        this.logger.logInformation(`Endorsements Query started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();

        try {
            if (before && after) {
                throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
            }

            const orderByObj = this.orderByUtils.createOrderBy(orderBy?.field, orderBy?.direction);

            const relayPageData = createRelayPageData(first, last, before, after);

            const endorsements = await this.accountRepository.getEndorsements(requestId, address, first, last, before, after, dateRange, orderByObj, relayPageData);
            queryResultsFound(endorsements);
            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`Endorsements Query  ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return await this.connectionUtils.getConnection('endorsement', endorsements, relayPageData, address, requestId, orderBy, dateRange) as EndorsementConnection;
        } catch (err: unknown) {
            this.logger.logError(`Endorsements Query failed - with {error} for {requestId}`, { error: err, requestId });
            throw err;
        }
    }
}
