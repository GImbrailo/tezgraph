/* eslint-disable no-warning-comments */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { PrismaClient, Prisma } from '@prisma/client';
import { merge } from 'lodash';
import { singleton } from 'tsyringe';

import { DelegationData } from '../../../entity/delegation';
import { OperationKind } from '../../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { BaseOperationData, managerNumbersFindOptions, OperationUtils } from './operation.utils';

@singleton()
export default class DelegationRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly operationUtils: OperationUtils,
        @injectLogger(DelegationRepository) private readonly logger: Logger,
    ) { }

    async findOneByOperationAlpha(operationAlphaAutoId: bigint, data: BaseOperationData): Promise<DelegationData | undefined> {
        const result = await this.prisma.delegation.findUnique(
            merge(
                this.operationUtils.getBaseOperationFindOptions(operationAlphaAutoId),
                managerNumbersFindOptions,
                Prisma.validator<Prisma.delegationFindFirstArgs>()({
                    select: {
                        consumed_milligas: true,
                        addresses_addressesTodelegation_pkh_id: {
                            select: {
                                address: true,
                            },
                        },
                    },
                }),
            ),
        );
        if (!result) {
            this.logger.logWarning('No Delegation found for {operationAlphaAutoId}.', { operationAlphaAutoId });
            return;
        }

        return {
            kind: OperationKind.delegation,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result, data),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            consumed_milligas: result.consumed_milligas,
            delegate: result.addresses_addressesTodelegation_pkh_id?.address,
        };
    }
}
