/* eslint-disable max-params */
/* eslint-disable no-warning-comments */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { PrismaClient, Prisma } from '@prisma/client';
import { merge } from 'lodash';
import { singleton } from 'tsyringe';

import { EndorsementData } from '../../../entity/endorsement';
import { OperationKind } from '../../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { BaseOperationData, OperationUtils } from './operation.utils';

@singleton()
export default class EndorsementRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly operationUtils: OperationUtils,
        @injectLogger(EndorsementRepository) private readonly logger: Logger,
    ) { }

    async findOneByOperationAlpha(operationAlphaAutoId: bigint, data: BaseOperationData): Promise<EndorsementData | undefined> {
        const result = await this.prisma.endorsement.findUnique(
            merge(
                this.operationUtils.getBaseOperationFindOptions(operationAlphaAutoId),
                Prisma.validator<Prisma.endorsementFindFirstArgs>()({
                    select: {
                        slots: true,
                        addresses: { select: { address: true } },
                    },
                }),
            ),
        );
        if (!result) {
            this.logger.logWarning('No Endorsement found for {operationAlphaAutoId}.', { operationAlphaAutoId });
            return;
        }

        return {
            kind: OperationKind.endorsement,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result, data),
            delegate: result.addresses?.address,
            slots: result.slots,
        };
    }
}
