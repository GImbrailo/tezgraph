/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable lines-around-comment */
/* eslint-disable no-param-reassign */
/* eslint-disable func-style */

import { UserInputError } from 'apollo-server';
import { format } from 'fecha';
import { performance } from 'perf_hooks';
import { singleton } from 'tsyringe';
import { InputType, Field } from 'type-graphql';

import { errorToString } from '../../../utils/conversion';
import { injectLogger, Logger } from '../../../utils/logging';

// DateRange Argument Fields
@InputType()
export class DateRange {
    @Field({ nullable: true })
    gte?: string;

    @Field({ nullable: true })
    lte?: string;
}

@singleton()
export default class DateRangeUtils {
    constructor(
        @injectLogger(DateRangeUtils) private readonly logger: Logger,
    ) { }

    // Takes a DateRange object and turns it into a SQL where clause string.
    getDateRangeClause(dateRange: DateRange | undefined, requestId: string): string {
        this.logger.logInformation(`GetDateRangeClause started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();
        try {
            let dateRangeString = '';
            if (dateRange) {
                dateRange = this.dateRangeFormatter(dateRange);
                this.checkDateRangeValid(dateRange);
                dateRangeString = this.buildDateRangeClause(dateRange);
            }
            const performanceEnd = performance.now();
            const duration = `${performanceEnd - performanceStart} ms`;
            this.logger.logInformation(`GetDateRangeClause ended - for {requestId}, {duration}.`, {
                requestId,
                duration,
            });

            return dateRangeString;
        } catch (err: unknown) {
            const errString = errorToString(err);
            // eslint-disable-next-line max-len
            this.logger.logError(`GetDateRangeClause failed - with {error} for {requestId}`, { error: err, requestId });
            throw new Error(errString);
        }
    }

    // Takes a DateRange object and formats the timestamps, allowing us to understand different timestamp formats provided by the user.
    dateRangeFormatter(dateRange: DateRange): DateRange {
        const gte = dateRange.gte ? this.dateRangeTimestampFormatter(dateRange.gte) : undefined;
        const lte = dateRange.lte ? this.dateRangeTimestampFormatter(dateRange.lte) : undefined;
        const formattedDateRange: DateRange = {
            gte,
            lte,
        };
        return formattedDateRange;
    }

    dateRangeTimestampFormatter(timestamp: string): string {
        const formattedTimestamp = this.dateRangeTimestampCleanUp(timestamp);
        let isoDateTime = this.dateRangeTimestampFormat(formattedTimestamp, 'isoDateTime');
        isoDateTime = isoDateTime.substring(0, 19);
        return isoDateTime;
    }

    dateRangeTimestampFormat(timestamp: string, fmt: string): string {
        try {
            const timestampDate = new Date(timestamp);
            return format(timestampDate, fmt);
        } catch (err: unknown) {
            throw new Error(`The DateRange value ${timestamp} is in a invalid format.`);
        }
    }

    // DateRange Argument Validator
    checkDateRangeValid(dateObj: DateRange): void {
        if (dateObj.lte && dateObj.gte) {
            if (!(new Date(dateObj.gte) < new Date(dateObj.lte))) {
                throw new UserInputError('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
            }
        }
    }

    // Takes a DateRange object and turns it into a SQL where clause string.
    buildDateRangeClause(dateRange: DateRange): string {
        if (dateRange.lte && dateRange.gte) {
            return `WHERE (timestamp >= '${dateRange.gte}' AND timestamp <= '${dateRange.lte}')`;
        } else if (dateRange.lte) {
            return `WHERE (timestamp <= '${dateRange.lte}')`;
        } else if (dateRange.gte) {
            return `WHERE (timestamp >= '${dateRange.gte}')`;
        }
        throw new Error('Argument "date" of type "dateArg?" must be in a valid format.');
    }

    dateRangeTimestampCleanUp(timestamp: string): string {
        /*
         * This is used to handle the case in which the user only enters the year.
         * Depending on the timezone, the date may be changed to something different.
         * This way we ensure that we use the 01/01 of the year the user entered.
         */
        if (timestamp.length === 7 || timestamp.length === 4) {
            timestamp += '/01';
        }

        /*
         * This is used to handle the case in which the user only enters a date without time.
         * Depending on the timezone, the date may be changed to something different.
         * This way we ensure that we use the 00:00:00 of the date the user entered.
         */
        if (timestamp.length < 11) {
            try {
                timestamp = this.dateRangeTimestampFormat(timestamp, 'shortDate');
            } catch (err: unknown) {
                throw new Error(`The DateRange value ${timestamp} passed is in a invalid format.`);
            }
        }

        switch (true) {
            case timestamp.includes('PM'):
                timestamp = timestamp.replace('PM', ' PM');
                break;
            case timestamp.includes('pm'):
                timestamp = timestamp.replace('pm', ' pm');
                break;
            case timestamp.includes('AM'):
                timestamp = timestamp.replace('AM', ' AM');
                break;
            case timestamp.includes('am'):
                timestamp = timestamp.replace('am', ' am');
                break;
        }

        switch (true) {
            case timestamp.includes('Z'):
                timestamp = timestamp.replace('Z', '');
                break;
        }

        return timestamp;
    }
}
