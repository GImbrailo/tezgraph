import { PrismaClient, Prisma, reveal, addresses } from '@prisma/client';
import { merge } from 'lodash';
import { singleton } from 'tsyringe';

import { RevealData } from '../../../entity/reveal';
import { OperationKind } from '../../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { BaseOperationData, managerNumbersFindOptions, OperationUtils } from './operation.utils';

@singleton()
export default class RevealRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly operationUtils: OperationUtils,
        @injectLogger(RevealRepository) private readonly logger: Logger,
    ) { }

    async findOneByOperationAlpha(operationAlphaAutoId: bigint, data: BaseOperationData): Promise<RevealData | undefined> {
        const result = await this.prisma.reveal.findUnique(
            merge(
                this.operationUtils.getBaseOperationFindOptions(operationAlphaAutoId),
                managerNumbersFindOptions,
                Prisma.validator<Prisma.revealFindFirstArgs>()({
                    select: {
                        consumed_milligas: true,
                        pk: true,
                    },
                }),
            ),
        );
        if (!result) {
            this.logger.logWarning('No Reveal found for {operationAlphaAutoId}.', { operationAlphaAutoId });
            return;
        }

        return {
            kind: OperationKind.reveal,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result, data),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            public_key: result.pk,
            consumed_milligas: result.consumed_milligas,
        };
    }

    async findLatestByPKH(pkh: string): Promise<(addresses & { reveal: reveal[] }) | undefined> {
        const result = await this.prisma.addresses.findUnique({
            where: {
                address: pkh,
            },
            include: {
                reveal: {
                    orderBy: {
                        operation_id: 'desc',
                    },
                },
            },
        });

        if (!result?.reveal[0]) {
            this.logger.logWarning('No Reveal found for {pkh}.', { pkh });
            return undefined;
        }

        return result;
    }
}
