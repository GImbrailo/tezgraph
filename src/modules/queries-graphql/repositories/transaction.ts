/* eslint-disable no-warning-comments */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { Prisma, PrismaClient } from '@prisma/client';
import { merge } from 'lodash';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../../entity/subscriptions/operation-notification';
import { TransactionData } from '../../../entity/transaction';
import { injectLogger, Logger } from '../../../utils/logging';
import { BaseOperationData, managerNumbersFindOptions, OperationUtils } from './operation.utils';

@singleton()
export default class TransactionRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly operationUtils: OperationUtils,
        @injectLogger(TransactionRepository) private readonly logger: Logger,
    ) { }

    async findOneByOperationAlpha(operationAlphaAutoId: bigint, data: BaseOperationData): Promise<TransactionData | undefined> {
        const result = await this.prisma.tx.findUnique(
            merge(
                this.operationUtils.getBaseOperationFindOptions(operationAlphaAutoId),
                managerNumbersFindOptions,
                Prisma.validator<Prisma.txFindFirstArgs>()({
                    select: {
                        consumed_milligas: true,
                        parameters: true,
                        entrypoint: true,
                        amount: true,
                        addresses_addressesTotx_source_id: {
                            select: {
                                address: true,
                            },
                        },
                        addresses_addressesTotx_destination_id: {
                            select: {
                                address: true,
                            },
                        },
                        operation_alpha: {
                            select: {
                                manager_numbers: {
                                    select: {
                                        storage_limit: true,
                                    },
                                },
                            },
                        },
                    },
                }),
            ),
        );
        if (!result) {
            this.logger.logWarning('No Transaction found for {operationAlphaAutoId}.', { operationAlphaAutoId });
            return;
        }

        return {
            kind: OperationKind.transaction,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result, data),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            consumed_milligas: result.consumed_milligas,
            destination: result.addresses_addressesTotx_destination_id.address,
            entrypoint: result.entrypoint,
            parameters: result.parameters,
            amount: result.amount,
            storage_limit: result.operation_alpha.manager_numbers?.storage_limit,
        };
    }
}
