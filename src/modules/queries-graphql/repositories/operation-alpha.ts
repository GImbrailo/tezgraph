/* eslint-disable no-warning-comments */
import { PrismaClient } from '@prisma/client';
import { singleton } from 'tsyringe';

@singleton()
export default class OperationAlphaRepository {
    constructor(
        private readonly prisma: PrismaClient,
    ) { }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/explicit-function-return-type
    findManyByOperationHash(operationHash: string) {
        return this.prisma.operation_alpha.findMany({
            where: { operation: { hash: operationHash } },
            select: {
                autoid: true,
                operation_kind: true,
                operation_sender_and_receiver: {
                    select: {
                        addresses_addressesTooperation_sender_and_receiver_sender_id: {
                            select: { address: true },
                        },
                    },
                },
            },
        });
    }
}
