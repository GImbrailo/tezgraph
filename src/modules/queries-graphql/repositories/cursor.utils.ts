/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { UserInputError } from 'apollo-server';
import { performance } from 'perf_hooks';
import { inject, singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../utils/logging';
import { PrismaRawClient, prismaRawClientDIToken } from '../database/prisma';
import { RelayPageData } from '../utils';
import DateRangeUtils, { DateRange } from './date-range.utils';
import { OrderBy, OrderByDirection } from './order-by.utils';

interface CursorParams {
    operation: string;
    pkh: string;
    requestId: string;
    takeLimit?: number;
    dateRange?: DateRange | undefined;
}

interface CursorPaginationQueryParams extends CursorParams {
    cursor: string;
    relayPageData: RelayPageData;
    orderBy: OrderBy;
}

interface GetCursorParameters extends CursorParams {
    orderByClause: string;
    selectStatement: string;
    paginationDirection: string;
    cursor: string;
    cursorRecordID: string;
    orderBy: OrderBy;
    relayPageData: RelayPageData;
}

@singleton()
export default class CursorUtils {
    constructor(
        @inject(prismaRawClientDIToken) private readonly prisma: PrismaRawClient,
        private readonly dateRangeUtils: DateRangeUtils,
        @injectLogger(CursorUtils) private readonly logger: Logger,
    ) { }

    isCursorValid(cursor: string): boolean {
        const cursorSplit = cursor.split(':');
        if (!cursorSplit[0] || (!cursorSplit[0].startsWith('oo') && !cursorSplit[0].startsWith('op') && !cursorSplit[0].startsWith('on'))) {
            return false;
        }
        if (cursorSplit[0].length !== 51 || isNaN(parseInt(cursorSplit[1] ?? '', 10))) {
            return false;
        }
        return true;
    }

    /*
     * This function is called by the Account Resolver.
     * When a user uses the before/after argument, it returns a SQL query that paginates the records according to the user's request.
     */
    // eslint-disable-next-line max-lines-per-function
    async cursorPaginationQueryBuilder(
        {
            operation,
            pkh,
            requestId,
            takeLimit,
            dateRange,
            cursor,
            relayPageData,
            orderBy,
        }: CursorPaginationQueryParams,
    ): Promise<string> {
        this.logger.logInformation(`CursorPaginationQueryBuilder started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();
        if (!this.isCursorValid(cursor)) {
            throw new UserInputError(
                'Argument "cursor" of type "string?" is in a invalid format. The valid cursor format is "operation_hash:operation_id".',
            );
        }
        const cursorRecord = await this.getCursorRecord(operation, pkh, cursor, requestId);
        if (cursorRecord[0] === undefined) {
            throw new UserInputError('Unknown Record: No results found for the provided cursor.');
        }
        const relayDirection = this.getRelayDirection(orderBy, relayPageData);
        const orderByDirection = this.getOrderByDirection(orderBy, relayPageData);
        const orderByClause = `ORDER BY ${orderBy.field} ${orderByDirection}, hash ASC, op_id ASC`;
        const selectStatement = this.getCursorSelectStatement(operation);
        const query = this.getCursorQuery(
            {
                operation,
                pkh,
                orderByClause,
                selectStatement,
                requestId,
                dateRange,
                takeLimit,
                paginationDirection: relayDirection,
                cursor,
                cursorRecordID: cursorRecord[0]?.id as string,
                orderBy,
                relayPageData,
            },
        );
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`CursorPaginationQueryBuilder ended - for {requestId}, {duration}.`, {
            requestId,
            duration,
        });
        return query;
    }

    async getCursorRecord(operation: string, pkh: string, cursor: string, requestId: string): Promise<Record<string, unknown>[]> {
        this.logger.logInformation(`GetCursorRecord started - for {requestId}.`, {
            requestId,
        });
        const performanceStart = performance.now();
        const cursorSplit = cursor.split(':');
        const opHash = cursorSplit[0];
        const opID = cursorSplit[1];
        const cursorRecordQuery
            = `SELECT * FROM get_${operation}('${pkh}') WHERE hash = '${opHash}' and op_id = '${opID}' limit 1`;
        const cursorRecord = await this.prisma.$queryRaw<Record<string, unknown>[]>(cursorRecordQuery);
        if (cursorRecord.length === 0 || cursorRecord[0] === undefined) {
            throw new UserInputError('Unknown Record: No results found for the provided cursor.');
        }
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetCursorRecord ended - for {requestId}, {duration}.`, {
            requestId,
            duration,
        });
        return cursorRecord;
    }

    getRelayDirection(orderBy: OrderBy, relayPageData: RelayPageData): string {
        let relayDirection = orderBy.direction === 'DESC' ? '<' : '>';

        if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'DESC') {
            relayDirection = '>';
        } else if (relayPageData.direction === 'first' && relayPageData.order === 'before' && orderBy.direction === 'ASC') {
            relayDirection = '<';
        }

        return relayDirection;
    }

    getOrderByDirection(orderBy: OrderBy, relayPageData: RelayPageData): string {
        let orderByDirection = orderBy.direction;
        if (relayPageData.direction === 'last' && relayPageData.order === 'after' && orderBy.direction === 'DESC') {
            orderByDirection = OrderByDirection.ASC;
        } else if (relayPageData.direction === 'last' && relayPageData.order === 'after' && orderBy.direction === 'ASC') {
            orderByDirection = OrderByDirection.DESC;
        }

        return orderByDirection;
    }

    getCursorSelectStatement(operation: string): string {
        // eslint-disable-next-line no-warning-comments
        // TODO: split and move sqls to the repository methods
        switch (operation) {
            case 'transaction':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, storage_limit, entrypoint, fee, amount, parameters, timestamp, contract_address, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor `;
            case 'delegation':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'operations':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, slots, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'reveal':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'origination':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            case 'endorsement':
                // eslint-disable-next-line max-len
                return `SELECT id, hash, op_id as batch_position, source, destination, delegate, slots, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, slots, contract_address, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            default:
                throw new UserInputError('Operation Error: Cursor query operation not recognized.');
        }
    }

    getCursorQuery(
        {
            operation,
            pkh,
            requestId,
            takeLimit,
            dateRange,
            relayPageData,
            selectStatement,
            paginationDirection,
            cursor,
            cursorRecordID,
            orderBy,
        }: GetCursorParameters,
    ): string {
        const cursorSplit = cursor.split(':');
        const opHash = cursorSplit[0];
        const opID = cursorSplit[1];
        const dateRangeWhereClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        let orderByDir = `${orderBy.direction}`;

        if (relayPageData.direction === 'last' && relayPageData.order === 'after') {
            orderByDir = paginationDirection === '>' ? 'DESC' : 'ASC';
        }

        return `
        SELECT * FROM 
        (
            SELECT * FROM
            (
                ${selectStatement} FROM
                get_${operation}_neighbour_${paginationDirection === '>' ? 'greater' : 'lesser'}(
                    '${pkh}', '${cursorRecordID}', '${opHash}', '${opID}'
                )
                ${dateRangeWhereClause}
            )
            AS results ORDER BY "id" ${orderByDir}
        )
        AS results ORDER BY "id" ${orderByDir} LIMIT ${takeLimit}`;
    }
}
