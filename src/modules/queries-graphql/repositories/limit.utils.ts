import { UserInputError } from 'apollo-server';

// Server Default Record Limit
export const pageSizeLimit: number = process.env.PAGE_SIZE_LIMIT ? parseInt(process.env.PAGE_SIZE_LIMIT, 10) : 200;

export function setTakeLimit(first: number | undefined, last: number | undefined): number {
    if (first !== undefined && last !== undefined) {
        throw new Error(`Argument Error: Cannot use both "first" and "last" arguments together.`);
    }

    const takeLimit = last ?? first ?? pageSizeLimit;

    checkLimitValid(takeLimit);
    return takeLimit;
}

// Limit Validator
export function checkLimitValid(limit: number | undefined): void {
    if (limit !== undefined && limit > pageSizeLimit) {
        throw new UserInputError(`Argument "limit" of type "Float?" cannot be greater than ${pageSizeLimit}.`);
    }
}
