import { singleton } from 'tsyringe';
import { Field, InputType, registerEnumType } from 'type-graphql';

// OrderBy Direction Enums
export enum OrderByDirection {
    ASC = 'ASC',
    DESC = 'DESC',
}

registerEnumType(OrderByDirection, {
    name: 'OrderByDirection',
    description: 'The available options for the order_by.direction field value.',
});

// OrderBy Field Enums
export enum OrderByField {
    TIMESTAMP = 'timestamp',
    HASH = 'hash',
    LEVEL = 'level',
    BLOCK = 'block',
    ID = 'id',
}

registerEnumType(OrderByField, {
    name: 'OrderByField',
    description: 'The available options for the order_by.field field value.',
});

@InputType()
export class OrderBy {
    @Field(() => OrderByField, { nullable: true })
    field!: OrderByField;

    @Field(() => OrderByDirection, { nullable: true })
    direction!: OrderByDirection;
}

@singleton()
export default class OrderByUtils {
    createOrderBy(field?: OrderByField, direction?: OrderByDirection): OrderBy {
        const obField = field ?? OrderByField.ID;
        const obDirection = direction ?? OrderByDirection.DESC;
        const orderBy = new OrderBy();
        Object.assign(orderBy, { field: obField, direction: obDirection });
        return orderBy;
    }

    createBeforeOrderBy(last: number | undefined, orderBy: OrderBy): OrderBy {
        if (last !== undefined) {
            return this.createOrderBy(orderBy.field, this.reverseDirection(orderBy.direction));
        }
        return this.createOrderBy(orderBy.field, orderBy.direction);
    }

    createOrderByClause(last: number | undefined, orderBy: OrderBy | undefined): string {
        const field = orderBy?.field ?? 'id';
        // eslint-disable-next-line no-nested-ternary
        const direction = orderBy?.direction
            ? last !== undefined
                ? this.reverseDirection(orderBy.direction)
                : orderBy.direction
            : OrderByDirection.DESC;

        return `ORDER BY ${field} ${direction} NULLS LAST, hash ASC, op_id ASC`;
    }

    reverseDirection(direction: OrderByDirection): OrderByDirection {
        return direction === OrderByDirection.ASC ? OrderByDirection.DESC : OrderByDirection.ASC;
    }

    checkRelayOrderByArgs(first: number | undefined, last: number | undefined, before: string | undefined, after: string | undefined): void {
        if (first !== undefined && last !== undefined) {
            throw new Error(`Argument Error: Cannot use both "first" and "last" arguments together.`);
        }

        if (before !== undefined && after !== undefined) {
            throw new Error(`Argument Error: Cannot use both "before" and "after" arguments together. This feature is not yet supported.`);
        }
    }

    // eslint-disable-next-line max-statements
    applyRelayOrder(
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        orderBy: OrderBy,
        operationQuery: string,
    ): string {
        this.checkRelayOrderByArgs(first, last, before, after);

        if (last !== undefined) {
            return `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ${orderBy.direction}`;
        }

        if (!after && orderBy.direction !== 'ASC') {
            return `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
        }

        return operationQuery;
    }
}
