/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable no-console */
/* eslint-disable max-lines */
/* eslint-disable max-params */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { PrismaClient } from '@prisma/client';
import { performance } from 'perf_hooks';
import { inject, singleton } from 'tsyringe';

import { DelegationData, DelegationSQLResults } from '../../../entity/delegation';
import { EndorsementData, EndorsementSQLResults } from '../../../entity/endorsement';
import { OperationsSQLResults } from '../../../entity/operations';
import { OriginationData, OriginationSQLResults } from '../../../entity/origination';
import { RevealData, RevealSQLResults } from '../../../entity/reveal';
import { TransactionData, TransactionSQLResults } from '../../../entity/transaction';
import { injectLogger, Logger } from '../../../utils/logging';
import { PrismaRawClient, prismaRawClientDIToken } from '../database/prisma';
import RevealRepository from '../repositories/reveal';
import { RelayPageData } from '../utils';
import CursorUtils from './cursor.utils';
import DateRangeUtils, { DateRange } from './date-range.utils';
import { setTakeLimit } from './limit.utils';
import OrderByUtils, { OrderBy } from './order-by.utils';

@singleton()
export default class AccountRepository {
    constructor(
        private readonly prismaClient: PrismaClient,
        @inject(prismaRawClientDIToken) private readonly prisma: PrismaRawClient,
        private readonly cursorUtils: CursorUtils,
        private readonly dateRangeUtils: DateRangeUtils,
        private readonly orderByUtils: OrderByUtils,
        private readonly revealRepository: RevealRepository,
        @injectLogger(AccountRepository) private readonly logger: Logger,
    ) { }

    // Activated Timestamp Query
    async getActivated(pkh: string, requestId: string): Promise<Date | undefined> {
        const performanceStart = performance.now();
        this.logger.logInformation(`GetActivated started - for {requestId}`, { requestId });
        const activationData = await this.prismaClient.addresses.findUnique({
            where: {
                address: pkh,
            },
            include: {
                activation: {
                    include: {
                        operation_alpha: {
                            include: {
                                block: true,
                            },
                        },
                    },
                },
            },
        });
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetActivated ended - for {requestId}, {duration}.`, { requestId, duration });
        return activationData?.activation[0]?.operation_alpha.block.timestamp;
    }

    // Public Key Query
    async getRevealedPublicKey(pkh: string, requestId: string): Promise<string | undefined> {
        this.logger.logInformation(`GetRevealedPublicKey started - for {requestId}`, { requestId });
        const performanceStart = performance.now();
        const revealData = await this.revealRepository.findLatestByPKH(pkh);
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetRevealedPublicKey ended - for {requestId}, {duration}.`, { requestId, duration });
        return revealData?.reveal[0]?.pk;
    }

    async getOriginations(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<OriginationData[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let originationsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            originationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'origination',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            originationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'origination',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            originationsQuery = /* sql */ `
            SELECT * FROM (
                SELECT id, hash, op_id as batch_position, source, destination, delegate, 
                kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, 
                amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor 
                FROM get_origination('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
            ) AS result LIMIT ${takeLimit}
            `;
        }

        originationsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, originationsQuery);
        const originations = await this.prisma.$queryRaw<OriginationSQLResults[]>(originationsQuery);
        return originations.map(result => ({ ...result, timestamp: new Date(result.timestamp), sourceAddress: result.source }));
    }

    async getReveals(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<RevealData[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let revealsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            revealsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'reveal',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            revealsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'reveal',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            revealsQuery = /* sql */ `
            SELECT * FROM (
                SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, 
                timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, 
                contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor
                FROM get_reveal('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause} 
            ) AS result LIMIT ${takeLimit}
            `;
        }

        revealsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, revealsQuery);
        const reveals = await this.prisma.$queryRaw<RevealSQLResults[]>(revealsQuery);
        return reveals.map(result => ({ ...result, timestamp: new Date(result.timestamp), sourceAddress: result.source }));
    }

    async getOperations(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<OperationsSQLResults[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let operationsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            operationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'operations',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            operationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'operations',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            operationsQuery = /* sql */ ` 
            SELECT * FROM (
                SELECT id, hash, op_id as batch_position, source, destination, delegate, slots,
                kind, timestamp, level, block, counter, gas_limit,
                storage_limit, public_key, amount, parameters, contract_address, fee, consumed_milligas, 
                CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_operations('${accountPKH}')
                ${dateRangeClause} 
                ${orderByClause}
            ) AS result LIMIT ${takeLimit}
            `;
        }

        operationsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, operationsQuery);
        const operations = await this.prisma.$queryRaw<OperationsSQLResults[]>(operationsQuery);
        return operations.map(result => ({ ...result, timestamp: new Date(result.timestamp) }));
    }

    async getTransactions(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<TransactionData[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let transactionsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            transactionsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'transaction',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            transactionsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'transaction',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            transactionsQuery = /* sql */ `
            SELECT * FROM (
                SELECT id, hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, 
                storage_limit, entrypoint, fee, amount, parameters, timestamp, contract_address, consumed_milligas, 
                CONCAT(hash, CONCAT(':', op_id)) AS cursor 
                FROM get_transaction('${accountPKH}') 
                ${dateRangeClause} 
                ${orderByClause} 
            ) AS result LIMIT ${takeLimit}
            `;
        }

        transactionsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, transactionsQuery);
        const transactions = await this.prisma.$queryRaw<TransactionSQLResults[]>(transactionsQuery);
        return transactions.map(result => ({ ...result, timestamp: new Date(result.timestamp), sourceAddress: result.source }));
    }

    async getDelegations(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<DelegationData[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let delegationsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            delegationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'delegation',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            delegationsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'delegation',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            delegationsQuery = /* sql */ `
            SELECT * FROM (
                SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, kind, 
                    timestamp, level, block, counter, gas_limit, storage_limit, public_key,
                    amount, parameters, contract_address, fee, consumed_milligas, CONCAT(hash, CONCAT(':', op_id)) AS cursor
                FROM get_delegation('${accountPKH}') 
                ${dateRangeClause} 
                ${orderByClause} 
            ) AS result LIMIT ${takeLimit}
            `;
        }

        delegationsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, delegationsQuery);
        const delegations = await this.prisma.$queryRaw<DelegationSQLResults[]>(delegationsQuery);
        return delegations.map(result => ({ ...result, timestamp: new Date(result.timestamp), sourceAddress: result.source }));
    }

    async getEndorsements(
        requestId: string,
        accountPKH: string,
        first: number | undefined,
        last: number | undefined,
        before: string | undefined,
        after: string | undefined,
        dateRange: DateRange | undefined,
        orderBy: OrderBy,
        relayPageData: RelayPageData,
    ): Promise<EndorsementData[]> {
        const takeLimit = setTakeLimit(first, last);
        const dateRangeClause = this.dateRangeUtils.getDateRangeClause(dateRange, requestId);
        const orderByClause = this.orderByUtils.createOrderByClause(last, orderBy);
        let endorsementsQuery = '';

        if (before) {
            const beforeOrderBy = this.orderByUtils.createBeforeOrderBy(last, orderBy);
            endorsementsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'endorsement',
                cursor: before,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy: beforeOrderBy,
                dateRange,
            });
        } else if (after) {
            endorsementsQuery = await this.cursorUtils.cursorPaginationQueryBuilder({
                operation: 'endorsement',
                cursor: after,
                relayPageData,
                pkh: accountPKH,
                takeLimit,
                requestId,
                orderBy,
                dateRange,
            });
        } else {
            endorsementsQuery = /* sql */ `
            SELECT * FROM (
                SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, slots, kind, timestamp, level, block, counter, gas_limit, 
                storage_limit, public_key, amount, parameters, contract_address, CONCAT(hash, CONCAT(':', op_id)) AS cursor
            FROM get_endorsement('${accountPKH}')
            ${dateRangeClause} 
            ${orderByClause} 
            ) AS result LIMIT ${takeLimit}
            `;
        }

        endorsementsQuery = this.orderByUtils.applyRelayOrder(first, last, before, after, orderBy, endorsementsQuery);
        const endorsements = await this.prisma.$queryRaw<EndorsementSQLResults[]>(endorsementsQuery);
        return endorsements.map(result => ({ ...result, timestamp: new Date(result.timestamp), sourceAddress: result.source }));
    }
}
