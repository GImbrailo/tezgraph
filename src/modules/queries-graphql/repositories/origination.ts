/* eslint-disable no-warning-comments */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { PrismaClient, Prisma } from '@prisma/client';
import { merge } from 'lodash';
import { singleton } from 'tsyringe';

import { OriginationData } from '../../../entity/origination';
import { OperationKind } from '../../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../../../utils/logging';
import { BaseOperationData, managerNumbersFindOptions, OperationUtils } from './operation.utils';

@singleton()
export default class OriginationRepository {
    constructor(
        private readonly prisma: PrismaClient,
        private readonly operationUtils: OperationUtils,
        @injectLogger(OriginationRepository) private readonly logger: Logger,
    ) { }

    async findOneByOperationAlpha(operationAlphaAutoId: bigint, data: BaseOperationData): Promise<OriginationData | undefined> {
        const result = await this.prisma.origination.findUnique(
            merge(
                this.operationUtils.getBaseOperationFindOptions(operationAlphaAutoId),
                managerNumbersFindOptions,
                Prisma.validator<Prisma.originationFindFirstArgs>()({
                    select: {
                        consumed_milligas: true,
                        addresses_addressesToorigination_k_id: {
                            select: {
                                address: true,
                            },
                        },
                    },
                }),
            ),
        );
        if (!result) {
            this.logger.logWarning('No Origination found for {operationAlphaAutoId}.', { operationAlphaAutoId });
            return;
        }

        return {
            kind: OperationKind.transaction,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result, data),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            consumed_milligas: result.consumed_milligas,
            contract_address: result.addresses_addressesToorigination_k_id.address,
        };
    }
}
