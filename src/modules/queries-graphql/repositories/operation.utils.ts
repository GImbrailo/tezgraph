/* eslint-disable no-warning-comments */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Prisma } from '@prisma/client';
import { singleton } from 'tsyringe';

export interface BaseOperationResult {
    operation_alpha: {
        id: number;
        autoid: bigint | null;
        block: {
            level: number;
            timestamp: Date;
            block_hash: {
                hash: string;
            };
        };
    };
}

export interface ManagerNumbersResult {
    fee: bigint;
    operation_alpha: {
        manager_numbers: {
            gas_limit: Prisma.Decimal | null;
            counter: Prisma.Decimal | null;
        } | null;
    };
}

export interface BaseOperationData {
    senderAddress: string;
    operationHash: string;
}

const commonOptionsValidator = Prisma.validator<
& Prisma.txFindFirstArgs
& Prisma.delegationFindFirstArgs
& Prisma.originationFindFirstArgs
& Prisma.endorsementFindFirstArgs
& Prisma.revealFindFirstArgs
>();

const managerNumbersOptionsValidator = Prisma.validator<
& Prisma.txFindFirstArgs
& Prisma.delegationFindFirstArgs
& Prisma.originationFindFirstArgs
& Prisma.revealFindFirstArgs
>();

export const managerNumbersFindOptions = managerNumbersOptionsValidator({
    select: {
        fee: true,
        operation_alpha: {
            select: {
                manager_numbers: {
                    select: {
                        gas_limit: true,
                        counter: true,
                    },
                },
            },
        },
    },
});

@singleton()
export class OperationUtils {
    getBaseOperationFindOptions(operationAlphaAutoId: bigint) {
        return commonOptionsValidator({
            where: {
                operation_id: operationAlphaAutoId,
            },
            select: {
                operation_alpha: {
                    select: {
                        id: true,
                        autoid: true,
                        block: {
                            select: {
                                timestamp: true,
                                level: true,
                                block_hash: { select: { hash: true } },
                            },
                        },
                    },
                },
            },
        });
    }

    mapBaseResultToBaseOperationData(
        result: BaseOperationResult,
        { operationHash, senderAddress }: BaseOperationData,
    ) {
        return {
            batch_position: result.operation_alpha.id,
            block: result.operation_alpha.block.block_hash.hash,
            cursor: `${operationHash}:${result.operation_alpha.id}`,
            hash: operationHash,
            id: result.operation_alpha.autoid,
            level: result.operation_alpha.block.level,
            timestamp: result.operation_alpha.block.timestamp,
            sourceAddress: senderAddress,
        };
    }

    mapManagerNumbersResultToOperationCostData(result: ManagerNumbersResult) {
        return {
            counter: result.operation_alpha.manager_numbers?.counter,
            fee: result.fee,
            gas_limit: result.operation_alpha.manager_numbers?.gas_limit,
        };
    }
}
