import { PrismaClient } from '@prisma/client';
import { DependencyContainer } from 'tsyringe';

import { BackgroundWorker, backgroundWorkersDIToken } from '../../bootstrap/background-worker';
import { graphQLResolversDIToken } from '../../bootstrap/graphql-resolver-type';
import { HealthCheck, healthChecksDIToken } from '../../utils/health/health-check';
import { Module, ModuleName } from '../module';
import DatabaseHealthCheck from './database/database-health-check';
import { prisma, PrismaDatabaseWorker, PrismaRawClient, prismaRawClient, prismaRawClientDIToken } from './database/prisma';
import { AccountResolver } from './resolvers/account';
import { OperationResolver } from './resolvers/operation';

export const queriesGraphQLModule: Module = {
    name: ModuleName.QueriesGraphQL,
    dependencies: [],

    initialize(diContainer: DependencyContainer): void {
        diContainer.registerInstance(graphQLResolversDIToken, AccountResolver);
        diContainer.registerInstance(graphQLResolversDIToken, OperationResolver);

        diContainer.registerInstance(PrismaClient, prisma);
        diContainer.register<PrismaRawClient>(prismaRawClientDIToken, { useValue: prismaRawClient });
        diContainer.register<HealthCheck>(healthChecksDIToken, { useToken: DatabaseHealthCheck });
        diContainer.register<BackgroundWorker>(backgroundWorkersDIToken, { useToken: PrismaDatabaseWorker });
    },
};
