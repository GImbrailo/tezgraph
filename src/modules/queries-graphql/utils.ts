/* eslint-disable */
import { UserInputError } from 'apollo-server-express';

// Server Default Record Limit
export const pageSizeLimit: number = process.env.PAGE_SIZE_LIMIT ? parseInt(process.env.PAGE_SIZE_LIMIT) : 200;

export class RelayPageData {
    direction?: string | undefined;
    order?: string;
}

export const createRelayPageData = function (_first: number | undefined, last: number | undefined, before: string | undefined, _after: string | undefined): RelayPageData {
    const relayPageDataData = Object.create(RelayPageData);

    if (last) {
        relayPageDataData.direction = 'last';
    } else {
        relayPageDataData.direction = 'first';
    }

    relayPageDataData.order = before ? 'before' : 'after';
    return relayPageDataData;
};

// Result Validator
export const queryResultsFound = function (results: any[]): boolean {
    if (results.length === 0) {
        throw new UserInputError('Unknown Account: No results found for the provided address.');
    }
    return true;
};
