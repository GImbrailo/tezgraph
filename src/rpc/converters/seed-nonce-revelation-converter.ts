import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { SeedNonceRevelationNotification } from '../../entity/subscriptions/seed-nonce-revelation-notification';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';

type RpcRevelation = rpc.OperationContentsRevelation | rpc.OperationContentsAndResultRevelation;

@singleton()
export class SeedNonceRevelationConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: RpcRevelation, baseProperties: BaseOperationProperties): SeedNonceRevelationNotification {
        return {
            ...baseProperties,
            kind: OperationKind.seed_nonce_revelation,
            level: rpcOperation.level,
            nonce: rpcOperation.nonce,
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }
}
