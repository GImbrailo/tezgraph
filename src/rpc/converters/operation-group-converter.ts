import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../entity/subscriptions/block-notification';
import { OperationNotification, OperationOrigin } from '../../entity/subscriptions/operation-notification';
import { RpcMempoolOperationGroup } from '../../modules/tezos-monitor/mempool/rpc-mempool-operation-group';
import { deepFreeze } from '../../utils/conversion';
import { injectLogger, Logger } from '../../utils/logging';
import { Clock } from '../../utils/time/clock';
import { BaseOperationProperties, OperationConverter } from './operation-converter';

/** Converts an operation group (either from block or mempool) from Tezos RPC format to a GraphQL object. */
@singleton()
export class OperationGroupConverter {
    constructor(
        private readonly operationConverter: OperationConverter,
        @injectLogger(OperationGroupConverter) private readonly logger: Logger,
        private readonly clock: Clock,
    ) {}

    convert(rpcGroup: rpc.OperationEntry, block: BlockNotification): readonly OperationNotification[];
    convert(rpcGroup: RpcMempoolOperationGroup): readonly OperationNotification[];
    convert(rpcGroup: rpc.OperationEntry | RpcMempoolOperationGroup, block?: BlockNotification): readonly OperationNotification[] {
        const baseProperties: BaseOperationProperties = {
            info: {
                protocol: rpcGroup.protocol,
                chain_id: rpcGroup.chain_id,
                hash: rpcGroup.hash,
                branch: rpcGroup.branch,
                signature: rpcGroup.signature,
                received_from_tezos_on: block?.received_from_tezos_on ?? this.clock.getNowDate(),
            },
            block,
            origin: block ? OperationOrigin.BLOCK : OperationOrigin.MEMPOOL,
        };

        const operations = rpcGroup.contents.map(rpcOperation => {
            const operation = this.operationConverter.convert(rpcOperation, baseProperties);

            this.logger.logDebug('Converted operation {kind} in group with {signature}.', {
                kind: rpcOperation.kind,
                signature: rpcGroup.signature,
            });
            return operation;
        });

        // Don't freeze when deserializing a block b/c the block is referenced -> would be frozen too -> can't add these operations.
        return block ? operations : deepFreeze(operations);
    }
}
