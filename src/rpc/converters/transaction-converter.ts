import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { TransactionNotification } from '../../entity/subscriptions/transaction-notification';
import { convertBigInt, convertMutez } from './common/big-int-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';
import { TransactionResultConverter } from './operation-results/transaction-result-converter';

type RpcTransaction = rpc.OperationContentsTransaction | rpc.OperationContentsAndResultTransaction;

@singleton()
export class TransactionConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: TransactionResultConverter,
    ) {}

    convert(rpcOperation: RpcTransaction, baseProperties: BaseOperationProperties): TransactionNotification {
        return {
            ...baseProperties,
            kind: OperationKind.transaction,
            source: rpcOperation.source,
            fee: convertMutez(rpcOperation.fee),
            counter: convertBigInt(rpcOperation.counter),
            gas_limit: convertBigInt(rpcOperation.gas_limit),
            storage_limit: convertBigInt(rpcOperation.storage_limit),
            amount: convertMutez(rpcOperation.amount),
            destination: rpcOperation.destination,
            parameters: rpcOperation.parameters,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        };
    }
}
