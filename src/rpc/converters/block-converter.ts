import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../entity/subscriptions/block-notification';
import { OperationNotification } from '../../entity/subscriptions/operation-notification';
import { deepFreeze } from '../../utils/conversion';
import { injectLogger, Logger } from '../../utils/logging';
import { convertDate } from './common/date-converter';
import { OperationGroupConverter } from './operation-group-converter';

/** Converts a block from Tezos RPC to a GraphQL object. */
@singleton()
export class BlockConverter {
    constructor(
        private readonly operationGroupConverter: OperationGroupConverter,
        @injectLogger(BlockConverter) private readonly logger: Logger,
    ) {}

    convert(rpcBlock: rpc.BlockResponse, receivedFromTezosOn: Date): BlockNotification {
        this.logger.logDebug('Converting block {hash}.', { hash: rpcBlock.hash });
        const block = {
            hash: rpcBlock.hash,
            protocol: rpcBlock.protocol,
            chain_id: rpcBlock.chain_id,
            header: {
                level: rpcBlock.header.level,
                proto: rpcBlock.header.proto,
                predecessor: rpcBlock.header.predecessor,
                timestamp: convertDate(rpcBlock.header.timestamp),
                signature: rpcBlock.header.signature,
            },
            operations: Array<OperationNotification>(),
            received_from_tezos_on: receivedFromTezosOn,
        };

        for (const rpcGroup of rpcBlock.operations.flat()) {
            const groupOperations = this.operationGroupConverter.convert(rpcGroup, block);
            block.operations.push(...groupOperations);
        }

        this.logger.logDebug('Converted block {hash}.', { hash: rpcBlock.hash });
        return deepFreeze(block);
    }
}
