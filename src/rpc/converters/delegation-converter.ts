import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationNotification } from '../../entity/subscriptions/delegation-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { convertBigInt, convertMutez } from './common/big-int-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';
import { DelegationResultConverter } from './operation-results/delegation-result-converter';

type RpcDelegation = rpc.OperationContentsDelegation | rpc.OperationContentsAndResultDelegation;

@singleton()
export class DelegationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: DelegationResultConverter,
    ) {}

    convert(rpcOperation: RpcDelegation, baseProperties: BaseOperationProperties): DelegationNotification {
        return {
            ...baseProperties,
            kind: OperationKind.delegation,
            source: rpcOperation.source,
            fee: convertMutez(rpcOperation.fee),
            counter: convertBigInt(rpcOperation.counter),
            gas_limit: convertBigInt(rpcOperation.gas_limit),
            storage_limit: convertBigInt(rpcOperation.storage_limit),
            delegate: rpcOperation.delegate,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        };
    }
}
