import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BigMapDiff } from '../../../entity/subscriptions/big-map-diff';
import { Nullish } from '../../../utils/reflection';

@singleton()
export class BigMapDiffConverter {
    convertNullish(rpcDiffs: Nullish<rpc.ContractBigMapDiffItem[]>): readonly BigMapDiff[] | undefined {
        return rpcDiffs?.map(d => ({
            key_hash: d.key_hash,
            key: d.key,
            value: d.value,
        }));
    }
}
