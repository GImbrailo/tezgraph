import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BalanceUpdate } from '../../../entity/subscriptions/balance-update';
import { SimpleOperationMetadata } from '../../../entity/subscriptions/operation-metadata';
import { InternalOperationResult } from '../../../entity/subscriptions/operation-result';
import { InternalOperationResultConverter } from '../operation-results/internal-operation-result-converter';
import { BalanceUpdateConverter } from './balance-update-converter';

export interface RpcOperation<TMetadata = rpc.OperationContentsAndResultMetadata> {
    kind: rpc.OpKind; // Used by compiler to make sure it's an operation.
    metadata?: TMetadata;
}

export interface RpcMetadataWithResult<TRpcResult> extends rpc.OperationContentsAndResultMetadata {
    operation_result: TRpcResult;
    internal_operation_results?: rpc.InternalOperationResult[];
}

export interface MetadataWithResult<TRpcResult> {
    balance_updates: readonly BalanceUpdate[];
    operation_result: TRpcResult;
    internal_operation_results: readonly InternalOperationResult[] | undefined;
}

export interface OperationResultConverter<TRpcResult, TResult> {
    convert(rpcResult: TRpcResult): TResult;
}

@singleton()
export class OperationMetadataConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly internalOperationResultConverter: InternalOperationResultConverter,
    ) {}

    convertSimple(rpcOperation: RpcOperation): SimpleOperationMetadata | undefined {
        return rpcOperation.metadata
            ? { balance_updates: this.balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates) }
            : undefined;
    }

    convert<TRpcResult, TResult>(
        rpcOperation: RpcOperation<RpcMetadataWithResult<TRpcResult>>,
        resultConverter: OperationResultConverter<TRpcResult, TResult>,
    ): MetadataWithResult<TResult> | undefined {
        return rpcOperation.metadata
            ? {
                balance_updates: this.balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates),
                operation_result: resultConverter.convert(rpcOperation.metadata.operation_result),
                internal_operation_results: rpcOperation.metadata.internal_operation_results
                    ?.map(r => this.internalOperationResultConverter.convert(r)),
            }
            : undefined;
    }
}
