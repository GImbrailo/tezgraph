import * as rpc from '@taquito/rpc';

export function convertDate(rpcDate: rpc.TimeStampMixed): Date {
    return rpcDate instanceof Date
        ? rpcDate
        : new Date(rpcDate);
}
