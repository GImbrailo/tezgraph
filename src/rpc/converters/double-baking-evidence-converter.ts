import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    DoubleBakingEvidenceBlockHeader,
    DoubleBakingEvidenceNotification,
} from '../../entity/subscriptions/double-baking-evidence-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { convertDate } from './common/date-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';

type RpcDoubleBaking = rpc.OperationContentsDoubleBaking | rpc.OperationContentsAndResultDoubleBaking;

@singleton()
export class DoubleBakingEvidenceConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: RpcDoubleBaking, baseProperties: BaseOperationProperties): DoubleBakingEvidenceNotification {
        return {
            ...baseProperties,
            kind: OperationKind.double_baking_evidence,
            bh1: this.convertHeader(rpcOperation.bh1),
            bh2: this.convertHeader(rpcOperation.bh2),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }

    private convertHeader(rpcHeader: rpc.BlockFullHeader): DoubleBakingEvidenceBlockHeader {
        return {
            level: rpcHeader.level,
            proto: rpcHeader.proto,
            predecessor: rpcHeader.predecessor,
            timestamp: convertDate(rpcHeader.timestamp),
            validation_pass: rpcHeader.validation_pass,
            operations_hash: rpcHeader.operations_hash,
            fitness: rpcHeader.fitness,
            context: rpcHeader.context,
            priority: rpcHeader.priority,
            proof_of_work_nonce: rpcHeader.proof_of_work_nonce,
            seed_nonce_hash: rpcHeader.seed_nonce_hash,
            signature: rpcHeader.signature,
        };
    }
}
