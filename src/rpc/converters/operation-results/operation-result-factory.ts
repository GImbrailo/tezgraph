import * as rpc from '@taquito/rpc';

import { OperationResult, OperationResultStatus } from '../../../entity/subscriptions/operation-result';
import { DefaultConstructor } from '../../../utils/reflection';
import { convertNullishBigInt } from '../common/big-int-converter';

interface RpcOperationResultBase {
    status: rpc.OperationResultStatusEnum;
    consumed_gas?: string;
    consumed_milligas?: string;
    errors?: rpc.TezosGenericOperationError[];
}

export function getBaseOperationResultProperties<TResult extends OperationResult>(
    resultType: DefaultConstructor<TResult>,
    rpcResult: RpcOperationResultBase,
): OperationResult {
    return {
        typeName: resultType.name,
        status: OperationResultStatus[rpcResult.status],
        consumed_gas: convertNullishBigInt(rpcResult.consumed_gas),
        consumed_milligas: convertNullishBigInt(rpcResult.consumed_milligas),
        errors: rpcResult.errors,
    };
}
