import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { TransactionResult } from '../../../entity/subscriptions/transaction-notification';
import { BalanceUpdateConverter } from '../common/balance-update-converter';
import { convertNullishBigInt } from '../common/big-int-converter';
import { BigMapDiffConverter } from '../common/big-map-diff-converter';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class TransactionResultConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
    ) {}

    convert(rpcResult: rpc.OperationResultTransaction): TransactionResult {
        return {
            ...getBaseOperationResultProperties(TransactionResult, rpcResult),
            storage: rpcResult.storage,
            big_map_diff: this.bigMapDiffConverter.convertNullish(rpcResult.big_map_diff),
            balance_updates: this.balanceUpdateConverter.convertNullish(rpcResult.balance_updates),
            originated_contracts: rpcResult.originated_contracts,
            storage_size: convertNullishBigInt(rpcResult.storage_size),
            paid_storage_size_diff: convertNullishBigInt(rpcResult.paid_storage_size_diff),
            allocated_destination_contract: rpcResult.allocated_destination_contract,
        };
    }
}
