import { asReadonly } from '../utils/conversion';

export const tezosRpcPaths = asReadonly({
    blockMonitor: '/monitor/heads/main',
    mempoolMonitor: '/chains/main/mempool/monitor_operations',

    block: (hash: string) => `/chains/main/blocks/${hash}`,
});
