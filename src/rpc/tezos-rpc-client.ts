import { BlockHeaderResponse, BlockResponse, RpcClient } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EnvConfig } from '../utils/configuration/env-config';
import { injectLogger, Logger } from '../utils/logging';
import { MetricsContainer } from '../utils/metrics/metrics-container';
import { RetryHelper } from '../utils/retry-helper';
import { Clock } from '../utils/time/clock';

enum EndpointName {
    GetBlock = 'getBlock',
    GetBlockHeader = 'getBlockHeader',
}

@singleton()
export class TezosRpcClient {
    constructor(
        private readonly rpcClient: RpcClient,
        private readonly retryHelper: RetryHelper,
        private readonly envConfig: EnvConfig,
        @injectLogger(TezosRpcClient) private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
        private readonly clock: Clock,
    ) {}

    async getBlock(hashOrLevel: string | number): Promise<BlockResponse> {
        return this.retry(
            async () => {
                this.logger.logDebug('Calling getBlock({hashOrLevel}).', { hashOrLevel });
                const start = this.clock.getNowTimestamp();
                const block = await this.rpcClient.getBlock({ block: hashOrLevel.toString() });

                this.metrics.rpcClientGetDuration.observe({ endpoint: EndpointName.GetBlock }, this.clock.getNowTimestamp() - start);
                this.logger.logDebug('Received block with {hash}.', { block, hash: block.hash });
                return block;
            },
            (delay: number, index: number) => {
                this.metrics.rpcClientRetryCount.inc({ endpoint: EndpointName.GetBlock, delay: delay.toString(), index: index.toString() });
            },
        );
    }

    async getHeadBlockHeader(): Promise<BlockHeaderResponse> {
        return this.retry(
            async () => {
                this.logger.logDebug('Calling getHeadBlockHeader().');
                const start = this.clock.getNowTimestamp();
                const blockHeader = await this.rpcClient.getBlockHeader();

                this.logger.logDebug('Received block header with {hash}.', { blockHeader, hash: blockHeader.hash });
                this.metrics.rpcClientGetDuration.observe({ endpoint: EndpointName.GetBlockHeader }, this.clock.getNowTimestamp() - start);
                return blockHeader;
            },
            (delay: number, index: number) => {
                this.metrics.rpcClientRetryCount.inc({ endpoint: EndpointName.GetBlockHeader, delay: delay.toString(), index: index.toString() });
            },
        );
    }

    private async retry<TData>(getData: () => Promise<TData>, reportMetrics: (delay: number, index: number) => void): Promise<TData> {
        return this.retryHelper.execute(getData, this.envConfig.rpcRetryDelaysMillis, reportMetrics);
    }
}
