import { RpcClient } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { HealthCheck, HealthCheckResult, HealthStatus } from '../utils/health/health-check';
import { injectLogger, Logger } from '../utils/logging';
import { Clock } from '../utils/time/clock';

@singleton()
export class TezosRpcHealthWatcher implements HealthCheck {
    readonly name = 'TezosRpcWatcher';
    private lastHealthyBlock?: unknown;

    constructor(
        private readonly rpcClient: RpcClient,
        private readonly clock: Clock,
        @injectLogger(TezosRpcHealthWatcher) private readonly logger: Logger,
    ) {}

    async checkHealth(): Promise<HealthCheckResult> {
        try {
            const header = await this.rpcClient.getBlockHeader();
            this.lastHealthyBlock = {
                hash: header.hash,
                level: header.level,
                chainId: header.chain_id,
                timestamp: header.timestamp,
                receivedOn: this.clock.getNowDate(),
            };

            return {
                status: HealthStatus.Healthy,
                data: {
                    headBlock: this.lastHealthyBlock,
                },
            };
        } catch (error: unknown) {
            this.logger.logError('Failed to download block header from Tezos RPC. Health check fails too. {error}', { error });

            return {
                status: HealthStatus.Unhealthy,
                data: {
                    error,
                    lastHealthyHeadBlock: this.lastHealthyBlock,
                },
            };
        }
    }
}
