{ "protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
    "chain_id": $Chain_id,
    "hash": $block_hash,
    "header": $raw_block_header,
    "metadata"?: $block_header_metadata,
    "operations": [ [ $operation ... ] ... ] }
  $007-PsDELPH1.block_header.alpha.full_header:
    { "level": integer ∈ [-2^31-2, 2^31+2],
      "proto": integer ∈ [0, 255],
      "predecessor": $block_hash,
      "timestamp": $timestamp.protocol,
      "validation_pass": integer ∈ [0, 255],
      "operations_hash": $Operation_list_list_hash,
      "fitness": $fitness,
      "context": $Context_hash,
      "priority": integer ∈ [0, 2^16-1],
      "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
      "seed_nonce_hash"?: $cycle_nonce,
      "signature": $Signature }
  $007-PsDELPH1.contract_id:
    /* A contract handle
       A contract notation as given to an RPC or inside scripts. Can be a
       base58 implicit contract hash or a base58 originated contract hash. */
    $unistring
  $007-PsDELPH1.entrypoint:
    /* entrypoint
       Named entrypoint to a Michelson smart contract */
    "default"
    || "root"
    || "do"
    || "set_delegate"
    || "remove_delegate"
    || string
    /* named */
  $007-PsDELPH1.error:
    /* The full list of RPC errors would be too long to include.
       It is available at RPC `/errors` (GET).
       Errors specific to protocol Alpha have an id that starts with
       `proto.alpha`. */
    any
  $007-PsDELPH1.inlined.endorsement:
    { "branch": $block_hash,
      "operations": $007-PsDELPH1.inlined.endorsement.contents,
      "signature"?: $Signature }
  $007-PsDELPH1.inlined.endorsement.contents:
    { /* Endorsement */
      "kind": "endorsement",
      "level": integer ∈ [-2^31-2, 2^31+2] }
  $007-PsDELPH1.michelson.v1.primitives:
    "ADD"
    | "IF_NONE"
    | "SWAP"
    | "set"
    | "nat"
    | "CHECK_SIGNATURE"
    | "IF_LEFT"
    | "LAMBDA"
    | "Elt"
    | "CREATE_CONTRACT"
    | "NEG"
    | "big_map"
    | "map"
    | "or"
    | "BLAKE2B"
    | "bytes"
    | "SHA256"
    | "SET_DELEGATE"
    | "CONTRACT"
    | "LSL"
    | "SUB"
    | "IMPLICIT_ACCOUNT"
    | "PACK"
    | "list"
    | "PAIR"
    | "Right"
    | "contract"
    | "GT"
    | "LEFT"
    | "STEPS_TO_QUOTA"
    | "storage"
    | "TRANSFER_TOKENS"
    | "CDR"
    | "SLICE"
    | "PUSH"
    | "False"
    | "SHA512"
    | "CHAIN_ID"
    | "BALANCE"
    | "signature"
    | "DUG"
    | "SELF"
    | "EMPTY_BIG_MAP"
    | "LSR"
    | "OR"
    | "XOR"
    | "lambda"
    | "COMPARE"
    | "key"
    | "option"
    | "Unit"
    | "Some"
    | "UNPACK"
    | "NEQ"
    | "INT"
    | "pair"
    | "AMOUNT"
    | "DIP"
    | "ABS"
    | "ISNAT"
    | "EXEC"
    | "NOW"
    | "LOOP"
    | "chain_id"
    | "string"
    | "MEM"
    | "MAP"
    | "None"
    | "address"
    | "CONCAT"
    | "EMPTY_SET"
    | "MUL"
    | "LOOP_LEFT"
    | "timestamp"
    | "LT"
    | "UPDATE"
    | "DUP"
    | "SOURCE"
    | "mutez"
    | "SENDER"
    | "IF_CONS"
    | "RIGHT"
    | "CAR"
    | "CONS"
    | "LE"
    | "NONE"
    | "IF"
    | "SOME"
    | "GET"
    | "Left"
    | "CAST"
    | "int"
    | "SIZE"
    | "key_hash"
    | "unit"
    | "DROP"
    | "EMPTY_MAP"
    | "NIL"
    | "DIG"
    | "APPLY"
    | "bool"
    | "RENAME"
    | "operation"
    | "True"
    | "FAILWITH"
    | "parameter"
    | "HASH_KEY"
    | "EQ"
    | "NOT"
    | "UNIT"
    | "Pair"
    | "ADDRESS"
    | "EDIV"
    | "CREATE_ACCOUNT"
    | "GE"
    | "ITER"
    | "code"
    | "AND"
  $007-PsDELPH1.mutez: $positive_bignum
  $007-PsDELPH1.operation.alpha.contents:
    { /* Endorsement */
      "kind": "endorsement",
      "level": integer ∈ [-2^31-2, 2^31+2] }
    || { /* Seed_nonce_revelation */
         "kind": "seed_nonce_revelation",
         "level": integer ∈ [-2^31-2, 2^31+2],
         "nonce": /^[a-zA-Z0-9]+$/ }
    || { /* Double_endorsement_evidence */
         "kind": "double_endorsement_evidence",
         "op1": $007-PsDELPH1.inlined.endorsement,
         "op2": $007-PsDELPH1.inlined.endorsement }
    || { /* Double_baking_evidence */
         "kind": "double_baking_evidence",
         "bh1": $007-PsDELPH1.block_header.alpha.full_header,
         "bh2": $007-PsDELPH1.block_header.alpha.full_header }
    || { /* Activate_account */
         "kind": "activate_account",
         "pkh": $Ed25519.Public_key_hash,
         "secret": /^[a-zA-Z0-9]+$/ }
    || { /* Proposals */
         "kind": "proposals",
         "source": $Signature.Public_key_hash,
         "period": integer ∈ [-2^31-2, 2^31+2],
         "proposals": [ $Protocol_hash ... ] }
    || { /* Ballot */
         "kind": "ballot",
         "source": $Signature.Public_key_hash,
         "period": integer ∈ [-2^31-2, 2^31+2],
         "proposal": $Protocol_hash,
         "ballot": "nay" | "yay" | "pass" }
    || { /* Reveal */
         "kind": "reveal",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "public_key": $Signature.Public_key }
    || { /* Transaction */
         "kind": "transaction",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "amount": $007-PsDELPH1.mutez,
         "destination": $007-PsDELPH1.contract_id,
         "parameters"?:
           { "entrypoint": $007-PsDELPH1.entrypoint,
             "value": $micheline.007-PsDELPH1.michelson_v1.expression } }
    || { /* Origination */
         "kind": "origination",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "balance": $007-PsDELPH1.mutez,
         "delegate"?: $Signature.Public_key_hash,
         "script": $007-PsDELPH1.scripted.contracts }
    || { /* Delegation */
         "kind": "delegation",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "delegate"?: $Signature.Public_key_hash }
  $007-PsDELPH1.operation.alpha.internal_operation_result:
    { /* reveal */
      "kind": "reveal",
      "source": $007-PsDELPH1.contract_id,
      "nonce": integer ∈ [0, 2^16-1],
      "public_key": $Signature.Public_key,
      "result": $007-PsDELPH1.operation.alpha.operation_result.reveal }
    || { /* transaction */
         "kind": "transaction",
         "source": $007-PsDELPH1.contract_id,
         "nonce": integer ∈ [0, 2^16-1],
         "amount": $007-PsDELPH1.mutez,
         "destination": $007-PsDELPH1.contract_id,
         "parameters"?:
           { "entrypoint": $007-PsDELPH1.entrypoint,
             "value": $micheline.007-PsDELPH1.michelson_v1.expression },
         "result": $007-PsDELPH1.operation.alpha.operation_result.transaction }
    || { /* origination */
         "kind": "origination",
         "source": $007-PsDELPH1.contract_id,
         "nonce": integer ∈ [0, 2^16-1],
         "balance": $007-PsDELPH1.mutez,
         "delegate"?: $Signature.Public_key_hash,
         "script": $007-PsDELPH1.scripted.contracts,
         "result": $007-PsDELPH1.operation.alpha.operation_result.origination }
    || { /* delegation */
         "kind": "delegation",
         "source": $007-PsDELPH1.contract_id,
         "nonce": integer ∈ [0, 2^16-1],
         "delegate"?: $Signature.Public_key_hash,
         "result": $007-PsDELPH1.operation.alpha.operation_result.delegation }
  $007-PsDELPH1.operation.alpha.operation_contents_and_result:
    { /* Endorsement */
      "kind": "endorsement",
      "level": integer ∈ [-2^31-2, 2^31+2],
      "metadata":
        { "balance_updates":
            $007-PsDELPH1.operation_metadata.alpha.balance_updates,
          "delegate": $Signature.Public_key_hash,
          "slots": [ integer ∈ [0, 255] ... ] } }
    || { /* Seed_nonce_revelation */
         "kind": "seed_nonce_revelation",
         "level": integer ∈ [-2^31-2, 2^31+2],
         "nonce": /^[a-zA-Z0-9]+$/,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates } }
    || { /* Double_endorsement_evidence */
         "kind": "double_endorsement_evidence",
         "op1": $007-PsDELPH1.inlined.endorsement,
         "op2": $007-PsDELPH1.inlined.endorsement,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates } }
    || { /* Double_baking_evidence */
         "kind": "double_baking_evidence",
         "bh1": $007-PsDELPH1.block_header.alpha.full_header,
         "bh2": $007-PsDELPH1.block_header.alpha.full_header,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates } }
    || { /* Activate_account */
         "kind": "activate_account",
         "pkh": $Ed25519.Public_key_hash,
         "secret": /^[a-zA-Z0-9]+$/,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates } }
    || { /* Proposals */
         "kind": "proposals",
         "source": $Signature.Public_key_hash,
         "period": integer ∈ [-2^31-2, 2^31+2],
         "proposals": [ $Protocol_hash ... ],
         "metadata": {  } }
    || { /* Ballot */
         "kind": "ballot",
         "source": $Signature.Public_key_hash,
         "period": integer ∈ [-2^31-2, 2^31+2],
         "proposal": $Protocol_hash,
         "ballot": "nay" | "yay" | "pass",
         "metadata": {  } }
    || { /* Reveal */
         "kind": "reveal",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "public_key": $Signature.Public_key,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates,
             "operation_result":
               $007-PsDELPH1.operation.alpha.operation_result.reveal,
             "internal_operation_results"?:
               [ $007-PsDELPH1.operation.alpha.internal_operation_result ... ] } }
    || { /* Transaction */
         "kind": "transaction",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "amount": $007-PsDELPH1.mutez,
         "destination": $007-PsDELPH1.contract_id,
         "parameters"?:
           { "entrypoint": $007-PsDELPH1.entrypoint,
             "value": $micheline.007-PsDELPH1.michelson_v1.expression },
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates,
             "operation_result":
               $007-PsDELPH1.operation.alpha.operation_result.transaction,
             "internal_operation_results"?:
               [ $007-PsDELPH1.operation.alpha.internal_operation_result ... ] } }
    || { /* Origination */
         "kind": "origination",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "balance": $007-PsDELPH1.mutez,
         "delegate"?: $Signature.Public_key_hash,
         "script": $007-PsDELPH1.scripted.contracts,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates,
             "operation_result":
               $007-PsDELPH1.operation.alpha.operation_result.origination,
             "internal_operation_results"?:
               [ $007-PsDELPH1.operation.alpha.internal_operation_result ... ] } }
    || { /* Delegation */
         "kind": "delegation",
         "source": $Signature.Public_key_hash,
         "fee": $007-PsDELPH1.mutez,
         "counter": $positive_bignum,
         "gas_limit": $positive_bignum,
         "storage_limit": $positive_bignum,
         "delegate"?: $Signature.Public_key_hash,
         "metadata":
           { "balance_updates":
               $007-PsDELPH1.operation_metadata.alpha.balance_updates,
             "operation_result":
               $007-PsDELPH1.operation.alpha.operation_result.delegation,
             "internal_operation_results"?:
               [ $007-PsDELPH1.operation.alpha.internal_operation_result ... ] } }
  $007-PsDELPH1.operation.alpha.operation_result.delegation:
    { /* Applied */
      "status": "applied",
      "consumed_gas"?: $positive_bignum,
      "consumed_milligas"?: $positive_bignum }
    || { /* Failed */
         "status": "failed",
         "errors": [ $007-PsDELPH1.error ... ] }
    || { /* Skipped */
         "status": "skipped" }
    || { /* Backtracked */
         "status": "backtracked",
         "errors"?: [ $007-PsDELPH1.error ... ],
         "consumed_gas"?: $positive_bignum,
         "consumed_milligas"?: $positive_bignum }
  $007-PsDELPH1.operation.alpha.operation_result.origination:
    { /* Applied */
      "status": "applied",
      "big_map_diff"?:
        [ { /* update */
            "action": "update",
            "big_map": $bignum,
            "key_hash": $script_expr,
            "key": $micheline.007-PsDELPH1.michelson_v1.expression,
            "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
          || { /* remove */
               "action": "remove",
               "big_map": $bignum }
          || { /* copy */
               "action": "copy",
               "source_big_map": $bignum,
               "destination_big_map": $bignum }
          || { /* alloc */
               "action": "alloc",
               "big_map": $bignum,
               "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
               "value_type": $micheline.007-PsDELPH1.michelson_v1.expression } ... ],
      "balance_updates"?:
        $007-PsDELPH1.operation_metadata.alpha.balance_updates,
      "originated_contracts"?: [ $007-PsDELPH1.contract_id ... ],
      "consumed_gas"?: $positive_bignum,
      "consumed_milligas"?: $positive_bignum,
      "storage_size"?: $bignum,
      "paid_storage_size_diff"?: $bignum }
    || { /* Failed */
         "status": "failed",
         "errors": [ $007-PsDELPH1.error ... ] }
    || { /* Skipped */
         "status": "skipped" }
    || { /* Backtracked */
         "status": "backtracked",
         "errors"?: [ $007-PsDELPH1.error ... ],
         "big_map_diff"?:
           [ { /* update */
               "action": "update",
               "big_map": $bignum,
               "key_hash": $script_expr,
               "key": $micheline.007-PsDELPH1.michelson_v1.expression,
               "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
             || { /* remove */
                  "action": "remove",
                  "big_map": $bignum }
             || { /* copy */
                  "action": "copy",
                  "source_big_map": $bignum,
                  "destination_big_map": $bignum }
             || { /* alloc */
                  "action": "alloc",
                  "big_map": $bignum,
                  "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
                  "value_type":
                    $micheline.007-PsDELPH1.michelson_v1.expression } ... ],
         "balance_updates"?:
           $007-PsDELPH1.operation_metadata.alpha.balance_updates,
         "originated_contracts"?: [ $007-PsDELPH1.contract_id ... ],
         "consumed_gas"?: $positive_bignum,
         "consumed_milligas"?: $positive_bignum,
         "storage_size"?: $bignum,
         "paid_storage_size_diff"?: $bignum }
  $007-PsDELPH1.operation.alpha.operation_result.reveal:
    { /* Applied */
      "status": "applied",
      "consumed_gas"?: $positive_bignum,
      "consumed_milligas"?: $positive_bignum }
    || { /* Failed */
         "status": "failed",
         "errors": [ $007-PsDELPH1.error ... ] }
    || { /* Skipped */
         "status": "skipped" }
    || { /* Backtracked */
         "status": "backtracked",
         "errors"?: [ $007-PsDELPH1.error ... ],
         "consumed_gas"?: $positive_bignum,
         "consumed_milligas"?: $positive_bignum }
  $007-PsDELPH1.operation.alpha.operation_result.transaction:
    { /* Applied */
      "status": "applied",
      "storage"?: $micheline.007-PsDELPH1.michelson_v1.expression,
      "big_map_diff"?:
        [ { /* update */
            "action": "update",
            "big_map": $bignum,
            "key_hash": $script_expr,
            "key": $micheline.007-PsDELPH1.michelson_v1.expression,
            "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
          || { /* remove */
               "action": "remove",
               "big_map": $bignum }
          || { /* copy */
               "action": "copy",
               "source_big_map": $bignum,
               "destination_big_map": $bignum }
          || { /* alloc */
               "action": "alloc",
               "big_map": $bignum,
               "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
               "value_type": $micheline.007-PsDELPH1.michelson_v1.expression } ... ],
      "balance_updates"?:
        $007-PsDELPH1.operation_metadata.alpha.balance_updates,
      "originated_contracts"?: [ $007-PsDELPH1.contract_id ... ],
      "consumed_gas"?: $positive_bignum,
      "consumed_milligas"?: $positive_bignum,
      "storage_size"?: $bignum,
      "paid_storage_size_diff"?: $bignum,
      "allocated_destination_contract"?: boolean }
    || { /* Failed */
         "status": "failed",
         "errors": [ $007-PsDELPH1.error ... ] }
    || { /* Skipped */
         "status": "skipped" }
    || { /* Backtracked */
         "status": "backtracked",
         "errors"?: [ $007-PsDELPH1.error ... ],
         "storage"?: $micheline.007-PsDELPH1.michelson_v1.expression,
         "big_map_diff"?:
           [ { /* update */
               "action": "update",
               "big_map": $bignum,
               "key_hash": $script_expr,
               "key": $micheline.007-PsDELPH1.michelson_v1.expression,
               "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
             || { /* remove */
                  "action": "remove",
                  "big_map": $bignum }
             || { /* copy */
                  "action": "copy",
                  "source_big_map": $bignum,
                  "destination_big_map": $bignum }
             || { /* alloc */
                  "action": "alloc",
                  "big_map": $bignum,
                  "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
                  "value_type":
                    $micheline.007-PsDELPH1.michelson_v1.expression } ... ],
         "balance_updates"?:
           $007-PsDELPH1.operation_metadata.alpha.balance_updates,
         "originated_contracts"?: [ $007-PsDELPH1.contract_id ... ],
         "consumed_gas"?: $positive_bignum,
         "consumed_milligas"?: $positive_bignum,
         "storage_size"?: $bignum,
         "paid_storage_size_diff"?: $bignum,
         "allocated_destination_contract"?: boolean }
  $007-PsDELPH1.operation_metadata.alpha.balance_updates:
    [ { "kind": "contract",
        "contract": $007-PsDELPH1.contract_id,
        "change": $int64 }
      || { "kind": "freezer",
           "category": "rewards",
           "delegate": $Signature.Public_key_hash,
           "cycle": integer ∈ [-2^31-2, 2^31+2],
           "change": $int64 }
      || { "kind": "freezer",
           "category": "fees",
           "delegate": $Signature.Public_key_hash,
           "cycle": integer ∈ [-2^31-2, 2^31+2],
           "change": $int64 }
      || { "kind": "freezer",
           "category": "deposits",
           "delegate": $Signature.Public_key_hash,
           "cycle": integer ∈ [-2^31-2, 2^31+2],
           "change": $int64 } ... ]
  $007-PsDELPH1.scripted.contracts:
    { "code": $micheline.007-PsDELPH1.michelson_v1.expression,
      "storage": $micheline.007-PsDELPH1.michelson_v1.expression }
  $Chain_id:
    /* Network identifier (Base58Check-encoded) */
    $unistring
  $Context_hash:
    /* A hash of context (Base58Check-encoded) */
    $unistring
  $Ed25519.Public_key_hash:
    /* An Ed25519 public key hash (Base58Check-encoded) */
    $unistring
  $Operation_hash:
    /* A Tezos operation ID (Base58Check-encoded) */
    $unistring
  $Operation_list_list_hash:
    /* A list of list of operations (Base58Check-encoded) */
    $unistring
  $Protocol_hash:
    /* A Tezos protocol ID (Base58Check-encoded) */
    $unistring
  $Signature:
    /* A Ed25519, Secp256k1 or P256 signature (Base58Check-encoded) */
    $unistring
  $Signature.Public_key:
    /* A Ed25519, Secp256k1, or P256 public key (Base58Check-encoded) */
    $unistring
  $Signature.Public_key_hash:
    /* A Ed25519, Secp256k1, or P256 public key hash (Base58Check-encoded) */
    $unistring
  $bignum:
    /* Big number
       Decimal representation of a big number */
    string
  $block_hash:
    /* A block identifier (Base58Check-encoded) */
    $unistring
  $block_header_metadata:
    { "protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
      "next_protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
      "test_chain_status": $test_chain_status,
      "max_operations_ttl": integer ∈ [-2^30-2, 2^30+2],
      "max_operation_data_length": integer ∈ [-2^30-2, 2^30+2],
      "max_block_header_length": integer ∈ [-2^30-2, 2^30+2],
      "max_operation_list_length":
        [ { "max_size": integer ∈ [-2^30-2, 2^30+2],
            "max_op"?: integer ∈ [-2^30-2, 2^30+2] } ... ],
      "baker": $Signature.Public_key_hash,
      "level":
        { "level":
            integer ∈ [-2^31-2, 2^31+2]
            /* The level of the block relative to genesis. This is also the
               Shell's notion of level */,
          "level_position":
            integer ∈ [-2^31-2, 2^31+2]
            /* The level of the block relative to the block that starts
               protocol alpha. This is specific to the protocol alpha. Other
               protocols might or might not include a similar notion. */,
          "cycle":
            integer ∈ [-2^31-2, 2^31+2]
            /* The current cycle's number. Note that cycles are a
               protocol-specific notion. As a result, the cycle number starts
               at 0 with the first block of protocol alpha. */,
          "cycle_position":
            integer ∈ [-2^31-2, 2^31+2]
            /* The current level of the block relative to the first block of
               the current cycle. */,
          "voting_period":
            integer ∈ [-2^31-2, 2^31+2]
            /* The current voting period's index. Note that cycles are a
               protocol-specific notion. As a result, the voting period index
               starts at 0 with the first block of protocol alpha. */,
          "voting_period_position":
            integer ∈ [-2^31-2, 2^31+2]
            /* The current level of the block relative to the first block of
               the current voting period. */,
          "expected_commitment":
            boolean
            /* Tells wether the baker of this block has to commit a seed
               nonce hash. */ },
      "voting_period_kind":
        "proposal" || "testing_vote" || "testing" || "promotion_vote",
      "nonce_hash": $cycle_nonce /* Some */ || null /* None */,
      "consumed_gas": $positive_bignum,
      "deactivated": [ $Signature.Public_key_hash ... ],
      "balance_updates":
        $007-PsDELPH1.operation_metadata.alpha.balance_updates }
  $cycle_nonce:
    /* A nonce hash (Base58Check-encoded) */
    $unistring
  $fitness:
    /* Block fitness
       The fitness, or score, of a block, that allow the Tezos to decide
       which chain is the best. A fitness value is a list of byte sequences.
       They are compared as follows: shortest lists are smaller; lists of the
       same length are compared according to the lexicographical order. */
    [ /^[a-zA-Z0-9]+$/ ... ]
  $int64:
    /* 64 bit integers
       Decimal representation of 64 bit integers */
    string
  $micheline.007-PsDELPH1.michelson_v1.expression:
    { /* Int */
      "int": $bignum }
    || { /* String */
         "string": $unistring }
    || { /* Bytes */
         "bytes": /^[a-zA-Z0-9]+$/ }
    || [ $micheline.007-PsDELPH1.michelson_v1.expression ... ]
    /* Sequence */
    || { /* Generic prim (any number of args with or without annot) */
         "prim": $007-PsDELPH1.michelson.v1.primitives,
         "args"?: [ $micheline.007-PsDELPH1.michelson_v1.expression ... ],
         "annots"?: [ string ... ] }
  $operation:
    { "protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
      "chain_id": $Chain_id,
      "hash": $Operation_hash,
      "branch": $block_hash,
      "contents":
        [ $007-PsDELPH1.operation.alpha.operation_contents_and_result ... ],
      "signature"?: $Signature }
    || { "protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
         "chain_id": $Chain_id,
         "hash": $Operation_hash,
         "branch": $block_hash,
         "contents": [ $007-PsDELPH1.operation.alpha.contents ... ],
         "signature"?: $Signature }
    || { "protocol": "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
         "chain_id": $Chain_id,
         "hash": $Operation_hash,
         "branch": $block_hash,
         "contents": [ $007-PsDELPH1.operation.alpha.contents ... ],
         "signature": $Signature }
  $positive_bignum:
    /* Positive big number
       Decimal representation of a positive big number */
    string
  $raw_block_header:
    { "level": integer ∈ [-2^31-2, 2^31+2],
      "proto": integer ∈ [0, 255],
      "predecessor": $block_hash,
      "timestamp": $timestamp.protocol,
      "validation_pass": integer ∈ [0, 255],
      "operations_hash": $Operation_list_list_hash,
      "fitness": $fitness,
      "context": $Context_hash,
      "priority": integer ∈ [0, 2^16-1],
      "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
      "seed_nonce_hash"?: $cycle_nonce,
      "signature": $Signature }
  $script_expr:
    /* A script expression ID (Base58Check-encoded) */
    $unistring
  $test_chain_status:
    /* The status of the test chain: not_running (there is no test chain at
       the moment), forking (the test chain is being setup), running (the
       test chain is running). */
    { /* Not_running */
      "status": "not_running" }
    || { /* Forking */
         "status": "forking",
         "protocol": $Protocol_hash,
         "expiration": $timestamp.protocol }
    || { /* Running */
         "status": "running",
         "chain_id": $Chain_id,
         "genesis": $block_hash,
         "protocol": $Protocol_hash,
         "expiration": $timestamp.protocol }
  $timestamp.protocol:
    /* A timestamp as seen by the protocol: second-level precision, epoch
       based. */
    $unistring
  $unistring:
    /* Universal string representation
       Either a plain UTF8 string, or a sequence of bytes for strings that
       contain invalid byte sequences. */
    string || { "invalid_utf8_string": [ integer ∈ [0, 255] ... ] }