/* istanbul ignore file */

import { ArgsType, Field, Int, ObjectType } from 'type-graphql';

import { DateRange } from '../modules/queries-graphql/repositories/date-range.utils';
import { OrderBy } from '../modules/queries-graphql/repositories/order-by.utils';
import { scalars } from './scalars';

@ObjectType()
export class Account {
    @Field(() => scalars.Address)
    address!: string;
}

@ArgsType()
export class AccountArgs {
    @Field(() => scalars.Address, {
        nullable: false,
        description: `The public key hash of an implicit or originated account.`,
    })
    address!: string;
}

@ArgsType()
export class AccountOperationsArgs {
    @Field(() => Int, {
        nullable: true,
        description: `Sets the number of records retrieved from the beginning. 
        Example: "originations (first: 50)"`,
    })
    first?: number;

    @Field(() => Int, {
        nullable: true,
        description: `Sets the number of records retrieved from the end. 
        Example: "originations (last: 50)"`,
    })
    last?: number;

    @Field(() => scalars.Cursor, {
        nullable: true,
        description: `Used with the argument "first" or "last" for backwards pagination. 
        Example: "originations (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"`,
    })
    before?: string;

    @Field(() => scalars.Cursor, {
        nullable: true,
        description: `Used with the argument "first" or "last" for forward pagination. 
        Example: "originations (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"`,
    })
    after?: string;

    @Field(() => DateRange, {
        nullable: true,
        description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". 
        Example: "originations (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"`,
    })
    date_range?: DateRange;

    @Field(() => OrderBy, {
        nullable: true,
        description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. 
        Example: "originations (order_by:{field: TIMESTAMP, direction: DESC}"`,
    })
    order_by?: OrderBy;
}
