/* istanbul ignore file */

import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { OperationCost } from './operation-cost';
import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType({ implements: Operation })
export class Delegation extends OperationCost implements Operation {
    @Field(() => scalars.Address, {
        nullable: true,
        description: 'An Implicit account to which an account has delegated their baking and endorsement rights.',
    })
    delegate?: string | null;

    @Field(() => scalars.Decimal, { nullable: true, description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas?: Prisma.Decimal | null;
}

@ObjectType()
export class DelegationEdge extends RelayEdge(Delegation) { }

@ObjectType()
export class DelegationConnection extends RelayConnection(DelegationEdge) { }

export interface DelegationSQLResults extends Omit<Delegation, 'timestamp' | 'source'> {
    cursor: string;
    timestamp: string;
    source: string;
}

export interface DelegationData extends Delegation {
    cursor: string;
}
