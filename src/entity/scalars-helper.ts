import { Prisma } from '@prisma/client';
import { UserInputError } from 'apollo-server-express';
import { GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';

import { errorToString } from '../utils/conversion';
import { guard } from '../utils/guard';

/* eslint-disable */
const bs58check: (s: string) => void = require('bs58check').decode;
/* eslint-enable */

export interface Base58HashScalarConfig {
    readonly name: string;
    readonly description: string;
    supportedPrefixes?: readonly { readonly prefix: string; readonly length: number }[];
}

export const base58CheckError = 'Given string is not a valid Base58Check encoded hash.';

export function createBase58HashScalar(config: Base58HashScalarConfig): GraphQLScalarType {
    const prefixes = config.supportedPrefixes;
    return createScalar({
        name: config.name,
        description: config.description,
        parseValue: value => {
            if (prefixes && !prefixes.some(p => value.startsWith(p.prefix) && value.length === p.length)) {
                throw new Error(`Invalid hash prefix. It must be one of: ${prefixes.map(p => `'${p.prefix}' with length ${p.length}`).join(', ')}.`);
            }
            try {
                bs58check(value);
            } catch {
                throw new Error(base58CheckError);
            }
            return value;
        },
    });
}

export interface ScalarConfig<T> {
    readonly name: string;
    readonly description: string;
    parseValue(value: string): T;
}

export function createScalar<T extends { toString(): string }>(config: ScalarConfig<T>): GraphQLScalarType {
    const scalarConfig: GraphQLScalarTypeConfig<T, string> = {
        name: config.name,
        description: config.description,
        parseValue: value => parse(typeof value === 'string' ? value : null, typeof value, value),
        serialize: (value: T) => value.toString(),
        parseLiteral: node => parse(node.kind === 'StringValue' ? node.value : null, node.kind, node),
    };
    return new GraphQLScalarType(scalarConfig);

    function parse(value: string | null, valueType: string, rawValue: unknown): T {
        try {
            if (value === null) {
                throw new Error(`Expected a string but there is ${valueType}.`);
            }
            return config.parseValue(value);
        } catch (error: unknown) {
            throw new UserInputError(errorToString(error, { onlyMessage: true }), {
                actualValue: rawValue,
                scalarType: config.name,
            });
        }
    }
}

export const bigIntError = 'Given string is not a valid integer number.';

export function parseBigInt(strValue: string): bigint {
    try {
        guard.notWhiteSpace(strValue);
        return BigInt(strValue);
    } catch {
        throw new Error(bigIntError);
    }
}

export const positiveBigIntError = 'Given integer number must be positive.';

export function parsePositiveBigInt(strValue: string): bigint {
    const value = parseBigInt(strValue);
    if (value < BigInt('0')) {
        throw new Error(positiveBigIntError);
    }
    return value;
}

export function parseCursor(strValue: string): string {
    try {
        const cursorSplit = strValue.split(':');
        if (!cursorSplit[0] || (!cursorSplit[0].startsWith('oo') && !cursorSplit[0].startsWith('op') && !cursorSplit[0].startsWith('on'))) {
            throw new UserInputError(
                'Argument "cursor" of type "string?" contains an invalid operation hash. An operation hash should start with "op" or "on".',
            );
        }
        if (cursorSplit[0]?.length !== 51 || isNaN(parseInt(cursorSplit[1] ?? '', 10))) {
            throw new UserInputError(
                'Argument "cursor" of type "string?" is in an invalid format. The valid cursor format is "operation_hash:operation_id".',
            );
        }
    } catch (error: unknown) {
        throw new UserInputError(errorToString(error));
    }
    return strValue;
}

export function parseDecimalJS(value: unknown): Prisma.Decimal {
    if (!(typeof value === 'string')) {
        throw new UserInputError(`Invalid argument of type '${typeof value}'. Expected string representing a decimal number.`);
    }
    return new Prisma.Decimal(value);
}
