/* istanbul ignore file */
import { Field, Int, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType({ implements: Operation })
export class Endorsement extends Operation {
    @Field(() => scalars.Address, {
        nullable: true,
        description: 'An Implicit account to which an account has delegated their baking and endorsement rights.',
    })
    delegate?: string | null;

    @Field(() => [Int], { nullable: true })
    slots?: number[] | null;
}

@ObjectType()
export class EndorsementEdge extends RelayEdge(Endorsement) { }

@ObjectType()
export class EndorsementConnection extends RelayConnection(EndorsementEdge) { }

export interface EndorsementSQLResults extends Omit<Endorsement, 'timestamp' | 'source'> {
    cursor: string;
    timestamp: string;
    source: string;
}

export interface EndorsementData extends Endorsement {
    cursor: string;
}
