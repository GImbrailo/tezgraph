/* istanbul ignore file */

import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { OperationCost } from './operation-cost';
import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType({ implements: Operation })
export class Reveal extends OperationCost implements Operation {
    @Field(() => scalars.PublicKey)
    public_key!: string;

    @Field(() => scalars.Decimal, { nullable: true, description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas?: Prisma.Decimal | null;
}

@ObjectType()
export class RevealEdge extends RelayEdge(Reveal) { }

@ObjectType()
export class RevealConnection extends RelayConnection(RevealEdge) { }

export interface RevealSQLResults extends Omit<Reveal, 'timestamp' | 'source'> {
    cursor: string;
    timestamp: string;
    source: string;
}

export interface RevealData extends Reveal {
    cursor: string;
}
