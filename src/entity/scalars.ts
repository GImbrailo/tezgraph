import { GraphQLScalarType } from 'graphql';
import GraphQLJSONObject from 'graphql-type-json';
import { GraphQLISODateTime } from 'type-graphql';

import { createBase58HashScalar, createScalar, parseBigInt, parsePositiveBigInt, parseCursor, parseDecimalJS } from './scalars-helper';

export const scalars = {
    Address: createBase58HashScalar({
        name: 'Address',
        description: 'Tezos address. Represented as public key hash (Base58Check-encoded) prefixed with tz1, tz2, tz3 or KT1',
        supportedPrefixes: [
            { prefix: 'tz1', length: 36 },
            { prefix: 'KT1', length: 36 },
            { prefix: 'tz2', length: 36 },
            { prefix: 'tz3', length: 36 },
        ],
    }),
    BigNumber: createScalar({
        name: 'BigNumber',
        description: 'Arbitrary precision integer number represented as string in JSON.',
        parseValue: parseBigInt,
    }),
    PositiveBigNumber: createScalar({
        name: 'PositiveBigNumber',
        description: 'Arbitrary precision positive integer number represented as string in JSON.',
        parseValue: parsePositiveBigInt,
    }),
    Mutez: createScalar({
        name: 'Mutez',
        description: 'Micro tez. It uses same rules as PositiveBigNumber. 1 tez = 1,000,000 micro tez.',
        parseValue: parsePositiveBigInt,
    }),
    OperationHash: createBase58HashScalar({
        name: 'OperationHash',
        description: 'Operation identifier (Base58Check-encoded) prefixed with o.Operation identifier (Base58Check-encoded) prefixed with o.',
        supportedPrefixes: [{ prefix: 'o', length: 51 }],
    }),
    BlockHash: createBase58HashScalar({
        name: 'BlockHash',
        description: 'Block identifier (Base58Check-encoded) prefixed with B.',
        supportedPrefixes: [{ prefix: 'B', length: 51 }],
    }),
    ProtocolHash: createBase58HashScalar({
        name: 'ProtocolHash',
        description: 'Protocol identifier (Base58Check-encoded) prefixed with P.',
        supportedPrefixes: [{ prefix: 'P', length: 51 }],
    }),
    ContextHash: createBase58HashScalar({
        name: 'ContextHash',
        description: 'ContextHash identifier (Base58Check-encoded) prefixed with Co.',
        supportedPrefixes: [{ prefix: 'Co', length: 52 }],
    }),
    OperationsHash: createBase58HashScalar({
        name: 'OperationsHash',
        description: 'OperationsHash identifier (Base58Check-encoded) prefixed with LLo.',
        supportedPrefixes: [{ prefix: 'LLo', length: 53 }],
    }),
    ChainId: createBase58HashScalar({
        name: 'ChainId',
        description: 'Chain identifier (Base58Check-encoded) prefixed with Net.',
        supportedPrefixes: [{ prefix: 'Net', length: 15 }],
    }),
    Signature: createBase58HashScalar({
        name: 'Signature',
        description: 'Generic signature (Base58Check-encoded) prefixed with sig.',
        supportedPrefixes: [{ prefix: 'sig', length: 96 }],
    }),
    PublicKey: createBase58HashScalar({
        name: 'PublicKey',
        description: 'Public key (Base58Check-encoded) prefixed with edpk, sppk or p2pk.',
        supportedPrefixes: [
            { prefix: 'edpk', length: 54 },
            { prefix: 'p2pk', length: 55 },
            { prefix: 'sppk', length: 55 },
        ],
    }),
    NonceHash: createBase58HashScalar({
        name: 'NonceHash',
        description: 'Nonce hash (Base58Check-encoded).',
    }),
    DateTime: GraphQLISODateTime,
    JSON: GraphQLJSONObject,
    MichelsonJson: new GraphQLScalarType({
        name: 'MichelsonJson',
        description: 'Raw Michelson expression represented as JSON.',
    }),
    Cursor: createScalar({
        name: 'Cursor',
        description: 'A string constructed with an operation hash and a batch position. Used for pagination.',
        parseValue: parseCursor,
    }),
    Decimal: createScalar({
        name: 'Decimal',
        description: 'GraphQL Scalar representing the Prisma.Decimal type, based on Decimal.js library.',
        parseValue: parseDecimalJS,
    }),
};
