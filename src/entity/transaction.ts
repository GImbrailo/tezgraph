/* istanbul ignore file */

import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { OperationCost } from './operation-cost';
import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType({ implements: Operation })
export class Transaction extends OperationCost implements Operation {
    @Field(() => scalars.Decimal, { nullable: true })
    storage_limit?: Prisma.Decimal | null;

    @Field(() => scalars.Mutez, { nullable: true, description: 'The amount of tz transfered.' })
    amount?: bigint | null;

    @Field(() => String, { nullable: true })
    parameters?: string | null;

    @Field(() => String, { nullable: true })
    entrypoint?: string | null;

    @Field(() => scalars.Address, { nullable: true, description: 'The address of an originated account or an implicit account.' })
    destination?: string | null;

    @Field(() => scalars.Decimal, { description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas!: Prisma.Decimal;
}

@ObjectType()
export class TransactionEdge extends RelayEdge(Transaction) { }

@ObjectType()
export class TransactionConnection extends RelayConnection(TransactionEdge) { }

export interface TransactionSQLResults extends Omit<Transaction, 'timestamp' | 'source'> {
    cursor: string;
    timestamp: string;
    source: string;
}

export interface TransactionData extends Transaction {
    cursor: string;
}
