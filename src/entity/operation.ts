import { Field, Int, InterfaceType } from 'type-graphql';

import { scalars } from './scalars';

@InterfaceType()
export class Operation {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    id?: bigint | null;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    batch_position?: number;

    @Field(() => String, { description: 'The kind of operation.' })
    kind!: string;

    @Field(() => scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @Field(() => Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @Field(() => scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    /**
     * The unique identifier of an implicit account for the source of the operation.
     * An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.
     */
    sourceAddress?: string | null;
}
