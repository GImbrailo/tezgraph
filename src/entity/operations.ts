/* istanbul ignore file */

import { Field, Int, ObjectType } from 'type-graphql';

import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType()
export class Operations {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    batch_position!: number;

    @Field(() => String, { description: 'The kind of operation.' })
    kind!: string;

    @Field(() => scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @Field(() => Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @Field(() => scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @Field(() => scalars.BigNumber)
    id!: bigint;

    @Field(() => scalars.Address, {
        nullable: true,
        // eslint-disable-next-line max-len
        description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.',
    })
    source?: string | null;

    @Field(() => scalars.Mutez, { nullable: true, description: 'The cost of an operation in mutez.' })
    fee?: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    counter!: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    gas_limit!: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    storage_limit?: bigint | null;

    @Field(() => scalars.PublicKey, { nullable: true })
    public_key?: string | null;

    @Field(() => scalars.Mutez, { nullable: true, description: 'The amount of tz transfered.' })
    amount?: bigint | null;

    @Field(() => String, { nullable: true })
    parameters?: string | null;

    @Field(() => String, { nullable: true })
    entrypoint?: string | null;

    @Field(() => scalars.Address, { nullable: true, description: 'The address of an originated account or an implicit account.' })
    contract_address?: string | null;

    @Field(() => scalars.Address, { nullable: true })
    destination?: string | null;

    @Field(() => scalars.Address, {
        nullable: true,
        description: 'An Implicit account to which an account has delegated their baking and Operations rights.',
    })
    delegate?: string | null;

    @Field(() => [Int], { nullable: true })
    slots?: number[] | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true, description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas?: bigint | null;
}

@ObjectType()
export class OperationsEdge extends RelayEdge(Operations) { }

@ObjectType()
export class OperationsConnection extends RelayConnection(OperationsEdge) { }

export interface OperationsSQLResults extends Operations {
    cursor: string;
}
