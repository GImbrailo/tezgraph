import { MichelsonV1Expression } from '@taquito/rpc';
import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { BigMapDiff } from './big-map-diff';
import { graphQLDescriptions } from './graphql-descriptions';
import { MoneyOperationNotification, MoneyOperationResult } from './money-operation-notification';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { OperationResult, TransactionOperationParameter } from './operation-result';

@ObjectType({
    implements: OperationResult,
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.transaction),
})
export class TransactionResult extends MoneyOperationResult {
    @Field(() => scalars.MichelsonJson, { nullable: true })
    readonly storage: MichelsonV1Expression | undefined;

    @Field(() => [BigMapDiff], { nullable: true })
    readonly big_map_diff: readonly BigMapDiff[] | undefined;

    @Field(() => Boolean, { nullable: true })
    readonly allocated_destination_contract: boolean | undefined;
}

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.transaction) })
export class TransactionMetadata extends AbstractOperationMetadataWithInternalResults {
    @Field(() => TransactionResult)
    readonly operation_result!: TransactionResult;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.transaction),
})
export class TransactionNotification extends MoneyOperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.transaction),
    })
    readonly kind = OperationKind.transaction;

    @Field(() => scalars.Mutez)
    readonly amount!: bigint;

    @Field(() => scalars.Address)
    readonly destination!: string;

    @Field(() => TransactionOperationParameter, { nullable: true })
    readonly parameters: TransactionOperationParameter | undefined;

    @Field(() => TransactionMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: TransactionMetadata | undefined;
}
