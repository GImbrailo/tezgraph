import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.proposals),
})
export class ProposalsNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.proposals),
    })
    readonly kind = OperationKind.proposals;

    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly period!: number;

    @Field(() => [scalars.ProtocolHash], { nullable: true })
    readonly proposals: readonly string[] | undefined;
}
