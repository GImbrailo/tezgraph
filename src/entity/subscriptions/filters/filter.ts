export interface Filter<TValue> {
    passes(value: TValue): boolean;
}
