import { Field, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { DefaultConstructor, isNullish, NonNullishValue, Nullish } from '../../../utils/reflection';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createEqualityFilterClass, EqualityFilter } from './equality-filter';

export interface ComparableFilter<TScalar> extends EqualityFilter<TScalar> {
    lessThan: Nullish<TScalar>;
    lessThanOrEqualTo: Nullish<TScalar>;
    greaterThanOrEqualTo: Nullish<TScalar>;
    greaterThan: Nullish<TScalar>;
}

export function createComparableFilterClass<TScalar extends NonNullishValue>(
    scalarType: ReturnTypeFuncValue,
): DefaultConstructor<ComparableFilter<TScalar>> {
    const baseType = createEqualityFilterClass<TScalar>(scalarType);

    @InputType({ isAbstract: true })
    class ComparableFilterClass extends baseType implements ComparableFilter<TScalar> {
        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is less than'),
        })
        lessThan: Nullish<TScalar>;

        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is less than or equal to'),
        })
        lessThanOrEqualTo: Nullish<TScalar>;

        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is greater than'),
        })
        greaterThanOrEqualTo: Nullish<TScalar>;

        @Field(() => scalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is greater than or equal to'),
        })
        greaterThan: Nullish<TScalar>;

        passes(value: TScalar): boolean {
            // Using isNullish() instead of ! operator b/c filters can be empty string/zero/false.
            return super.passes(value)
                && (isNullish(this.lessThan) || value < this.lessThan)
                && (isNullish(this.lessThanOrEqualTo) || value <= this.lessThanOrEqualTo)
                && (isNullish(this.greaterThanOrEqualTo) || value >= this.greaterThanOrEqualTo)
                && (isNullish(this.greaterThan) || value > this.greaterThan);
        }
    }
    return ComparableFilterClass;
}
