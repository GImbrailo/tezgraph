import { InputType } from 'type-graphql';

import { scalars } from '../../scalars';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createArrayFilterClass } from './array-filter';
import { createEqualityFilterClass } from './equality-filter';
import { makeNullable } from './nullable-filter';

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.Address.name) })
export class AddressFilter extends createEqualityFilterClass<string>(scalars.Address) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.Address.name, { nullable: true }) })
export class NullableAddressFilter extends makeNullable(AddressFilter) {}

@InputType({ description: graphQLDescriptions.getArrayFilter(scalars.Address.name) })
export class AddressArrayFilter extends createArrayFilterClass<string>(scalars.Address) {}

@InputType({ description: graphQLDescriptions.getArrayFilter(scalars.Address.name, { nullable: true }) })
export class NullableAddressArrayFilter extends makeNullable(AddressArrayFilter) {}
