import { InputType } from 'type-graphql';

import { graphQLDescriptions } from '../graphql-descriptions';
import { OperationResultStatus, OperationResultStatusEnumName } from '../operation-result';
import { createEqualityFilterClass } from './equality-filter';
import { makeNullable } from './nullable-filter';

@InputType({ description: graphQLDescriptions.getEqualityFilter(OperationResultStatusEnumName) })
export class OperationResultStatusFilter extends createEqualityFilterClass<OperationResultStatus>(OperationResultStatus) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(OperationResultStatusEnumName, { nullable: true }) })
export class NullableOperationResultStatusFilter extends makeNullable(OperationResultStatusFilter) {}
