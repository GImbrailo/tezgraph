import { Field, InputType } from 'type-graphql';

import { DefaultConstructor, isNullish, Nullish } from '../../../utils/reflection';
import { Filter } from './filter';

// It would be nice to somehow expose base filter signature too but that's tricky :-(
export interface NullableFilter<TValue> extends Filter<Nullish<TValue>> {
    isNull: Nullish<boolean>;
}

export function makeNullable<TValue>(
    baseFilterType: DefaultConstructor<Filter<TValue>>,
): DefaultConstructor<NullableFilter<TValue>> {
    @InputType({ isAbstract: true })
    class NullableFilterClass extends baseFilterType implements NullableFilter<TValue> {
        @Field(() => Boolean, {
            nullable: true,
            description: 'If the filter value is `true` then matches `null` property value.'
                + ' If the filter value is `false` then matches property values different from `null`.',
        })
        isNull: Nullish<boolean>;

        passes(value: Nullish<TValue>): boolean {
            // Using isNullish() instead of ! operator b/c value or filters can be empty string/zero/false.
            return (isNullish(value) || super.passes(value))
                && (isNullish(this.isNull) || this.isNull === isNullish(value));
        }
    }
    return NullableFilterClass;
}
