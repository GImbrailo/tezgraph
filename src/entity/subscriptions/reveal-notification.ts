import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { MoneyOperationNotification } from './money-operation-notification';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { OperationResult } from './operation-result';

@ObjectType({
    implements: OperationResult,
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.reveal),
})
export class RevealResult extends OperationResult {}

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.reveal) })
export class RevealMetadata extends AbstractOperationMetadataWithInternalResults {
    @Field(() => RevealResult)
    readonly operation_result!: RevealResult;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.reveal),
})
export class RevealNotification extends MoneyOperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.reveal),
    })
    readonly kind = OperationKind.reveal;

    @Field(() => scalars.PublicKey)
    readonly public_key!: string;

    @Field(() => RevealMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: RevealMetadata | undefined;
}
