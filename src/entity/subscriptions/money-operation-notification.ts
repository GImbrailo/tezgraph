import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { BalanceUpdate } from './balance-update';
import { OperationNotification } from './operation-notification';
import { OperationResult } from './operation-result';

@ObjectType({ isAbstract: true })
export class MoneyOperationResult extends OperationResult {
    @Field(() => [BalanceUpdate], { nullable: true })
    readonly balance_updates: readonly BalanceUpdate[] | undefined;

    @Field(() => [scalars.Address], { nullable: true })
    readonly originated_contracts!: readonly string[] | undefined;

    @Field(() => scalars.BigNumber, { nullable: true })
    readonly storage_size: bigint | undefined;

    @Field(() => scalars.BigNumber, { nullable: true })
    readonly paid_storage_size_diff: bigint | undefined;
}

@ObjectType({ isAbstract: true })
export abstract class MoneyOperationNotification extends OperationNotification {
    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => scalars.Mutez)
    readonly fee!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly counter!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly gas_limit!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly storage_limit!: bigint;
}
