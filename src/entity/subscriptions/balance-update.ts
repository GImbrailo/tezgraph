import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';

export enum BalanceUpdateKind {
    contract = 'contract',
    freezer = 'freezer',
}

registerEnumType(BalanceUpdateKind, {
    name: 'BalanceUpdateKind',
    description: 'The kind of a balance update.',
});

export enum BalanceUpdateCategory {
    rewards = 'rewards',
    fees = 'fees',
    deposits = 'deposits',
}

registerEnumType(BalanceUpdateCategory, {
    name: 'BalanceUpdateCategory',
    description: 'The category of a balance update.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$operation_metadata.alpha.balance_updates']) })
export class BalanceUpdate {
    @Field(() => BalanceUpdateKind)
    readonly kind!: BalanceUpdateKind;

    @Field(() => BalanceUpdateCategory, { nullable: true })
    readonly category!: BalanceUpdateCategory | undefined;

    @Field(() => scalars.Address, { nullable: true })
    readonly contract!: string | undefined;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | undefined;

    @Field(() => Int, { nullable: true })
    readonly cycle!: number | undefined;

    @Field(() => scalars.BigNumber)
    readonly change!: bigint;
}
