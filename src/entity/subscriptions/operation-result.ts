import { MichelsonV1Expression } from '@taquito/rpc';
import { Field, Int, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { OperationKind } from './operation-notification';
import { ScriptedContracts } from './scripted-contracts';

export const OperationResultStatusEnumName = 'OperationResultStatus';

export enum OperationResultStatus {
    applied = 'applied',
    failed = 'failed',
    skipped = 'skipped',
    backtracked = 'backtracked',
}

registerEnumType(OperationResultStatus, {
    name: OperationResultStatusEnumName,
    description: 'Operation result status.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$error']) })
export class OperationError {
    @Field(() => String)
    readonly kind!: string;

    @Field(() => String)
    readonly id!: string;
}

@InterfaceType({ resolveType: (value: OperationResult): string => value.typeName })
export abstract class OperationResult {
    @Field(() => OperationResultStatus)
    readonly status!: OperationResultStatus;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly consumed_gas: bigint | undefined;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly consumed_milligas: bigint | undefined;

    @Field(() => [OperationError], { nullable: true })
    readonly errors: readonly OperationError[] | undefined;

    // Used to determine concrete type because usually it's an object literal, not concrete instance.
    readonly typeName!: string;
}

export enum InternalOperationResultKind {
    reveal = 'reveal',
    transaction = 'transaction',
    origination = 'origination',
    delegation = 'delegation',
}

registerEnumType(InternalOperationResultKind, {
    name: 'InternalOperationResultKind',
    description: 'The kind of an internal operation result.',
});

@ObjectType({
    description: `${graphQLDescriptions.getRpcOperation(OperationKind.transaction, ['parameters'], { trailingPeriod: false })}`
        + ` or ${graphQLDescriptions.getRpc(['$operation.alpha.internal_operation_result', 'parameters'], { capitalize: false })}`,
})
export class TransactionOperationParameter {
    @Field()
    readonly entrypoint!: string;

    @Field(() => scalars.MichelsonJson)
    readonly value!: MichelsonV1Expression;
}

@ObjectType({ description: graphQLDescriptions.getRpc(['$operation.alpha.internal_operation_result']) })
export class InternalOperationResult {
    @Field(() => InternalOperationResultKind)
    readonly kind!: InternalOperationResultKind;

    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly nonce!: number;

    @Field(() => scalars.Mutez, { nullable: true })
    readonly amount: bigint | undefined;

    @Field(() => scalars.Address, { nullable: true })
    readonly destination!: string | undefined;

    @Field(() => TransactionOperationParameter, { nullable: true })
    readonly parameters: TransactionOperationParameter | undefined;

    @Field(() => scalars.PublicKey, { nullable: true })
    readonly public_key: string | undefined;

    @Field(() => scalars.Mutez, { nullable: true })
    readonly balance: bigint | undefined;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @Field(() => ScriptedContracts, { nullable: true })
    readonly script: ScriptedContracts | undefined;

    @Field(() => OperationResult)
    readonly result!: OperationResult;
}
