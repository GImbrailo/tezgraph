import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { OperationNotification } from './operation-notification';

export const numberOfLastBlocksToDeduplicate = 100;

@ObjectType()
export class BlockHeader {
    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly proto!: number;

    @Field(() => scalars.BlockHash)
    readonly predecessor!: string;

    @Field(() => scalars.DateTime)
    readonly timestamp!: Date;

    @Field(() => scalars.Signature)
    readonly signature!: string;
}

@ObjectType()
export class BlockNotification {
    @Field(() => scalars.BlockHash)
    readonly hash!: string;

    @Field(() => BlockHeader)
    readonly header!: BlockHeader;

    @Field(() => scalars.ProtocolHash)
    readonly protocol!: string;

    @Field(() => scalars.ChainId)
    readonly chain_id!: string;

    // Internal property for calculating metrics.
    readonly received_from_tezos_on!: Date;

    // Operations are exposed per kind in a corresponding resolver.
    readonly operations!: readonly OperationNotification[];
}
