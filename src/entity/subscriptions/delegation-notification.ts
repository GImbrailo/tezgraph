import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { MoneyOperationNotification } from './money-operation-notification';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { OperationResult } from './operation-result';

@ObjectType({
    implements: OperationResult,
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.delegation),
})
export class DelegationResult extends OperationResult {}

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.delegation) })
export class DelegationMetadata extends AbstractOperationMetadataWithInternalResults {
    @Field(() => DelegationResult)
    readonly operation_result!: DelegationResult;
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.delegation),
})
export class DelegationNotification extends MoneyOperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.delegation),
    })
    readonly kind = OperationKind.delegation;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @Field(() => DelegationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: DelegationMetadata | undefined;
}
