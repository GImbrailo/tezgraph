import { Field, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';

import { DefaultConstructor } from '../../utils/reflection';
import { isWhiteSpace } from '../../utils/string-manipulation';
import { scalars } from '../scalars';
import { BlockNotification } from './block-notification';
import { graphQLDescriptions } from './graphql-descriptions';

@ObjectType({ description: `${graphQLDescriptions.getRpc(['$operation'], { trailingPeriod: false })} but its \`contents\` is the parent object.` })
export class OperationNotificationInfo {
    @Field(() => scalars.ProtocolHash)
    readonly protocol!: string;

    @Field(() => scalars.ChainId, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly chain_id!: string | undefined;

    @Field(() => scalars.OperationHash, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly hash!: string | undefined;

    @Field(() => scalars.BlockHash)
    readonly branch!: string;

    @Field(() => scalars.Signature, { nullable: true })
    readonly signature!: string | undefined;

    readonly received_from_tezos_on!: Date;
}

export enum OperationKind {
    origination = 'origination',
    delegation = 'delegation',
    reveal = 'reveal',
    transaction = 'transaction',
    activate_account = 'activate_account',
    endorsement = 'endorsement',
    seed_nonce_revelation = 'seed_nonce_revelation',
    double_endorsement_evidence = 'double_endorsement_evidence',
    double_baking_evidence = 'double_baking_evidence',
    proposals = 'proposals',
    ballot = 'ballot',
}

registerEnumType(OperationKind, {
    name: 'OperationKind',
    description: 'The kind of an operation.',
});

export enum OperationOrigin {
    BLOCK = 'block',
    MEMPOOL = 'mempool',
}

registerEnumType(OperationOrigin, {
    name: 'OperationOrigin',
    description: 'Indicates if the operation is just in *mempool* or already baked into a *block*.',
});

@InterfaceType({ description: graphQLDescriptions.getRpc([graphQLDescriptions.rpcOperation]) })
export abstract class OperationNotification {
    @Field()
    readonly info!: OperationNotificationInfo;

    @Field(() => OperationKind, {
        description: 'The kind of the operation. It is constant for a conrete *Operation* type'
            + ` e.g. it is always \`${OperationKind.transaction}\` for a *Transaction Operation*`,
    })
    readonly abstract kind: OperationKind;

    @Field(() => BlockNotification, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly block!: BlockNotification | undefined;

    @Field(() => OperationOrigin)
    readonly origin!: OperationOrigin;
}

export function getOperationKind<TOperation extends OperationNotification>(operationType: DefaultConstructor<TOperation>): OperationKind {
    return new operationType().kind;
}

export const numberOfLastMempoolGroupsToDeduplicate = 3_000;

export type MempoolOperationGroup = readonly OperationNotification[];

export function getSignature(group: MempoolOperationGroup): string {
    if (!group[0]) {
        throw new Error('Unexpectedly, there are no operations in the mempool operation group.');
    }

    const signature = group[0].info.signature;
    if (isWhiteSpace(signature)) {
        throw new Error('Unexpectedly, there is no signature on the first operation in the mempool operation group.');
    }
    return signature;
}
