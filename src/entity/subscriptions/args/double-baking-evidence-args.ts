import { Field, InputType } from 'type-graphql';

import { isNotNullish, Nullish } from '../../../utils/reflection';
import { DoubleBakingEvidenceNotification } from '../double-baking-evidence-notification';
import { NullableAddressArrayFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class DoubleBakingEvidenceSpecificFilter implements Filter<DoubleBakingEvidenceNotification> {
    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressArrayFilter>;

    passes(operation: DoubleBakingEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata?.balance_updates.map(u => u.delegate).filter(isNotNullish));
    }
}

export const DoubleBakingEvidenceFilter = createOperationFilterClass(DoubleBakingEvidenceSpecificFilter);
export const DoubleBakingEvidenceArgs = createOperationArgsClass(DoubleBakingEvidenceFilter);
