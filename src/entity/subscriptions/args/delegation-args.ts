import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { DelegationNotification } from '../delegation-notification';
import { AddressFilter, NullableAddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { NullableOperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class DelegationSpecificFilter implements Filter<DelegationNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: DelegationNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.delegate || this.delegate.passes(operation.delegate))
            && (!this.status || this.status.passes(operation.metadata?.operation_result.status));
    }
}

export const DelegationFilter = createOperationFilterClass(DelegationSpecificFilter);
export const DelegationArgs = createOperationArgsClass(DelegationFilter);
