import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { AddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { MutezFilter } from '../filters/numeric-filters';
import { NullableOperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { TransactionNotification } from '../transaction-notification';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class TransactionSpecificFilter implements Filter<TransactionNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => AddressFilter, { nullable: true })
    readonly destination: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    @Field(() => MutezFilter, { nullable: true })
    readonly amount: Nullish<MutezFilter>;

    passes(operation: TransactionNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.destination || this.destination.passes(operation.destination))
            && (!this.status || this.status.passes(operation.metadata?.operation_result.status))
            && (!this.amount || this.amount.passes(operation.amount));
    }
}

export const TransactionFilter = createOperationFilterClass(TransactionSpecificFilter);
export const TransactionArgs = createOperationArgsClass(TransactionFilter);
