import { InputType } from 'type-graphql';

import { Filter } from '../filters/filter';
import { SeedNonceRevelationNotification } from '../seed-nonce-revelation-notification';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class SeedNonceRevelationSpecificFilter implements Filter<SeedNonceRevelationNotification> {
    passes(): boolean {
        return true;
    }
}

export const SeedNonceRevelationFilter = createOperationFilterClass(SeedNonceRevelationSpecificFilter);
export const SeedNonceRevelationArgs = createOperationArgsClass(SeedNonceRevelationFilter);
