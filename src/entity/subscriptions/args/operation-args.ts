import { ArgsType, Field, InputType } from 'type-graphql';

import { DefaultConstructor, nameof, Nullish } from '../../../utils/reflection';
import { removeRequiredSuffix } from '../../../utils/string-manipulation';
import { Filter } from '../filters/filter';
import { BlockHashFilter, NullableOperationHashFilter, ProtocolHashFilter } from '../filters/hash-filters';
import { OperationNotification } from '../operation-notification';
import { SubscriptionArgs } from './subscription-args';

export interface OperationFilter<TOperation extends OperationNotification> extends Filter<TOperation> {
    readonly hash: Nullish<NullableOperationHashFilter>;
    readonly protocol: Nullish<ProtocolHashFilter>;
    readonly branch: Nullish<BlockHashFilter>;
    readonly and: Nullish<readonly OperationFilter<TOperation>[]>;
    readonly or: Nullish<readonly OperationFilter<TOperation>[]>;
}

export function createOperationFilterClass<TOperation extends OperationNotification>(
    specificFilterType: DefaultConstructor<Filter<TOperation>>,
): DefaultConstructor<OperationFilter<TOperation>> {
    const operationName = removeRequiredSuffix(specificFilterType.name, 'SpecificFilter');

    @InputType(`${operationName}Filter`, {
        description: `Provides filters for filtering *${operationName}* operations.`,
    })
    class OperationFilterClass extends specificFilterType {
        @Field(() => NullableOperationHashFilter, { nullable: true })
        readonly hash: Nullish<NullableOperationHashFilter>;

        @Field(() => ProtocolHashFilter, { nullable: true })
        readonly protocol: Nullish<ProtocolHashFilter>;

        @Field(() => BlockHashFilter, { nullable: true })
        readonly branch: Nullish<BlockHashFilter>;

        @Field(() => [OperationFilterClass], {
            description: getFilterArrayDescription({ mustPass: 'all filters' }),
            nullable: true,
        })
        readonly and: Nullish<OperationFilterClass[]>;

        @Field(() => [OperationFilterClass], {
            description: getFilterArrayDescription({ mustPass: 'at least one filter' }),
            nullable: true,
        })
        readonly or: Nullish<OperationFilterClass[]>;

        passes(operation: TOperation): boolean {
            return super.passes(operation)
                && (!this.hash || this.hash.passes(operation.info.hash))
                && (!this.protocol || this.protocol.passes(operation.info.protocol))
                && (!this.branch || this.branch.passes(operation.info.branch))
                && (!this.and || this.and.length === 0 || this.and.every(f => f.passes(operation)))
                && (!this.or || this.or.length === 0 || this.or.some(f => f.passes(operation)));
        }
    }
    return OperationFilterClass;
}

function getFilterArrayDescription(options: { mustPass: string }): string {
    return `The recursive array of filters of the same type. It can be used to specify advanced composite conditions.`
        + ` If specified (non-empty array) then ${options.mustPass} in the array must pass (in addition to other filter properties).`;
}

export interface OperationArgs<TOperation extends OperationNotification> {
    readonly replayFromBlockLevel: Nullish<number>;
    readonly includeMempool: boolean;
    readonly filter: Nullish<OperationFilter<TOperation>>;
}

export function createOperationArgsClass<TOperation extends OperationNotification>(
    filterType: DefaultConstructor<OperationFilter<TOperation>>,
): DefaultConstructor<OperationArgs<TOperation>> {
    @ArgsType()
    class OperationArgsClass extends SubscriptionArgs {
        @Field(() => Boolean, {
            description: `If combined with \`${nameof<SubscriptionArgs>('replayFromBlockLevel')}\``
                + ' then while returning historical operations, no mempool operations are returned.'
                + ' Once switched to new operations then also mempool operations are included.',
        })
        readonly includeMempool = false;

        @Field(() => filterType, { nullable: true })
        readonly filter: Nullish<OperationFilter<TOperation>>;
    }
    return OperationArgsClass;
}
