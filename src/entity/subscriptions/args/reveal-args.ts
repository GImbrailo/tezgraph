import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { AddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { NullableOperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { RevealNotification } from '../reveal-notification';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class RevealSpecificFilter implements Filter<RevealNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: RevealNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.status || this.status.passes(operation.metadata?.operation_result.status));
    }
}

export const RevealFilter = createOperationFilterClass(RevealSpecificFilter);
export const RevealArgs = createOperationArgsClass(RevealFilter);
