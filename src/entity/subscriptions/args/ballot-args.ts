import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { BallotNotification, BallotVote } from '../ballot-notification';
import { AddressFilter } from '../filters/address-filters';
import { createEqualityFilterClass } from '../filters/equality-filter';
import { Filter } from '../filters/filter';
import { ProtocolHashFilter } from '../filters/hash-filters';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType()
export class BallotVoteFilter extends createEqualityFilterClass<BallotVote>(BallotVote) {}

@InputType({ isAbstract: true })
export class BallotSpecificFilter implements Filter<BallotNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => ProtocolHashFilter, { nullable: true })
    readonly proposal: Nullish<ProtocolHashFilter>;

    @Field(() => BallotVoteFilter, { nullable: true })
    readonly ballot: Nullish<BallotVoteFilter>;

    passes(operation: BallotNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.proposal || this.proposal.passes(operation.proposal))
            && (!this.ballot || this.ballot.passes(operation.ballot));
    }
}

export const BallotFilter = createOperationFilterClass(BallotSpecificFilter);
export const BallotArgs = createOperationArgsClass(BallotFilter);
