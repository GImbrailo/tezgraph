import { ArgsType, Field, Int } from 'type-graphql';

import { EnvConfig } from '../../../utils/configuration/env-config';
import { EnvConfigProvider } from '../../../utils/configuration/env-config-provider';
import { Nullish } from '../../../utils/reflection';

const envConfig = new EnvConfig(new EnvConfigProvider(process.env));

@ArgsType()
export class SubscriptionArgs {
    @Field(() => Int, {
        nullable: true,
        description: 'Specifies a historical block level to start to retrieve operations. It seamlessly switches to new operations.'
            + ` The value can go only ${envConfig.replayFromBlockLevelMaxFromHead} levels back from the level of the head block.`,
    })
    readonly replayFromBlockLevel: Nullish<number>;
}
