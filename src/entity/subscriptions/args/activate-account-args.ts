import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../utils/reflection';
import { ActivateAccountNotification } from '../activate-account-notification';
import { AddressFilter } from '../filters/address-filters';
import { Filter } from '../filters/filter';
import { createOperationArgsClass, createOperationFilterClass } from './operation-args';

@InputType({ isAbstract: true })
export class ActivateAccountSpecificFilter implements Filter<ActivateAccountNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly pkh: Nullish<AddressFilter>;

    passes(operation: ActivateAccountNotification): boolean {
        return !this.pkh || this.pkh.passes(operation.pkh);
    }
}

export const ActivateAccountFilter = createOperationFilterClass(ActivateAccountSpecificFilter);
export const ActivateAccountArgs = createOperationArgsClass(ActivateAccountFilter);
