import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.activate_account),
})
export class ActivateAccountNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.activate_account),
    })
    readonly kind = OperationKind.activate_account;

    @Field(() => scalars.Address)
    readonly pkh!: string;

    @Field(() => String)
    readonly secret!: string;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | undefined;
}
