import { MichelsonV1Expression } from '@taquito/rpc';
import { Field, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';

@ObjectType({ description: `The code and the storage of a contract. ${graphQLDescriptions.getRpc(['$scripted.contracts'])}` })
export class ScriptedContracts {
    @Field(() => [scalars.MichelsonJson], {
        description: 'The code of the contract.',
    })
    readonly code!: readonly MichelsonV1Expression[];

    @Field(() => scalars.MichelsonJson, {
        description: 'The current storage value in Michelson format.',
    })
    readonly storage!: MichelsonV1Expression;
}
