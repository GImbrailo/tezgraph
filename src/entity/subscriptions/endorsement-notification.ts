import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';
import { graphQLDescriptions } from './graphql-descriptions';
import { AbstractOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.endorsement) })
export class EndorsementMetadata extends AbstractOperationMetadata {
    @Field(() => scalars.Address)
    readonly delegate!: string;

    @Field(() => [Int])
    readonly slots!: readonly number[];
}

@ObjectType({
    implements: OperationNotification,
    description: graphQLDescriptions.getRpcOperation(OperationKind.endorsement),
})
export class EndorsementNotification extends OperationNotification {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getOperationKind(OperationKind.endorsement),
    })
    readonly kind = OperationKind.endorsement;

    @Field(() => Int)
    readonly level!: number;

    @Field(() => EndorsementMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: EndorsementMetadata | undefined;
}
