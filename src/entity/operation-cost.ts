import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { scalars } from './scalars';

@ObjectType({ isAbstract: true })
export class OperationCost extends Operation {
    @Field(() => scalars.Mutez, { description: 'The cost of an operation in mutez.' })
    fee!: bigint;

    @Field(() => scalars.Decimal, { nullable: true })
    counter?: Prisma.Decimal | null;

    @Field(() => scalars.Decimal, { nullable: true })
    gas_limit?: Prisma.Decimal | null;
}
