// Disabling this rules because for generic types, TypeGraphQL has to rely on inference (no explicit return type)
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Field, ObjectType, ClassType } from 'type-graphql';

import { PageInfo } from './pageInfo';
import { scalars } from './scalars';

export function RelayConnection<TOperationEdge>(OperationEdgeClass: ClassType<TOperationEdge>) {
    @ObjectType({ isAbstract: true })
    abstract class RelayConnectionClass {
        @Field(() => PageInfo, { description: 'An object containing the details of the current page.' })
        page_info!: PageInfo;

        @Field(() => [OperationEdgeClass], { nullable: true, description: 'A list containing the operation data.' })
        edges?: TOperationEdge[] | null;
    }

    return RelayConnectionClass;
}

export function RelayEdge<TOperation>(OperationClass: ClassType<TOperation>) {
    @ObjectType({ isAbstract: true })
    abstract class RelayEdgeClass {
        @Field(() => scalars.Cursor, { description: 'A string build from the operation hash and the batch position. Used for pagination.' })
        cursor!: string;

        @Field(() => OperationClass, { nullable: true, description: 'An object containing operation data.' })
        node?: TOperation | null;
    }

    return RelayEdgeClass;
}
