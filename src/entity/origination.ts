/* istanbul ignore file */

import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { Operation } from './operation';
import { OperationCost } from './operation-cost';
import { RelayEdge, RelayConnection } from './relay';
import { scalars } from './scalars';

@ObjectType({ implements: Operation })
export class Origination extends OperationCost implements Operation {
    @Field(() => scalars.Address, { description: 'The address of an originated account or an implicit account.' })
    contract_address!: string;

    @Field(() => scalars.Decimal, { description: 'A measure of the milligas consumed during the operation.' })
    consumed_milligas!: Prisma.Decimal;
}

@ObjectType()
export class OriginationEdge extends RelayEdge(Origination) { }

@ObjectType()
export class OriginationConnection extends RelayConnection(OriginationEdge) { }

export interface OriginationSQLResults extends Omit<Origination, 'timestamp' | 'source'> {
    cursor: string;
    timestamp: string;
    source: string;
}

export interface OriginationData extends Origination {
    cursor: string;
}
