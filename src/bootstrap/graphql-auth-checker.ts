import { ForbiddenError } from 'apollo-server';
import { singleton } from 'tsyringe';

import { EnvConfig } from '../utils/configuration/env-config';
import { ResolverContext } from './resolver-context';

export const apiKeyHeader = 'X-TezGraph-Key';

@singleton()
export class GraphQLAuthChecker {
    constructor(private readonly envConfig: EnvConfig) {}

    check(context: ResolverContext): boolean {
        if (!this.envConfig.apiKey) {
            return true;
        }

        const apiKey: string | undefined = context.connection?.context[apiKeyHeader]; // eslint-disable-line
        if (apiKey !== this.envConfig.apiKey) {
            throw new ForbiddenError(`Specified value '${apiKey ?? ''}' of '${apiKeyHeader}' authorization header/variable is invalid.`);
        }
        return true;
    }
}
