import { AsyncOrSync } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

/** Arbitrary logic which is started and stopped together with the app. */
export interface BackgroundWorker {
    readonly name: string;
    start(): AsyncOrSync<void>;
    stop?(): AsyncOrSync<void>;
}

export const backgroundWorkersDIToken: InjectionToken<BackgroundWorker> = 'BackgroundWorkers';
