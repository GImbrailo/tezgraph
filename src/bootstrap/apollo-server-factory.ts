import { GraphQLServerOptions } from 'apollo-server-core/src/graphqlOptions';
import { formatApolloErrors } from 'apollo-server-errors';
import { ApolloServer, ApolloServerExpressConfig } from 'apollo-server-express';
import { Request, Response } from 'express';
import { ExecutionResult, GraphQLError, GraphQLFormattedError } from 'graphql';
import depthLimit from 'graphql-depth-limit';
import { createComplexityLimitRule } from 'graphql-validation-complexity';
import { DependencyContainer, inject, injectAll, singleton } from 'tsyringe';
import { buildSchema, NonEmptyArray } from 'type-graphql';

import { EnvConfig } from '../utils/configuration/env-config';
import { diContainerDIToken } from '../utils/dependency-injection';
import { UuidGenerator } from '../utils/uuid-generator';
import { GraphQLAuthChecker } from './graphql-auth-checker';
import { GraphQLDependencyContainer } from './graphql-dependency-container';
import { GraphQLErrorHandler } from './graphql-error-handler';
import { graphQLMiddlewaresDIToken, Middleware } from './graphql-middleware-type';
import { graphQLResolversDIToken, GraphQLResolverType } from './graphql-resolver-type';
import { ResolverContext } from './resolver-context';

type ErrorFormatter = (error: GraphQLError) => GraphQLFormattedError;
type CreateErrorFormatter = (context: ResolverContext | undefined) => ErrorFormatter;

export interface CustomApolloServerConfig extends ApolloServerExpressConfig {
    createErrorFormatter: CreateErrorFormatter;
}

export class CustomApolloServer extends ApolloServer {
    private readonly createErrorFormatter: CreateErrorFormatter;

    constructor(config: CustomApolloServerConfig) {
        super(config);
        this.createErrorFormatter = config.createErrorFormatter;
    }

    async createGraphQLServerOptions(req: Request, res: Response): Promise<GraphQLServerOptions> {
        const options = await super.createGraphQLServerOptions(req, res);
        return {
            ...options,
            formatError: this.createErrorFormatter(options.context as ResolverContext),
        };
    }
}

@singleton()
export class ApolloServerFactory {
    constructor(
        private readonly envConfig: EnvConfig,
        @injectAll(graphQLResolversDIToken) private readonly resolvers: NonEmptyArray<GraphQLResolverType>,
        @injectAll(graphQLMiddlewaresDIToken) private readonly middlewares: Middleware[],
        @inject(diContainerDIToken) private readonly container: DependencyContainer,
        private readonly errorHandler: GraphQLErrorHandler,
        private readonly authChecker: GraphQLAuthChecker,
        private readonly uuidGenerator: UuidGenerator,
    ) { }

    async create(): Promise<ApolloServer> {
        return new CustomApolloServer({
            schema: await buildSchema({
                resolvers: this.resolvers,
                validate: false,
                container: new GraphQLDependencyContainer(this.container),
                authChecker: c => this.authChecker.check(c.context),
                globalMiddlewares: this.middlewares,
            }),
            context: (original): ResolverContext => {
                const context = {
                    ...original,
                    requestId: this.uuidGenerator.generate(),
                    container: this.container,
                };
                this.registerErrorFormatter(context);
                return context;
            },
            // CustomApolloServer.createGraphQLServerOptions() also sets formatError but it's not called always -> we keep it here too.
            formatError: this.createErrorFormatter(undefined),
            createErrorFormatter: this.createErrorFormatter.bind(this),
            subscriptions: this.envConfig.subscriptionKeepAliveMillis > 0 ? { keepAlive: this.envConfig.subscriptionKeepAliveMillis } : {},
            validationRules: [
                ...this.envConfig.graphQLDepthLimit > 0 ? [depthLimit(this.envConfig.graphQLDepthLimit)] : [],
                ...this.envConfig.graphQLComplexityLimit > 0 ? [createComplexityLimitRule(this.envConfig.graphQLComplexityLimit)] : [],
            ],
            debug: this.envConfig.enableDebug, // E.g. shows error stack traces.
            playground: true, // We want in also on prod b/c it's a documentation.
            introspection: true, // Needed for the playground.
        });
    }

    private registerErrorFormatter(context: ResolverContext): void {
        if (!context.connection) {
            return;
        }

        context.connection.formatError = this.createErrorFormatter(context);
        context.connection.formatResponse = (value: ExecutionResult): ExecutionResult => {
            if (value.errors && value.errors.length > 0) {
                value.errors = formatApolloErrors(value.errors, {
                    formatter: this.createErrorFormatter(context),
                    debug: this.envConfig.enableDebug,
                });
            }
            return value;
        };
    }

    private createErrorFormatter(context: ResolverContext | undefined): ErrorFormatter {
        return (error): GraphQLFormattedError => this.errorHandler.handle(error, context);
    }
}
