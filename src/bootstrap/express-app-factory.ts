import express from 'express';
import { injectAll, InjectionToken, singleton } from 'tsyringe';

import { HttpHandler, httpHandlersDIToken } from '../utils/http-handler';

export const expressAppDIToken: InjectionToken<express.Express> = 'ExpressApp';

@singleton()
export class ExpressAppFactory {
    constructor(
        @injectAll(httpHandlersDIToken) private readonly httpHandlers: readonly HttpHandler[],
    ) { }

    create(): express.Express {
        const expressApp = express();

        for (const httpHandler of this.httpHandlers) {
            expressApp.get(httpHandler.urlPath, (request, response) => httpHandler.handle(request, response));
        }
        return expressApp;
    }
}
