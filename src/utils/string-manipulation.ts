import { isNullish, Nullish } from './reflection';

export function removeRequiredSuffix(str: string, suffixToRemove: string): string {
    if (!str.endsWith(suffixToRemove)) {
        throw new Error(`Given string "${str}" doesn't end with suffix "${suffixToRemove}" which should be removed.`);
    }
    return str.slice(undefined, str.length - suffixToRemove.length);
}

const whiteSpaceRegex = /^\s*$/u;

export function isWhiteSpace(str: Nullish<string>): str is null | undefined | '' {
    return isNullish(str) || str.length === 0 || whiteSpaceRegex.test(str);
}

export function joinQuoted(strs: readonly string[]): string {
    return strs.map(s => `'${s}'`).join(', ');
}
