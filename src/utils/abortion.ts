const abortErrorName = 'AbortError' as const;

export function isAbortError(error: unknown): boolean {
    return error instanceof Error && error.name === abortErrorName;
}

export function addAbortListener(signal: AbortSignal, listener: () => void): void {
    signal.addEventListener('abort', listener);
}
