import { addEnvVarsFromFile } from './env-vars-loader';

addEnvVarsFromFile('.env.local');
addEnvVarsFromFile('.env');
