import { inject, InjectionToken, singleton } from 'tsyringe';

import { errorToString } from '../conversion';
import { getEnumValues } from '../reflection';

export enum IntegerLimit {
    Positive = 'Positive',
    PositiveOrZero = 'PositiveOrZero',
}

export const processDIToken: InjectionToken<NodeJS.Process> = 'Process';
export const processEnvDIToken: InjectionToken<NodeJS.ProcessEnv> = 'ProcessEnv';

/** Generic provider of the configuration from environment variables or ".env" file. */
@singleton()
export class EnvConfigProvider {
    constructor(@inject(processEnvDIToken) private readonly processEnv: NodeJS.ProcessEnv) {}

    getString(variableName: string): string {
        return this.getVariable(variableName, parseString);
    }

    getStringArray(variableName: string): string[] {
        return this.getVariable(variableName, v => parseArray(v, parseString));
    }

    getInteger(variableName: string, limit?: IntegerLimit): number {
        return this.getVariable(variableName, v => parseInteger(v, limit));
    }

    getIntegerArray(variableName: string, limit?: IntegerLimit): number[] {
        return this.getVariable(variableName, v => parseArray(v, s => parseInteger(s, limit)));
    }

    getAbsoluteUrl(variableName: string): string {
        return this.getVariable(variableName, value => {
            try {
                return new URL(value).toString();
            } catch {
                throw new Error('Value must be an absolute URL.');
            }
        });
    }

    getBoolean(variableName: string): boolean {
        return this.getOneOf(variableName, ['true', 'false']) === 'true';
    }

    getEnum<T extends string>(variableName: string, enumType: Record<string, T>): T {
        return this.getOneOf(variableName, getEnumValues(enumType));
    }

    getOneOf<T extends string>(variableName: string, supported: T[]): T {
        return this.getVariable(variableName, rawValue => {
            for (const value of supported) {
                if (value === rawValue) {
                    return value;
                }
            }
            throw new Error(`Supported values are only: ${supported.map(s => `"${s}"`).join(', ')}.`);
        });
    }

    private getVariable<T>(variableName: string, parseFn: (rawValue: string) => T): T {
        const rawValue = this.processEnv[variableName];
        try {
            const normalizedValue = rawValue?.trim();
            if (!normalizedValue) {
                throw new Error('Variable or its value is missing.');
            }
            return parseFn(normalizedValue);
        } catch (error: unknown) {
            throw new Error(`Failed parsing value ${JSON.stringify(rawValue)} of environment variable "${variableName}". ${errorToString(error)}`);
        }
    }
}

function parseString(str: string): string {
    const normalized = str.trim();
    if (!normalized) {
        throw new Error('Value must be a non-empty string.');
    }
    return normalized;
}

function parseArray<T>(rawValue: string, parseItem: (s: string) => T): T[] {
    return rawValue.split(',').map(parseItem);
}

function parseInteger(rawValue: string, limit: IntegerLimit | undefined): number {
    switch (limit) {
        case IntegerLimit.Positive:
            return parse(i => i > 0, 'an integer greater than zero');
        case IntegerLimit.PositiveOrZero:
            return parse(i => i >= 0, 'an integer greater than zero or equal to zero');
        case undefined:
            return parse(() => true, 'an integer');
    }

    function parse(condition: (i: number) => boolean, valueDescription: string): number {
        const value = parseFloat(rawValue);
        if (isNaN(value) || (value % 1) !== 0 || !condition(value)) {
            throw new Error(`Value must be ${valueDescription}.`);
        }
        return value;
    }
}
