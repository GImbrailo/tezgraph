import { singleton } from 'tsyringe';

import { LogFormat } from '../logging/log-config';
import { LogLevel } from '../logging/log-entry';
import { getEnumValues } from '../reflection';
import { EnvConfigProvider, IntegerLimit } from './env-config-provider';

export enum Names {
    Host = 'HOST',
    Port = 'PORT',
    TezosNode = 'TEZOS_NODE',
    EnableDebug = 'ENABLE_DEBUG',
    ReplayFromBlockLevelMaxFromHead = 'REPLAY_FROM_BLOCK_LEVEL_MAX_FROM_HEAD',
    RpcRetryDelaysMillis = 'RPC_RETRY_DELAYS_MILLIS',
    SubscriptionKeepAliveMillis = 'SUBSCRIPTION_KEEP_ALIVE_MILLIS',
    HealthStatusCacheTtlSeconds = 'HEALTH_STATUS_CACHE_TTL_SECONDS',
    GraphQLDepthLimit = 'GRAPH_QL_DEPTH_LIMIT',
    GraphQLComplexityLimit = 'GRAPH_QL_COMPLEXITY_LIMIT',
    EnableApiKey = 'ENABLE_API_KEY',
    ApiKey = 'API_KEY',
    Modules = 'MODULES',
    TezosProcessingMaxAgeSecondsDegraded = 'TEZOS_PROCESSING_MAX_AGE_SECONDS_DEGRADED',
    TezosProcessingMaxAgeSecondsUnhealthy = 'TEZOS_PROCESSING_MAX_AGE_SECONDS_UNHEALTHY',

    LogConsoleMinLevel = 'LOG_CONSOLE_MIN_LEVEL',
    LogConsoleFormat = 'LOG_CONSOLE_FORMAT',
    LogFileMinLevel = 'LOG_FILE_MIN_LEVEL',
    LogFileFormat = 'LOG_FILE_FORMAT',
    LogFilePath = 'LOG_FILE_PATH',
}

export const offLogLevel = 'off';

@singleton()
export class EnvConfig {
    readonly host: string;
    readonly port: number;
    readonly tezosNodeUrl: string;
    readonly enableDebug: boolean;
    readonly replayFromBlockLevelMaxFromHead: number;
    readonly rpcRetryDelaysMillis: readonly number[];
    readonly subscriptionKeepAliveMillis: number;
    readonly healthStatusCacheTtlSeconds: number;
    readonly graphQLDepthLimit: number;
    readonly graphQLComplexityLimit: number;
    readonly apiKey: string | null;
    readonly modules: readonly string[];

    readonly tezosProcessingMaxAgeMillis: {
        readonly degraded: number;
        readonly unhealthy: number;
    };

    readonly logging: Readonly<{
        console: null | Readonly<{
            minLevel: LogLevel;
            format: LogFormat;
        }>;
        file: null | Readonly<{
            minLevel: LogLevel;
            format: LogFormat;
            path: string;
        }>;
    }>;

    constructor(env: EnvConfigProvider) {
        this.host = env.getString(Names.Host);
        this.port = env.getInteger(Names.Port, IntegerLimit.PositiveOrZero);
        this.tezosNodeUrl = env.getAbsoluteUrl(Names.TezosNode);
        this.enableDebug = env.getBoolean(Names.EnableDebug);
        this.replayFromBlockLevelMaxFromHead = env.getInteger(Names.ReplayFromBlockLevelMaxFromHead, IntegerLimit.Positive);
        this.rpcRetryDelaysMillis = env.getIntegerArray(Names.RpcRetryDelaysMillis, IntegerLimit.PositiveOrZero);
        this.subscriptionKeepAliveMillis = env.getInteger(Names.SubscriptionKeepAliveMillis, IntegerLimit.PositiveOrZero);
        this.healthStatusCacheTtlSeconds = env.getInteger(Names.HealthStatusCacheTtlSeconds, IntegerLimit.Positive);
        this.graphQLDepthLimit = env.getInteger(Names.GraphQLDepthLimit, IntegerLimit.PositiveOrZero);
        this.graphQLComplexityLimit = env.getInteger(Names.GraphQLComplexityLimit, IntegerLimit.PositiveOrZero);
        this.modules = env.getStringArray(Names.Modules);

        const enableApiKey = env.getBoolean(Names.EnableApiKey);
        this.apiKey = enableApiKey ? env.getString(Names.ApiKey) : null;

        this.tezosProcessingMaxAgeMillis = {
            degraded: 1_000 * env.getInteger(Names.TezosProcessingMaxAgeSecondsDegraded, IntegerLimit.Positive),
            unhealthy: 1_000 * env.getInteger(Names.TezosProcessingMaxAgeSecondsUnhealthy, IntegerLimit.Positive),
        };

        const minLogLevels: (LogLevel | 'off')[] = [offLogLevel, ...getEnumValues(LogLevel)];
        const consoleMinLogLevel = env.getOneOf(Names.LogConsoleMinLevel, minLogLevels);
        const fileMinLogLevel = env.getOneOf(Names.LogFileMinLevel, minLogLevels);

        this.logging = {
            console: consoleMinLogLevel !== offLogLevel
                ? {
                    minLevel: consoleMinLogLevel,
                    format: env.getEnum(Names.LogConsoleFormat, LogFormat),
                }
                : null,
            file: fileMinLogLevel !== offLogLevel
                ? {
                    minLevel: fileMinLogLevel,
                    format: env.getEnum(Names.LogFileFormat, LogFormat),
                    path: env.getString(Names.LogFilePath),
                }
                : null,
        };
    }
}
