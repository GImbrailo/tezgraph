/* istanbul ignore file */
import dotenv from 'dotenv';
import fs from 'fs';

/**
 * Doesn't overwrite existing variables.
 * This must be standalone from actual config instance so that it can be applied in tests before the config is instantiated.
 */
export function addEnvVarsFromFile(filePath: string): void {
    if (fs.existsSync(filePath)) {
        const result = dotenv.config({ path: filePath });
        if (result.error) {
            throw result.error;
        }
    }
}
