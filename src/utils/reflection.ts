/**
 * Useful for generic constraints `T extends NonNullValue`.
 * More explicit than writing `T extends {}` and it passes eslint there.
 */
export interface NonNullishValue {} /* eslint-disable-line @typescript-eslint/no-empty-interface */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Constructor<T extends NonNullishValue = NonNullishValue> = new(...args: any[]) => T;

export type DefaultConstructor<T extends NonNullishValue = NonNullishValue> = new() => T;

export function nameof<T>(name: keyof T): string {
    return name.toString();
}

export type KeyOfType<TObject, TProperty> = {
    [P in keyof TObject]: TObject[P] extends TProperty ? P : never
} [keyof TObject];

export type Nullish<T> = T | null | undefined;

export function isNullish<TValue>(value: Nullish<TValue>): value is null | undefined {
    return value === undefined || value === null;
}

export function isNotNullish<TValue>(value: Nullish<TValue>): value is TValue {
    return !isNullish(value);
}

export function getEnumValues<T extends string>(enumType: Record<string, T>): T[] {
    return Object.values(enumType);
}

export type AllProperties<T> = {
    [P in keyof Required<T>]: Pick<T, P> extends Required<Pick<T, P>> ? T[P] : (T[P] | undefined);
};
