import { DeepReadonly } from 'ts-essentials';

import { DefaultConstructor } from './reflection';

export function asReadonly<T>(items: T[]): readonly T[];
export function asReadonly<T>(obj: T): Readonly<T>;
export function asReadonly<T>(obj: T[] | T): readonly T[] | Readonly<T> {
    return obj;
}

export function errorToString(error: unknown, options?: { onlyMessage?: boolean }): string {
    if (error instanceof Error) {
        return options?.onlyMessage === true
            ? error.message
            : errorObjToString(error);
    }
    switch (typeof error) {
        case 'string':
            return error;
        case 'undefined':
            return 'undefined';
        default:
            return JSON.stringify(error);
    }
}

export function errorObjToString(error: Error): string {
    return error.stack ?? error.toString();
}

/**
 * Creates a new object of given type and assigns its specified properties.
 * Useful when properties are read-only.
 * Compared to Object.assign(), this method makes sure that only existing properties are being assigned.
 */
export function create<T>(type: DefaultConstructor<T>, ...properties: Partial<T>[]): T {
    const obj = new type();
    properties.forEach(p => Object.assign(obj, p));
    return obj;
}

/** Deeply freezes an object even if it has circular references. */
export function deepFreeze<T extends object>(obj: T): DeepReadonly<T> {
    deepFreezeInternal(obj, new Set<unknown>());
    return obj as DeepReadonly<T>;
}

function deepFreezeInternal(obj: object, alreadyFrozen: Set<unknown>): void {
    if (alreadyFrozen.has(obj)) {
        return;
    }

    Object.freeze(obj);
    alreadyFrozen.add(obj);

    for (const propertyName of Object.getOwnPropertyNames(obj)) {
        const value = (obj as Record<string, unknown>)[propertyName];

        if (typeof value === 'object' && value !== null) {
            deepFreezeInternal(value, alreadyFrozen);
        } else if (typeof value === 'function') {
            Object.freeze(value);
        }
    }
}
