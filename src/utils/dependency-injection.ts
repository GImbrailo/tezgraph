import { DependencyContainer, InjectionToken, instanceCachingFactory } from 'tsyringe';

export const diContainerDIToken: InjectionToken<DependencyContainer> = 'Container';

export function registerSingleton<T>(container: DependencyContainer, token: InjectionToken<T>, factory: (c: DependencyContainer) => T): void {
    container.register(token, { useFactory: instanceCachingFactory(factory) });
}
