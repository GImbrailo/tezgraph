export function addMillis(date: Date, millis: number): Date {
    return new Date(date.getTime() + millis);
}
