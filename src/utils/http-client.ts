/* istanbul ignore file */
import fetch, { RequestInfo, RequestInit, Response } from 'node-fetch';
import { singleton } from 'tsyringe';

/** Simple wrapper around "fetch" for easier mocking in tests. */
@singleton()
export class HttpClient {
    async fetch(
        url: RequestInfo,
        init?: RequestInit,
    ): Promise<Response> {
        return fetch(url, init);
    }
}
