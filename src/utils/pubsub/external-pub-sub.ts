import { PubSubEngine } from 'graphql-subscriptions';
import { inject, InjectionToken, singleton } from 'tsyringe';

import { BlockNotification } from '../../entity/subscriptions/block-notification';
import { MempoolOperationGroup } from '../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../logging';
import { PubSubTrigger, TypedPubSub } from './typed-pub-sub';

export const externalPubSubDIToken: InjectionToken<PubSubEngine> = 'ExternalPubSub';

/** Strongly-typed triggers for particular payloads. */
export const externalTriggers = {
    blocks: new PubSubTrigger<BlockNotification>('BLOCKS'),
    mempoolOperationGroups: new PubSubTrigger<MempoolOperationGroup>('MEMPOOL_OPERATION_GROUPS'),
} as const;

/**
 * Wraps external instance of PubSub in fail-over deployment.
 * That's why the communication and payload sizes should be minimal.
 */
@singleton()
export class ExternalPubSub extends TypedPubSub {
    constructor(
        @inject(externalPubSubDIToken) pubSub: PubSubEngine, // eslint-disable-line @typescript-eslint/indent
        @injectLogger(ExternalPubSub) logger: Logger,
    ) {
        super(pubSub, logger);
    }
}
