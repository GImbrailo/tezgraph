import { PubSubEngine } from 'graphql-subscriptions';

import { asIterableIterator } from '../collections/async-iterable-utils';
import { Logger } from '../logging';

/** Strongly-typed PubSub trigger. */
export class PubSubTrigger<_TPayload> { /* eslint-disable-line @typescript-eslint/no-unused-vars */
    constructor(readonly name: string) {}
}

/** Base class for strongly-typed PubSub. */
export abstract class TypedPubSub {
    constructor(
        private readonly pubSub: PubSubEngine,
        private readonly logger: Logger,
    ) {}

    async publish<TPayload>(trigger: PubSubTrigger<TPayload>, payload: TPayload): Promise<void> {
        this.logger.logDebug('Publishing a payload to {trigger}.', { payload, trigger: trigger.name });
        await this.pubSub.publish(trigger.name, payload);
    }

    subscribe<TPayload>(trigger: PubSubTrigger<TPayload>, onMessage: (p: TPayload) => void): void {
        void this.pubSub.subscribe(trigger.name, onMessage, {});
    }

    iterate<TPayload>(triggers: PubSubTrigger<TPayload>[]): AsyncIterableIterator<TPayload> {
        return asIterableIterator(this.pubSub.asyncIterator(triggers.map(t => t.name)));
    }
}
