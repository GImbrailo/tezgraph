export enum HttpStatusCode {
    OK = 200,
    InternalServerError = 500,
}

export enum HttpHeader {
    ContentType = 'Content-Type',
}

export enum ContentType {
    Json = 'application/json',
}
