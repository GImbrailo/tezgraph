import { AsyncOrSync } from 'ts-essentials';

import { ItemProcessor } from './interfaces';

export interface ItemConverter<TSource, TTarget> {
    convert(item: TSource): AsyncOrSync<TTarget>;
}

/** Converts item using given converter and passes the execution to the next processor. */
export class ConvertItemProcessor<TSource, TTarget> implements ItemProcessor<TSource> {
    constructor(
        private readonly converter: ItemConverter<TSource, TTarget>,
        private readonly nextProcessor: ItemProcessor<TTarget>,
    ) {}

    async processItem(sourceItem: TSource): Promise<void> {
        const targetItem = await this.converter.convert(sourceItem);
        await this.nextProcessor.processItem(targetItem);
    }
}
