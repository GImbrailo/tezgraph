import { FixedQueue } from '../collections/fixed-queue';
import { ItemProcessor } from './interfaces';

/** Removes duplicates by ID in the processing pipeline. */
export class DeduplicateItemProcessor<TItem> implements ItemProcessor<TItem> {
    private readonly lastIds: FixedQueue<string>;

    constructor(
        numberOfLastItemsToCheck: number,
        private readonly getItemId: (item: TItem) => string,
        private readonly nextProcessor: ItemProcessor<TItem>,
    ) {
        this.lastIds = new FixedQueue(numberOfLastItemsToCheck);
    }

    async processItem(item: TItem): Promise<void> {
        const itemId = this.getItemId(item);

        if (!this.lastIds.has(itemId)) {
            this.lastIds.add(itemId);
            await this.nextProcessor.processItem(item);
        }
    }
}
