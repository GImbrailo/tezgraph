import { AbortSignal } from 'abort-controller';

import { isAbortError } from '../abortion';
import { Logger } from '../logging';
import { Clock } from '../time/clock';
import { InfoProvider, ItemProcessor, IterableProvider } from './interfaces';

/** Processes items from given iterable one by one in a loop. */
export class IterableProcessor<TItem = unknown> {
    currentIterable: AsyncIterableIterator<TItem> | undefined;
    lastItemInfo: unknown;
    lastError: unknown;
    lastSuccessTime: Date = new Date(0);

    constructor(
        private readonly iterableProvider: IterableProvider<TItem>,
        private readonly itemProcessor: ItemProcessor<TItem>,
        private readonly itemInfoProvider: InfoProvider<TItem>,
        private readonly abortSignal: AbortSignal,
        private readonly clock: Clock,
        private readonly logger: Logger,
    ) {}

    async processItems(): Promise<void> {
        this.logger.logInformation('Starting to process items.');
        while (!this.abortSignal.aborted) {
            try {
                this.currentIterable = this.iterableProvider.iterate();
                for await (const item of this.currentIterable) {
                    try {
                        this.lastItemInfo = this.itemInfoProvider.getInfo(item);
                        await this.itemProcessor.processItem(item);

                        this.lastError = undefined;
                        this.lastSuccessTime = this.clock.getNowDate();
                    } catch (error: unknown) {
                        this.lastError = error;
                        this.logger.logError('Failed to process {item} because of {error}.', {
                            item: this.lastItemInfo,
                            error,
                        });
                    }
                }
                throw new Error('Iteration ended which should not happen.');
            } catch (error: unknown) {
                if (isAbortError(error)) {
                    break;
                }

                this.lastError = error;
                this.lastItemInfo = undefined;
                this.logger.logError('Failed iterating items. It will retry. {error}.', { error });
            }
        }

        this.logger.logInformation('Iteration stopped (aborted).');
        this.currentIterable = undefined;
    }
}
