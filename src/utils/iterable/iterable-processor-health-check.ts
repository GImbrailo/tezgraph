import { EnvConfig } from '../configuration/env-config';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../health/health-check';
import { isNotNullish } from '../reflection';
import { Clock } from '../time/clock';
import { IterableProcessor } from './iterable-processor';

export const messages = {
    notRunning: 'The worker is NOT running - it was not started or it was stopped.',
    noSuccessForLongTime: 'Last successfully processed item is older than max configured age.',
    errorRecently: 'Recently an error occurred.',
    everythingWorks: 'Everything works perfectly.',
} as const;

/** Reports health of processing of given IterableProcessor. */
export class IterableProcessorHealthCheck implements HealthCheck {
    constructor(
        readonly name: string,
        private readonly processor: Readonly<IterableProcessor>,
        private readonly envConfig: EnvConfig,
        private readonly clock: Clock,
    ) {}

    checkHealth(): HealthCheckResult {
        if (!this.processor.currentIterable) {
            return {
                status: HealthStatus.Unhealthy,
                data: messages.notRunning,
            };
        }

        const successAgeMillis = this.clock.getNowDate().getTime() - this.processor.lastSuccessTime.getTime();
        const maxAgeMillisConfig = this.envConfig.tezosProcessingMaxAgeMillis;

        if (successAgeMillis > Math.min(maxAgeMillisConfig.degraded, maxAgeMillisConfig.unhealthy)) {
            return {
                status: successAgeMillis > maxAgeMillisConfig.unhealthy ? HealthStatus.Unhealthy : HealthStatus.Degraded,
                data: {
                    reason: messages.noSuccessForLongTime,
                    lastItem: this.processor.lastItemInfo,
                    lastError: this.processor.lastError,
                    lastSuccessTime: this.processor.lastSuccessTime,
                    successAgeMillis,
                    maxAgeMillisConfig,
                },
            };
        }

        if (isNotNullish(this.processor.lastError)) {
            return {
                status: HealthStatus.Degraded,
                data: {
                    reason: messages.errorRecently,
                    failedItem: this.processor.lastItemInfo,
                    error: this.processor.lastError,
                    lastSuccessTime: this.processor.lastSuccessTime,
                },
            };
        }

        return {
            status: HealthStatus.Healthy,
            data: {
                reason: messages.everythingWorks,
                lastItem: this.processor.lastItemInfo,
                lastItemTime: this.processor.lastSuccessTime,
            },
        };
    }
}
