import { deepFreeze } from '../conversion';
import { ItemProcessor } from './interfaces';

/**
 * Deep-freezes items to prevent their further modification
 * including hacks (e.g. Object.assign()) if they are already read-only on TypeScript level.
 */
export class FreezeItemProcessor<TItem extends object> implements ItemProcessor<TItem> {
    constructor(
        private readonly nextProcessor: ItemProcessor<TItem>,
    ) {}

    async processItem(item: TItem): Promise<void> {
        deepFreeze(item);
        await this.nextProcessor.processItem(item);
    }
}
