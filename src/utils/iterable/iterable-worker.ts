import { BackgroundWorker } from '../../bootstrap/background-worker';
import { IterableProcessor } from './iterable-processor';

/** Executes given iterable processor on the background of the app. */
export class IterableWorker implements BackgroundWorker {
    constructor(
        readonly name: string,
        private readonly processor: IterableProcessor,
    ) {}

    start(): void {
        if (this.processor.currentIterable) {
            throw new Error('This cannot be started because it is already running.');
        }

        void this.processor.processItems();
    }

    stop(): void {
        if (!this.processor.currentIterable?.return) {
            throw new Error('This cannot be stopped because it is NOT running or missing return.');
        }

        // Don't await b/c it would wait for one more item. So it must be stopped using AbortSignal anyway.
        void this.processor.currentIterable.return();
        this.processor.currentIterable = undefined;
    }
}
