import { AbortSignal } from 'abort-controller';
import { DependencyContainer, FactoryFunction, InjectionToken } from 'tsyringe';

import { backgroundWorkersDIToken } from '../../bootstrap/background-worker';
import { EnvConfig } from '../configuration/env-config';
import { registerSingleton } from '../dependency-injection';
import { healthChecksDIToken } from '../health/health-check';
import { LoggerFactory } from '../logging';
import { Clock } from '../time/clock';
import { InfoProvider, ItemProcessor, IterableProvider } from './interfaces';
import { IterableProcessor } from './iterable-processor';
import { IterableProcessorHealthCheck } from './iterable-processor-health-check';
import { IterableWorker } from './iterable-worker';

/** Registers iterable processor based on given token with related worker and health check. */
export function registerIterableWorker<TItem>(
    container: DependencyContainer,
    name: string,
    createIterableProvider: FactoryFunction<IterableProvider<TItem>>,
    createItemProcessor: FactoryFunction<ItemProcessor<TItem>>,
    itemInfoProviderDIToken: InjectionToken<InfoProvider<TItem>>,
): void {
    const processorDIToken: InjectionToken<IterableProcessor<TItem>> = `${name}Processor`;
    registerSingleton(container, processorDIToken, c => new IterableProcessor(
        createIterableProvider(c),
        createItemProcessor(c),
        c.resolve(itemInfoProviderDIToken),
        c.resolve(AbortSignal),
        c.resolve(Clock),
        c.resolve(LoggerFactory).getLogger(name),
    ));

    container.register(backgroundWorkersDIToken, {
        useFactory: c => new IterableWorker(
            `${name}Worker`,
            c.resolve(processorDIToken),
        ),
    });

    container.register(healthChecksDIToken, {
        useFactory: c => new IterableProcessorHealthCheck(
            name,
            c.resolve(processorDIToken),
            c.resolve(EnvConfig),
            c.resolve(Clock),
        ),
    });
}
