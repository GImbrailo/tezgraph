import { AsyncOrSync } from 'ts-essentials';

export interface IterableProvider<TItem> {
    iterate(): AsyncIterableIterator<TItem>;
}

export interface ItemProcessor<TItem> {
    processItem(item: TItem): AsyncOrSync<void>;
}

export interface InfoProvider<TValue> {
    getInfo(value: TValue): unknown;
}
