/* istanbul ignore file */
import winston from 'winston';

import { EnvConfig } from '../configuration/env-config';
import { safeJsonStringify } from '../safe-json-stringifier';
import { LogFormat } from './log-config';
import { LogEntry, LogLevel } from './log-entry';
import { formatEntry, FormatEntryOptions } from './logger-formatter';

/** Root logger API from Winston that we intend to use. */
export interface RootLogger {
    isLevelEnabled(level: LogLevel): boolean;
    log(entry: LogEntry): void;
    close(): void;
}

/** Initializes root logger according to env config. */
export function createRootLogger(envConfig: EnvConfig): RootLogger {
    const levelNumbers: Record<LogLevel, number> = {
        critical: 1,
        error: 2,
        warning: 3,
        information: 4,
        debug: 5,
    };
    const logger = winston.createLogger({
        levels: { off: 0, ...levelNumbers },
        level: 'off', // Must be defined for isLevelEnabled() to work properly.
        format: winston.format(e => e)(), // Root format is not a final one -> just a pass-through.
    });

    const consoleConfig = envConfig.logging.console;
    if (consoleConfig) {
        logger.add(new winston.transports.Console({
            level: consoleConfig.minLevel,
            format: getFormat(consoleConfig.format, { colorize: true }),
        }));
    }

    const fileConfig = envConfig.logging.file;
    if (fileConfig) {
        logger.add(new winston.transports.File({
            level: fileConfig.minLevel,
            filename: fileConfig.path,
            format: getFormat(fileConfig.format),
        }));
    }

    if (envConfig.enableDebug) {
        logger.add(new winston.transports.File({
            level: LogLevel.Debug,
            filename: debugFilePaths.json,
            format: getJsonFormat(),
        }));
        logger.add(new winston.transports.File({
            level: LogLevel.Debug,
            filename: debugFilePaths.messages,
            format: getMessagesFormat(),
        }));
    }

    return logger.transports.length > 0 ? logger : offRootLogger;
}

export const offRootLogger: RootLogger = {
    isLevelEnabled: (_level: LogLevel): boolean => false,
    log: (_entry: LogEntry): void => { /* The log entry is dismissed. */ },
    close: (): void => { /* Nothing to close. */ },
};

export const debugFilePaths = {
    json: 'logs/debug.log.jsonl',
    messages: 'logs/debug.log.txt',
} as const;

function getFormat(format: LogFormat, messagesOptions?: FormatEntryOptions): winston.Logform.Format {
    switch (format) {
        case LogFormat.Json:
            return getJsonFormat();
        case LogFormat.Messages:
            return getMessagesFormat(messagesOptions);
    }
}

function getJsonFormat(): winston.Logform.Format {
    return winston.format.printf(safeJsonStringify);
}

function getMessagesFormat(options?: FormatEntryOptions): winston.Logform.Format {
    return winston.format.printf(e => formatEntry(e as LogEntry, options));
}
