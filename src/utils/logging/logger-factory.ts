import { MetricsContainer } from '../metrics/metrics-container';
import { Constructor } from '../reflection';
import { Clock } from '../time/clock';
import { Logger } from './logger';
import { RootLogger } from './root-logger-factory';

export type LoggerCategory = string | Constructor;

/** Creates a Logger with given category. */
export class LoggerFactory {
    constructor(
        private readonly rootLogger: RootLogger,
        private readonly clock: Clock,
        private readonly metrics: MetricsContainer,
    ) {}

    getLogger(category: LoggerCategory): Logger {
        const categoryStr = typeof category === 'string' ? category : category.name;
        return new Logger(categoryStr, this.rootLogger, this.clock, this.metrics);
    }
}
