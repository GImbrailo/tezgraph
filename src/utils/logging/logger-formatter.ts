import { cyan, gray, green, red, yellow } from 'colors/safe';

import { safeJsonStringify } from '../safe-json-stringifier';
import { LogEntry, LogLevel } from './log-entry';

export interface FormatEntryOptions {
    colorize?: boolean;
}

/** Formats entire log entry to a string. */
export function formatEntry(entry: LogEntry, options?: FormatEntryOptions): string {
    const colorizer = getColorizer(options?.colorize);
    const timestampStr = entry.timestamp.toISOString();
    const message = formatMessage(entry, d => colorizer.gray(d));
    return `${colorizer.gray(timestampStr)} ${colorizer.colorizeLevel(entry.level)} [${colorizer.cyan(entry.category)}] ${message}`;
}

/** Replaces message placeholders with values from data. */
export function formatMessage(entry: LogEntry, emphasizeData: (d: string) => string): string {
    let message = entry.message;
    if (entry.data) {
        for (const [dataName, rawValue] of Object.entries(entry.data)) {
            const formattedValue = emphasizeData(safeJsonStringify(rawValue));
            message = message.replace(`{${dataName}}`, `${dataName} ${formattedValue}`);
        }
    }
    return message;
}

function getColorizer(colorize: boolean | undefined): Colorizer {
    return colorize === true ? consoleColorizer : emptyColorizer;
}

interface Colorizer {
    cyan(str: string): string;
    gray(str: string): string;
    colorizeLevel(level: LogLevel): string;
}

const consoleColorizer: Colorizer = {
    cyan,
    gray,
    colorizeLevel(level: LogLevel): string {
        switch (level) {
            case LogLevel.Critical:
            case LogLevel.Error:
                return red(level);
            case LogLevel.Warning:
                return yellow(level);
            case LogLevel.Information:
                return green(level);
            case LogLevel.Debug:
                return gray(level);
        }
    },
};

const emptyColorizer: Colorizer = {
    cyan: s => s,
    gray: s => s,
    colorizeLevel: l => l,
};
