import { MetricsContainer } from '../metrics/metrics-container';
import { Clock } from '../time/clock';
import { LogData, LogLevel } from './log-entry';
import { RootLogger } from './root-logger-factory';

/** Main API for logging. */
export class Logger {
    constructor(
        private readonly category: string,
        private readonly rootLogger: RootLogger,
        private readonly clock: Clock,
        private readonly metrics: MetricsContainer,
    ) {}

    isEnabled(level: LogLevel): boolean {
        return this.rootLogger.isLevelEnabled(level);
    }

    log(level: LogLevel, message: string, data?: LogData): void {
        if (!this.isEnabled(level)) {
            return;
        }

        this.rootLogger.log({
            timestamp: this.clock.getNowDate(),
            category: this.category,
            level,
            message,
            data,
        });
        this.metrics.loggerLogCount.inc({ level: level.toString() });
    }

    get isCriticalEnabled(): boolean {
        return this.isEnabled(LogLevel.Critical);
    }

    logCritical(message: string, data?: LogData): void {
        this.log(LogLevel.Critical, message, data);
    }

    get isErrorEnabled(): boolean {
        return this.isEnabled(LogLevel.Error);
    }

    logError(message: string, data?: LogData): void {
        this.log(LogLevel.Error, message, data);
    }

    get isWarningEnabled(): boolean {
        return this.isEnabled(LogLevel.Warning);
    }

    logWarning(message: string, data?: LogData): void {
        this.log(LogLevel.Warning, message, data);
    }

    get isInformationEnabled(): boolean {
        return this.isEnabled(LogLevel.Information);
    }

    logInformation(message: string, data?: LogData): void {
        this.log(LogLevel.Information, message, data);
    }

    get isDebugEnabled(): boolean {
        return this.isEnabled(LogLevel.Debug);
    }

    logDebug(message: string, data?: LogData): void {
        this.log(LogLevel.Debug, message, data);
    }

    close(): void {
        this.rootLogger.close();
    }
}
