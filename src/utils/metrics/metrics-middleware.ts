import { assert } from 'ts-essentials';
import { singleton } from 'tsyringe';
import { MiddlewareInterface, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../bootstrap/resolver-context';
import { OperationArgs } from '../../entity/subscriptions/args/operation-args';
import { SubscriptionArgs } from '../../entity/subscriptions/args/subscription-args';
import { BlockNotification } from '../../entity/subscriptions/block-notification';
import { OperationNotification, OperationOrigin } from '../../entity/subscriptions/operation-notification';
import { injectLogger, Logger } from '../logging';
import { isNullish, nameof } from '../reflection';
import { Clock } from '../time/clock';
import { MetricsContainer, OperationSource } from './metrics-container';

export const SERVICE_NAME = 'indexer';

export enum ParentType {
    Query = 'Query',
    Mutation = 'Mutation',
    Subscription = 'Subscription',
}

@singleton()
export class MetricsMiddleware implements MiddlewareInterface<ResolverContext> {
    constructor(
        private readonly metrics: MetricsContainer,
        private readonly clock: Clock,
        @injectLogger(MetricsMiddleware) private readonly logger: Logger,
    ) {}

    async use(resolverData: ResolverData<ResolverContext>, next: () => Promise<unknown>): Promise<unknown> {
        const typeName = resolverData.info.parentType.name;
        const fieldName = resolverData.info.fieldName;

        switch (typeName) {
            case ParentType.Query:
            case ParentType.Mutation:
                this.reportQueryMetrics(fieldName);
                break;
            case ParentType.Subscription:
                this.reportSubscriptionMetrics(fieldName, resolverData);
                break;
        }

        try {
            return await next();
        } catch (error: unknown) {
            this.metrics.graphQLErrorCounter.inc({
                type: typeName,
                field: fieldName,
                service: SERVICE_NAME,
            });
            throw error;
        }
    }

    private reportQueryMetrics(fieldName: string): void {
        this.metrics.graphQLQueryMethodCounter.inc({
            method: fieldName,
            service: SERVICE_NAME,
        });
    }

    private reportSubscriptionMetrics(fieldName: string, resolverData: ResolverData<ResolverContext>): void {
        const includeMempool = !isNullish(resolverData.args[nameof<OperationArgs<OperationNotification>>('includeMempool')]);
        const replayFromBlockLevel = !isNullish(resolverData.args[nameof<SubscriptionArgs>('replayFromBlockLevel')]);
        this.metrics.graphQLSubscriptionMethodCounter.inc({
            method: fieldName,
            mempool: includeMempool.toString(),
            replay: replayFromBlockLevel.toString(),
        });

        const nowTimestamp = this.clock.getNowTimestamp();
        try {
            const data: unknown = resolverData.root;
            if (isOperation(data) && data.origin === OperationOrigin.MEMPOOL) {
                const relativeDuration = nowTimestamp - data.info.received_from_tezos_on.getTime();
                this.metrics.processNotificationRelativeDuration.observe({ source: OperationSource.Mempool }, relativeDuration);
            } else {
                const block = isOperation(data) ? data.block : data as BlockNotification;
                assert(block?.hash);

                const relativeDuration = nowTimestamp - block.received_from_tezos_on.getTime();
                const absoluteDuration = nowTimestamp - block.header.timestamp.getTime();
                this.metrics.processNotificationRelativeDuration.observe({ source: OperationSource.Block }, relativeDuration);
                this.metrics.processNotificationAbsoluteDuration.observe(absoluteDuration);
            }
        } catch (error: unknown) {
            this.logger.logWarning('Unable to collect duration metrics due to {error}.', { error });
        }
    }
}

function isOperation(data: unknown): data is OperationNotification {
    return typeof data === 'object'
        && data !== null
        && nameof<OperationNotification>('origin') in data
        && nameof<OperationNotification>('kind') in data;
}
