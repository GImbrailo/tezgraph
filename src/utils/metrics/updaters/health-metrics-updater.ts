import { singleton } from 'tsyringe';

import { isHealthy } from '../../health/health-check';
import { HealthProvider } from '../../health/health-provider';
import { MetricsUpdater } from '../metrics-collector';
import { HealthyMetricName, MetricsContainer } from '../metrics-container';

@singleton()
export class HealthMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: MetricsContainer,
        private readonly healthProvider: HealthProvider,
    ) {}

    async update(): Promise<void> {
        const health = await this.healthProvider.generateHealthReport();

        this.reportComponent(HealthyMetricName.Aggregated, health.isHealthy);

        for (const [name, result] of Object.entries(health.results)) {
            this.reportComponent(name, isHealthy(result));
        }
    }

    private reportComponent(name: string, healthy: boolean): void {
        this.metrics.unhealthyCount.set({ name }, healthy ? 0 : 1);
    }
}
