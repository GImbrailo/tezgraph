import { inject, singleton } from 'tsyringe';

import { processDIToken } from '../../configuration/env-config-provider';
import { MetricsUpdater } from '../metrics-collector';
import { MetricsContainer } from '../metrics-container';

@singleton()
export class AppUptimeMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: MetricsContainer,
        @inject(processDIToken) private readonly process: NodeJS.Process,
    ) {}

    update(): void {
        const uptimeSeconds = Math.floor(this.process.uptime());
        this.metrics.uptime.set(uptimeSeconds);
    }
}
