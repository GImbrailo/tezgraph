import { Counter, Gauge, Histogram, Metric, Registry } from 'prom-client';
import { singleton } from 'tsyringe';

import { getEnumValues } from '../reflection';

export enum BuildInfoLabel {
    GitSha = 'git_sha',
    Tag = 'tag',
}

export enum UnhealthyComponentLabel {
    Name = 'name',
}

export enum GraphQLQueryMethodLabel {
    Method = 'method',
    Service = 'service',
}

export enum GraphQLSubscriptionMethodLabel {
    Method = 'method',
    Mempool = 'mempool',
    Replay = 'replay',
}

export enum GraphQLErrorCounterLabel {
    Type = 'type',
    Field = 'field',
    Service = 'service',
}

export enum SubscriptionCountLabel {
    Name = 'name',
    Mempool = 'mempool',
    Replay = 'replay',
}

export enum LoggerLogCountLabel {
    Level = 'level',
}

export enum RetryCountLabel {
    Endpoint = 'endpoint',
    Delay = 'delay',
    Index = 'index',
}

export enum OperationCountLabel {
    Source = 'source',
}

export enum RelativeDurationLabel {
    Source = 'source',
}

export enum RpcClientGetDurationLabel {
    Endpoint = 'endpoint',
}

export enum OperationSource {
    Block = 'block',
    Mempool = 'mempool',
}

export enum HealthyMetricName {
    Aggregated = 'aggregated',
}

const doNotRegisterGlobally: Registry[] = [];

export type MetricsRecord<TKey extends string = string> = Record<TKey, Metric<string>>;

/** Container with Prometheus metric objects. */
@singleton()
export class MetricsContainer implements MetricsRecord<keyof MetricsContainer> {
    readonly buildInfoCounter = new Counter({
        name: 'tezgraph_build_info',
        help: 'TezGraph build info: git commit hash and git tag name',
        labelNames: getEnumValues(BuildInfoLabel),
        registers: doNotRegisterGlobally,
    });

    readonly uptime = new Gauge({
        name: 'tezgraph_uptime',
        help: 'TezGraph uptime in seconds',
        registers: doNotRegisterGlobally,
    });

    readonly graphQLQueryMethodCounter = new Counter({
        name: 'graphql_query_method_total',
        help: 'TezGraph query calls',
        labelNames: getEnumValues(GraphQLQueryMethodLabel),
        registers: doNotRegisterGlobally,
    });

    readonly graphQLSubscriptionMethodCounter = new Counter({
        name: 'graphql_subscription_method_total',
        help: 'TezGraph subscription notifications',
        labelNames: getEnumValues(GraphQLSubscriptionMethodLabel),
        registers: doNotRegisterGlobally,
    });

    readonly graphQLErrorCounter = new Counter({
        name: 'graphql_resolver_error_total',
        help: 'TezGraph resolvers errors',
        labelNames: getEnumValues(GraphQLErrorCounterLabel),
        registers: doNotRegisterGlobally,
    });

    // Intentionally negative name so that the default state (zero value) is healthy.
    readonly unhealthyCount = new Gauge({
        name: 'unhealthy_components',
        help: 'Number of currently unhealthy components',
        labelNames: getEnumValues(UnhealthyComponentLabel),
        registers: doNotRegisterGlobally,
    });

    readonly subscriptionsCurrent = new Gauge({
        name: 'subscriptions_active',
        help: 'Number of currently active subscriptions',
        labelNames: getEnumValues(SubscriptionCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly subscriptionsTotal = new Counter({
        name: 'subscriptions_total',
        help: 'Number of subscriptions',
        labelNames: getEnumValues(SubscriptionCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly httpServerConnectionCount = new Gauge({
        name: 'http_server_connections_active',
        help: 'Number of currently active http server connections',
        registers: doNotRegisterGlobally,
    });

    readonly loggerLogCount = new Counter({
        name: 'logger_logs_total',
        help: 'Number of logs',
        labelNames: getEnumValues(LoggerLogCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly tezosMonitorReconnectCount = new Counter({
        name: 'tezos_monitor_reconnects_total',
        help: 'Number of tezos monitor reconnects',
        registers: doNotRegisterGlobally,
    });

    readonly tezosMonitorInfiniteLoopCount = new Counter({
        name: 'tezos_monitor_infinite_loops_total',
        help: 'Number of tezos monitor enters to infinite retry loop',
        registers: doNotRegisterGlobally,
    });

    readonly tezosMonitorRetryCount = new Counter({
        name: 'tezos_monitor_retries_total',
        help: 'Number of tezos monitor call retries',
        labelNames: getEnumValues(RetryCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly tezosMonitorBlockLevel = new Gauge({
        name: 'tezos_monitor_block_level',
        help: 'Last block level processed by tezos monitor',
        registers: doNotRegisterGlobally,
    });

    readonly tezosMonitorOperationCount = new Counter({
        name: 'tezos_monitor_operations_total',
        help: 'Number of operations published by tezos monitor',
        labelNames: getEnumValues(OperationCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly rpcClientRetryCount = new Counter({
        name: 'tezos_rpc_client_retries_total',
        help: 'Number of RPC client call retries',
        labelNames: getEnumValues(RetryCountLabel),
        registers: doNotRegisterGlobally,
    });

    readonly processNotificationRelativeDuration = new Histogram({
        name: 'process_notification_duration_relative',
        help: 'How long it took to execute all processing from block receive (ms)',
        labelNames: getEnumValues(RelativeDurationLabel),
        buckets: [0.1, 5, 10, 15, 20, 50, 100, 500],
        registers: doNotRegisterGlobally,
    });

    readonly processNotificationAbsoluteDuration = new Histogram({
        name: 'process_notification_duration_absolute',
        help: 'How long it took to execute all processing from block create (ms)',
        buckets: [1000, 5000, 10000, 15000, 20000, 50000, 100000],
        registers: doNotRegisterGlobally,
    });

    readonly rpcClientGetDuration = new Histogram({
        name: 'rpc_client_get_duration',
        help: 'How long it took to execute RPC client get endpoint (ms)',
        labelNames: getEnumValues(RpcClientGetDurationLabel),
        buckets: [100, 500, 1000, 1500, 2000, 5000, 10000],
        registers: doNotRegisterGlobally,
    });
}
