import { Registry } from 'prom-client';
import { AsyncOrSync } from 'ts-essentials';
import { injectAll, InjectionToken, singleton } from 'tsyringe';

import { Clock } from '../time/clock';

export interface MetricsUpdater {
    update(): AsyncOrSync<void>;
}

export const metricsUpdatersDIToken: InjectionToken<MetricsUpdater> = 'MetricsUpdaters';

const METRICS_UPDATE_MILLIS = 1_000;

/** Collects all up-to-date metrics. */
@singleton()
export class MetricsCollector {
    private lastUpdateTimestamp = 0;

    constructor(
        private readonly prometheusRegistry: Registry,
        @injectAll(metricsUpdatersDIToken) private readonly updaters: readonly MetricsUpdater[],
        private readonly clock: Clock,
    ) {}

    async collectMetrics(): Promise<string> {
        if (this.lastUpdateTimestamp + METRICS_UPDATE_MILLIS < this.clock.getNowTimestamp()) {
            await Promise.all(this.updaters.map(u => u.update()));
            this.lastUpdateTimestamp = this.clock.getNowTimestamp();
        }

        return await this.prometheusRegistry.metrics();
    }
}
