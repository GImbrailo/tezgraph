import express from 'express';
import { Registry } from 'prom-client';
import { singleton } from 'tsyringe';

import { HttpHeader } from '../http-constants';
import { appUrlPaths, HttpHandler } from '../http-handler';
import { MetricsCollector } from './metrics-collector';

/** Exposes Prometheus metrics as a web page at particular HTTP path. */
@singleton()
export class MetricsHttpHandler implements HttpHandler {
    readonly urlPath = appUrlPaths.metrics;

    constructor(
        private readonly prometheusRegistry: Registry,
        private readonly collector: MetricsCollector,
    ) { }

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const metrics = await this.collector.collectMetrics();

        response.setHeader(HttpHeader.ContentType, this.prometheusRegistry.contentType);
        response.send(metrics);
    }
}
