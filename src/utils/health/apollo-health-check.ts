import { singleton } from 'tsyringe';

import { HealthProvider } from './health-provider';

@singleton()
export class ApolloHealthCheck {
    constructor(private readonly healthProvider: HealthProvider) {}

    async checkIsHealthy(): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        if (!report.isHealthy) {
            throw new Error('Health check error.');
        }
    }
}
