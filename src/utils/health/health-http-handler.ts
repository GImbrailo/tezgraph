import express from 'express';
import { singleton } from 'tsyringe';

import { ContentType, HttpHeader, HttpStatusCode } from '../http-constants';
import { appUrlPaths, HttpHandler } from '../http-handler';
import { safeJsonStringify } from '../safe-json-stringifier';
import { HealthProvider } from './health-provider';

@singleton()
export class HealthStatusHttpHandler implements HttpHandler {
    readonly urlPath = appUrlPaths.health;

    constructor(private readonly healthProvider: HealthProvider) {}

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        response.setHeader(HttpHeader.ContentType, ContentType.Json);
        response.send(safeJsonStringify(report, { indent: 2 }));
    }
}

@singleton()
export class HealthCheckHttpHandler implements HttpHandler {
    readonly urlPath = appUrlPaths.check;

    constructor(private readonly healthProvider: HealthProvider) {}

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        if (!report.isHealthy) {
            response.sendStatus(HttpStatusCode.InternalServerError);
        } else {
            response.send('OK');
        }
    }
}
