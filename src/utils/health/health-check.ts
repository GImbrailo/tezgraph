import { AsyncOrSync } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

export enum HealthStatus {
    Degraded = 'Degraded',
    Healthy = 'Healthy',
    Unhealthy = 'Unhealthy',
}

export interface HealthCheckResult {
    readonly status: HealthStatus;
    readonly data: unknown;
    readonly evaluatedOn?: Date;
}

export function isHealthy(result: HealthCheckResult): boolean {
    return result.status !== HealthStatus.Unhealthy;
}

/** Common top level interface for health checks. */
export interface HealthCheck {
    readonly name: string;
    checkHealth(): AsyncOrSync<HealthCheckResult>;
}

export const healthChecksDIToken: InjectionToken<HealthCheck> = 'HealthChecks';
