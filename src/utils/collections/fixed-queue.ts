import { guard } from '../guard';
import { NonNullishValue } from '../reflection';

/**
 * Fixed round-robin queue - after it's full it removes (overwrites) firsly added items.
 * Useful when you want to cache few last items.
 */
export class FixedQueue<T extends NonNullishValue> {
    private readonly items: (T | undefined)[];
    private index = 0;

    constructor(size: number) {
        this.items = Array<T | undefined>(size);
    }

    add(value: T): void {
        guard.notNullish(value);
        this.items[this.index] = value;
        this.index = this.index + 1 < this.items.length ? this.index + 1 : 0;
    }

    has(value: T): boolean {
        guard.notNullish(value);
        return this.items.some(i => i === value);
    }
}
