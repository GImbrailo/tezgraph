declare module 'graphql-validation-complexity' {
    import { ValidationContext } from 'graphql';

    export function createComplexityLimitRule(
        maxCost: number,
    ): (context: ValidationContext) => unknown;
}
