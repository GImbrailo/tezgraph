{
    "ignorePatterns": [
        "src/generated/**/*.ts"
    ],
    "env": {
        "browser": true,
        "es2020": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:import/typescript",
        "plugin:jest/recommended",
        "plugin:jest/style"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 2020,
        "sourceType": "module",
        "project": [
            "./tsconfig.json"
        ]
    },
    "plugins": [
        "@typescript-eslint",
        "import",
        "jest"
    ],
    "rules": {
        // Possible Errors https://eslint.org/docs/rules/#possible-errors
        "no-console": "error",
        "no-promise-executor-return": "error",
        "no-template-curly-in-string": "error",
        "no-unreachable-loop": "error",
        "no-useless-backreference": "error",
        "require-atomic-updates": "error",
        // Best Practices https://eslint.org/docs/rules/#best-practices
        "accessor-pairs": "error",
        "array-callback-return": "error",
        "complexity": "error",
        "curly": "error",
        "default-case-last": "error",
        "dot-location": [
            "error",
            "property"
        ],
        "eqeqeq": "error",
        "grouped-accessor-pairs": "error",
        "no-alert": "error",
        "no-caller": "error",
        "no-constructor-return": "error",
        "no-div-regex": "error",
        "no-else-return": "error",
        "no-eq-null": "error",
        "no-eval": "error",
        "no-extend-native": "error",
        "no-extra-bind": "error",
        "no-floating-decimal": "error",
        "no-implicit-coercion": "error",
        "no-implicit-globals": "error",
        "no-implied-eval": "error",
        "no-iterator": "error",
        "no-labels": "error",
        "no-lone-blocks": "error",
        "no-multi-spaces": "error",
        "no-multi-str": "error",
        "no-new": "error",
        "no-new-func": "error",
        "no-new-wrappers": "error",
        "no-octal-escape": "error",
        "no-param-reassign": "error",
        "no-proto": "error",
        "no-redeclare": "off",
        // See @typescript-eslint/no-redeclare 
        "no-return-assign": "error",
        "no-script-url": "error",
        "no-self-compare": "error",
        "no-sequences": "error",
        "no-throw-literal": "error",
        "no-unmodified-loop-condition": "error",
        "no-useless-call": "error",
        "no-useless-concat": "error",
        "no-useless-return": "error",
        "no-warning-comments": "error",
        "prefer-named-capture-group": "error",
        "prefer-promise-reject-errors": "error",
        "prefer-regex-literals": "error",
        "radix": "error",
        "require-unicode-regexp": "error",
        "wrap-iife": "error",
        "yoda": "error",
        // Variables https://eslint.org/docs/rules/#variables
        "no-undef-init": "error",
        "no-unused-vars": "off", // See @typescript-eslint/no-unused-vars
        // Stylistic Issues https://eslint.org/docs/rules/#stylistic-issues
        "array-bracket-newline": "error",
        "array-bracket-spacing": "error",
        "array-element-newline": [
            "error",
            "consistent"
        ],
        "block-spacing": "error",
        "brace-style": "error",
        "capitalized-comments": "error",
        "comma-style": "error",
        "computed-property-spacing": "error",
        "eol-last": "error",
        "func-name-matching": "error",
        "func-style": [
            "error",
            "declaration",
            {
                "allowArrowFunctions": true
            }
        ],
        "function-call-argument-newline": [
            "error",
            "consistent"
        ],
        "function-paren-newline": [
            "error",
            "multiline-arguments"
        ],
        "implicit-arrow-linebreak": "error",
        "key-spacing": "error",
        "lines-around-comment": "error",
        "max-depth": "error",
        "max-len": [
            "error",
            {
                "code": 150
            }
        ],
        "max-lines": "error",
        "max-lines-per-function": "error",
        "max-nested-callbacks": "error",
        "max-params": [
            "error",
            {
                "max": 7
            }
        ],
        "max-statements": [
            "error",
            {
                "max": 25
            }
        ],
        "max-statements-per-line": "error",
        "multiline-comment-style": "error",
        "multiline-ternary": [
            "error",
            "always-multiline"
        ],
        "new-parens": "error",
        "newline-per-chained-call": "error",
        "no-bitwise": "error",
        "no-lonely-if": "error",
        "no-mixed-operators": "error",
        "no-multi-assign": "error",
        "no-multiple-empty-lines": [
            "error",
            {
                "max": 1,
                "maxEOF": 0,
                "maxBOF": 0
            }
        ],
        "no-nested-ternary": "error",
        "no-new-object": "error",
        "no-tabs": "error",
        "no-trailing-spaces": "error",
        "no-underscore-dangle": "error",
        "no-unneeded-ternary": "error",
        "no-whitespace-before-property": "error",
        "nonblock-statement-body-position": "error",
        "object-curly-newline": [
            "error",
            {
                "consistent": true
            }
        ],
        "object-curly-spacing": [
            "error",
            "always"
        ],
        "one-var": [
            "error",
            "never"
        ],
        "operator-assignment": "error",
        "operator-linebreak": [
            "error",
            "before"
        ],
        "padded-blocks": [
            "error",
            "never"
        ],
        "prefer-object-spread": "error",
        "quote-props": [
            "error",
            "as-needed"
        ],
        "semi-spacing": "error",
        "semi-style": "error",
        "space-before-blocks": "error",
        "space-in-parens": "error",
        "space-unary-ops": "error",
        "spaced-comment": "error",
        "switch-colon-spacing": "error",
        "template-tag-spacing": "error",
        "unicode-bom": "error",
        // ECMAScript 6 https://eslint.org/docs/rules/#ecmascript-6
        "arrow-spacing": "error",
        "generator-star-spacing": "error",
        "no-confusing-arrow": "error",
        "no-useless-rename": "error",
        "no-restricted-imports": [
            "error",
            {
                "patterns": [
                    "**/tests/**",
                    // There cannot be cross-module imports. If the code is shared then move it outside 'modules' folder.
                    "**/../memory-pubsub/**",
                    "**/../redis-pubsub/**",
                    "**/../queries-graphql/**",
                    "**/../subscriptions-subscriber/**",
                    "**/../tezos-monitor/**"
                ]
            }
        ],
        "no-var": "error",
        "object-shorthand": "error",
        "prefer-arrow-callback": "error",
        "prefer-const": "error",
        "prefer-numeric-literals": "error",
        "prefer-rest-params": "error",
        "prefer-spread": "error",
        "prefer-template": "error",
        "rest-spread-spacing": "error",
        "symbol-description": "error",
        "template-curly-spacing": "error",
        "yield-star-spacing": "error",
        // @typescript-eslint Supported Rules https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#supported-rules
        "@typescript-eslint/array-type": "error",
        "@typescript-eslint/ban-tslint-comment": "error",
        "@typescript-eslint/class-literal-property-style": "error",
        "@typescript-eslint/consistent-indexed-object-style": "error",
        "@typescript-eslint/consistent-type-assertions": [
            "error",
            {
                "assertionStyle": "as",
                "objectLiteralTypeAssertions": "never"
            }
        ],
        "@typescript-eslint/consistent-type-definitions": "error",
        "@typescript-eslint/explicit-function-return-type": "error",
        "@typescript-eslint/explicit-member-accessibility": [
            "error",
            {
                "accessibility": "no-public"
            }
        ],
        "@typescript-eslint/member-delimiter-style": "error",
        "@typescript-eslint/member-ordering": "error",
        "@typescript-eslint/method-signature-style": [
            "error",
            "method"
        ],
        "@typescript-eslint/no-base-to-string": [
            "error",
            {
                "ignoredTypeNames": [
                    "RegExp",
                    "Error"
                ]
            }
        ],
        "@typescript-eslint/no-confusing-non-null-assertion": "error",
        "@typescript-eslint/no-dynamic-delete": "error",
        "@typescript-eslint/no-explicit-any": "error",
        "@typescript-eslint/no-extraneous-class": "error",
        "@typescript-eslint/no-implicit-any-catch": "error",
        "@typescript-eslint/no-invalid-void-type": "error",
        "@typescript-eslint/no-non-null-assertion": "error",
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-throw-literal": "error",
        "@typescript-eslint/no-unnecessary-boolean-literal-compare": "error",
        "@typescript-eslint/no-unnecessary-condition": "error",
        "@typescript-eslint/no-unnecessary-qualifier": "error",
        "@typescript-eslint/no-unnecessary-type-arguments": "error",
        "@typescript-eslint/no-unnecessary-type-constraint": "error",
        "@typescript-eslint/prefer-enum-initializers": "error",
        "@typescript-eslint/prefer-literal-enum-member": "error",
        "@typescript-eslint/prefer-for-of": "error",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/prefer-includes": "error",
        "@typescript-eslint/prefer-nullish-coalescing": "error",
        "@typescript-eslint/prefer-optional-chain": "error",
        "@typescript-eslint/prefer-readonly": "error",
        "@typescript-eslint/prefer-reduce-type-parameter": "error",
        "@typescript-eslint/prefer-string-starts-ends-with": "error",
        "@typescript-eslint/prefer-ts-expect-error": "error",
        "@typescript-eslint/promise-function-async": "error",
        "@typescript-eslint/require-array-sort-compare": [
            "error",
            {
                "ignoreStringArrays": true
            }
        ],
        "@typescript-eslint/restrict-template-expressions": [
            "error",
            {
                "allowNumber": true,
                "allowBoolean": true
            }
        ],
        "@typescript-eslint/strict-boolean-expressions": [
            "error",
            {
                "allowNullableString": true
            }
        ],
        "@typescript-eslint/switch-exhaustiveness-check": "error",
        "@typescript-eslint/type-annotation-spacing": "error",
        "@typescript-eslint/typedef": "error",
        "@typescript-eslint/unified-signatures": "error",
        "@typescript-eslint/ban-types": [
            "error",
            {
                "extendDefaults": true,
                "types": {
                    "object": false
                }
            }
        ],
        // @typescript-eslint Extension Rules https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#extension-rules
        "@typescript-eslint/comma-dangle": [
            "error",
            "always-multiline"
        ],
        "@typescript-eslint/comma-spacing": "error",
        "@typescript-eslint/default-param-last": "error",
        "@typescript-eslint/dot-notation": "error",
        "@typescript-eslint/func-call-spacing": "error",
        "@typescript-eslint/indent": [
            "error",
            4,
            {
                "SwitchCase": 1
            }
        ],
        "@typescript-eslint/init-declarations": "error",
        "@typescript-eslint/keyword-spacing": "error",
        "@typescript-eslint/lines-between-class-members": [
            "error",
            "always",
            {
                "exceptAfterSingleLine": true
            }
        ],
        "@typescript-eslint/no-dupe-class-members": "error",
        "@typescript-eslint/no-duplicate-imports": "error",
        "@typescript-eslint/no-extra-parens": [
            "error",
            "all",
            {
                "nestedBinaryExpressions": false
            }
        ],
        "@typescript-eslint/no-invalid-this": "error",
        "@typescript-eslint/no-loop-func": "error",
        "@typescript-eslint/no-loss-of-precision": "error",
        "@typescript-eslint/no-redeclare": "error",
        "@typescript-eslint/no-shadow": "error",
        "@typescript-eslint/no-unused-expressions": "error",
        "@typescript-eslint/no-unused-vars": [
            "error",
            {
                "argsIgnorePattern": "^_"
            }
        ],
        "@typescript-eslint/quotes": [
            "error",
            "single",
            {
                "allowTemplateLiterals": true
            }
        ],
        "@typescript-eslint/require-await": "error",
        "@typescript-eslint/return-await": "error",
        "@typescript-eslint/semi": [
            "error",
            "always"
        ],
        "@typescript-eslint/space-before-function-paren": [
            "error",
            {
                "named": "never",
                "anonymous": "always",
                "asyncArrow": "always"
            }
        ],
        "@typescript-eslint/space-infix-ops": "error",
        // https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/order.md
        "import/order": [
            "error",
            {
                "groups": [
                    [
                        "builtin",
                        "external"
                    ]
                ],
                "newlines-between": "always",
                "alphabetize": {
                    "order": "asc",
                    "caseInsensitive": true
                }
            }
        ],
        "import/no-duplicates": "error"
    },
    "overrides": [
        {
            "files": [
                "tests/**/*.ts"
            ],
            "rules": {
                "jest/expect-expect": "off",
                "jest/no-conditional-expect": "off",
                "jest/valid-title": "off",
                "max-lines-per-function": "off",
                "max-statements": "off",
                "@typescript-eslint/explicit-function-return-type": "off",
                "@typescript-eslint/explicit-module-boundary-types": "off",
                "@typescript-eslint/no-magic-numbers": "off",
                "@typescript-eslint/no-extraneous-class": "off",
                "@typescript-eslint/init-declarations": "off",
                "@typescript-eslint/no-explicit-any": "off",
                "@typescript-eslint/no-non-null-assertion": "off",
                "@typescript-eslint/no-unsafe-assignment": "off",
                "@typescript-eslint/no-unsafe-member-access": "off",
                "@typescript-eslint/no-unsafe-return": "off",
                "@typescript-eslint/no-unsafe-call": "off",
                "@typescript-eslint/prefer-readonly-parameter-types": "off",
                "@typescript-eslint/no-implicit-any-catch": "off",
                "@typescript-eslint/restrict-template-expressions": [
                    "error",
                    {
                        "allowNumber": true,
                        "allowBoolean": true,
                        "allowNullish": true
                    }
                ],
                "@typescript-eslint/consistent-type-assertions": [
                    "error",
                    {
                        "assertionStyle": "as",
                        "objectLiteralTypeAssertions": "allow"
                    }
                ],
                "no-restricted-imports": "off",
                "newline-per-chained-call": "off"
            }
        }
    ]
}