import React from 'react';
import styled from '@emotion/styled'

const Header = () => (
  <HeaderWrapper>
    <Container>
      <Text>
        <h1>
          Create Richer and Faster Applications
          on premise and in the cloud
        </h1>
        <P>
          TezGraph is a simple, highly compatible, and reliable open-source API
          that provides access to historic and real-time Tezos blockchain data
          with the convenience of GraphQL.
          </P>
        {/* <HeaderButton>Get Started</HeaderButton> */}
      </Text>
    </Container>
  </HeaderWrapper>
);

export const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 0 16px;
  width: 65%;
  text-align: center;

  @media (min-width: ${props => props.theme.screen.xs}) {
    max-width: 540px;
  }

  @media (min-width: ${props => props.theme.screen.sm}) {
    max-width: 720px;
  }

  @media (min-width: ${props => props.theme.screen.md}) {
    max-width: 960px;
  }

  @media (min-width: ${props => props.theme.screen.lg}) {
    max-width: 1200px;
  }

  ${props =>
    props.fluid &&
    `
    max-width: 1200px !important;
  `};
`;

const HeaderWrapper = styled.header`
  background-color: #f6f6f6;

  @media (max-width: ${props => props.theme.screen.md}) {
    padding-top: 128px;
  }
`;

const HeaderButton = styled.button`
  font-weight: 500;
  font-size: 14px;
  color: black;
  letter-spacing: 1px;
  height: 60px;
  display: flex;
  margin-left: 8px;
  text-transform: uppercase;
  cursor: pointer;
  white-space: nowrap;
  border-radius: 4px;
  padding: 0px 40px;
  border-width: 1px;
  border-style: solid;
  border-color: #6c63ff;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.22) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }
`
const Art = styled.figure`
  width: 100%;
  margin: 0;

  > div {
    width: 120%;
    margin-bottom: -4.5%;

    @media (max-width: ${props => props.theme.screen.md}) {
      width: 100%;
    }
  }
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  grid-gap: 64px;

  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 80px;

    > ${Art} {
      order: 2;
    }
  }
`;

const Text = styled.div`
  justify-self: center;
  padding-top: 20%;
  padding-bottom: 10%;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: start;
  }
`;

const P = styled.div`
  font-size: 20px;
`


export default Header;
