import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from './tab';
import styled from 'styled-components';

const TabList = styled.div`
  display: flex;
  flex-direction: row;
  overflow-wrap:break-word;
`
const Wrapper = styled.div`
  disply: flex;
  position: relative;
  border-bottom: 1px solid rgb(223, 223, 223);
`
const TabContent = styled.div`
  display:flex;
`

class Tabs extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      activeTab: this.props.children[0].props.label,
    };
  }

  onClickTabItem = (tab) => {
    this.setState({ activeTab: tab });
  }

  render() {
    const {
      onClickTabItem,
      props: {
        children,
      },
      state: {
        activeTab,
      }
    } = this;

    return (
      <Wrapper>
        <TabList>
          {children.map((child) => {
            const { label } = child.props;

            return (
              <Tab
                activeTab={activeTab}
                key={label}
                label={label}
                onClick={onClickTabItem}
              />
            );
          })}
        </TabList>
        <TabContent>
          {children.map((child) => {
            if (child.props.label === activeTab) return child.props.children;
          })}
        </TabContent>
      </Wrapper>
    );
  }
}

export default Tabs;