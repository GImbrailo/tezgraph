import React from "react";
import styled from '@emotion/styled';
import ReactHtmlParser from 'react-html-parser';
import Tabs from './tabs'

const TabsComponent = ({ children }) => {
  const tabItems = ReactHtmlParser(children)

  return (
   <Tabs> 
       {tabItems.map((tab, i) => {
         const {title, children} = tab.props
         const tabId = `tab${i+1}`
        return (
          <div label={title}>
          <Wrapper>{children}</Wrapper>
          </div>   
        )
       })}
   </Tabs> 
  );
};

const Wrapper = styled.div`
  display: flex;
  overflow: auto;
  padding: 3%;
`

export default TabsComponent;
