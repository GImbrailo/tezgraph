---
title: "Quick Start"
metaTitle: "This is the quick start guide tag of this page"
metaDescription: "This is the meta description"
---

# Running TezGraph Using Docker-Compose

This guide serves to help a newcomer deploy a Tezos Indexer and TezGraph API in 4 commands or less. The only requirement for the user would be to have a machine with Docker and Docker-compose installed. The process involves downloading a docker-compose file and a dump of an already indexed database, bringing up the stack with docker-compose, and testing the TezGraph API.

### Production TezGraph Links

It is not necessary to set up TezGraph locally. You can use our public TezGraph servers linked here.

**Mainnet:** [https://mainnet.tezgraph.tez.ie/graphql](https://mainnet.tezgraph.tez.ie/graphql)

**Delphinet:** [https://delphinet.tezgraph.tez.ie/graphql](https://delphinet.tezgraph.tez.ie/graphql)

### Tezos RPC node
This guide makes use of a public Tezos node but for a fully private environment you use your own Tezos node. This can be done by changing the `TEZOS_NODE` variable in the `docker-compose.yml` file after it has been downloaded.

### Prerequisites

- Docker
- Docker-compose
- About 15GB of available disk space for Delphinet or 100GB for Mainnet
- Tezos RPC node

### Steps to run TezGraph locally

1. Create a new directory and change directory

    ```bash
    mkdir tezgraph-deploy && cd tezgraph-deploy
    ```

2. Download the database dump and `docker-compose.yml` file. The `docker-compose.yml` file will be different based on the Tezos network and whether you want the API to cater to queries to the database or not. Choose one of the following commands based on your use case.

```tabs
<tabItem title='Delphinet Queries and Subscriptions'>
curl -LJo docker-compose.yml \
    	https://gitlab.com/tezgraph/tezgraph/-/raw/master/examples/delphinet-docker-compose.yml \
    	&& curl -LJo delphinet-docker-compose-dump.sql.gz https://storage.googleapis.com/tezgraph-db-snaps/$(curl https://storage.googleapis.com/tezgraph-db-snaps/delphinet.json| jq -r '.latest')
</tabItem>
<tabItem title='Delphinet Subscriptions only'>
curl -LJo docker-compose.yml \
    	https://gitlab.com/tezgraph/tezgraph/-/raw/master/examples/delphinet-subscriptions-docker-compose.yml 
</tabItem>
<tabItem title='Mainnet Queries and Subscriptions'>
curl -LJo docker-compose.yml \
    	https://gitlab.com/tezgraph/tezgraph/-/raw/master/examples/mainnet-docker-compose.yml \
    	&& curl -LJo mainnet-docker-compose-dump.sql.gz https://storage.googleapis.com/tezgraph-db-snaps/$(curl https://storage.googleapis.com/tezgraph-db-snaps/mainnet.json| jq -r '.latest')
</tabItem>
<tabItem title='Mainnet Subscriptions only'>
curl -LJo docker-compose.yml \
    	https://gitlab.com/tezgraph/tezgraph/-/raw/master/examples/mainnet-subscriptions-docker-compose.yml
</tabItem>
```

3. Startup the Indexer, Database, and API using docker compose. Follow the logs in the console to see how the stack is progressing. There are a few pitfalls to watch out for listed in the Pitfalls and Known Issues section below. An example of this would be that the database takes some time to start up, and there will be some errors in the console that can be ignored.

    ```bash
    docker-compose up
    ```

    A good indication of the stack being ready and waiting for tests is when you see something  similar to the following in the console: 

    ```bash
    indexer_database           | LOG:  starting PostgreSQL 13.1 (Debian 13.1-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
    indexer_database           | LOG:  listening on IPv4 address "0.0.0.0", port 5432
    indexer_database           | LOG:  listening on IPv6 address "::", port 5432
    indexer_database           | LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
    indexer_database           | LOG:  database system was shut down at 2020-12-15 05:54:49 UTC
    indexer_database           | LOG:  database system is ready to accept connection
    ```

### Testing

Once the stack is up and running you can navigate to [https://localhost:3000/graphql](https://localhost:3000/graphql). The page will look like this:

![TezGraph Local Host 3000](./images/screenshots/localhost3000.png "TezGraph Local Host 3000")

You can then execute GraphQL queries on the database through the TezGraph API. An example of a test on Delphinet would be:

```graphql
query {
    account (address: "tz1LpmZmB1yJJBcCrBDLSAStmmugGDEghdVv"){
    transactions {
      edges {
        node {
          source,
          kind
        }
      }
      }
    }
}
```

### Pitfalls and Known Issues

- If there are any issues that are not listed here or that you are unable to overcome, you can restart the process by bringing down the stack and deleting the volumes using the following command:

```bash
docker-compose down -v
```

- The most important thing to note is that the database will take time to restore from the backup. The database restore time can differ based on the Tezos network, CPU, or RAM on the host, but the approximate times are 10 minutes for Delphinet and 70 minutes for Mainnet. During this time, you will not be able to execute queries on the database. When you try and execute a query, you will receive a response similar to:

    ```graphql
    {
      "errors": [
        {
          "message": "\nInvalid `prisma.queryRaw()` invocation:\n\n\n  Can't reach database server at `indexer_database`:`5432`\n\nPlease make sure your database server is running at `indexer_database`:`5432`.",
          "locations": [
            {
              "line": 2,
              "column": 3
            }
          ],
          "path": [
            "account"
          ],
          "extensions": {
            "code": "INTERNAL_SERVER_ERROR",
            "exception": {
              "clientVersion": "2.12.1",
              "stacktrace": [
                "Error: ",
                "Invalid `prisma.queryRaw()` invocation:",
                "",
                "",
                "  Can't reach database server at `indexer_database`:`5432`",
                "",
                "Please make sure your database server is running at `indexer_database`:`5432`.",
                "    at PrismaClientFetcher.request (/app/node_modules/@prisma/client/runtime/index.js:79361:15)",
                "    at processTicksAndRejections (internal/process/task_queues.js:97:5)"
              ]
            }
          }
        }
      ],
      "data": null
    }
    ```

- Another known issue is sometimes after the stack has started, the TezGraph container is still unable to connect to the database container (You will see an error similar to the one above), the easiest way to remedy this issue is to bring the stack down by typing `ctrl + c`, waiting for the containers to stop gracefully, and then bringing the stack up again using the `docker-compose up` command.

- Here are some of the errors in the console that can safely be ignored while the stack is starting up and the database is being restored.

```bash
indexer_database           |  ERROR:  canceling autovacuum task
indexer_database           |  CONTEXT:  while scanning block 190427 of relation "public.balance"
indexer_database           | 	automatic vacuum of table "indexer.public.balance"
```

```bash
indexer_database           | LOG:  checkpoints are occurring too frequently (22 seconds apart)
indexer_database           | HINT:  Consider increasing the configuration parameter "max_wal_size".
```

```bash
indexer          | Database connection error: Failed to connect to <postgresql://indexer:_@indexer_database:5432/indexer?sslmode=disable>: Connection failure: could not connect to server: Connection refused
indexer          | 	Is the server running on host "indexer_database" (XXX.XXX.XXX.XXX) and accepting
indexer          | 	TCP/IP connections on port 5432?
indexer          | 
indexer          | To get more information about specifying how to connect to the database, run:
indexer          | /indexer/tezos-indexer --help
indexer exited with code 3
```

This error will repeatedly happen until the database becomes available and should be ignored until then.
