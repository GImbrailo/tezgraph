---
title: "Subscription API"
metaTitle: "This is the Subscription API guide tag of this page"
metaDescription: "This is the meta description"
---

## Block Subscription

In this example, we subscribe to entire new blocks and also get reveals which can be filtered too. Similarly there are subscriptions for all operation kinds supported on Tezos.

This allows clients to make sure they have not missed any updates as they can easily keep track of the last seen block level on their side. For example, if a connection is dropped, it is easy for the client to reconnect and get all the updates from the last seen block using `replayFromBlockLevel` parameter.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  blockAdded {
    hash
    header {
      level
    }
    reveals(filter: { source: { notEqualTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" } }) {
      fee
      public_key
    }
  }
}
</content>
```

## Operation Subscription
 
In this example, we subscribe directly to transactions in newly baked blocks. We retrieve some of their fields and also info about the block.

Similarly there are subscriptions for all operation kinds supported on Tezos.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded {
    kind
    source
    destination
    amount
    info {
      signature
    }
    block {
      hash
      header {
        level
      }
    }
  }
}
</content>
```

## Filtering

If you want to subscribe only to a particular subset of operations according to your criteria, you can leverage the `filter`.

The example demonstrates subscriptions to new transactions from all addresses except *tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS* which have `amount` greater than *7* mutez.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content> 
subscription($excludedSource: Address, $minAmount: Mutez) {
  transactionAdded(
    filter: {
      source: { notEqualTo: $excludedSource }
      amount: { greaterThan: $minAmount }
    }
  ) {
    source
    destination
    amount
  }
}
</content> 
<variables>
{
  "excludedSource": "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS",
  "minAmount": "7"
}
</variables>
```

## Advanced Filtering

If you want to specify more advanced filters with alternative conditions then you can leverage `and` and `or` properties.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded(
    filter: {
      or: [
        { source: { equalTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" } }
        {
          source: { notEqualTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" }
          amount: { greaterThan: "7" }
        }
      ]
    }
  ) {
    source
    destination
    amount
  }
}
</content>
```

## Mempool

If you want to be notified about operations when they are put to a mempool before it gets baked into a block, then specify `includeMempool` flag. Then `origin` field indicates where the operation originated.

```gql url=https://mainnet.tezgraph.tez.ie/graphql
<content>
subscription {
  transactionAdded(includeMempool: true) {
    origin
    source
    destination
    amount
  }
}
</content>
```
