---
title: "Components"
metaTitle: "This is the Components tag of this page"
metaDescription: "This is the meta description"
---

# Context Dependency Map

![image info](./images/contextDependencyMap.png)

## Roles

- ***Context:* Tezos RPC** - API interface to Tezos Blockchain. Tezos Blockchain is the system of record for blockchain data. See [documentation for the RPC](https://tezos.gitlab.io/shell/rpc.html).
- ***Context:*** **Tezos Indexer -** stores the full history of Tezos blockchain data and optimizes the storage of it for fast data retrieval by applications that leverage Tezos blockchain data. See [Tezos Indexer repository](https://gitlab.com/nomadic-labs/tezos-indexer).
- ***Context:* TezGraph** - Provides public API for a *Client* to:
    - retrieve in near real-time, new operations that are inserted into Tezos Blockchain
    - query for blockchain data are optimized for fast data retrieval

# Components

![image info](./images/components.png)

The TezGraph Context is made up of two components.

- **Subscribe API -** A public interface used by a *Client* to subscribe to new operations.
- **Query API** - A public interface used by a *Client* to query the Index Store for blockchain data.

## Responsibilities

### Component: Subscribe API

- Accepts a request from a *Client* for near real-time subscription to
    - new operations of a given kind on a particular account
    - new operations of a given kind on a particular set of accounts
    - life-cycle progress a particular operation (operation position in mempool, on the blockchain, its status, etc.)
- Opens a connection with Tezos RPC to discover
    - new head blocks
    - new mempool operations
- Receives from Tezos RPC
    - new head block header
    - new mempool operation
- Immediately fetches block from Tezos RPC when new head block header is received
- Parses a new head block for
    - operations on a particular account
    - operations for a particular set of accounts
    - a particular operation
- Determines when a new mempool operation is a particular operation
- Transforms new operation from Tezos RPC schema to TezGraph schema
- Immediately provides a subscribed *Client* a new operation
- Cancels a particular subscription from a *Client* when requested

### Context: Tezos RPC

- Immediately returns new head block header via a connection established by Subscribe API
- Immediately returns new mempool operation via a connection established by Subscribe API
- Gives a particular block to Subscribe API when requested

### Component: Query API

- Accepts request to execute query from a *Client*
- Executes query for blockchain data against the Tezos Indexer
- Transforms query results from Tezos Indexer from Tezos RPC schema to TezGraph schema
- Returns query results to a *Client*

### Context: Tezos Indexer

- Executes query for blockchain data when requested by Query API