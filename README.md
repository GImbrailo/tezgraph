# TezGraph

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/4392/badge)](https://bestpractices.coreinfrastructure.org/projects/4392)

TezGraph is an open-source GraphQL API that provides access to historical and real-time Tezos blockchain data with the convenience of GraphQL.

TezGraph allows Application Developers building on Tezos blockchain to create more complex user workflows and build faster, whether in the cloud or on-premise.

## Why TezGraph?

1. The ability to query historical and real-time blockchain data using Tezos RPC directly is time-consuming. There are operational considerations for applications since it takes time to traverse the blockchain and development considerations since more in-depth knowledge of the Tezos domain is required.

Building an indexer solution by every wallet application or dApp developer is not feasible. Current indexer solutions require ramp-up time and need client tools to be a good fit for development environments.

2. Development solutions that provide practical and straightforward subscriptions and notifications for near real-time new blockchain events are not as developed and hence not adopted by applications. Events notification could trigger workflows and business rules based on the changes to enrich end-user experiences.

    Information points that require significant effort:
   - account's balance
   - account operations within a period
   - most recent real-time operations
   - smart-contract execution
   - paged results of the above
   <br>
3. Existing Tezos indexing offerings are typically available as a SaaS and operated by a single entity. Centralized indexer infrastructure puts the risk on builders. It is possible to self-host an indexer, but the undertaking to self-host an incumbent indexer is non-trivial and introduces cost.

## Running Locally with Docker-Compose

1. Create a new directory and change the directory.
```
mkdir tezgraph-deploy && cd tezgraph-deploy
```
2. Download the database dump and docker-compose.yml file. The docker-compose.yml file will be different based on the Tezos network and whether you want the API to cater to queries to the database or not. You can find all of the commands [here](https://tezgraph.com/quickstart). 

The following command will download a database dump of Tezos Delphinet and the docker-compose.yml for TezGraph.
```
curl -LJo docker-compose.yml \
    https://gitlab.com/tezgraph/tezgraph/-/raw/master/examples/delphinet-docker-compose.yml \
    && curl -LJo delphinet-docker-compose-dump.sql.gz https://storage.googleapis.com/tezgraph-db-snaps/$(curl https://storage.googleapis.com/tezgraph-db-snaps/delphinet.json| jq -r '.latest')
```
3. Startup the Indexer, Database, and API using docker-compose. Follow the logs in the console to see how the stack is progressing.
```
docker-compose up
```
4. More details at [https://tezgraph.com/quickstart](https://tezgraph.com/quickstart).

## Running Locally for Development

Prerequisites:
 - [Node.js](https://nodejs.org/)
   <br>
1. `npm install`
2. Use `.env` to specify the desired configurations.
2. `npm run dev`
